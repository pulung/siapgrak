<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pelaporan extends CI_Controller {

    function __construct() {
        parent::__construct();
        // $this->load->model(array('Model_beranda'));
		$this->load->model(array('Model_pelaporan','profil/Model_profil'));
        if (!$this->session->userdata('username')):
            redirect('login');
            return;
        endif;
        @date_default_timezone_set('Asia/Jakarta');
		$this->load->config('path_upload');
    }

    public function index() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $data['tagmenu'] = 'pelaporan';
        $data['body'] = 'pelaporan/view_pelaporan';
		
		$data['pelaporan'] = $this->Model_pelaporan->getfilepelaporan();
		
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function do_upload() { 
         //UPLOAD
         $file = $_FILES['userfile'];
         $file_name = $file['name'];
         $config['upload_path']   = './assets/images/'; 
         $config['allowed_types'] = 'gif|jpg|png|pdf|doc|docx'; 
         $config['max_size']      = 10000; 
         $config['max_width']     = 1024; 
         $config['max_height']    = 768;  
         $config['file_name']       = $file['name'];
         $this->load->library('upload', $config);
   //CEK APAKAH UPLOAD BERHASIL ATAU TIDAK, JIKA TIDAK BERHASIL MUNCULKAN ERROR 
         //JIKA BERHASIL LANJUT KE PROSES SELANJUTNYA
         if ( ! $this->pelaporan->do_upload('userfile')) {
            $error = array('error' => $this->pelaporan->display_errors()); 
            $this->load->view('view_pelaporan', $error); 
         }
         //PROSES MENYIMPAN DATA KE DATABASE
         else { 
            $data['nama']=$this->input->post('namafile');
            $data['path']=$file_name;
            $this->Model_pelaporan->simpan($data);
            $error=array('error'=>'File has been uploaded'); 
            $this->load->view('view_pelaporan', $error); 
         } 
      }
	  
		public function upload() {
        
        $now = date('Y-m-d H:i:s');
        
        
        $data_non = array(
                'files' => strip_tags(trim($_POST['nama_dokumen'])),
				'nama_files' => strip_tags(trim($_POST['nama_dokumen'])),
                'tanggal_upload' => $now
            );
            $data = $this->security->xss_clean($data_non);var_dump($data); exit;
            $insert = $this->Model_pelaporan->simpan($data); 

            //tabel file
            if (!empty($_FILES['filesatu']['name'])):
                $file_name = $_FILES['filesatu']['name'];
                $file_tmp = $_FILES['filesatu']['tmp_name'];
                $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                $path = $this->config->item('path_file_infografis_pelaporan');
                $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                move_uploaded_file($file_tmp, $lokasi);
                $datafilesatu_non = array(
                    'nama_dokumen' => strip_tags(trim($_POST['nama_dokumen'])),
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                );
                $datafilesatu = $this->security->xss_clean($datafilesatu_non);
                $insert_file = $this->Model_pelaporan->insert_file_infografis($datafilesatu);
            endif;

            if (!empty($_FILES['file']['name'])):
                for ($j = 0; $j < count($_FILES["file"]['name']); $j++):
                    $file_name = $_FILES['file']['name']["$j"];
                    $file_tmp = $_FILES['file']['tmp_name']["$j"];
                    $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                    $path = $this->config->item('path_file_infografis_pelaporan');
                    $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                    move_uploaded_file($file_tmp, $lokasi);
                    $datafile_non = array(
                    'files' => strip_tags(trim($_POST['nama_dokumen'])),
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                    );
                    $datafile = $this->security->xss_clean($datafile_non);
                    $insert_file = $this->Model_pelaporan->insert_file_infografis($datafile);
                endfor;
            endif; 
        

        if ($insert == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Data Anda berhasil disimpan.</div>');
            redirect('pelaporan');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Data Anda gagal disimpan.</div>');
            redirect('pelaporan');
        }
    }
	
	public function insert() {
		$this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $now = date('Y-m-d H:i:s');
        $id_gratifikasi_terakhir = $this->Model_pelaporan->get_id_terakhir();
        $id_gratifikasi = $id_gratifikasi_terakhir + 1;
		

            //tabel file
            
                $file_name = $_FILES['filesatu']['name'];
                $file_tmp = $_FILES['filesatu']['tmp_name'];
                $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                $path = $this->config->item('path_file_infografis_pelaporan');
                $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                move_uploaded_file($file_tmp, $lokasi);
                $datafilesatu_non = array(
                    'id' => '',
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                );
                $datafilesatu = $this->security->xss_clean($datafilesatu_non);
                $insert_file = $this->Model_pelaporan->insert_file_infografis($datafilesatu);
            
			
			

            //if (!empty($_FILES['file']['name'])):
              //  for ($j = 0; $j < count($_FILES["file"]['name']); $j++):
               //     $file_name = $_FILES['file']['name']["$j"];
                 //   $file_tmp = $_FILES['file']['tmp_name']["$j"];
                   // $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                    //$path = $this->config->item('path_file_infografis_pelaporan');
                    //$lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                    //move_uploaded_file($file_tmp, $lokasi);
                    //$datafile_non = array(
                    //'files' => $name,
                    //'nama_files' => $file_name,
                    //'tanggal_upload' => $now
                    //);
                    //$datafile = $this->security->xss_clean($datafile_non);
                    //$insert_file = $this->Model_pelaporan->insert_file_infografis($datafile);
                //endfor;
            //endif;
        

        if ($insert_file == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Data berhasil diupload.</div>');
            redirect('pelaporan');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Data gagal diupload.</div>');
            redirect('pelaporan');
        }
    }
	
	public function download_file(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEG'));
        $this->load->helper('download');
        $id = strip_tags(trim($this->uri->segment(3)));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(4)));
        $show = $this->Model_pelaporan->getfile_pelaporan_byid($id)->row();

        if (count($show) > 0) {
            $files = $show->files;
            $nama_files = $show->nama_files;
            $path = $this->config->item('path_file_infografis_pelaporan');
            ob_clean();
            $lokasi = file_get_contents($path . $files);
            force_download($nama_files, $lokasi);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-remove"></i>Gagal!</strong> Data tidak ditemukan.</div>');
            redirect('pelaporan');
        }
    }

}
