<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_pelaporan extends CI_Model {

	private $table_pelaporan;

    function __construct() {
        parent::__construct();
		$this->table_pelaporan = "file_infografis_pelaporan";
    }

	function simpan($data){
        $this->db->insert('file_infografis_pelaporan', $data);
    }
	
	function insert_file_infografis($data){
        $res = $this->db->insert('file_infografis_pelaporan', $data);
        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }
	
	function get_id_terakhir(){
        $data = array();
        $sql = "SELECT COUNT(id) FROM file_infografis_pelaporan ORDER BY id DESC limit 1";
        $q = $this->db->query($sql);
        
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        } else{
            $data['id'] = 0;
        }
        $q->free_result();
        return $data['id'];
    }
	
	function getfilepelaporan() {
        $data = array();

        $sql = "SELECT * FROM file_infografis_pelaporan";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }
	
	function getfile_pelaporan_byid($id)
    {
        $this->db->select('*');
        $this->db->from('file_infografis_pelaporan');
        $this->db->where('id', $id);

        return $this->db->get();
    }

}
