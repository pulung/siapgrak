<?php  defined('BASEPATH') OR exit('No direct script access allowed');

//Path di production
/*$config['path_file_gratifikasi'] = '/mnt/siapgrak_data/files/file_gratifikasi/';
$config['path_file_verifikasi'] = '/mnt/siapgrak_data/files/file_verifikasi/';
$config['path_file_rekomendasi'] = '/mnt/siapgrak_data/files/file_rekomendasi/';
$config['path_file_tindak_lanjut'] = '/mnt/siapgrak_data/files/file_tindak_lanjut/';
$config['path_file_ctt_verifikasi_kpk'] = '/mnt/siapgrak_data/files/file_ctt_verifikasi_kpk/';
$config['path_file_infografis_pelaporan'] = '/mnt/siapgrak_data/files/file_infografis_pelaporan/';
$config['path_file_verifikasi_kpk'] = '/mnt/siapgrak_data/files/file_verifikasi_kpk/';
$config['path_file_pasca_tindak_lanjut'] = '/mnt/siapgrak_data/files/file_pasca_tindak_lanjut/';*/

//Path di dev
$config['path_file_gratifikasi'] = '/usr/local/www/apache24/data/siapgrak/assets/files/file_gratifikasi/';
$config['path_file_verifikasi'] = '/usr/local/www/apache24/data/siapgrak/assets/files/file_verifikasi/';
$config['path_file_rekomendasi'] = '/usr/local/www/apache24/data/siapgrak/assets/files/file_rekomendasi/';
$config['path_file_tindak_lanjut'] = '/usr/local/www/apache24/data/siapgrak/assets/files/file_tindak_lanjut/';
$config['path_file_ctt_verifikasi_kpk'] = '/usr/local/www/apache24/data/siapgrak/assets/files/file_ctt_verifikasi_kpk/';
$config['path_file_infografis_pelaporan'] = '/usr/local/www/apache24/data/siapgrak/assets/files/file_infografis_pelaporan/';
$config['path_file_verifikasi_kpk'] = '/usr/local/www/apache24/data/siapgrak/assets/files/file_verifikasi_kpk/';
$config['path_file_pasca_tindak_lanjut'] = '/usr/local/www/apache24/data/siapgrak/assets/files/file_pasca_tindak_lanjut/';