<!--<div class="main">
  <div class="container">
   
   
    <div class="row">
      <div class="col-md-12">
       
        <div class="portlet box">
          <div class="portlet-body">


<div class="container">
  <div class="col-md-6 col-sm-offset-0" style="margin-top:50px;">
          
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Upload File</h3>
            </div>
            

          <?php //echo form_open_multipart('pelaporan/upload',array('method'=>'POST'));?> 
              <div class="box-body">
                <div class="form-group ">
                  <label for="exampleInputEmail1">Nama File</label>
                  <input type="text" class="form-control" name="nama_dokumen" id="nama_dokumen" placeholder="Nama File">
                </div>
                <div class="form-group hide">
                  <label for="exampleInputPassword1">Jenis</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Jenifile">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" name="name" id="name" class="filestyle" data-buttonName="btn-primary"/>
                  <span class="label label-danger">Maksimal 3MB</span>
                  <br>
                  <?php 
                  //echo $error;?> 

                  
                </div>
                <div class="checkbox hide">
                  <label>
                    <input type="checkbox"> Check me out
                  </label>
                </div>
              </div>
              

              <div class="box-footer">
                <button type="submit" class="btn btn-danger btn-fla pull-right">Upload</button>
              </div>
            </form>
			<fieldset>
                  <legend style="color: #65aed9">Data Tambahan</legend>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Upload File</label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <div class="input-group">
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                              <span class="btn default btn-file">
                                  <span class="fileinput-new">
                                      Pilih File </span>
                                  <span class="fileinput-exists">
                                      Ubah </span>
                                  <input type="file" name="filesatu" id="filesatu" onchange="document.getElementById('moreFileLink_pel').style.display = 'block';">
                              </span>
                              <span class="fileinput-filename">
                              </span>
                              &nbsp; <a href="#" class="close fileinput-exists" data-dismiss="fileinput">
                              </a>
                          </div>
                          <div id="moreFile_pel"></div>
                          <div id="moreFileLink_pel" style="display:none;"><a href="javascript:addFile_pel();">Tambah File Lain</a></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </fieldset>
          </div>
          
</div>

            <!--<p style="font-size: 17px; text-align: justify; margin-left: 0cm;">
              <ol>
                <li>
                  Pegawai negeri atau Penyelenggara Negara melaporkan penerimaan gratifikasi kepada KPK dengan mengisi formulir secara lengkap sebelum 30 hari kerja terhitung sejak tanggal gratifikasi diterima oleh penerima gratifikasi, atau kepada KPK melalui UPG sebelum 7 hari kerja terhitung sejak tanggal gratifikasi diterima.
                  <br>Hal lain yang perlu diperhatikan dalam kelengkapan data perlu dicantumkan kontak pelapor berupa nomor telepon, nomor telepon kantor, alamat email dan nomor komunikasi lain yang bisa dihubungi mengingat adanya proses klarifikasi dan keterbatasan waktu pemrosesan laporan yang ditentukan oleh undang-undang. Penyampaian formulir dapat disampaikan secara langsung kepada KPK atau melalui UPG melalui pos, e-mail, atau website KPK/pelaporan online.
                </li><br>
                <li>
                  UPG atau Tim atau Satuan Tugas yang ditunjuk wajib meneruskan laporan gratifikasi kepada KPK dalam jangka waktu 14 (empat belas) hari kerja sejak laporan gratifikasi diterima oleh UPG atau Tim/Satgas.
                </li><br>
                <li>
                  KPK menetapkan status penerimaan gratifikasi dalam jangka waktu 30 (tiga puluh) hari kerja terhitung sejak laporan gratifikasi diterima oleh KPK secara lengkap.
                </li><br>
                <li>
                  KPK melakukan penanganan laporan gratifikasi yang meliputi:<br>
                  &nbsp;a.  Verifikasi atas kelengkapan laporan gratifikasi;<br>
                  &nbsp;b.  Permintaan data dan keterangan kepada pihak terkait;<br>
                  &nbsp;c.  Analisis atas penerimaan gratifikasi; dan<br>
                  &nbsp;d.  Penetapan status kepemilikan gratifikasi.
                </li><br>
                <li>
                  Dalam hal KPK menetapkan gratifikasi menjadi milik penerima gratifikasi, KPK menyampaikan Surat Keputusan kepada penerima gratifikasi paling lambat 7 (tujuh) hari kerja terhitung sajak tanggal ditetapkan, yang dapat disampaikan melalui sarana elektronik atau non elektronik.
                </li><br>
                <li>
                  Dalam hal KPK menetapkan gratifikasi menjadi milik negara, penerima gratifikasi menyerahkan gratifikasi yang diterimanya paling lambat 7 (tujuh) hari kerja terhitung sejak tanggal ditetapkan.
                </li><br>
                <li>
                  Penyerahan gratifikasi dilakukan dengan cara sebagai berikut :<br><br>
                  Apabila gratifikasi dalam bentuk uang maka penerima gratifikasi menyetorkan kepada :<br>
                  1.  Rekening kas negara yang untuk selanjutnya menyampaikan bukti penyetoran kepada KPK;<br>
                  2.  Rekening KPK yang untuk selanjutnya KPK akan menyetorkannya ke rekening kas negara dan menyampaikan bukti penyetoran kepada penerima gratifikasi.<br><br>
                  Apabila gratifikasi dalam bentuk barang maka penerima gratifikasi menyerahkan kepada :<br>
                  1.  Direktorat Jenderal Kekayaan Negara atau Kantor Wilayah atau perwakilan Direktorat Jenderal Kekayaan Negara di tempat barang berada dan menyampaikan bukti penyerahan barang kepada KPK;<br>
                  2.  KPK yang untuk selanjutnya diserahkan kepada Direktorat Jenderal Kekayaan Negara dan menyampaikan bukti penyerahan barang kepada penerima gratifikasi.
                </li><br>
              </ol>
            </p>
          </div>
        </div>
        
      </div>
    </div>
    	
  </div>
</div> -->

<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
    <?php if($this->session->userdata('role') == '2'): ?>  
      <div class="content-form-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="alert alert-info display-hide">
              <i class="fa fa-spin fa-spinner"></i> Silahkan tunggu. Proses penyimpanan sedang berjalan.
            </div>
            <div class="alert alert-danger display-hide">
              <button class="close" data-close="alert"></button>
              <i class="fa fa-ban"></i> <strong>Peringatan!</strong> Data gagal ditambahkan. 
            </div>
            <?= $this->session->flashdata('message'); ?>
           
            <form method="post" id="form_penerima" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('pelaporan/insert'); ?>">
              <?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>
             
              <fieldset>
                <legend style="color: #65aed9">Data</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Upload File</label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <div class="input-group">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn default btn-file">
                                <span class="fileinput-new">
                                    Pilih File </span>
                                <span class="fileinput-exists">
                                    Ubah </span>
                                <input type="file" name="filesatu" id="filesatu" onchange="document.getElementById('moreFileLink_pen').style.display = 'block';">
                            </span>
                            <span class="fileinput-filename">
                            </span>
                            &nbsp; <a href="#" class="close fileinput-exists" data-dismiss="fileinput">
                            </a>
                        </div>
                        <div id="moreFile_pen"></div>
                        <!--<div id="moreFileLink_pen" style="display:none;"><a href="javascript:addFile_pen();">Tambah File Lain</a></div>-->
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
              <hr>
              
              <br><br>
              <div class="row">
                <div class="col-md-offset-4 col-md-9">
                  <button id="submit" name="send" type="submit" onclick="return confirm('Apakah anda yakin mengirimkan data ini?');" class="btn btn-sm blue-dark"><i class="fa fa-paper-plane-o"></i>&nbsp;Upload</button>
                  <a href="<?php echo site_url('beranda'); ?>" class="btn btn-sm grey"><i class="fa fa-times"></i>&nbsp;Batal</a>
                </div>
              </div>
            </form>
            
          </div>
        </div>
      </div>
	  <br>
	  <?php endif; ?>
 
	  <div class="content-form-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
                       
            <form method="post" id="form_faq" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('bantuan/tambah_pertanyaan'); ?>">
				 <div class="col-md-12 col-sm-12">
				  <br><br>
				  <h1>Tabel File</h1>
				  <div class="content-page">
					<div class="row">
					  <div class="col-md-12 col-sm-12">
						<div class="tab-content" style="padding:0; background: #fff;">
							<table id="tabel-data" name="tabel-data" class="table table-striped table-bordered" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>Nama File</th>
										<th>File</th>
										<th>Tanggal Upload</th>
										
									</tr>
								</thead>
								
								<tbody>
								
								<?php foreach ($pelaporan as $row): ?>
									  
									<tr>
									
										<td><?php cetak($row['nama_files']); ?></td>
										<td><a target="_blank" href="<?= site_url('pelaporan/download_file/'.$row['id']); ?>">Download</a></td>
										<td><?php cetak($row['tanggal_upload']); ?></td>
										
									</tr>
									
								<?php endforeach; ?>
													   
								</tbody>
							</table>
							
						</div>
					  </div>
					</div>
				  </div>
				 </div>
				</form>

            
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
</div>
<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Informasi Gratifikasi</h4>
      </div>
      <div class="modal-body">
         <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption" style="font-size: 14px">
              <i class="icon-bulb "></i> UU Nomor 20 Tahun 2001, tentang Perubahan atas UU Nomor 31 Tahun 1999 tentang Pemberantasan Tindak Pidana Korupsi
            </div>
          </div>
          <div class="portlet-body">
            <p style="font-size: 10px">
              <b>Pasal 12B</b><br>
              (1) Setiap gratifikasi kepada pegawai negeri atau penyelenggara negara dianggap pemberi suap, apabila berhubungan dengan jabatannya dan yang berlawan dengan kewajibannya atau tugasnya, dengan ketentuan sebagai berikut:<br>
              &nbsp;&nbsp;&nbsp;&nbsp; a. Yang nilainya Rp 10.000.000,00 (sepuluh juta rupiah) atau lebih, pembuktian bahwa gratifikasi tersebut bukan merupakan suap dilakukan oleh penerima gratifikasi;<br>
              &nbsp;&nbsp;&nbsp;&nbsp; b. Yang nilainya kurang dari Rp 10.000.000,00 (sepuluh juta rupiah), pembuktian bahwa gratifikasi tersebut suap dilakukan oleh penuntut hukum;<br>
              (2) Pidana bagi pegawai negeri atau penyelenggara negara sebagaimana dimaksud dalam ayat (1) adalah pidana penjara seumur hidup atau pidana penjara paling singkat 4 (empat) tahun dan paling lama 20 (dua puluh) tahun, dan pidana denda paling sedikit Rp 200.000.000,00 (dua ratus juta rupiah) dan paling banyak Rp 1.000.000.000,00 (satu miyar rupiah).<br>
              <b>Pasal 12C ayat (1):</b> Ketentuan sebagaimana dimaksud dalam Pasal 12B ayat 1 tidak berlaku jika penerima melaporkan gratifikasi yang diterimanya kepada Komisi Pemberantasan Tindak Pidana Korupsi.<br>
              <b>Pasal 12C ayat (2):</b> Penyampaian laporan sebagaimana dimaksud dalam ayat (1) wajib dilakukan oleh penerima gratifikasi paling lambat 30 (tiga puluh) hari kerja terhitung sejak tanggal gratifikasi tersebut diterima.<br>
            </p>
          </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Saya Mengerti</button>
        <button type="button" class="btn default" data-dismiss="modal" onclick="history.back();">Batal</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script src="<?= site_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?= site_url('assets/frontend/pages/scripts/jquery.price_format.1.8.min.js'); ?>"></script>
<script src="<?= site_url('assets/frontend/pages/scripts/charcount.js'); ?>"></script>
<script>
  $(window).on('load',function(){
      $('#static').modal('show');
  });

  jQuery(document).ready(function () {
      $("#id").change(function () {
          var id = {"id": $('#id').val()};

          $.ajax({
              type: "POST",
              data: id,
              url: "<?= site_url('gratifikasi/get_data_user') ?>",
              success: function (data) {
                  result = jQuery.parseJSON(data);
                  $('#name').val(result[0].name);
                  $('#tempatlahir').val(result[0].tempatlahir);
                  $('#tgllahir').val(result[0].tgllahir);
                  $('#no_ktp').val(result[0].no_ktp);
                  $('#jabatan').val(result[0].jabatan);
                  $('#email').val(result[0].email);
                  $('#nohp').val(result[0].nohp);
                  $('#pin_bb').val(result[0].pin_bb);
                  $('#inskerja').val(result[0].inskerja);
                  $('#biro').val(result[0].biro);
                  $('#bagian').val(result[0].bagian);
                  $('#golongan').val(result[0].golongan);
                  $('#pangkat').val(result[0].pangkat);
                  $('#alamat_kantor').val(result[0].alamat_kantor);
                  $('#provinsi_kantor').val(result[0].provinsi_kantor);
                  $('#kota_kantor').val(result[0].kota_kantor);
                  $('#kecamatan_kantor').val(result[0].kecamatan_kantor);
                  $('#kelurahan_kantor').val(result[0].kelurahan_kantor);
                  $('#kode_pos_kantor').val(result[0].kode_pos_kantor);
                  $('#no_kantor').val(result[0].no_kantor);
                  $('#alamat_rumah').val(result[0].alamat_rumah);
                  $('#provinsi_rumah').val(result[0].provinsi_rumah);
                  $('#kota_rumah').val(result[0].kota_rumah);
                  $('#kecamatan_rumah').val(result[0].kecamatan_rumah);
                  $('#kelurahan_rumah').val(result[0].kelurahan_rumah);
                  $('#kode_pos_rumah').val(result[0].kode_pos_rumah);
                  $('#no_rumah').val(result[0].no_rumah);
                  $('#nama_penerima').val(result[0].name);
              }
          });
      });
  });

  /*jQuery(function($) {
    $("#nilai_nominal_pen").priceFormat({
        prefix: 'Rp. ',
        centsLimit: 0,
        //centsLimit: 0,
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
    $("#nilai_nominal_pel").priceFormat({
        prefix: 'Rp. ',
        centsLimit: 0,
        centsLimit: 0
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
  }); */

  //var maxAmount = 250;
  var maxAmount = 100000;
  function textCounter(textField, showCountField) {
    if (textField.value.length > maxAmount) {
      textField.value = textField.value.substring(0, maxAmount);
    } else { 
      showCountField.value = maxAmount - textField.value.length;
    }
  }

  function checkonlynumber(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber1(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber2(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber3(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber4(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber5(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber6(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber7(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber8(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber9(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber10(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber11(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber12(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber13(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber14(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }

  function Check(that) {
      if (that.value == "1"){
        document.getElementById("form_penerima").style.display = "block";
        document.getElementById("pelapor_check").style.display = "none";
        document.getElementById("identitas_check").style.display = "none";
        document.getElementById("data_penerima_check").style.display = "none";
        document.getElementById("data_pemberi_check").style.display = "none";
        document.getElementById("alasan_check").style.display = "none";
        document.getElementById("data_tambahan_check").style.display = "none";
        document.getElementById("persyaratan_check").style.display = "none";
        document.getElementById("button_check").style.display = "none";
      } else if (that.value == "2"){
        document.getElementById("pelapor_check").style.display = "block";
        document.getElementById("identitas_check").style.display = "block";
        document.getElementById("data_penerima_check").style.display = "block";
        document.getElementById("data_pemberi_check").style.display = "block";
        document.getElementById("alasan_check").style.display = "block";
        document.getElementById("data_tambahan_check").style.display = "block";
        document.getElementById("button_check").style.display = "block";
        document.getElementById("persyaratan_check").style.display = "block";
        document.getElementById("form_penerima").style.display = "none";
      }
  }

  function Check1a(that) {
      if (that.value == "7"){
          document.getElementById("peristiwa_lainnya_check_pen").style.display = "block";
      } else {
        document.getElementById("peristiwa_lainnya_check_pen").style.display = "none";
      }
  }

  function Check1b(that) {
      if (that.value == "7"){
          document.getElementById("peristiwa_lainnya_check_pel").style.display = "block";
      } else {
        document.getElementById("peristiwa_lainnya_check_pel").style.display = "none";
      }
  }

  function Check2(that) {
      if (that.value == "ada"){
        document.getElementById("dokumen_dilampirkan_check_pen").style.display = "block";
      } else {
        document.getElementById("dokumen_dilampirkan_check_pen").style.display = "none";
      }
  }

  function Check3(that) {
      if (that.value == "ada"){
          document.getElementById("dokumen_dilampirkan_check_pel").style.display = "block";
      } else {
        document.getElementById("dokumen_dilampirkan_check_pel").style.display = "none";
      }
  }

  var upload_number = 2;

  function addFile_pen() {
        var br = document.createElement("br");
        var br2 = document.createElement("br");
        var d = document.createElement("div");
        d.setAttribute("class", "fileinput fileinput-new");
        d.setAttribute("data-provides", "fileinput");

        var s = document.createElement("span");
        s.setAttribute("class", "btn default btn-file");
        d.appendChild(s);


        var s2 = document.createElement("span");
        s2.setAttribute("class", "fileinput-new");
        s.appendChild(s2);

        var p = document.createTextNode("Pilih File");

        s2.appendChild(p);

        var s3 = document.createElement("span");
        s3.setAttribute("class", "fileinput-exists");
        s.appendChild(s3);

        var u = document.createTextNode("Ubah");
        s3.appendChild(u);

        var file = document.createElement("input");
        file.setAttribute("type", "file");
        file.setAttribute("name", "file[]");
        file.setAttribute("id", "file");
        file.setAttribute("onchange", "document.getElementById('moreFileLink_pen').style.display = 'block';");
        s.appendChild(file);

        var s4 = document.createElement("span");
        s4.setAttribute("class", "fileinput-filename");
        d.appendChild(s4);

        var spasi = document.createTextNode("\u00A0");
        d.appendChild(spasi);

        var s5 = document.createElement("a");
        s5.setAttribute("href", "#");
        s5.setAttribute("class", "close fileinput-exists");
        s5.setAttribute("data-dismiss", "fileinput");
        d.appendChild(s5);

        document.getElementById("moreFile_pen").appendChild(br);
        document.getElementById("moreFile_pen").appendChild(d);
        document.getElementById("moreFile_pen").appendChild(br2);
        upload_number++;
    }
  function addFile_pel() {
        var br = document.createElement("br");
        var br2 = document.createElement("br");
        var d = document.createElement("div");
        d.setAttribute("class", "fileinput fileinput-new");
        d.setAttribute("data-provides", "fileinput");

        var s = document.createElement("span");
        s.setAttribute("class", "btn default btn-file");
        d.appendChild(s);


        var s2 = document.createElement("span");
        s2.setAttribute("class", "fileinput-new");
        s.appendChild(s2);

        var p = document.createTextNode("Pilih File");

        s2.appendChild(p);

        var s3 = document.createElement("span");
        s3.setAttribute("class", "fileinput-exists");
        s.appendChild(s3);

        var u = document.createTextNode("Ubah");
        s3.appendChild(u);

        var file = document.createElement("input");
        file.setAttribute("type", "file");
        file.setAttribute("name", "file[]");
        file.setAttribute("id", "file");
        file.setAttribute("onchange", "document.getElementById('moreFileLink_pel').style.display = 'block';");
        s.appendChild(file);

        var s4 = document.createElement("span");
        s4.setAttribute("class", "fileinput-filename");
        d.appendChild(s4);

        var spasi = document.createTextNode("\u00A0");
        d.appendChild(spasi);

        var s5 = document.createElement("a");
        s5.setAttribute("href", "#");
        s5.setAttribute("class", "close fileinput-exists");
        s5.setAttribute("data-dismiss", "fileinput");
        d.appendChild(s5);

        document.getElementById("moreFile_pel").appendChild(br);
        document.getElementById("moreFile_pel").appendChild(d);
        document.getElementById("moreFile_pel").appendChild(br2);
        upload_number++;
    }
</script>