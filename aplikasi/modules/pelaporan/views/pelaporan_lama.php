<div class="main">
  <div class="container">
    <!-- Services block BEGIN -->
    <div class="services-block content content-center" id="services">
      <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-12 item">
          <i class="fa fa-money"></i>
          <h3>Uang</h3>
          <p>Pemberian uang sebagai ucapan terima kasih karena telah dibantu.</p>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 item">
          <i class="fa fa-gift"></i>
          <h3>Hadiah</h3>
          <p>Pemberian hadiah atau souvenir atau parsel pada acara-acara dari rekanan.</p>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 item">
          <i class="fa fa-ticket"></i>
          <h3>Tiket Perjalanan</h3>
          <p>Pemberian tiket perjalanan kepada pejabat atau keluarganya untuk keperluan pribadi secara cuma-cuma. </p>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 item">
          <i class="fa fa-percent"></i>
          <h3>Potongan Harga</h3>
          <p>Pemberian potongan harga khusus bagi pejabat untuk pembelian barang atau jasa dari rekanan. </p>
        </div>
      </div>
    </div>
    <!-- Services block END -->
    <hr>
    <!-- BEGIN STEPS -->
    <div class="row margin-bottom-40 front-steps-wrapper front-steps-count-3">
      <div class="col-md-4 col-sm-4 front-step-col">
        <div class="front-step front-step1">
          <h2>Lengkapi Profil</h2>
          <p>Silahkan Anda lengkapi profil Anda sebelum melaporkan gratifikasi sebagai identitas pelapor. <a href="" style="color: white"> Silahkan Klik disini </a></p>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 front-step-col">
        <div class="front-step front-step2">
          <h2>Laporkan Gratifikasi</h2>
          <p>Setelah Anda melengkapi profil Anda, Anda sudah dapat melaporkan gratifikasi. <a href="" style="color: white"> Silahkan Klik disini </a></p>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 front-step-col">
        <div class="front-step front-step3">
          <h2>Tunggu Rekomendasi</h2>
          <p>Silahkan tunggu rekomendasi dari KPK terkait laporan gratifikasi Anda.</p>
        </div>
      </div>
    </div>
    <!-- END STEPS -->
    <hr>
    <!-- BEGIN SERVICE BOX -->   
    <div class="row service-box margin-bottom-40">
      <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
          <em><i class="icon-clock blue"></i></em>
          <span>Paling Lambat 30 Hari Kerja</span>
        </div>
        <p>Laporan gratifikasi dilaporkan oleh penerima gratifikasi <b> paling lambat 30 (tiga puluh) hari kerja</b> terhitung sejak tanggal gratifikasi tersebut diterima.</p>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
          <em><i class="icon-doc red"></i></em>
          <span>Sertai Dokumen Terkait</span>
        </div>
        <p>Laporan disampaikan dengan menyertakan dokumen yang terkait penerimaan gratifikasi.</p>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
          <em><i class="icon-share-alt green"></i></em>
          <span>Wajib Melaporkan</span>
        </div>
        <p>Setiap pegawai negeri atau penyelenggara negara yang menerima gratifikasi wajib melaporkan kepada Komisi Pemberantasan Korupsi.</p>
      </div>
    </div>
    <!-- END SERVICE BOX -->
    <!-- BEGIN BOX -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption" style="font-size: 14px">
              <i class="icon-bulb "></i> UU Nomor 20 Tahun 2001, tentang Perubahan atas UU Nomor 31 Tahun 1999 tentang Pemberantasan Tindak Pidana Korupsi
            </div>
          </div>
          <div class="portlet-body">
            <p style="font-size: 10px">
              <b>Pasal 12B</b><br>
              (1) Setiap gratifikasi kepada pegawai negeri atau penyelenggara negara dianggap pemberi suap, apabila berhubungan dengan jabatannya dan yang berlawan dengan kewajibannya atau tugasnya, dengan ketentuan sebagai berikut:<br>
              &nbsp;&nbsp;&nbsp;&nbsp; a. Yang nilainya Rp 10.000.000,00 (sepuluh juta rupiah) atau lebih, pembuktian bahwa gratifikasi tersebut bukan merupakan suap dilakukan oleh penerima gratifikasi;<br>
              &nbsp;&nbsp;&nbsp;&nbsp; b. Yang nilainya kurang dari Rp 10.000.000,00 (sepuluh juta rupiah), pembuktian bahwa gratifikasi tersebut suap dilakukan oleh penuntut hukum;<br>
              (2) Pidana bagi pegawai negeri atau penyelenggara negara sebagaimana dimaksud dalam ayat (1) adalah pidana penjara seumur hidup atau pidana penjara paling singkat 4 (empat) tahun dan paling lama 20 (dua puluh) tahun, dan pidana denda paling sedikit Rp 200.000.000,00 (dua ratus juta rupiah) dan paling banyak Rp 1.000.000.000,00 (satu miyar rupiah).<br>
              <b>Pasal 12C ayat (1):</b> Ketentuan sebagaimana dimaksud dalam Pasal 12B ayat 1 tidak berlaku jika penerima melaporkan gratifikasi yang diterimanya kepada Komisi Pemberantasan Tindak Pidana Korupsi.<br>
              <b>Pasal 12C ayat (2):</b> Penyampaian laporan sebagaimana dimaksud dalam ayat (1) wajib dilakukan oleh penerima gratifikasi paling lambat 30 (tiga puluh) hari kerja terhitung sejak tanggal gratifikasi tersebut diterima.<br>
            </p>
          </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
      </div>
    </div>
    <!-- END BOX -->
    <!-- BEGIN BOX -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption" style="font-size: 14px">
              <i class="icon-bulb "></i> Tata Cara Pelaporan Gratifikasi 
            </div>
          </div>
          <div class="portlet-body">
            <p>Berdasarkan UU No. 31 tahun 1999 jo UU No. 20 tahun 2001 Pasal 12c ayat 2 dan UU No. 30 tahun 2002 Pasal 16, setiap Pegawai Negeri atau Penyelenggara Negara yang menerima gratifikasi wajib melaporkan kepada Komisi Pemberantasan Korupsi, dengan cara sebagai berikut :</p>
            <ol>
              <li>
                 Penerima gratifikasi wajib melaporkan penerimaanya selambat-lambatnya 30 (tiga puluh) hari kerja kepada KPK, terhitung sejak tanggal gratifikasi tersebut diterima.
              </li>
              <li>
                 Laporan disampaikan secara online dengan mengisi formulir sebagaimana ditetapkan oleh Komisi Pemberantasan Korupsi dengan melampirkan dokumen yang berkaitan dengan gratifikasi.
              </li>
            </ol>
            <p>Formulir sebagaimana angka 2, sekurang-kurangnya memuat :</p>
            <ol>
              <li>
                 Nama dan alamat lengkap penerima dan pemberi gratifikasi.
              </li>
              <li>
                 Jabatan Pegawai Negeri atau Penyelenggara Negara.
              </li>
              <li>
                 Tempat dan waktu penerima gratifikasi.
              </li>
              <li>
                 Uraian jenis gratifikasi yang diterima; dan
              </li>
              <li>
                 Nilai gratifikasi yang diterima.
              </li>
            </ol>
          </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
      </div>
    </div>
    <!-- END BOX -->
    <!-- BEGIN BOX -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption" style="font-size: 14px">
              <i class="icon-bulb "></i> Contoh Pemberian yang dapat dikategorikan sebagai Gratifikasi :
            </div>
          </div>
          <div class="portlet-body">
            <ol>
              <li>
                 Pemberian hadiah atau uang sebagai ucapan terima kasih karena telah dibantu.
              </li>
              <li>
                 Hadiah atau sumbangan pada saat perkawinan anak dari pejabat oleh rekanan kantor pejabat tersebut.
              </li>
              <li>
                 Pemberian tiket perjalanan kepada pejabat atau keluarganya untuk keperluan pribadi secara cuma-cuma.
              </li>
              <li>
                 Pemberian potongan harga khusus bagi pejabat untuk pembelian barang atau jasa dari rekanan.
              </li>
              <li>
                 Pemberian biaya atau ongkos naik haji dari rekanan kepada pejabat.
              </li>
              <li>
                 Pemberian hadiah ulang tahun atau pada acara-acara pribadi lainnya dari rekanan.
              </li>
              <li>
                 Pemberian hadiah atau souvenir kepada pejabat pada saat kunjungan kerja.
              </li>
              <li>
                 Pemberian hadiah atau parsel kepada pejabat pada saat hari raya keagamaan, oleh rekanan atau bawahannya.
              </li>
            </ol>
            <p>Seluruh pemberian tersebut diatas, dapat dikategorikan sebagai gratifikasi, apabila ada hubungan kerja atau kedinasan antara pemberi dan dengan pejabat yang menerima, dan/atau semata-mata karena keterkaitan dengan jabatan atau kedudukan pejabat tersebut.</p>
          </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
      </div>
    </div>
    <!-- END BOX -->
  </div>
</div>