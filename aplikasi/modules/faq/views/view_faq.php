<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
      <h1>Frequently Asked Questions</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
              <!-- START TAB 1 -->
               <div class="panel-group" id="accordion1">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a href="#accordion1_1" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" style="font-size: 20px;">
                           A. Apa Kriteria Gratifikasi Yang Dilarang?
                           </a>
                        </h4>
                     </div>
                     <div class="panel-collapse collapse in" id="accordion1_1" style="font-size: 17px;">
                        <div class="panel-body">
                           1.	Gratifikasi yang diterima berhubungan dengan jabatan;<br><br>
						         2.	Penerimaan tersebut dilarang oleh peraturan yang berlaku, bertentangan dengan kode etik, memiliki konflik kepentingan atau merupakan penerimaan yang tidak patut/tidak wajar.

                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a href="#accordion1_2" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed" style="font-size: 20px;">
                           B. Mengapa Gratifikasi itu dilarang?
                           </a>
                        </h4>
                     </div>
                     <div class="panel-collapse collapse" id="accordion1_2" style="font-size: 17px;">
                        <div class="panel-body">
                           Gratifikasi pada dasarnya adalah suap yang tertunda atau sering juga disebut suap terselubung. Pegawai negeri atau penyelenggara negara (Pn/PN) yang terbiasa menerima gratifikasi terlarang lama kelamaan dapat terjerumus melakukan korupsi bentuk lain, seperti suap, pemerasan dan korupsi lainnya. Sehingga gratifikasi dianggap sebagai akar korupsi.
						   <br><br>
						   Gratifikasi tersebut dilarang karena dapat mendorong Pn/PN bersikap tidak obyektif, tidak adil dan tidak professional. Sehingga Pn/PN tidak dapat melaksanakan tugasnya dengan baik.
						   <br><br>
						   Undang-undang menggunakan istilah gratifikasi yang dianggap pemberian suap untuk menunjukkan bahwa penerimaan gratifikasi yang berhubungan dengan jabatan dan berlawanan dengan kewajiban atau tugasnya.

                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a href="#accordion1_3" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed" style="font-size: 20px;">
                           C. Apa saja gratifikasi yang boleh diterima?
                           </a>
                        </h4>
                     </div>
                     <div class="panel-collapse collapse" id="accordion1_3" style="font-size: 17px;">
                        <div class="panel-body">
                           Pada dasarnya semua gratifikasi yang diterima oleh Pn/PN wajib dilaporkan pada KPK, kecuali :<br><br>
						   1.	Pemberian dari keluarga, yakni kakek/nenek , bapak/ibu/mertua, suami/istri, anak/anak menantu, cucu, besan, paman/bibi, kakak ipar/adik ipar, sepupu/keponakan. Gratifikasi dari pihak-pihak tersebut boleh diterima dengan syarat tidak memiliki benturan kepentingan dengan posisi ataupun jabatan penerima;<br>
						   <br>2.	Hadiah tanda kasih dalam bentuk uang atau barang yang memiliki nilai jual dalam penyelenggaraan pesta pernikahan, kelahiran, aqiqah, baptis, khitanan dan potong gigi, atau upacara adat/agama lainnya dengan batasan nilai per pemberi dalam setiap acara paling banyak Rp1.000.000,00 (satu juta rupiah);<br>
						   <br>3.	Pemberian terkait dengan musibah atau bencana yang dialami oleh penerima, bapak/ibu/mertua, suami/istri, atau anak penerima gratifikasi paling banyak Rp1.000.000,00 (satu juta rupiah);<br>
						   <br>4.	Pemberian dari sesama pegawai dalam rangka pisah sambut, pensiun, promosi jabatan, ulang tahun ataupun perayaan lainnya yang lazim dilakukan dalam konteks sosial sesama rekan kerja. Pemberian tersebut tidak berbentuk uang ataupun setara uang, misalnya pemberian voucher belanja, pulsa, check atau giro. Nilai pemberian paling banyak Rp300.000,00 (tiga ratus ribu rupiah) per pemberian per orang, dengan batasan total pemberian selama satu tahun sebesar Rp1.000.000,00 (satu juta rupiah) dari pemberi yang sama;<br>
						   <br>5.	Pemberian sesama pegawai dengan batasan paling banyak Rp200.000,00 (dua ratus ribu rupiah) per pemberian per orang, dengan batasan total pemberian selama satu tahun sebesar Rp1.000.000,00 (satu juta rupiah) dari pemberi yang sama. Pemberian tersebut tidak berbentuk uang ataupun setara uang, misalnya voucher belanja, pulsa, check atau giro;<br>
						   <br>6.	Hidangan atau sajian yang berlaku umum;<br>
						   <br>7.	Prestasi akademis atau non akademis yang diikuti dengan menggunakan biaya sendiri seperti kejuaraan, perlombaan atau kompetisi tidak terkait kedinasan;<br>
						   <br>8.	Keuntungan atau bunga dari penempatan dana, investasi atau kepemilikan saham pribadi yang berlaku umum;<br>
						   <br>9.	Manfaat bagi seluruh peserta koperasi pegawai berdasarkan keanggotaan koperasi Pegawai Negeri yang berlaku umum;<br>
						   <br>10.	Seminar Kit yang berbentuk seperangkat modul dan alat tulis serta sertifikat yang diperoleh dari kegiatan resmi kedinasan seperti rapat, seminar, workshop, konferensi, pelatihan, atau kegiatan lain sejenis yang berlaku umum;<br>
						   <br>11.	Penerimaan hadiah atau tunjangan baik berupa uang atau barang yang ada kaitannya dengan peningkatan prestasi kerja yang diberikan oleh Pemerintah sesuai dengan peraturan perundang-undangan yang berlaku; atau,<br>
						   <br>12.	Diperoleh dari kompensasi atas profesi di luar kedinasan, yang tidak terkait dengan tugas pokok dan fungsi dari pejabat/pegawai, tidak memiliki konflik kepentingan dan tidak melanggar aturan internal instansi pegawai.
  					    </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a href="#accordion1_4" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed" style="font-size: 20px;">
                           D. Apakah gratifikasi boleh diterima dari pihak yang memiliki konflik kepentingan dalam pelaksanaan resepsi, upacara adat/budaya/tradisi, dan perayaan agama?
                           </a>
                        </h4>
                     </div>
                     <div class="panel-collapse collapse" id="accordion1_4" style="font-size: 17px;">
                        <div class="panel-body">
                           Boleh diterima. Namun untuk penerimaan yang melebihi nilai wajar tertentu (saat ini batasannya adalah Rp1.000.000,00) maka penerimaan itu wajib dilaporkan pada KPK. Hal ini dikarenakan penyelenggaraan acara tersebut membutuhkan biaya, dan sudah menjadi bagian dari tradisi yang sudah berjalan. <br><br>
						   Tidak semua penerimaan di atas Rp1.000.000,00 (satu juta rupiah) secara otomatis menjadi milik negara, karena KPK akan mempertimbangkan aspek hubungan dengan jabatan penerima. Penerimaan gratifikasi yang nilainya di atas Rp1.000.000,00 (satu juta rupiah) dan mempunyai potensi konflik kepentingan akan menjadi milik negara.

                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a href="#accordion1_5" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed" style="font-size: 20px;">
                           E. Apa saja gratifikasi yang tidak boleh diterima?
                           </a>
                        </h4>
                     </div>
                     <div class="panel-collapse collapse" id="accordion1_5" style="font-size: 17px;">
                        <div class="panel-body">
                           Gratifikasi yang tidak boleh diterima adalah gratifikasi terlarang, yaitu yang berhubungan dengan jabatan dan berlawanan dengan tugas dan kewajiban Pn/PN.<br>
						   <br>Untuk memudahkan pemahaman, berikut adalah contoh gratifikasi yang tidak boleh diterima :<br><br>
						   1.	Terkait dengan pemberian layanan pada masyarakat di luar penerimaan yang sah;<br>
						   2.	Terkait dengan tugas dalam proses penyusunan anggaran di luar penerimaan yang sah;<br>
						   3.	Terkait dengan tugas dalam proses pemeriksaan, audit, monitoring dan evaluasi di luar penerimaan yang sah;<br>
						   4.	Terkait dengan pelaksanaan perjalanan dinas di luar penerimaan yang sah/resmi dari instansi;<br>
						   5.	Dalam proses penerimaan/promosi/mutasi pegawai.

                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a href="#accordion1_7" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed" style="font-size: 20px;">
                           F. Bagaimana jika saya tidak meminta gratifikasi, namun masih tetap diberi, bolehkah saya menerimanya? 
                           </a>
                        </h4>
                     </div>
                     <div class="panel-collapse collapse" id="accordion1_7" style="font-size: 17px;">
                        <div class="panel-body">
                           Jika pemberian tersebut berhubungan dengan jabatan kita atau ada ketentuan yang melarang, maka pemberian tersebut harus ditolak, walaupun kita tidak memintanya. Jika pada keadaan tertentu kita tidak dapat menolaknya, seperti dikirimkan ke rumah, diberikan melalui anggota keluarga, atau untuk menjaga hubungan baik antar lembaga, maka pemberian tersebut wajib dilaporkan kepada KPK.<br>
						   <br>Berikut beberapa pertanyaan yang dapat diajukan kepada diri sendiri saat mempertimbangkan apakah sebuah hadiah boleh kita terima atau tidak. Metode ini disebut dengan istilah PROVE IT.<br>
						   <br>
						    P	Purpose	Atau tujuan, apakah tujuan pemberian ini?<br><br>
							R	Rules Atau aturan, bagaimanakah aturan perundangan mengatur tentang gratifikasi?<br><br>
							O	Openess	Atau keterbukaan, bagaimana substansi keterbukaan pemberian tersebut?<br>Apakah hadiah diberikan secara sembunyi-sembunyi atau di depan umum.<br><br>
							V	Value Atau nilai, berapa nilai dari gratifikasi tersebut?<br>Jika gratifikasi memiliki nilai yang cukup tinggi maka sebaiknya Pn/PN bersikap lebih berhati-hati dan menolak pemberian tersebut<br><br>
							E	Ethics Atau etika, apakah nilai moral pribadi anda memperbolehkan penerimaan hadiah tersebut?<br><br>
							I	Identity Atau identitas pemberi, apakah pemberi memiliki hubungan jabatan, calon rekanan, atau rekanan instansi?<br><br>
							T	Timing Atau waktu pemberian, apakah pemberian gratifikasi berhubungan dengan pengambilan keputusan, pelayanan atau perizinan?<br><br>

                        </div>
                     </div>
                  </div>
				   <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a href="#accordion1_8" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed" style="font-size: 20px;">
                           G. Apa perbedaan antara gratifikasi, suap dan pemerasan? 
                           </a>
                        </h4>
                     </div>
                     <div class="panel-collapse collapse" id="accordion1_8" style="font-size: 17px;">
                        <div class="panel-body">
                           Secara sederhana gratifikasi tidak membutuhkan sesuatu yang transaksional atau ditujukan untuk mempengaruhi keputusan atau kewenangan secara langsung. Hal ini berbeda dengan suap yang bersifat transaksional.<br>
						  <br> Sedangkan pidana pemerasan, inisiatif permintaan dan paksaan berasal dari Pegawai Negeri atau Penyelenggara Negara. Pada pidana pemerasan yang dihukum pidana hanyalah pihak penerima saja.

                        </div>
                     </div>
                  </div>
               </div>
              <!-- END TAB 1 -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
</div>