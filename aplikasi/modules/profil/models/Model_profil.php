<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_profil extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getprovinsi() {
        $data = array();

        $sql = "SELECT * FROM mst_provinsi ";
                
        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }

    function show_profil($a){
        $this->db->select('a.*, b.nama, c.name as nprovinsi_kantor, d.name as nprovinsi_rumah');
        $this->db->from('conf_user a');
        $this->db->join('mst_alamat_pengirim b','a.alamat_pengiriman = b.id','LEFT');
        $this->db->join('mst_provinsi c','a.provinsi_kantor = c.id','LEFT');
        $this->db->join('mst_provinsi d','a.provinsi_rumah = d.id','LEFT');
        $this->db->where('a.id', $a);

        return $this->db->get();
    }

    function insert($data){
        $res = $this->db->insert('conf_user', $data);

        if (!$res) {
            return 'proses gagal';
        } else {
            return 'sukses';
        }
    }

    function update($id_user,$data){
        $res = $this->db->where('id', $id_user)
                        ->update('conf_user', $data);
        
        if (!$res):
            return 'proses gagal';
        else:
            return 'sukses';
        endif;
    }

    function show_profil_inspektorat($id_laporan){
        $this->db->select('a.*, b.nama, c.name as nprovinsi_kantor, d.name as nprovinsi_rumah');
        $this->db->from('conf_user a');
        $this->db->join('mst_alamat_pengirim b','a.alamat_pengiriman = b.id','LEFT');
        $this->db->join('mst_provinsi c','a.provinsi_kantor = c.id','LEFT');
        $this->db->join('mst_provinsi d','a.provinsi_rumah = d.id','LEFT');
	$this->db->join('lap_tahunan e','a.id = e.id_user','LEFT');

	$this->db->where('e.id', $id_laporan);

        return $this->db->get();
    }
	
	function update_passwd($id_user,$data){
        $res = $this->db->where('id', $id_user)
                        ->update('conf_user', $data);
        
        if (!$res):
            return 'gagal';
        else:
            return 'sukses';
        endif;
    }

}
