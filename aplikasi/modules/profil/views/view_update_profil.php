<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
      <h1>Profil Saya</h1>
      <div class="content-form-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <form role="form" id="insert_profil" method="post" autocomplete="off" action="<?= site_url('profil/update'); ?>">
              <?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>
              <div class="form-body row">
                <div class="col-md-6 col-sm-6">
                  <h3 style="color: #65aed9">Identitas Pelapor</h3>
                  <div class="form-group">
                    <label>Nama Lengkap <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="name" name="name" <?php if (!empty($show_profil->name)):?> value="<?php cetak($show_profil->name) ?>" readonly <?php endif; ?>>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Tempat Lahir <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="tempatlahir" name="tempatlahir" <?php if (!empty($show_profil->tempatlahir)):?> value="<?php cetak($show_profil->tempatlahir) ?>" readonly <?php endif; ?>>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Tanggal Lahir <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                        <i class="fa"></i>
                        <div class="input-group">    
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input class="form-control date-picker input-sm" size="16" type="text" id="tgllahir" name="tgllahir" <?php if (!empty($show_profil->tgllahir)):?> value="<?php cetak($show_profil->tgllahir) ?>" readonly <?php endif; ?>>
                            </div>
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>No. KTP (NIK) <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber(this);" class="form-control" id="no_ktp" name="no_ktp" value="<?php cetak($show_profil->no_ktp) ?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <h3 style="color: #65aed9">&nbsp;</h3>
                  <div class="form-group">
                    <label>Alamat Email <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="email" name="email" value="<?php cetak($show_profil->email) ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Nomor Telepon Seluler <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber1(this);" class="form-control" id="nohp" name="nohp" value="<?php cetak($show_profil->nohp) ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Pin BB/WA </label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="pin_bb" name="pin_bb" value="<?php cetak($show_profil->pin_bb) ?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-body row">
                <div class="col-md-6 col-sm-6">
                  <h3 style="color: #65aed9">Uraian Instansi</h3>
                  <div class="form-group">
                    <label>Nama Instansi <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="inskerja" name="inskerja" value="<?php cetak($show_profil->inskerja) ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Unit Kerja <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="biro" name="biro" value="<?php cetak($show_profil->biro) ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Bagian </label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="bagian" name="bagian" value="<?php cetak($show_profil->bagian) ?>"> 
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <h3 style="color: #65aed9">&nbsp;</h3>
                  <div class="form-group">
                    <label>Jabatan <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                     <input type="text" class="form-control" id="jabatan" name="jabatan" value="<?php cetak($show_profil->jabatan) ?>"> 
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Golongan <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                     <input type="text" class="form-control" id="golongan" name="golongan" value="<?php cetak($show_profil->golongan) ?>"> 
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Pangkat <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                     <input type="text" class="form-control" id="pangkat" name="pangkat" value="<?php cetak($show_profil->pangkat) ?>"> 
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-body row">
                <div class="col-md-6 col-sm-6">
                  <h3 style="color: #65aed9">Alamat Kantor</h3>
                  <div class="form-group">
                    <label>Alamat Kantor <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="alamat_kantor" name="alamat_kantor" placeholder="Alamat Kantor"  onkeyup="checkonlynumber6(this);" ><?php cetak($show_profil->alamat_kantor) ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Kelurahan/Desa <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                     <input type="text" class="form-control" name="kelurahan_kantor" id="kelurahan_kantor" value="<?php cetak($show_profil->kelurahan_kantor) ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Kecamatan <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                     <input type="text" class="form-control" name="kecamatan_kantor" id="kecamatan_kantor" value="<?php cetak($show_profil->kecamatan_kantor) ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Kabupaten/Kota <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                     <input type="text" class="form-control" name="kota_kantor" id="kota_kantor" value="<?php cetak($show_profil->kota_kantor) ?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <h3 style="color: #65aed9">&nbsp;</h3>
                  <div class="form-group">
                    <label>Provinsi <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="provinsi_kantor" id="provinsi_kantor" class="form-control">
                        <option value="">-- Silahkan Pilih --</option>
                        <?php foreach ($provinsi as $row): ?>
                          <option value="<?php cetak($row['id']) ?>" <?php if ($row['id'] == $show_profil->provinsi_kantor): echo 'selected="selected"'; endif; ?>><?php cetak($row['name']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Kode Pos <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                     <input type="text" onkeyup="checkonlynumber2(this);" class="form-control" id="kode_pos_kantor" name="kode_pos_kantor" value="<?php cetak($show_profil->kode_pos_kantor) ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Nomor Telepon Kantor <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                     <input type="text" onkeyup="checkonlynumber2(this);" class="form-control" id="no_kantor" name="no_kantor" value="<?php cetak($show_profil->no_kantor) ?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-body row">
                <div class="col-md-6 col-sm-6">
                  <h3 style="color: #65aed9">Alamat Rumah</h3>
                  <div class="form-group">
                    <label>Alamat Rumah<span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="alamat_rumah" name="alamat_rumah" placeholder="Alamat rumah"><?php cetak($show_profil->alamat_rumah) ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Kelurahan/Desa <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                     <input type="text" class="form-control" name="kelurahan_rumah" id="kelurahan_rumah" value="<?php cetak($show_profil->kelurahan_rumah) ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Kecamatan <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                     <input type="text" class="form-control" name="kecamatan_rumah" id="kecamatan_rumah" value="<?php cetak($show_profil->kecamatan_rumah) ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Kabupaten/Kota <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                     <input type="text" class="form-control" name="kota_rumah" id="kota_rumah" value="<?php cetak($show_profil->kota_rumah) ?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <h3 style="color: #65aed9">&nbsp;</h3>
                  <div class="form-group">
                    <label>Provinsi <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="provinsi_rumah" id="provinsi_rumah" class="form-control">
                        <option value="">-- Silahkan Pilih --</option>
                        <?php foreach ($provinsi as $row): ?>
                          <option value="<?php cetak($row['id']) ?>" <?php if ($row['id'] == $show_profil->provinsi_rumah): echo 'selected="selected"'; endif; ?>><?php cetak($row['name']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Kode Pos <span class="require" style="color:red">*</span></label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                     <input type="text" onkeyup="checkonlynumber2(this);" class="form-control" id="kode_pos_rumah" name="kode_pos_rumah" value="<?php cetak($show_profil->kode_pos_rumah) ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Nomor Telepon rumah</label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                     <input type="text" onkeyup="checkonlynumber2(this);" class="form-control" id="no_rumah" name="no_rumah" value="<?php cetak($show_profil->no_rumah) ?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-body row">
                <div class="col-md-12 col-sm-12">
                  <button type="submit" class="btn btn-primary">Simpan Data</button>
                  <a href="<?= site_url('profil') ?>" class="btn btn-default">Batal</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
</div>
<script>
  function checkonlynumber(ob) {
      var validChars = /[^0-9-+ ]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber1(ob) {
      var validChars = /[^0-9-+ ]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber2(ob) {
      var validChars = /[^0-9-+ ]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber3(ob) {
      var validChars = /[^0-9-+ ]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber4(ob) {
      var validChars = /[^0-9-+ ]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber5(ob) {
      var validChars = /[^0-9-+ ]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber6(ob) {
      var validChars = /^[a-z0-9]+$/;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
</script>