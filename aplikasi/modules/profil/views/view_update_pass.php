<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
      <h1>Profil Saya</h1>
      <div class="content-form-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <form role="form" id="insert_profil" method="post" autocomplete="off" action="<?= site_url('profil/update_passwd'); ?>">
              <?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>
              <div class="form-body row">
                <div class="col-md-6 col-sm-6">
                  <h3 style="color: #65aed9">Ganti Password</h3>
                  <div class="form-group">
                    <label>Username</label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="name" name="name" <?php if (!empty($show_profil->username)):?> value="<?php cetak($show_profil->username) ?>" readonly <?php endif; ?>>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Password Baru</label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="password" class="form-control" id="pass_baru" name="pass_baru">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Ulangi Password Baru</label>
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="password" class="form-control" id="ulang_pass" name="ulang_pass">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-body row">
                <div class="col-md-12 col-sm-12">
                  <button type="submit" class="btn btn-primary">Simpan Data</button>
                  <a href="<?= site_url('profil') ?>" class="btn btn-default">Batal</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
</div>
<script>
  function checkonlynumber(ob) {
      var validChars = /[^0-9-+ ]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber1(ob) {
      var validChars = /[^0-9-+ ]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber2(ob) {
      var validChars = /[^0-9-+ ]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber3(ob) {
      var validChars = /[^0-9-+ ]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber4(ob) {
      var validChars = /[^0-9-+ ]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber5(ob) {
      var validChars = /[^0-9-+ ]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber6(ob) {
      var validChars = /^[a-z0-9]+$/;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
</script>