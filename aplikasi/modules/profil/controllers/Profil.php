<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('Model_profil'));
        if (!$this->session->userdata('username')):
            redirect('login');
            return;
        endif;
        @date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data['provinsi'] = $this->Model_profil->getprovinsi();
        $data['show_profil'] = $this->Model_profil->show_profil($id_user)->row();
        $data['tagmenu'] = 'profil';
        $data['body'] = 'profil/view_profil';
        $this->load->vars($data);
        $this->load->view('view_main');
    }

    public function insert() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $data_non = array(
            'name' => strip_tags(trim($_POST['name'])),
            'tempatlahir' => strip_tags(trim($_POST['tempatlahir'])),
            'tgllahir' => strip_tags(trim($_POST['tgllahir'])),
            'no_ktp' => strip_tags(trim($_POST['no_ktp'])),
            'jabatan' => strip_tags(trim($_POST['jabatan'])),
            'email' => strip_tags(trim($_POST['email'])),
            'nohp' => strip_tags(trim($_POST['nohp'])),
            'pin_bb' => strip_tags(trim($_POST['pin_bb'])),
            'inskerja' => strip_tags(trim($_POST['inskerja'])),
            'biro' => strip_tags(trim($_POST['biro'])),
            'bagian' => strip_tags(trim($_POST['bagian'])),
            'golongan' => strip_tags(trim($_POST['golongan'])),
            'pangkat' => strip_tags(trim($_POST['pangkat'])),
            'alamat_kantor' => strip_tags(trim($_POST['alamat_kantor'])),
            'provinsi_kantor' => strip_tags(trim($_POST['provinsi_kantor'])),
            'kota_kantor' => strip_tags(trim($_POST['kota_kantor'])),
            'kecamatan_kantor' => strip_tags(trim($_POST['kecamatan_kantor'])),
            'kelurahan_kantor' => strip_tags(trim($_POST['kelurahan_kantor'])),
            'kode_pos_kantor' => strip_tags(trim($_POST['kode_pos_kantor'])),
            'no_kantor' => strip_tags(trim($_POST['no_kantor'])),
            'alamat_rumah' => strip_tags(trim($_POST['alamat_rumah'])),
            'provinsi_rumah' => strip_tags(trim($_POST['provinsi_rumah'])),
            'kota_rumah' => strip_tags(trim($_POST['kota_rumah'])),
            'kecamatan_rumah' => strip_tags(trim($_POST['kecamatan_rumah'])),
            'kelurahan_rumah' => strip_tags(trim($_POST['kelurahan_rumah'])),
            'kode_pos_rumah' => strip_tags(trim($_POST['kode_pos_rumah'])),
            'no_rumah' => strip_tags(trim($_POST['no_rumah'])),
            // 'alamat_pengiriman' => strip_tags(trim($_POST['alamat_pengiriman']))
        );
        $data = $this->security->xss_clean($data_non);
        $insert = $this->Model_profil->insert($data);

        if ($insert == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Data berhasil ditambah.</div>');
            redirect('profil');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Data gagal ditambah.</div>');
            redirect('profil');
        }
    }

    public function view_update() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data['show_profil'] = $this->Model_profil->show_profil($id_user)->row();
        $data['provinsi'] = $this->Model_profil->getprovinsi();
        $data['tagmenu'] = 'profil';
        $data['body'] = 'profil/view_update_profil';
        $this->load->vars($data);
        $this->load->view('view_main');
    }

    public function update() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data_non = array(
            'name' => strip_tags(trim($_POST['name'])),
            'tempatlahir' => strip_tags(trim($_POST['tempatlahir'])),
            'tgllahir' => strip_tags(trim($_POST['tgllahir'])),
            'no_ktp' => strip_tags(trim($_POST['no_ktp'])),
            'jabatan' => strip_tags(trim($_POST['jabatan'])),
            'email' => strip_tags(trim($_POST['email'])),
            'nohp' => strip_tags(trim($_POST['nohp'])),
            'pin_bb' => strip_tags(trim($_POST['pin_bb'])),
            'inskerja' => strip_tags(trim($_POST['inskerja'])),
            'biro' => strip_tags(trim($_POST['biro'])),
            'bagian' => strip_tags(trim($_POST['bagian'])),
            'golongan' => strip_tags(trim($_POST['golongan'])),
            'pangkat' => strip_tags(trim($_POST['pangkat'])),
            'alamat_kantor' => strip_tags(trim($_POST['alamat_kantor'])),
            'provinsi_kantor' => strip_tags(trim($_POST['provinsi_kantor'])),
            'kota_kantor' => strip_tags(trim($_POST['kota_kantor'])),
            'kecamatan_kantor' => strip_tags(trim($_POST['kecamatan_kantor'])),
            'kelurahan_kantor' => strip_tags(trim($_POST['kelurahan_kantor'])),
            'kode_pos_kantor' => strip_tags(trim($_POST['kode_pos_kantor'])),
            'no_kantor' => strip_tags(trim($_POST['no_kantor'])),
            'alamat_rumah' => strip_tags(trim($_POST['alamat_rumah'])),
            'provinsi_rumah' => strip_tags(trim($_POST['provinsi_rumah'])),
            'kota_rumah' => strip_tags(trim($_POST['kota_rumah'])),
            'kecamatan_rumah' => strip_tags(trim($_POST['kecamatan_rumah'])),
            'kelurahan_rumah' => strip_tags(trim($_POST['kelurahan_rumah'])),
            'kode_pos_rumah' => strip_tags(trim($_POST['kode_pos_rumah'])),
            'no_rumah' => strip_tags(trim($_POST['no_rumah'])),
            // 'alamat_pengiriman' => strip_tags(trim($_POST['alamat_pengiriman']))
        );
        $data = $this->security->xss_clean($data_non);
        $update = $this->Model_profil->update($id_user,$data);

        if ($update == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Data berhasil disimpan.</div>');
            redirect('profil');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Data gagal disimpan.</div>');
            redirect('profil');
        }
    }
	
	public function view_passwd() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data['show_profil'] = $this->Model_profil->show_profil($id_user)->row();
        $data['provinsi'] = $this->Model_profil->getprovinsi();
        $data['tagmenu'] = 'profil';
        $data['body'] = 'profil/view_update_pass';
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function update_passwd() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
		$password = strip_tags(trim($_POST['pass_baru']));
		$hash = $this->bcrypt->hash_password($password);
		
		if (strip_tags(trim($_POST['pass_baru'])) != strip_tags(trim($_POST['ulang_pass']))) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Gagal! </strong></h4> Password yang Anda masukkan tidak sama.</div>');
            redirect('profil');
        } else {
            
        $data_non = array(
            'username' => strip_tags(trim($_POST['name'])),
			'password' => $hash
        );
        $data = $this->security->xss_clean($data_non);
        $update = $this->Model_profil->update_passwd($id_user,$data);

        if ($update == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Data berhasil disimpan.</div>');
            redirect('profil');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Data gagal disimpan.</div>');
            redirect('profil');
        }
		
		}
    }
}