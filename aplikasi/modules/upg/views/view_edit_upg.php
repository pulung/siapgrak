<div class="main">
  <div class="container">
    	
	<?php //if($this->session->userdata('role') == '1': ?>
	<!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
	 	
	<!-- BEGIN CONTENT -->
	<form method="post" id="form_upg" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('upg/tambah_upg'); ?>">
	 <div class="col-md-12 col-sm-12">
	  <br><br>
	  <?= $this->session->flashdata('message'); ?>
      <h1>Tambah UPG</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<span style="font-size:12px"><font style="color:red; ">*</font><i> Wajib diisi</i></span><br><br>
				<label for="role">Nama UPG<font style="color:red">*</font></label>
				<input type="text" id="nama" name="nama" class="form-control" placeholder="Masukkan nama" style="width:1000px; height:40px" required><br>
				<label for="role">Alamat <font style="color:red">*</font></label>
				<textarea type="text" id="alamat" name="alamat" class="form-control" placeholder="Masukkan alamat" style="width:1000px; height:100px" required></textarea><br>
				<label for="role">No. Telepon <font style="color:red">*</font></label>
				<input type="text" id="notelp" name="notelp" class="form-control" placeholder="Masukkan nomor telepon" style="width:1000px; height:40px" required><br>
			    <label for="role">Fax <font style="color:red">*</font></label>
				<input type="text" id="fax" name="fax" class="form-control" placeholder="Masukkan nomor Fax" style="width:1000px; height:40px" required><br>
				<label for="role">Email <font style="color:red">*</font></label>
				<input type="text" id="email" name="email" class="form-control" placeholder="Masukkan alamat email" style="width:1000px; height:40px" required><br>
				<label for="role">Longitude <font style="color:red">*</font></label>
				<input type="text" id="longitude" name="longitude" class="form-control" placeholder="Masukkan longitude" style="width:1000px; height:40px" required><br>
				<label for="role">Latitude <font style="color:red">*</font></label>
				<input type="text" id="latitude" name="latitude" class="form-control" placeholder="Masukkan latitude" style="width:1000px; height:40px" required><br>
				
				
				<input type="submit" id="btnKirimUPG" class="btn btn-success" placeholder="Kirim">
			</div>
		  </div>
		</div>
	  </div>
	 </div>
	</form>
	<!-- END CONTENT -->
	
	<!-- BEGIN CONTENT -->
	<form method="post" id="form_upg" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('upg/tambah_upg'); ?>">
	 <div class="col-md-12 col-sm-12">
	  <br><br>
      <h1>UPG</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<table id="tabel-data" name="tabel-data" class="table table-striped table-bordered" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Nama UPG</th>
							<th>Alamat</th>
							<th>No. Telepon</th>
							<th>No. Fax</th>
							<th>Email</th>
							<th>Aksi</th>
						</tr>
					</thead>
					
					<tbody>
					<?php //if(($row['jawaban']) != 'NULL'){ ?>
					<?php foreach ($upg as $row): ?>
                          
						<tr>
						
							<td><?php cetak($row['nama_upg']); ?></td>
							<td><?php cetak($row['alamat']); ?></td>
							<td><?php cetak($row['no_telp']); ?></td>
							<td><?php cetak($row['fax']); ?></td>
							<td><?php cetak($row['email']); ?></td>
							
							<td>
								    
								  <a href="<?= site_url()?>upg/edit_upg/<?php cetak($row['id_upg']); ?>" title="Edit">
									  <span class="btn btn-xs btn-primary">
											<i class="fa fa-pencil"></i> 
									  </span>
								  </a>
								  <a href="<?= site_url()?>upg/hapus_upg/<?php cetak($row['id_upg']); ?>" onclick="return window.confirm('Apakah Anda yakin ingin menghapus data tersebut?');"  title="Hapus">
									   <span class="btn btn-xs btn-danger">
											<i class="fa fa-remove"></i> 
									   </span>
								  </a>
								  
							</td>
						</tr>
						
					<?php endforeach; ?>
					<?php //} ?>						   
					</tbody>
				</table>
				
			</div>
		  </div>
		</div>
	  </div>
	 </div>
	</form>
	<!-- END CONTENT -->
	
  </div>
</div>