<div class="main">
  <div class="container">
  <?php //$jml = count($upg); ?>
  <?php //for($i = 0; $i < $jml; $i++): ?>
  <?php foreach ($upg as $row): ?>
  <!-- BEGIN CONTENT -->
	<form method="post" id="form_faq" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= base_url()?>upg/update_upg/<?= $upg->id_upg; ?>">
	 <div class="col-md-12 col-sm-12">
	  <br><br>
      <h1>Data UPG</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<span style="font-size:12px"><font style="font-size:7px; color:red; ">*</font><i> Wajib diisi</i></span><br><br>
				<label for="role">Nama UPG <font style="font-size:7px; color:red">*</font></label>
				<input type="text" id="nama" name="nama" class="form-control" value="<?php cetak($row['nama_upg']); ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Alamat <font style="font-size:7px; color:red">*</font></label>
				<input type="text" id="alamat" name="alamat" class="form-control" value="<?php cetak($row['alamat']); ?>" style="width:1000px; height:100px" required></input><br>
				<label for="role">No. Telepon <font style="font-size:7px; color:red">*</font></label>
				<input type="text" id="notelp" name="notelp" class="form-control" value="<?php cetak($row['no_telp']); ?>" style="width:1000px; height:40px" required><br>
			    <label for="role">Fax</label>
				<input type="text" id="fax" name="fax" class="form-control" value="<?php cetak($row['fax']); ?>" style="width:1000px; height:40px"><br>
				<label for="role">Email </label>
				<input type="text" id="email" name="email" class="form-control" value="<?php cetak($row['email']); ?>" style="width:1000px; height:40px"><br>
				<label for="role">Longitude </label>
				<input type="text" id="longitude" name="longitude" class="form-control" value="<?php cetak($row['longitude']); ?>" style="width:1000px; height:40px"><br>
				<label for="role">Latitude </label>
				<input type="text" id="latitude" name="latitude" class="form-control" value="<?php cetak($row['latitude']); ?>" style="width:1000px; height:40px"><br>
				
				
				<!--<div class="col-md-2">
					<td>
						<a href="<?php echo base_url('upg'); ?>">
							<button type="button" class="btn btn-block btn-primary btn-lg bg-red"><i class="fa fa-times "></i>&nbsp;&nbsp;Kembali</button>
						</a>
					</td>
				</div>-->
				<div class="col-md-2">
					<td>
						
							<button type="submit" class="btn btn-block btn-success btn-lg" style="width:130px;"><i class="fa fa-save "></i>&nbsp;Simpan&nbsp;&nbsp;</button>
						
					</td>                 
				</div>
			</div>
		  </div>
		</div>
	  </div>
	 </div>
	</form>
	<!-- END CONTENT -->
  <?php //endfor; ?>
  <?php endforeach; ?>
  </div>
</div>