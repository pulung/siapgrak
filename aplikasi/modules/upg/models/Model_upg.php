<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_upg extends CI_Model {

	private $table_upg;

    function __construct() {
        parent::__construct();
		$this->table_upg = "upg";
    }

	function getupg() {
        $data = array();

        $sql = "SELECT * FROM upg";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }
	
	function tambah_upg($data){
        $res = $this->db->insert('upg', $data);

        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }
	
	public function get($id) {
        $query = $this->db->get_where($this->table_upg, array('id_upg' => $id), 1, 0);
        $result = $query->result();
        return $result ? $result[0] : NULL;
    }
	
	public function delete_upg($id) {
		$res = $this->db->delete('upg', array('id_upg' => $id));
        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }
	
	function update_upg($id,$data){
        $res = $this->db->where('id_upg', $id)
                        ->update('upg', $data);

        if (!$res):
            return 'gagal';
        else:
            return 'sukses';
        endif;
    }
	
 
}
