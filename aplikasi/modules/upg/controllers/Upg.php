<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Upg extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->load->model(array('Model_upg','profil/Model_profil'));
        if (!$this->session->userdata('username')):
            redirect('login');
            return;
        endif;
        @date_default_timezone_set('Asia/Jakarta');
		
    }

    public function index() {
			
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $data['tagmenu'] = 'upg';
        $data['body'] 	 = 'upg/view_upg';
		
		$data['addcsslib'] 	 = 'css/view_css_datatables';
        $data['addjslib'] 	 = 'js/view_js_datatables';
        $data['addjslibadd'] = 'js/script_datatable';
		
		$id_user = strip_tags(trim($this->session->userdata('id')));
        $data['upg'] = $this->Model_upg->getupg();
		
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function update_upg($id = "") {
		
		$data_form = array(
                'id_upg' => '',
                'nama_upg' => strip_tags(trim($_POST['nama'])),
                'alamat' => strip_tags(trim($_POST['alamat'])),
                'no_telp' => strip_tags(trim($_POST['notelp'])),
                'fax' => strip_tags(trim($_POST['fax'])),
                'email' => strip_tags(trim($_POST['email'])),
                'longitude' => strip_tags(trim($_POST['longitude'])),
                'latitude' => strip_tags(trim($_POST['latitude']))
                
            );
        
		$data_upg = $this->security->xss_clean($data_form);
		 
		$ubah_faq = $this->Model_upg->update_upg($id,$data_upg); 
				 
		if($ubah_faq == 'sukses') {
			$this->session->set_flashdata('message', '<div><div class="alert alert-success"><i class="fa fa-check"></i> UPG berhasil diubah. </div></div>');
			redirect('upg');
		}else{
			$this->session->set_flashdata('message', '<div><div class="alert alert-danger"><i class="fa fa-times"></i> UPG gagal diubah. </div></div>');
			redirect('upg');
		} 
	}
	
	public function hapus_upg($id = ""){
        
        $hapus = $this->Model_upg->delete_upg($id);
        if ($hapus == 'sukses') {
            $this->session->set_flashdata('message', '<div><div class="alert alert-success"><i class="fa fa-check"></i> UPG berhasil dihapus.</div></div>');
            redirect('upg');
        } else {
            $this->session->set_flashdata('message', '<div><div class="alert alert-danger"><i class="fa fa-times"></i> UPG gagal dihapus.</div></div>');
            redirect('upg');
        }
    }
	
	public function tambah_upg() {
		
		$data_non = array(
                'id_upg' => '',
                'nama_upg' => strip_tags(trim($_POST['nama'])),
                'alamat' => strip_tags(trim($_POST['alamat'])),
                'no_telp' => strip_tags(trim($_POST['notelp'])),
                'fax' => strip_tags(trim($_POST['fax'])),
                'email' => strip_tags(trim($_POST['email'])),
                'longitude' => strip_tags(trim($_POST['longitude'])),
                'latitude' => strip_tags(trim($_POST['latitude']))
                
            );
        
		$data = $this->security->xss_clean($data_non);
        $insert = $this->Model_upg->tambah_upg($data); 
			
		if ($insert == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil. </strong></h4> Data berhasil disimpan.</div>');
            redirect('upg');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Gagal. </strong></h4> Data gagal disimpan.</div>');
            redirect('upg');
        }
				     
    }
	
	public function edit_upg($id = "") {
        $data['upg'] = $this->Model_upg->getupg();   
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $data['tagmenu'] = 'upg';
        $data['body'] 	 = 'upg/view_edit_upg';
				
		$data['getupg'] = $this->Model_upg->get($id);
       
        $this->load->vars($data);
		$this->load->view('view_main');
        
			
        }
	
}
