<div class="main">
  <div class="container">
  <?php //$jml = count($pengguna); ?>
  <?php //for($i = 0; $i < $jml; $i++): ?>
  <!-- BEGIN CONTENT -->
	<form method="post" id="form_faq" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= base_url()?>pengguna/update_pengguna/<?= $getpengguna->id; ?>">
	 <div class="col-md-12 col-sm-12">
	   <h1>Akun Pengguna</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<span style="font-size:12px"><font style="color:red; font-size: 7px;">*</font><i> Semua form wajib diisi</i></span><br><br>
				<label for="role">Username</label>
				<input type="text" id="username" name="username" class="form-control" value="<?= $getpengguna->username; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Password </label>
				<input type="password" id="password" name="password" class="form-control" value="<?= $getpengguna->password; ?>" style="width:1000px; height:40px" required><br>
			   
			</div>
		  </div>
		</div>
	  </div>
	  <br>
	  <h1>Data Pribadi Pengguna</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<span style="font-size:12px"><font style="color:red; font-size:7px;">*</font><i> Semua form wajib diisi</i></span><br><br>
				<label for="role">Nama</label>
				<input type="text" id="nama" name="nama" class="form-control" value="<?= $getpengguna->name; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Tempat Lahir </label>
				<input type="text" id="tmp_lahir" name="tmp_lahir" class="form-control" value="<?= $getpengguna->tempatlahir; ?>" style="width:1000px; height:40px" required><br>
			    <label for="role">Tanggal Lahir </label>
				<input type="text" id="tgl_lahir" name="tgl_lahir" class="form-control" value="<?= $getpengguna->tgllahir; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">No. KTP </label>
				<input type="text" id="no_ktp" name="no_ktp" class="form-control" value="<?= $getpengguna->no_ktp; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Email </label>
				<input type="text" id="email" name="email" class="form-control" value="<?= $getpengguna->email; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">No. HP </label>
				<input type="text" id="no_hp" name="no_hp" class="form-control" value="<?= $getpengguna->nohp; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">PIN BB </label>
				<input type="text" id="pin_bb" name="pin_bb" class="form-control" value="<?= $getpengguna->pin_bb; ?>" style="width:1000px; height:40px" required><br>
				
			</div>
		  </div>
		</div>
	  </div>
	  
	  <br>
	  <h1>Data Instansi / Kantor</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<span style="font-size:12px"><font style="color:red; font-size:7px;">*</font><i> Semua form wajib diisi</i></span><br><br>
				<label for="role">Instansi Kerja</label>
				<input type="text" id="inskerja" name="inskerja" class="form-control" value="<?= $getpengguna->inskerja; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Biro </label>
				<input type="text" id="biro" name="biro" class="form-control" value="<?= $getpengguna->biro; ?>" style="width:1000px; height:40px" required><br>
			    <label for="role">Bagian </label>
				<input type="text" id="bagian" name="bagian" class="form-control" value="<?= $getpengguna->bagian; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Jabatan </label>
				<input type="text" id="jabatan" name="jabatan" class="form-control" value="<?= $getpengguna->jabatan; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Pangkat </label>
				<input type="text" id="pangkat" name="pangkat" class="form-control" value="<?= $getpengguna->pangkat; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Golongan </label>
				<input type="text" id="golongan" name="golongan" class="form-control" value="<?= $getpengguna->golongan; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Alamat Kantor </label>
				<input type="text" id="alamat_kantor" name="alamat_kantor" class="form-control" value="<?= $getpengguna->alamat_kantor; ?>" style="width:1000px; height:100px" required><br>
				<label for="role">Kelurahan </label>
				<input type="text" id="kelurahan_kantor" name="kelurahan_kantor" class="form-control" value="<?= $getpengguna->kelurahan_kantor; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Kecamatan </label>
				<input type="text" id="kecamatan_kantor" name="kecamatan_kantor" class="form-control" value="<?= $getpengguna->kecamatan_kantor; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Kota/Kabupaten </label>
				<input type="text" id="kabkot_kantor" name="kabkot_kantor" class="form-control" value="<?= $getpengguna->kota_kantor; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Provinsi </label>
				<input type="text" id="provinsi_kantor" name="provinsi_kantor" class="form-control" value="<?= $getpengguna->provinsi_kantor; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Kode Pos</label>
				<input type="text" id="kode_pos_kantor" name="kode_pos_kantor" class="form-control" value="<?= $getpengguna->kode_pos_kantor; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Nomor Kantor </label>
				<input type="text" id="no_kantor" name="no_kantor" class="form-control" value="<?= $getpengguna->no_kantor; ?>" style="width:1000px; height:40px" required><br>
			</div>
		  </div>
		</div>
	  </div>
	  
	  <br>
	  <h1>Data Rumah</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<span style="font-size:12px"><font style="color:red; font-size:7px;">*</font><i> Semua form wajib diisi</i></span><br><br>
				<label for="role">Alamat Rumah</label>
				<input type="text" id="alamat_rumah" name="alamat_rumah" class="form-control" value="<?= $getpengguna->alamat_rumah; ?>" style="width:1000px; height:100px" required><br>
				<label for="role">Kelurahan </label>
				<input type="text" id="kelurahan_rumah" name="kelurahan_rumah" class="form-control" value="<?= $getpengguna->kelurahan_rumah; ?>" style="width:1000px; height:40px" required><br>
			    <label for="role">Kecamatan </label>
				<input type="text" id="kecamatan_rumah" name="kecamatan_rumah" class="form-control" value="<?= $getpengguna->kecamatan_rumah; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Kota/Kabupaten </label>
				<input type="text" id="kabkot_rumah" name="kabkot_rumah" class="form-control" value="<?= $getpengguna->kota_rumah; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Provinsi </label>
				<input type="text" id="provinsi_rumah" name="provinsi_rumah" class="form-control" value="<?= $getpengguna->provinsi_rumah; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Kode Pos </label>
				<input type="text" id="kode_pos_rumah" name="kode_pos_rumah" class="form-control" value="<?= $getpengguna->kode_pos_rumah; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Nomor Rumah </label>
				<input type="text" id="no_rumah" name="no_rumah" class="form-control" value="<?= $getpengguna->no_rumah; ?>" style="width:1000px; height:40px" required><br>
				<label for="role">Alamat Pengiriman </label>
				<input type="text" id="alamat_pengiriman" name="alamat_pengiriman" class="form-control" value="<?= $getpengguna->alamat_pengiriman; ?>" style="width:1000px; height:40px" required><br>
				
				<div class="col-md-2">
					<td>
						<a href="<?php echo base_url('pengguna'); ?>">
							<button type="button" class="btn btn-block btn-primary btn-lg bg-red"><i class="fa fa-times "></i>&nbsp;Kembali</button>
						</a>
					</td>
				</div>
				<div class="col-md-2">
					<td>
						
							<button type="submit" class="btn btn-block btn-success btn-lg"><i class="fa fa-save "></i>&nbsp;Simpan</button>
						
					</td>                 
				</div>
			</div>
		  </div>
		</div>
	  </div>
	  
	 </div>
	</form>
	<!-- END CONTENT -->
  <?php //endfor; ?>
  </div>
</div>