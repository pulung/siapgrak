<div class="main">
  <div class="container">
    	
	<?php //if($this->session->userdata('role') == '1': ?>
	<!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
	<form method="post" id="form_upg" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('pengguna/upload'); ?>">
	 <div class="col-md-12 col-sm-12">
	  <br><br>
	  <?= $this->session->flashdata('message'); ?>
       <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff; text-align:right;">
				<a onclick="myFunction()">
				   <span class="btn btn-xs btn-warning" style="padding:7px">
						<i class="fa fa-upload"> Import Excel</i> 
				   </span>
			    </a>
				<a href="<?= site_url()?>pengguna/download" title="Download Template">
				   <span class="btn btn-xs btn-primary" style="padding:7px">
						<i class="fa fa-download"> Download Template</i> 
				   </span>
			    </a>
				<div id="form-upload" style="margin-top:20px; display: none;">
				<form action="<?= site_url()?>pengguna/upload/" method="post" enctype="multipart/form-data">
					<input type="file" name="file"/>
					<input type="submit" value="Upload file"/>
				</form>
				</div>
			</div>
		  </div>
		</div>
	  </div>
	  
	 </div>
	</form>
	 	
	<!-- BEGIN CONTENT -->
	<form method="post" id="form_upg" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('pengguna/tambah_pengguna'); ?>">
	 <div class="col-md-12 col-sm-12">
	  <br><br>
	  
      <h1>Akun Pengguna</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<span style="font-size:12px"><font style="font-size:7px; color:red; ">*</font><i> Semua form wajib diisi</i></span><br><br>
				<label for="role">Username</label>
				<input type="text" id="username" name="username" class="form-control" placeholder="Masukkan username" style="width:1000px; height:40px" required><br>
				<label for="role">Password </label>
				<input type="password" id="password" name="password" class="form-control" placeholder="Masukkan password" style="width:1000px; height:40px" required><br>
			    <label for="role">Ulangi Password</label>
				<input type="password" id="ulang_password" name="ulang_password" class="form-control" placeholder="Ulangi password" style="width:1000px; height:40px" required><br>
				
			</div>
		  </div>
		</div>
	  </div>
	  <br>
	  <h1>Data Pribadi Pengguna</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<span style="font-size:12px"><font style="font-size:7px; color:red; ">*</font><i> Semua form wajib diisi</i></span><br><br>
				<label for="role">Nama</label>
				<input type="text" id="nama" name="nama" class="form-control" placeholder="Masukkan nama" style="width:1000px; height:40px" required><br>
				<label for="role">Tempat Lahir </label>
				<input type="text" id="tmp_lahir" name="tmp_lahir" class="form-control" placeholder="Masukkan tempat lahir" style="width:1000px; height:40px" required><br>
			    <label for="role">Tanggal Lahir </label>
				<input type="text" id="tgl_lahir" name="tgl_lahir" class="form-control" placeholder="yyyy-mm-dd" style="width:1000px; height:40px" required><br>
				<label for="role">No. KTP </label>
				<input type="text" id="no_ktp" name="no_ktp" class="form-control" placeholder="Masukkan nomor KTP" style="width:1000px; height:40px" required><br>
				<label for="role">Email </label>
				<input type="text" id="email" name="email" class="form-control" placeholder="Masukkan alamat email" style="width:1000px; height:40px" required><br>
				<label for="role">No. HP </label>
				<input type="text" id="no_hp" name="no_hp" class="form-control" placeholder="Masukkan nomor HP" style="width:1000px; height:40px" required><br>
				<label for="role">PIN BB </label>
				<input type="text" id="pin_bb" name="pin_bb" class="form-control" placeholder="Masukkan PIN BB" style="width:1000px; height:40px" required><br>
				
			</div>
		  </div>
		</div>
	  </div>
	  
	  <br>
	  <h1>Data Instansi / Kantor</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<span style="font-size:12px"><font style="font-size:7px; color:red; ">*</font><i> Semua form wajib diisi</i></span><br><br>
				<label for="role">Instansi Kerja</label>
				<input type="text" id="inskerja" name="inskerja" class="form-control" placeholder="Masukkan nama instansi kerja" style="width:1000px; height:40px" required><br>
				<label for="role">Biro </label>
				<input type="text" id="biro" name="biro" class="form-control" placeholder="Masukkan nama biro" style="width:1000px; height:40px" required><br>
			    <label for="role">Bagian </label>
				<input type="text" id="bagian" name="bagian" class="form-control" placeholder="Masukkan nama bagian" style="width:1000px; height:40px" required><br>
				<label for="role">Jabatan </label>
				<input type="text" id="jabatan" name="jabatan" class="form-control" placeholder="Masukkan jabatan" style="width:1000px; height:40px" required><br>
				<label for="role">Pangkat </label>
				<input type="text" id="pangkat" name="pangkat" class="form-control" placeholder="Masukkan pangkat" style="width:1000px; height:40px" required><br>
				<label for="role">Golongan </label>
				<input type="text" id="golongan" name="golongan" class="form-control" placeholder="Masukkan golongan" style="width:1000px; height:40px" required><br>
				<label for="role">Eselon </label>
				<input type="text" id="eselon" name="eselon" class="form-control" placeholder="Masukkan eselon" style="width:1000px; height:40px" required><br>
				<label for="role">Alamat Kantor </label>
				<textarea type="text" id="alamat_kantor" name="alamat_kantor" class="form-control" placeholder="Masukkan alamat kantor" style="width:1000px; height:40px" required></textarea><br>
				<label for="role">Kelurahan </label>
				<input type="text" id="kelurahan_kantor" name="kelurahan_kantor" class="form-control" placeholder="Masukkan nama kelurahan" style="width:1000px; height:40px" required><br>
				<label for="role">Kecamatan </label>
				<input type="text" id="kecamatan_kantor" name="kecamatan_kantor" class="form-control" placeholder="Masukkan kecamatan" style="width:1000px; height:40px" required><br>
				<label for="role">Kota/Kabupaten </label>
				<input type="text" id="kabkot_kantor" name="kabkot_kantor" class="form-control" placeholder="Masukkan kabupaten/kota" style="width:1000px; height:40px" required><br>
				<label for="role">Provinsi </label>
				<input type="text" id="provinsi_kantor" name="provinsi_kantor" class="form-control" placeholder="Masukkan provinsi" style="width:1000px; height:40px" required><br>
				<label for="role">Kode Pos </label>
				<input type="text" id="kode_pos_kantor" name="kode_pos_kantor" class="form-control" placeholder="Masukkan kode pos" style="width:1000px; height:40px" required><br>
				<label for="role">Nomor Kantor </label>
				<input type="text" id="no_kantor" name="no_kantor" class="form-control" placeholder="Masukkan nomor kantor" style="width:1000px; height:40px" required><br>
			</div>
		  </div>
		</div>
	  </div>
	  
	  <br>
	  <h1>Data Rumah</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<span style="font-size:12px"><font style="font-size:7px; color:red; ">*</font><i> Semua form wajib diisi</i></span><br><br>
				<label for="role">Alamat Rumah</label>
				<textarea type="text" id="alamat_rumah" name="alamat_rumah" class="form-control" placeholder="Masukkan alamat rumah" style="width:1000px; height:40px" required></textarea><br>
				<label for="role">Kelurahan </label>
				<input type="text" id="kelurahan_rumah" name="kelurahan_rumah" class="form-control" placeholder="Masukkan nama kelurahan" style="width:1000px; height:40px" required><br>
			    <label for="role">Kecamatan </label>
				<input type="text" id="kecamatan_rumah" name="kecamatan_rumah" class="form-control" placeholder="Masukkan nama kecamatan" style="width:1000px; height:40px" required><br>
				<label for="role">Kota/Kabupaten </label>
				<input type="text" id="kabkot_rumah" name="kabkot_rumah" class="form-control" placeholder="Masukkan kabupaten/kota" style="width:1000px; height:40px" required><br>
				<label for="role">Provinsi </label>
				<input type="text" id="provinsi_rumah" name="provinsi_rumah" class="form-control" placeholder="Masukkan provinsi" style="width:1000px; height:40px" required><br>
				<label for="role">Kode Pos </label>
				<input type="text" id="kode_pos_rumah" name="kode_pos_rumah" class="form-control" placeholder="Masukkan kode pos rumah" style="width:1000px; height:40px" required><br>
				<label for="role">Nomor Rumah </label>
				<input type="text" id="no_rumah" name="no_rumah" class="form-control" placeholder="Masukkan nomor rumah" style="width:1000px; height:40px" required><br>
				<label for="role">Alamat Pengiriman </label>
				<input type="text" id="alamat_pengiriman" name="alamat_pengiriman" class="form-control" placeholder="Masukkan alamat pengiriman" style="width:1000px; height:40px" required><br>
				
				
				
				
				
				
				<input type="submit" id="btnKirimUPG" class="btn btn-success" value="Tambah User">
			</div>
		  </div>
		</div>
	  </div>
	  
	 </div>
	</form>
	<!-- END CONTENT -->
	
	<!-- BEGIN CONTENT -->
	<form method="post" id="form_pengguna" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('pengguna/tambah_pengguna'); ?>">
	 <div class="col-md-12 col-sm-12">
	  <br><br>
      <h1>Daftar Pengguna</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<table id="tabel-data" name="tabel-data" class="table table-striped table-bordered" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Nama Pengguna</th>
							<th>Alamat</th>
							<th>No. Telepon</th>
							<th>No. Fax</th>
							<th>Email</th>
							<th>Aksi</th>
						</tr>
					</thead>
					
					<tbody>
					<?php //if(($row['jawaban']) != 'NULL'){ ?>
					<?php foreach ($pengguna as $row): ?>
                          
						<tr>
						
							<td><?php cetak($row['name']); ?></td>
							<td><?php cetak($row['alamat_rumah']); ?></td>
							<td><?php cetak($row['nohp']); ?></td>
							<td><?php cetak($row['no_ktp']); ?></td>
							<td><?php cetak($row['email']); ?></td>
							
							<td>
								    
								  <a href="<?= site_url()?>pengguna/edit_pengguna/<?php cetak($row['id']); ?>" title="Edit">
									  <span class="btn btn-xs btn-primary">
											<i class="fa fa-pencil"></i> 
									  </span>
								  </a>
								  <a href="<?= site_url()?>pengguna/hapus_pengguna/<?php cetak($row['id']); ?>" onclick="return window.confirm('Apakah Anda yakin ingin menghapus data tersebut?');"  title="Hapus">
									   <span class="btn btn-xs btn-danger">
											<i class="fa fa-remove"></i> 
									   </span>
								  </a>
								  
							</td>
						</tr>
						
					<?php endforeach; ?>
					<?php //} ?>						   
					</tbody>
				</table>
				
			</div>
		  </div>
		</div>
	  </div>
	 </div>
	</form>
	<!-- END CONTENT -->
	
  </div>
</div>