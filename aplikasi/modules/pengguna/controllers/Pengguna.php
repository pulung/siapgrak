<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->load->helper(array('url','download'));	
		$this->load->model(array('Model_pengguna','profil/Model_profil'));
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        if (!$this->session->userdata('username')):
            redirect('login');
            return;
        endif;
        @date_default_timezone_set('Asia/Jakarta');
		
    }

    public function index() {
			
        //$this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $data['tagmenu'] = 'pengguna';
        $data['body'] 	 = 'pengguna/view_pengguna';
		
		$data['addcsslib'] 	 = 'css/view_css_datatables';
        $data['addjslib'] 	 = 'js/view_js_datatables';
        $data['addjslibadd'] = 'js/script_datatable';
		
		$id_user = strip_tags(trim($this->session->userdata('id')));
        $data['pengguna'] = $this->Model_pengguna->getpengguna();
		
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function update_pengguna($id = "") {
		//var_dump($id); exit;
		$data_form = array(
                'id' => '',
                'username' => strip_tags(trim($_POST['nama'])),
                'password' => strip_tags(trim($_POST['password'])),
                'name' => strip_tags(trim($_POST['nama'])),
                'tempatlahir' => strip_tags(trim($_POST['tmp_lahir'])),
                'tgllahir' => strip_tags(trim($_POST['tgl_lahir'])),
                'no_ktp' => strip_tags(trim($_POST['no_ktp'])),
                'email' => strip_tags(trim($_POST['email'])),
                'nohp' => strip_tags(trim($_POST['no_hp'])),
                'pin_bb' => strip_tags(trim($_POST['pin_bb'])),
                'inskerja' => strip_tags(trim($_POST['inskerja'])),
                'biro' => strip_tags(trim($_POST['biro'])),
                'bagian' => strip_tags(trim($_POST['bagian'])),
                'jabatan' => strip_tags(trim($_POST['jabatan'])),
                'pangkat' => strip_tags(trim($_POST['pangkat'])),
                'golongan' => strip_tags(trim($_POST['golongan'])),
                'alamat_kantor' => strip_tags(trim($_POST['alamat_kantor'])),
                'kelurahan_kantor' => strip_tags(trim($_POST['kelurahan_kantor'])),
                'kecamatan_kantor' => strip_tags(trim($_POST['kecamatan_kantor'])),
                'kota_kantor' => strip_tags(trim($_POST['kabkot_kantor'])),
                'provinsi_kantor' => strip_tags(trim($_POST['provinsi_kantor'])),
                'kode_pos_kantor' => strip_tags(trim($_POST['kode_pos_kantor'])),
                'no_kantor' => strip_tags(trim($_POST['no_kantor'])),
				'alamat_rumah' => strip_tags(trim($_POST['alamat_rumah'])),
                'kelurahan_rumah' => strip_tags(trim($_POST['kelurahan_rumah'])),
                'kecamatan_rumah' => strip_tags(trim($_POST['kecamatan_rumah'])),
                'kota_rumah' => strip_tags(trim($_POST['kabkot_rumah'])),
                'provinsi_rumah' => strip_tags(trim($_POST['provinsi_rumah'])),
                'kode_pos_rumah' => strip_tags(trim($_POST['kode_pos_rumah'])),
                'no_rumah' => strip_tags(trim($_POST['no_rumah'])),
                'alamat_pengiriman' => strip_tags(trim($_POST['alamat_pengiriman'])),
                'eselon' => strip_tags(trim($_POST['eselon']))
                
            );
        
		$data_pengguna = $this->security->xss_clean($data_form);
		 
		$ubah_pengguna = $this->Model_pengguna->update_pengguna($id,$data_pengguna); 
		
		 
		if($ubah_pengguna == 'sukses') {
			$this->session->set_flashdata('message', '<div><div class="alert alert-success"><i class="fa fa-check"></i> Data pengguna berhasil diubah. </div></div>');
			redirect('pengguna');
		}else{
			$this->session->set_flashdata('message', '<div><div class="alert alert-danger"><i class="fa fa-times"></i> Data pengguna gagal diubah. </div></div>');
			redirect('pengguna');
		} 
	}
	
	public function hapus_pengguna($id = ""){
        
        $hapus = $this->Model_pengguna->delete_pengguna($id);
        if ($hapus == 'sukses') {
            $this->session->set_flashdata('message', '<div><div class="alert alert-success"><i class="fa fa-check"></i> Pengguna berhasil dihapus.</div></div>');
            redirect('pengguna');
        } else {
            $this->session->set_flashdata('message', '<div><div class="alert alert-danger"><i class="fa fa-times"></i> Pengguna gagal dihapus.</div></div>');
            redirect('pengguna');
        }
    }
	
	public function tambah_pengguna() {
		
		$id_pengguna_terakhir = $this->Model_pengguna->get_id_terakhir();
        $id_pengguna = $id_pengguna_terakhir + 1;
		
		$data_non = array(
                'id' => $id_pengguna,
                'username' => strip_tags(trim($_POST['nama'])),
                'password' => strip_tags(trim($_POST['password'])),
                'name' => strip_tags(trim($_POST['nama'])),
                'tempatlahir' => strip_tags(trim($_POST['tmp_lahir'])),
				'tgllahir' => strip_tags(trim(date('Y-M-d', strtotime($_POST['tgl_lahir'])))),
                //'tgllahir' => strip_tags(trim($_POST['tgl_lahir'])),
                'no_ktp' => strip_tags(trim($_POST['no_ktp'])),
                'email' => strip_tags(trim($_POST['email'])),
                'nohp' => strip_tags(trim($_POST['no_hp'])),
                'pin_bb' => strip_tags(trim($_POST['pin_bb'])),
                'inskerja' => strip_tags(trim($_POST['inskerja'])),
                'biro' => strip_tags(trim($_POST['biro'])),
                'bagian' => strip_tags(trim($_POST['bagian'])),
                'jabatan' => strip_tags(trim($_POST['jabatan'])),
                'pangkat' => strip_tags(trim($_POST['pangkat'])),
                'golongan' => strip_tags(trim($_POST['golongan'])),
                'alamat_kantor' => strip_tags(trim($_POST['alamat_kantor'])),
                'kelurahan_kantor' => strip_tags(trim($_POST['kelurahan_kantor'])),
                'kecamatan_kantor' => strip_tags(trim($_POST['kecamatan_kantor'])),
                'kota_kantor' => strip_tags(trim($_POST['kabkot_kantor'])),
                'provinsi_kantor' => strip_tags(trim($_POST['provinsi_kantor'])),
                'kode_pos_kantor' => strip_tags(trim($_POST['kode_pos_kantor'])),
                'no_kantor' => strip_tags(trim($_POST['no_kantor'])),
				'alamat_rumah' => strip_tags(trim($_POST['alamat_rumah'])),
                'kelurahan_rumah' => strip_tags(trim($_POST['kelurahan_rumah'])),
                'kecamatan_rumah' => strip_tags(trim($_POST['kecamatan_rumah'])),
                'kota_rumah' => strip_tags(trim($_POST['kabkot_rumah'])),
                'provinsi_rumah' => strip_tags(trim($_POST['provinsi_rumah'])),
                'kode_pos_rumah' => strip_tags(trim($_POST['kode_pos_rumah'])),
                'no_rumah' => strip_tags(trim($_POST['no_rumah'])),
                'alamat_pengiriman' => strip_tags(trim($_POST['alamat_pengiriman'])),
                'eselon' => strip_tags(trim($_POST['eselon']))
                
            );
        
		$data = $this->security->xss_clean($data_non);
        $insert = $this->Model_pengguna->tambah_pengguna($data); 
			
		if ($insert == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil. </strong></h4> Data berhasil disimpan.</div>');
            redirect('pengguna');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Gagal. </strong></h4> Data gagal disimpan.</div>');
            redirect('pengguna');
        }
				     
    }
	
	public function edit_pengguna($id = "") {
        $data['pengguna'] = $this->Model_pengguna->getpengguna();   
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $data['tagmenu'] = 'pengguna';
        $data['body'] 	 = 'pengguna/view_edit_pengguna';
				
		$data['getpengguna'] = $this->Model_pengguna->get($id);
       
        $this->load->vars($data);
		$this->load->view('view_main');
        
			
   }
   
   public function download(){				
		//force_download('assets/template_tambah_user.xls',NULL);
		force_download('assets/template_data_user.xls',NULL);
	}
	
   public function upload(){
	   
        $fileName = time().$_FILES['file']['name'];
        $config['upload_path'] = getcwd().'/assets/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
		        
        $this->load->library('upload');
        $this->upload->initialize($config);
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
        //$inputFileName = getcwd().'/assets/'.$media['file_name'];
        $inputFileName = $config['upload_path'].$fileName;
		//$inputFileName = './assets/'.$config['file_name'];
         //var_dump($inputFileName); exit;
        try {
                //$inputFileType = IOFactory::identify($config['upload_path'].$inputFileName);
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                //$objPHPExcel = $objReader->load($config['upload_path'].$inputFileName);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            var_dump($highestRow); var_dump($highestColumn); exit;
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                //var_dump($rowData); exit;                                 
                //Sesuaikan sama nama kolom tabel di database                                
                 $data = array(
                    "id"=> $rowData[0][0],
                    "username"=> $rowData[0][1],
                    "name"=> $rowData[0][2],
                    "password"=> '$2a$08$5BjQq7XyYe2GRaRToU9Rd.pXrqiztPgd4EbQav.cIivfBR5ThYHhu',
                    "tempatlahir"=> $rowData[0][3],
                    "tgllahir"=> $rowData[0][4],
                    "no_ktp"=> $rowData[0][5],
                    "email"=> $rowData[0][6],
                    "nohp"=> $rowData[0][7],
                    "pin_bb"=> $rowData[0][8],
                    "inskerja"=> $rowData[0][9],
                    "biro"=> $rowData[0][10],
                    "bagian"=> $rowData[0][11],
                    "jabatan"=> $rowData[0][12],
                    "pangkat"=> $rowData[0][13],
                    "golongan"=> $rowData[0][14],
                    "alamat_kantor"=> $rowData[0][15],
                    "kelurahan_kantor"=> $rowData[0][16],
                    "kecamatan_kantor"=> $rowData[0][17],
                    "kota_kantor"=> $rowData[0][18],
                    "provinsi_kantor"=> $rowData[0][19],
                    "kode_pos_kantor"=> $rowData[0][20],
                    "no_kantor"=> $rowData[0][21],
					"alamat_rumah"=> $rowData[0][22],
                    "kelurahan_rumah"=> $rowData[0][23],
                    "kecamatan_rumah"=> $rowData[0][24],
                    "kota_rumah"=> $rowData[0][25],
                    "provinsi_rumah"=> $rowData[0][26],
                    "kode_pos_rumah"=> $rowData[0][27],
                    "no_rumah"=> $rowData[0][28],
                    "alamat_pengiriman"=> $rowData[0][29],
                    "isactive"=> '1',
                    "isldap"=> '0',
                    "id_role"=> '1',
					"eselon" => $rowData[0][30]
                );
                // var_dump($data); exit;
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("conf_user_2",$data);
				
				if($insert == 'true') {
					$this->session->set_flashdata('message', '<div><div class="alert alert-success"><i class="fa fa-check"></i> Import data berhasil. </div></div>');
					redirect('pengguna');
				}else{
					$this->session->set_flashdata('message', '<div><div class="alert alert-danger"><i class="fa fa-times"></i> Import data gagal. </div></div>');
					redirect('pengguna');
				} 
                delete_files($media['file_path']);
				//delete_files($config['file_path'],TRUE);
                     
            }
        
    }
	
}
