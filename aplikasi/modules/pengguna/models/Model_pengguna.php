<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_pengguna extends CI_Model {

	private $table_pengguna;

    function __construct() {
        parent::__construct();
		$this->table_pengguna = "conf_user";
    }

	function getpengguna() {
        $data = array();

        $sql = "SELECT * FROM conf_user";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }
	
	function tambah_pengguna($data){
        $res = $this->db->insert('conf_user', $data);

        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }
	
	public function get($id) {
        $query = $this->db->get_where($this->table_pengguna, array('id' => $id), 1, 0);
        $result = $query->result();
        return $result ? $result[0] : NULL;
    }
	
	public function delete_pengguna($id) {
		$res = $this->db->delete('conf_user', array('id' => $id));
        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }
	
	function update_pengguna($id,$data){
        $res = $this->db->where('id', $id)
                        ->update('conf_user', $data);

        if (!$res):
            return 'gagal';
        else:
            return 'sukses';
        endif;
    }
	
	public function get_id_terakhir(){
        $data = array();
        $sql = "SELECT * FROM conf_user ORDER BY id DESC";
        $q = $this->db->query($sql);
        
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        } else{
            $data['id'] = 0;
        }
        $q->free_result();
        //return $data['id'];
        return $q->num_rows();
    }
 
}
