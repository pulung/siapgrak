<div class="main">
  <div class="container">
  <?php foreach ($faq as $row): ?>
  <!-- BEGIN CONTENT -->
	<form method="post" id="form_faq" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= base_url()?>bantuan/update_pertanyaan/<?php cetak($row['id']); ?>">
	 <div class="col-md-12 col-sm-12">
	  <br><br>
      <h1>Edit Pertanyaan</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<span style="font-size:12px"><font style="color:red; ">*</font><i> Wajib diisi</i></span><br><br>
				<label for="role">Nama <font style="color:red">*</font></label>
				<input type="text" id="nama" name="nama" class="form-control" value="<?= $tanya->nama; ?>" style="width:1060px; height:40px" required><br>
				<label for="role">Email <font style="color:red">*</font></label>
				<input type="text" id="email" name="email" class="form-control" value="<?= $tanya->email; ?>" style="width:1060px; height:40px" required><br>
				
				<label for="role">Pilih Kategori Pertanyaan <font style="color:red">*</font></label>
				<select class="form-control select2" name="kategori" onchange="myFunctionAgenda()" id="mySelectAgenda" required>
					
					<option value="" disabled selected hidden>-- Pilih Kategori --</option>
                    <option value='1' <?php echo $tanya->kategori == 1 ? ' selected="selected"' : ''; ?>> Sistem</option>
                    <option value='2' <?php echo $tanya->kategori == 2 ? ' selected="selected"' : ''; ?>> Prosedur</option>
                                    
					<?php //foreach ($agenda as $jb) { ?>
						<!-- dibawah -->
						<!--option value="<?php echo $agenda->id_agenda; ?>"><?php echo $agenda->nama_agenda . ' - (PIC Agenda' .' - '. $agenda->penanggung_jawab_agenda.')'; ?></option-->
						<!--option value="<?php echo $jb->id_agenda; ?>"<?= $idagenda == $jb->id_agenda ? 'selected' : ''; ?> ><?php echo $jb->nama_agenda. ' - (PIC Agenda' .' - '. $jb->penanggung_jawab_agenda.')'; ?></option-->
						
					<?php //} ?>
				</select><br>
				<label for="role">Pertanyaan <font style="color:red">*</font></label>
				<input type="text" id="pertanyaan" name="pertanyaan" class="form-control" value="<?= $tanya->pertanyaan; ?>" style="width:1060px; height:40px" required><br>
				<?php if($this->session->userdata('role') == '2'): ?>
				<label for="role">Jawaban <font style="color:red">*</font></label>
				<input type="text" id="jawaban" name="jawaban" class="form-control" value="<?= $tanya->jawaban; ?>" style="width:1060px; height:100px" required></input><br><br>
				<label for="role">Publik</label>
				<select class="form-control select2" name="publik" onchange="myFunctionAgenda()" id="mySelectPublik">
					
					<option value="" disabled selected hidden>-- Pilih --</option>
                    <option value='1' <?php echo $tanya->publik == 1 ? ' selected="selected"' : ''; ?>> Publik</option>
                    <option value='2' <?php echo $tanya->publik == 2 ? ' selected="selected"' : ''; ?>> Tidak</option>
                                    
					<?php //foreach ($agenda as $jb) { ?>
						<!-- dibawah -->
						<!--option value="<?php echo $agenda->id_agenda; ?>"><?php echo $agenda->nama_agenda . ' - (PIC Agenda' .' - '. $agenda->penanggung_jawab_agenda.')'; ?></option-->
						<!--option value="<?php echo $jb->id_agenda; ?>"<?= $idagenda == $jb->id_agenda ? 'selected' : ''; ?> ><?php echo $jb->nama_agenda. ' - (PIC Agenda' .' - '. $jb->penanggung_jawab_agenda.')'; ?></option-->
						
					<?php //} ?>
				</select><br>
				<?php endif; ?>
				<div class="col-md-2">
					<td>
						<a href="<?php echo base_url('bantuan'); ?>">
							<button type="button" class="btn btn-block btn-primary btn-lg bg-red" style="width:150px;"><i class="fa fa-times "></i>&nbsp;Kembali</button>
						</a>
					</td>
				</div>
				
				<div class="col-md-2" style="margin-left:10px;">
					<td>
						
							<button type="submit" class="btn btn-block btn-success btn-lg" style="width:150px;"><i class="fa fa-save "></i>&nbsp;Simpan</button>
						
					</td>                 
				</div>
			</div>
		  </div>
		</div>
	  </div>
	 </div>
	</form>
	<!-- END CONTENT -->
  <?php endforeach; ?>
  </div>
</div>