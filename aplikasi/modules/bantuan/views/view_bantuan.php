<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12">
	  <?= $this->session->flashdata('message'); ?>
      <h1>Petunjuk Penggunaan Aplikasi</h1>
      <div class="content-page">
        <div class="row">
		<?php if($this->session->userdata('role') == '1'): ?>
			<div class="col-md-6">
				<iframe src="assets/usermanualsiapgrak-pegawai.pdf" width="500" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		<?php elseif($this->session->userdata('role') == '2' || $this->session->userdata('role') == '4'): ?>
			<div class="col-md-6">
				<iframe src="assets/usermanualsiapgrak-menteriinspektorat.pdf" width="500" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		<?php elseif($this->session->userdata('role') == '5' || $this->session->userdata('role') == '7'): ?>
			<div class="col-md-6">
				<iframe src="assets/usermanualsiapgrak-menteriinspektorat.pdf" width="500" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<div class="col-md-6">
				<iframe src="assets/usermanualsiapgrak-pegawai.pdf" width="500" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		
		<?php endif;?>
			<!--<iframe src="assets/usermanual.pdf" width="1070" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
			<iframe src="assets/usermanualsiapgrak-pegawai.pdf" width="500" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>-->
			
		</div>
      </div>
    </div>
    <!-- END CONTENT -->
	
	<?php //if($this->session->userdata('role') == '1': ?>
	<!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
	  <br><br>
      <h1>Frequently Asked Questions</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
              
               <div class="panel-group" id="accordion1">
                
				<?php foreach ($faq as $row): ?>  
					<?php if(($row['publik']) == 1){?>
					  <div class="panel panel-default">
						 <div class="panel-heading">
							<h4 class="panel-title">
							   <a href="#accordion1_1" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" style="font-size: 20px;">
									<?php cetak($row['pertanyaan']); ?>
							   </a>
							</h4>
						 </div>
						 <div class="panel-collapse collapse in" id="accordion1_1" style="font-size: 17px;">
							<div class="panel-body">
									<?php cetak($row['jawaban']); ?>
							</div>
						 </div>
					  </div>
					  <?php } else {}?>
                  <?php endforeach; ?>
				
               </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
     <!-- END CONTENT -->
	 <?php //endif; ?>
	
	
	<!-- BEGIN CONTENT -->
	<form method="post" id="form_faq" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('bantuan/tambah_pertanyaan'); ?>">
	 <div class="col-md-12 col-sm-12">
	  <br><br>
      <h1>Tambah Pertanyaan</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<span style="font-size:12px"><font style="color:red; ">*</font><i> Wajib diisi</i></span><br><br>
				<label for="role">Nama <font style="color:red">*</font></label>
				<input type="text" id="nama" name="nama" class="form-control" placeholder="Masukkan nama" style="width:1060px; height:40px" required><br>
				<label for="role">Email </label>
				<input type="text" id="email" name="email" class="form-control" placeholder="Masukkan alamat email" style="width:1060px; height:40px" ><br>
				<label for="role">Pertanyaan <font style="color:red">*</font></label>
				<input type="text" id="pertanyaan" name="pertanyaan" class="form-control" placeholder="Masukkan pertanyaan" style="width:1060px; height:40px" required><br>
				<?php if($this->session->userdata('role') == '2'): ?>
				<label for="role">Pilih Kategori Pertanyaan </label>
				<select class="form-control select2" name="kategori" onchange="myFunctionAgenda()" id="mySelectAgenda" >
					<option value="" disabled selected hidden>Pilih kategori pertanyaan</option>
					<option value="1">Sistem</option>
					<option value="2">Prosedur</option>
					<?php //foreach ($agenda as $jb) { ?>
						<!-- dibawah -->
						<!--option value="<?php echo $agenda->id_agenda; ?>"><?php echo $agenda->nama_agenda . ' - (PIC Agenda' .' - '. $agenda->penanggung_jawab_agenda.')'; ?></option-->
						<!--option value="<?php echo $jb->id_agenda; ?>"<?= $idagenda == $jb->id_agenda ? 'selected' : ''; ?> ><?php echo $jb->nama_agenda. ' - (PIC Agenda' .' - '. $jb->penanggung_jawab_agenda.')'; ?></option-->
						
					<?php //} ?>
				</select><br>
				
				<label for="role">Jawaban </label>
				<textarea type="text" id="jawaban" name="jawaban" class="form-control" placeholder="Masukkan jawaban" style="width:1060px; height:100px" ></textarea><br>
				<label for="role">Publik</label>
				<select class="form-control select2" name="publik" onchange="myFunctionAgenda()" id="mySelectPublik">
					<option value="" disabled selected hidden>Pilih</option>
					<option value="1">Publik</option>
					<option value="2">Tidak</option>
					<?php //foreach ($agenda as $jb) { ?>
						<!-- dibawah -->
						<!--option value="<?php echo $agenda->id_agenda; ?>"><?php echo $agenda->nama_agenda . ' - (PIC Agenda' .' - '. $agenda->penanggung_jawab_agenda.')'; ?></option-->
						<!--option value="<?php echo $jb->id_agenda; ?>"<?= $idagenda == $jb->id_agenda ? 'selected' : ''; ?> ><?php echo $jb->nama_agenda. ' - (PIC Agenda' .' - '. $jb->penanggung_jawab_agenda.')'; ?></option-->
						
					<?php //} ?>
				</select>
				<?php endif; ?>
				<br><input type="submit" id="btnKirimPertanyaan" class="btn btn-success" placeholder="Kirim Pertanyaan">
			</div>
		  </div>
		</div>
	  </div>
	 </div>
	</form>
	<!-- END CONTENT -->
	
	<?php //if($this->session->userdata('role') == '5': ?>
	<!-- BEGIN CONTENT -->
	<form method="post" id="form_faq" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('bantuan/tambah_pertanyaan'); ?>">
	 <div class="col-md-12 col-sm-12">
	  <br><br>
      <h1>Pertanyaan & Jawaban</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<table id="tabel-data" name="tabel-data" class="table table-striped table-bordered" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Nama</th>
							<th>Email</th>
							<th>Pertanyaan</th>
							<th>Jawaban</th>
							<th>Kategori</th>
							<th>Publik</th>
							<th>Aksi</th>
						</tr>
					</thead>
					
					<tbody>
					<?php if(($row['jawaban']) != 'NULL'){ ?>
					<?php foreach ($faq as $row): ?>
                          
						<tr>
						
							<td><?php cetak($row['nama']); ?></td>
							<td><?php cetak($row['email']); ?></td>
							<td><?php cetak($row['pertanyaan']); ?></td>
							<td><?php cetak($row['jawaban']); ?></td>
							<td><?php 
									if(($row['kategori']) == 1){
										echo "Sistem";
									}elseif(($row['kategori']) == 2){
										echo "Prosedur";
									}else{
										
									}; 
								?>
							</td>
							<td><?php 
									if(($row['publik']) == 1){
										echo "Publik";
									}elseif(($row['publik']) == 2){
										echo "Tidak";
									}else{
										
									}; 
								?>
							</td>
							<td>
								    
								  <a href="<?= site_url()?>bantuan/edit_pertanyaan/<?php cetak($row['id']); ?>" title="Edit">
									  <span class="btn btn-xs btn-primary">
											<i class="fa fa-pencil"></i> 
									  </span>
								  </a>
								  <a href="<?= site_url()?>bantuan/hapus_pertanyaan/<?php cetak($row['id']); ?>" onclick="return window.confirm('Apakah Anda yakin ingin menghapus pertanyaan <?php cetak('"'.$row['pertanyaan'].'"'); ?> tersebut?');"  title="Hapus">
									   <span class="btn btn-xs btn-danger">
											<i class="fa fa-remove"></i> 
									   </span>
								  </a>
								  
							</td>
						</tr>
						
					<?php endforeach; ?>
					<?php } ?>						   
					</tbody>
				</table>
				
			</div>
		  </div>
		</div>
	  </div>
	 </div>
	</form>
	<!-- END CONTENT -->
	<?php //endif; ?>
	
  </div>
</div>