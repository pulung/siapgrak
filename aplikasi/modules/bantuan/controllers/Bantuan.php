<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bantuan extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->load->model(array('Model_bantuan','profil/Model_profil'));
        if (!$this->session->userdata('username')):
            redirect('login');
            return;
        endif;
        @date_default_timezone_set('Asia/Jakarta');
		
    }

    public function index() {
			
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $data['tagmenu'] = 'bantuan';
        $data['body'] 	 = 'bantuan/view_bantuan';
		
		$data['addcsslib'] 	 = 'css/view_css_datatables';
        $data['addjslib'] 	 = 'js/view_js_datatables';
        $data['addjslibadd'] = 'js/script_datatable';
		
		$id_user = strip_tags(trim($this->session->userdata('id')));
        $data['faq'] = $this->Model_bantuan->getpertanyaan();
		$role = $this->session->userdata('role');
		
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function update_pertanyaan($id = "") {
		
		$data_tanya = array(
                'id' => $id,
                'nama' => strip_tags(trim($_POST['nama'])),
                'email' => strip_tags(trim($_POST['email'])),
                'pertanyaan' => strip_tags(trim($_POST['pertanyaan'])),
                'jawaban' => strip_tags(trim($_POST['jawaban'])),
                'kategori' => strip_tags(trim($_POST['kategori'])),
                'publik' => strip_tags(trim($_POST['publik']))
                
            );
        
		$data = $this->security->xss_clean($data_tanya);
        		 
		$ubah_faq = $this->Model_bantuan->update_pertanyaan($id,$data); 
		
		 
		if($ubah_faq == 'sukses') {
			$this->session->set_flashdata('message', '<div><div class="alert alert-success"><i class="fa fa-check"></i> Pertanyaan berhasil diubah. </div></div>');
			redirect('bantuan');
		}else{
			$this->session->set_flashdata('message', '<div><div class="alert alert-danger"><i class="fa fa-times"></i> Pertanyaan gagal diubah. </div></div>');
			redirect('bantuan');
		} 
	}
	
	public function hapus_pertanyaan($id = ""){
        
        $hapus = $this->Model_bantuan->delete_pertanyaan($id);
        if ($hapus == 'sukses') {
            $this->session->set_flashdata('message', '<div><div class="alert alert-success"><i class="fa fa-check"></i> Pertanyaan berhasil dihapus.</div></div>');
            redirect('bantuan');
        } else {
            $this->session->set_flashdata('message', '<div><div class="alert alert-danger"><i class="fa fa-times"></i> Pertanyaan gagal dihapus.</div></div>');
            redirect('bantuan');
        }
    }
	
	public function tambah_pertanyaan() {
		
		$data_non = array(
                'id' => '',
                'nama' => strip_tags(trim($_POST['nama'])),
                'email' => strip_tags(trim($_POST['email'])),
                'pertanyaan' => strip_tags(trim($_POST['pertanyaan'])),
                'jawaban' => strip_tags(trim($_POST['jawaban'])),
                'kategori' => strip_tags(trim($_POST['kategori'])),
                'publik' => strip_tags(trim($_POST['publik']))
                
            );
        
		$data = $this->security->xss_clean($data_non);
        $insert = $this->Model_bantuan->tambah_faq($data); 
			
		if ($insert == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil. </strong></h4> Data berhasil disimpan.</div>');
            redirect('bantuan');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Gagal. </strong></h4> Data gagal disimpan.</div>');
            redirect('bantuan');
        }
				     
    }
	
	public function edit_pertanyaan($id = "") {
        $data['faq'] = $this->Model_bantuan->getpertanyaan();   
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $data['tagmenu'] = 'bantuan';
        $data['body'] 	 = 'bantuan/view_edit_pertanyaan';
				
		$data['tanya'] = $this->Model_bantuan->get($id);
       
        $this->load->vars($data);
		$this->load->view('view_main');
        
			
        }
	
}
