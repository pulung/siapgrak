<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gratifikasi extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('Model_gratifikasi','profil/Model_profil'));
        if (!$this->session->userdata('username')):
            redirect('login');
            return;
        endif;
        @date_default_timezone_set('Asia/Jakarta');
        $this->load->config('path_upload');
    }

    public function form_gratifikasi() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $data['status_penerimaan'] = strip_tags(trim($this->uri->segment(3)));
        $id_user = strip_tags(trim($this->session->userdata('id', TRUE)));
        $data['show_profil'] = $this->Model_profil->show_profil($id_user)->row();
        $data['provinsi'] = $this->Model_profil->getprovinsi();
        $data['user'] = $this->Model_gratifikasi->getuser($id_user);
        $data['identitas'] = $this->Model_gratifikasi->getidentitas();
        $data['jenis_penerimaan'] = $this->Model_gratifikasi->getjenis_penerimaan();
        $data['peristiwa_penerimaan'] = $this->Model_gratifikasi->getperistiwa_penerimaan();
        $data['alamat_pengirim'] = $this->Model_gratifikasi->getalamat_pengirim();
		$data['mata_uang'] = $this->Model_gratifikasi->getmata_uang();
        $data['tanggal'] = date('Y-m-d');
        $data['tagmenu'] = 'form_gratifikasi';
        $data['body'] = 'gratifikasi/view_form_gratifikasi';
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function laporan_tahunan() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $data['status_penerimaan'] = strip_tags(trim($this->uri->segment(3)));
        $id_user = strip_tags(trim($this->session->userdata('id', TRUE)));
        $data['show_profil'] = $this->Model_profil->show_profil($id_user)->row();
        $data['provinsi'] = $this->Model_profil->getprovinsi();
        $data['user'] = $this->Model_gratifikasi->getuser($id_user);
        $data['identitas'] = $this->Model_gratifikasi->getidentitas();
        $data['jenis_penerimaan'] = $this->Model_gratifikasi->getjenis_penerimaan();
        $data['peristiwa_penerimaan'] = $this->Model_gratifikasi->getperistiwa_penerimaan();
        $data['alamat_pengirim'] = $this->Model_gratifikasi->getalamat_pengirim();
		
		$data['show_riwayat'] = $this->Model_gratifikasi->laporan_tahunan()->result();
		$data['show_riwayat_pegawai'] = $this->Model_gratifikasi->laporan_tahunan_pegawai($id_user)->result();
		
        $data['tanggal'] = date('Y-m-d');
        $data['tagmenu'] = 'laporan_tahunan';
        $data['body'] = 'gratifikasi/view_form_laporan_tahunan';
        $this->load->vars($data);
        $this->load->view('view_main');
    }

    public function get_data_user(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $id = strip_tags(trim($this->input->post('id', TRUE))); 
        $data = array();
        if($id == ""){
            $data = array(
                '0' => array(
                    'name' => "",
                    'tempatlahir' => "",
                    'tgllahir' => "",
                    'no_ktp' => "",
                    'jabatan' => "",
                    'email' => "",
                    'nohp' => "",
                    'pin_bb' => "",
                    'inskerja' => "",
                    'biro' => "",
                    'bagian' => "",
                    'golongan' => "",
                    'pangkat' => "",
                    'alamat_kantor' => "",
                    'provinsi_kantor' => "",
                    'kota_kantor' => "",
                    'kecamatan_kantor' => "",
                    'kelurahan_kantor' => "",
                    'kode_pos_kantor' => "",
                    'no_kantor' => "",
                    'alamat_rumah' => "",
                    'provinsi_rumah' => "",
                    'kota_rumah' => "",
                    'kecamatan_rumah' => "",
                    'kelurahan_rumah' => "",
                    'kode_pos_rumah' => "",
                    'no_rumah' => "",
                )
            );
        }else{
            $data = $this->Model_gratifikasi->show_penerima($id);
        }                
        echo json_encode($data);
    }

    public function insert() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $now = date('Y-m-d H:i:s');
        $id_gratifikasi_terakhir = $this->Model_gratifikasi->get_id_terakhir();
        $id_gratifikasi = $id_gratifikasi_terakhir + 1;
        $b = $this->common->filter(strip_tags(trim($_POST['nilai_nominal'])));
        $b = str_replace('Rp.', '', $b);
        $b = str_replace('.', '', $b);
        $nilai_nominal = str_replace('Rp.', '', $b);
        if (isset($_POST['send'])) {
            $jenis_dokumen = 'send';
            $status_dokumen = '2';
        }
        elseif (isset($_POST['save'])) {
            $jenis_dokumen = 'draft';
            $status_dokumen = '1';
        }
        if ($_POST['identitas'] == 'Sebagai Penerima Gratifikasi'):
			if ($_POST['status_penerimaan'] == 'tolak'):
			$id_user = strip_tags(trim($this->session->userdata('id')));
			//$id_gratifikasi = strip_tags(trim($this->uri->segment(3)));
			$now = date('Y-m-d H:i:s');
			$tahun = date('Y');
			$nomor_laporan = "$id_gratifikasi/GRF/$tahun";
			$show_data = $this->Model_gratifikasi->show_data($id_gratifikasi)->row();
			$id_penerima = $show_data->id_penerima;
			$urutan = $this->Model_gratifikasi->get_sum_cetak_excel($id_penerima);
			$urutan_ke = $urutan + 1;
        
            $data_non = array(
                'id' => $id_gratifikasi,
                'id_pelapor' => strip_tags(trim($this->session->userdata('id'))),
                'id_penerima' => strip_tags(trim($this->session->userdata('id'))),
				'nomor_laporan' => $nomor_laporan,
                'identitas' => strip_tags(trim($_POST['identitas'])),
                'jenis_penerimaan' => strip_tags(trim($_POST['jenis_penerimaan'])),
                'uraian' => strip_tags(trim($_POST['uraian'])),
                'mata_uang' => strip_tags(trim($_POST['mata_uang'])),
                'nilai_nominal' => $nilai_nominal,
                'kode_peristiwa' => strip_tags(trim($_POST['kode_peristiwa'])),
                'peristiwa_lainnya' => strip_tags(trim($_POST['peristiwa_lainnya'])),
                'tempat_penerimaan' => strip_tags(trim($_POST['tempat_penerimaan'])),
                'tanggal_penerimaan' => strip_tags(trim($_POST['tanggal_penerimaan'])),
                'nama_pemberi' => strip_tags(trim($_POST['nama_pemberi'])),
                'pekerjaan_pemberi' => strip_tags(trim($_POST['pekerjaan_pemberi'])),
                'alamat_pemberi' => strip_tags(trim($_POST['alamat_pemberi'])),
                'telepon_pemberi' => strip_tags(trim($_POST['telepon_pemberi'])),
                'faks_pemberi' => strip_tags(trim($_POST['faks_pemberi'])),
                'email_pemberi' => strip_tags(trim($_POST['email_pemberi'])),
                'hubungan_pemberi' => strip_tags(trim($_POST['hubungan_pemberi'])),
                'alasan_pemberian' => strip_tags(trim($_POST['alasan_pemberian'])),
                'kronologi_penerimaan' => strip_tags(trim($_POST['kronologi_penerimaan'])),
                'status_dokumen_dilampirkan' => strip_tags(trim($_POST['status_dokumen_dilampirkan'])),
                'dokumen_dilampirkan' => strip_tags(trim($_POST['dokumen_dilampirkan'])),
                'catatan_tambahan' => strip_tags(trim($_POST['catatan_tambahan'])),
                'tempat_laporan' => strip_tags(trim($_POST['tempat_laporan'])),
                'tgl_laporan' => $now,
                'jenis_dokumen' => $jenis_dokumen,
                'status_dokumen' => '5',
                'status_penerimaan' => strip_tags(trim($_POST['status_penerimaan'])),
				'verifikasi_oleh' => $id_user,
				'tgl_verifikasi' => $now,
				'urutan' => $urutan_ke
            );
			else :
			$data_non = array(
                'id' => $id_gratifikasi,
                'id_pelapor' => strip_tags(trim($this->session->userdata('id'))),
                'id_penerima' => strip_tags(trim($this->session->userdata('id'))),
                'identitas' => strip_tags(trim($_POST['identitas'])),
                'jenis_penerimaan' => strip_tags(trim($_POST['jenis_penerimaan'])),
                'uraian' => strip_tags(trim($_POST['uraian'])),
                'mata_uang' => strip_tags(trim($_POST['mata_uang'])),
                'nilai_nominal' => $nilai_nominal,
                'kode_peristiwa' => strip_tags(trim($_POST['kode_peristiwa'])),
                'peristiwa_lainnya' => strip_tags(trim($_POST['peristiwa_lainnya'])),
                'tempat_penerimaan' => strip_tags(trim($_POST['tempat_penerimaan'])),
                'tanggal_penerimaan' => strip_tags(trim($_POST['tanggal_penerimaan'])),
                'nama_pemberi' => strip_tags(trim($_POST['nama_pemberi'])),
                'pekerjaan_pemberi' => strip_tags(trim($_POST['pekerjaan_pemberi'])),
                'alamat_pemberi' => strip_tags(trim($_POST['alamat_pemberi'])),
                'telepon_pemberi' => strip_tags(trim($_POST['telepon_pemberi'])),
                'faks_pemberi' => strip_tags(trim($_POST['faks_pemberi'])),
                'email_pemberi' => strip_tags(trim($_POST['email_pemberi'])),
                'hubungan_pemberi' => strip_tags(trim($_POST['hubungan_pemberi'])),
                'alasan_pemberian' => strip_tags(trim($_POST['alasan_pemberian'])),
                'kronologi_penerimaan' => strip_tags(trim($_POST['kronologi_penerimaan'])),
                'status_dokumen_dilampirkan' => strip_tags(trim($_POST['status_dokumen_dilampirkan'])),
                'dokumen_dilampirkan' => strip_tags(trim($_POST['dokumen_dilampirkan'])),
                'catatan_tambahan' => strip_tags(trim($_POST['catatan_tambahan'])),
                'tempat_laporan' => strip_tags(trim($_POST['tempat_laporan'])),
                'tgl_laporan' => $now,
                'jenis_dokumen' => $jenis_dokumen,
                'status_dokumen' => $status_dokumen,
                'status_penerimaan' => strip_tags(trim($_POST['status_penerimaan']))
            );
			endif;
			
            $data = $this->security->xss_clean($data_non);
            $insert = $this->Model_gratifikasi->insert($data); 

            //tabel file
            if (!empty($_FILES['filesatu']['name'])):
                $file_name = $_FILES['filesatu']['name'];
                $file_tmp = $_FILES['filesatu']['tmp_name'];
                $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                $path = $this->config->item('path_file_gratifikasi');
                $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                move_uploaded_file($file_tmp, $lokasi);
                $datafilesatu_non = array(
                    'id_gratifikasi' => $id_gratifikasi,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                );
                $datafilesatu = $this->security->xss_clean($datafilesatu_non);
                $insert_file = $this->Model_gratifikasi->insert_file($datafilesatu);
            endif;

            if (!empty($_FILES['file']['name'])):
                for ($j = 0; $j < count($_FILES["file"]['name']); $j++):
                    $file_name = $_FILES['file']['name']["$j"];
                    $file_tmp = $_FILES['file']['tmp_name']["$j"];
                    $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                    $path = $this->config->item('path_file_gratifikasi');
                    $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                    move_uploaded_file($file_tmp, $lokasi);
                    $datafile_non = array(
                    'id_gratifikasi' => $id_gratifikasi,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                    );
                    $datafile = $this->security->xss_clean($datafile_non);
                    $insert_file = $this->Model_gratifikasi->insert_file($datafile);
                endfor;
            endif; 
        else:
            $id_penerima = strip_tags(trim($_POST['id']));
            $data_penerima_non = array(
                'name' => strip_tags(trim($_POST['name'])),
                'tempatlahir' => strip_tags(trim($_POST['tempatlahir'])),
                'tgllahir' => strip_tags(trim($_POST['tgllahir'])),
                'no_ktp' => strip_tags(trim($_POST['no_ktp'])),
                'jabatan' => strip_tags(trim($_POST['jabatan'])),
                'email' => strip_tags(trim($_POST['email'])),
                'nohp' => strip_tags(trim($_POST['nohp'])),
                'pin_bb' => strip_tags(trim($_POST['pin_bb'])),
                'inskerja' => strip_tags(trim($_POST['inskerja'])),
                'biro' => strip_tags(trim($_POST['biro'])),
                'bagian' => strip_tags(trim($_POST['bagian'])),
                'golongan' => strip_tags(trim($_POST['golongan'])),
                'pangkat' => strip_tags(trim($_POST['pangkat'])),
                'alamat_kantor' => strip_tags(trim($_POST['alamat_kantor'])),
                'provinsi_kantor' => strip_tags(trim($_POST['provinsi_kantor'])),
                'kota_kantor' => strip_tags(trim($_POST['kota_kantor'])),
                'kecamatan_kantor' => strip_tags(trim($_POST['kecamatan_kantor'])),
                'kelurahan_kantor' => strip_tags(trim($_POST['kelurahan_kantor'])),
                'kode_pos_kantor' => strip_tags(trim($_POST['kode_pos_kantor'])),
                'no_kantor' => strip_tags(trim($_POST['no_kantor'])),
                'alamat_rumah' => strip_tags(trim($_POST['alamat_rumah'])),
                'provinsi_rumah' => strip_tags(trim($_POST['provinsi_rumah'])),
                'kota_rumah' => strip_tags(trim($_POST['kota_rumah'])),
                'kecamatan_rumah' => strip_tags(trim($_POST['kecamatan_rumah'])),
                'kelurahan_rumah' => strip_tags(trim($_POST['kelurahan_rumah'])),
                'kode_pos_rumah' => strip_tags(trim($_POST['kode_pos_rumah'])),
                'no_rumah' => strip_tags(trim($_POST['no_rumah'])),
            );
            $data_penerima = $this->security->xss_clean($data_penerima_non);
            $insert_penerima = $this->Model_profil->update($id_penerima, $data_penerima);
			
			if ($_POST['status_penerimaan'] == 'tolak'):
			$id_user = strip_tags(trim($this->session->userdata('id')));
			//$id_gratifikasi = strip_tags(trim($this->uri->segment(3)));
			$now = date('Y-m-d H:i:s');
			$tahun = date('Y');
			$nomor_laporan = "$id_gratifikasi/GRF/$tahun";
			$show_data = $this->Model_gratifikasi->show_data($id_gratifikasi)->row();
			$id_penerima = $show_data->id_penerima;
			$urutan = $this->Model_gratifikasi->get_sum_cetak_excel($id_penerima);
			$urutan_ke = $urutan + 1;
			
            $data_non = array(
                'id' => $id_gratifikasi,
                'id_pelapor' => strip_tags(trim($this->session->userdata('id'))),
                'id_penerima' => $id_penerima,
				'nomor_laporan' => $nomor_laporan,
                'identitas' => strip_tags(trim($_POST['identitas'])),
                'jenis_penerimaan' => strip_tags(trim($_POST['jenis_penerimaan'])),
                'uraian' => strip_tags(trim($_POST['uraian'])),
                'mata_uang' => strip_tags(trim($_POST['mata_uang'])),
                'nilai_nominal' => $nilai_nominal,
                'kode_peristiwa' => strip_tags(trim($_POST['kode_peristiwa'])),
                'peristiwa_lainnya' => strip_tags(trim($_POST['peristiwa_lainnya'])),
                'tempat_penerimaan' => strip_tags(trim($_POST['tempat_penerimaan'])),
                'tanggal_penerimaan' =>strip_tags(trim($_POST['tanggal_penerimaan'])),
                'nama_pemberi' => strip_tags(trim($_POST['nama_pemberi'])),
                'pekerjaan_pemberi' => strip_tags(trim($_POST['pekerjaan_pemberi'])),
                'alamat_pemberi' => strip_tags(trim($_POST['alamat_pemberi'])),
                'telepon_pemberi' => strip_tags(trim($_POST['telepon_pemberi'])),
                'faks_pemberi' => strip_tags(trim($_POST['faks_pemberi'])),
                'email_pemberi' => strip_tags(trim($_POST['email_pemberi'])),
                'hubungan_pemberi' => strip_tags(trim($_POST['hubungan_pemberi'])),
                'alasan_pemberian' => strip_tags(trim($_POST['alasan_pemberian'])),
                'kronologi_penerimaan' => strip_tags(trim($_POST['kronologi_penerimaan'])),
                'status_dokumen_dilampirkan' => strip_tags(trim($_POST['status_dokumen_dilampirkan'])),
                'dokumen_dilampirkan' => strip_tags(trim($_POST['dokumen_dilampirkan'])),
                'catatan_tambahan' => strip_tags(trim($_POST['catatan_tambahan'])),
                'tempat_laporan' => strip_tags(trim($_POST['tempat_laporan'])),
                'tgl_laporan' => $now,
                'jenis_dokumen' => $jenis_dokumen,
                'status_dokumen' => '5',
                'status_penerimaan' => strip_tags(trim($_POST['status_penerimaan'])),
				'verifikasi_oleh' => $id_user,
				'tgl_verifikasi' => $now,
				'urutan' => $urutan_ke
            );
			else :
			$data_non = array(
                'id' => $id_gratifikasi,
                'id_pelapor' => strip_tags(trim($this->session->userdata('id'))),
                'id_penerima' => $id_penerima,
                'identitas' => strip_tags(trim($_POST['identitas'])),
                'jenis_penerimaan' => strip_tags(trim($_POST['jenis_penerimaan'])),
                'uraian' => strip_tags(trim($_POST['uraian'])),
                'mata_uang' => strip_tags(trim($_POST['mata_uang'])),
                'nilai_nominal' => $nilai_nominal,
                'kode_peristiwa' => strip_tags(trim($_POST['kode_peristiwa'])),
                'peristiwa_lainnya' => strip_tags(trim($_POST['peristiwa_lainnya'])),
                'tempat_penerimaan' => strip_tags(trim($_POST['tempat_penerimaan'])),
                'tanggal_penerimaan' =>strip_tags(trim($_POST['tanggal_penerimaan'])),
                'nama_pemberi' => strip_tags(trim($_POST['nama_pemberi'])),
                'pekerjaan_pemberi' => strip_tags(trim($_POST['pekerjaan_pemberi'])),
                'alamat_pemberi' => strip_tags(trim($_POST['alamat_pemberi'])),
                'telepon_pemberi' => strip_tags(trim($_POST['telepon_pemberi'])),
                'faks_pemberi' => strip_tags(trim($_POST['faks_pemberi'])),
                'email_pemberi' => strip_tags(trim($_POST['email_pemberi'])),
                'hubungan_pemberi' => strip_tags(trim($_POST['hubungan_pemberi'])),
                'alasan_pemberian' => strip_tags(trim($_POST['alasan_pemberian'])),
                'kronologi_penerimaan' => strip_tags(trim($_POST['kronologi_penerimaan'])),
                'status_dokumen_dilampirkan' => strip_tags(trim($_POST['status_dokumen_dilampirkan'])),
                'dokumen_dilampirkan' => strip_tags(trim($_POST['dokumen_dilampirkan'])),
                'catatan_tambahan' => strip_tags(trim($_POST['catatan_tambahan'])),
                'tempat_laporan' => strip_tags(trim($_POST['tempat_laporan'])),
                'tgl_laporan' => $now,
                'jenis_dokumen' => $jenis_dokumen,
                'status_dokumen' => '1',
                'status_penerimaan' => strip_tags(trim($_POST['status_penerimaan']))
            );
			endif;
            $data = $this->security->xss_clean($data_non);
            $insert = $this->Model_gratifikasi->insert($data);

            ///tabel file
            if (!empty($_FILES['filesatu']['name'])):
                $file_name = $_FILES['filesatu']['name'];
                $file_tmp = $_FILES['filesatu']['tmp_name'];
                $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                $path = $this->config->item('path_file_gratifikasi');
                $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                move_uploaded_file($file_tmp, $lokasi);
                $datafilesatu_non = array(
                    'id_gratifikasi' => $id_gratifikasi,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                );
                $datafilesatu = $this->security->xss_clean($datafilesatu_non);
                $insert_file = $this->Model_gratifikasi->insert_file($datafilesatu);
            endif;

            if (!empty($_FILES['file']['name'])):
                for ($j = 0; $j < count($_FILES["file"]['name']); $j++):
                    $file_name = $_FILES['file']['name']["$j"];
                    $file_tmp = $_FILES['file']['tmp_name']["$j"];
                    $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                    $path = $this->config->item('path_file_gratifikasi');
                    $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                    move_uploaded_file($file_tmp, $lokasi);
                    $datafile_non = array(
                        'id_gratifikasi' => $id_gratifikasi,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                    );
                    $datafile = $this->security->xss_clean($datafile_non);
                    $insert_file = $this->Model_gratifikasi->insert_file($datafile);
                endfor;
            endif;
        endif;
		
        if ($insert == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Laporan Anda berhasil disimpan.</div>');
            redirect('beranda');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Laporan Anda gagal disimpan.</div>');
            redirect('beranda');
        }
    }

    public function riwayat_gratifikasi() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data['draft'] = $this->Model_gratifikasi->get_data_draft($id_user);
        $data['count_draft'] = $this->Model_gratifikasi->get_count_draft($id_user);
        $data['blm_verified'] = $this->Model_gratifikasi->get_data_blm_verified_peg($id_user);
        $data['count_blm_verified'] = $this->Model_gratifikasi->get_count_blm_verified_peg($id_user);
        $data['revisi'] = $this->Model_gratifikasi->get_data_revisi_peg($id_user);
        $data['count_revisi'] = $this->Model_gratifikasi->get_count_revisi_peg($id_user);
        $data['verified'] = $this->Model_gratifikasi->get_data_verified_peg($id_user);
        $data['count_verified'] = $this->Model_gratifikasi->get_count_verified_peg($id_user);
		
		$data['laporan'] = $this->Model_gratifikasi->get_data_laporan_peg($id_user);
        $data['count_laporan'] = $this->Model_gratifikasi->get_count_laporan_peg($id_user);
		$data['riwayat'] = $this->Model_gratifikasi->get_data_riwayat();
		$data['riwayat_peg'] = $this->Model_gratifikasi->get_data_riwayat_pegawai($id_user);
        $data['count_riwayat'] = $this->Model_gratifikasi->get_count_riwayat();
        $data['count_riwayat_peg'] = $this->Model_gratifikasi->get_count_riwayat_pegawai($id_user);
		
        $data['tagmenu'] = 'riwayat_gratifikasi';
        $data['body'] = 'gratifikasi/view_riwayat_gratifikasi';
        $this->load->vars($data);
        $this->load->view('view_main');
    }

    public function send_to() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(3)));

        $data_non = array(
            'jenis_dokumen' => 'send',
            'status_dokumen' => '2',
        );
        $data = $this->security->xss_clean($data_non);
        $update = $this->Model_gratifikasi->update($id_gratifikasi,$data);

        if ($update == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Laporan berhasil dikirim.</div>');
            redirect('gratifikasi/riwayat_gratifikasi');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Laporan gagal dikirim.</div>');
            redirect('gratifikasi/riwayat_gratifikasi');
        }
    }

    public function detail_gratifikasi() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(3)));
        $data['id_gratifikasi'] = $id_gratifikasi;
        $data['show_profil'] = $this->Model_gratifikasi->show_pelapor($id_gratifikasi)->row();
        $data['show_data'] = $this->Model_gratifikasi->show_data($id_gratifikasi)->row();
        $data['file_gratifikasi'] = $this->Model_gratifikasi->getfile_gratifikasi($id_gratifikasi);
        $data['verifikasi'] = $this->Model_gratifikasi->get_verifikasi($id_gratifikasi);
        $data['file_verifikasi'] = $this->Model_gratifikasi->getfile_verifikasi($id_gratifikasi);
        $data['file_rekomendasi'] = $this->Model_gratifikasi->getfile_rekomendasi($id_gratifikasi);
        $data['file_tindak_lanjut'] = $this->Model_gratifikasi->getfile_tindak_lanjut($id_gratifikasi);
        $data['tagmenu'] = 'riwayat_gratifikasi';
        $data['body'] = 'gratifikasi/view_detail_gratifikasi';
        $this->load->vars($data);
        $this->load->view('view_main');
    }

    public function download_file(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEG'));
        $this->load->helper('download');
        $id = strip_tags(trim($this->uri->segment(3)));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(4)));
        $show = $this->Model_gratifikasi->getfile_gratifikasi_byid($id)->row();

        if (count($show) > 0) {
            $files = $show->files;
            $nama_files = $show->nama_files;
            $path = $this->config->item('path_file_gratifikasi');
            ob_clean();
            $lokasi = file_get_contents($path . $files);
            force_download($nama_files, $lokasi);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-remove"></i>Gagal!</strong> Data tidak ditemukan.</div>');
            redirect('gratifikasi/detail_gratifikasi/'.$id_gratifikasi);
        }
    }

    public function update_gratifikasi() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(3)));
        $data['id_gratifikasi'] = $id_gratifikasi;
        $data['show_profil'] = $this->Model_profil->show_profil($id_user)->row();
        $data['show_pelapor'] = $this->Model_gratifikasi->show_pelapor($id_gratifikasi)->row();
        $data['show_data'] = $this->Model_gratifikasi->show_data($id_gratifikasi)->row();
        $data['identitas'] = $this->Model_gratifikasi->getidentitas();
        $data['provinsi'] = $this->Model_profil->getprovinsi();
        $data['jenis_penerimaan'] = $this->Model_gratifikasi->getjenis_penerimaan();
        $data['peristiwa_penerimaan'] = $this->Model_gratifikasi->getperistiwa_penerimaan();
        $data['alamat_pengirim'] = $this->Model_gratifikasi->getalamat_pengirim();
        $data['file_gratifikasi'] = $this->Model_gratifikasi->getfile_gratifikasi($id_gratifikasi);
        $data['tanggal'] = date('Y-m-d');
        $data['tagmenu'] = 'riwayat_gratifikasi';
        $data['body'] = 'gratifikasi/view_update_gratifikasi';
        $this->load->vars($data);
        $this->load->view('view_main');
    }

    function delete_file() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $id = strip_tags(trim($this->uri->segment(3)));
        $lokasi = $this->config->item('path_file_gratifikasi');
        $show_data = $this->Model_gratifikasi->getfile_gratifikasi_byid($id)->row();
        $id_gratifikasi = $show_data->id_gratifikasi;
        $files = $show_data->files;
        @unlink($lokasi . $files);
        $resp = $this->Model_gratifikasi->deletefile_gratifikasi_byid($id);

        if ($resp == 'sukses'):
            $this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil!</strong> File berhasil dihapus.</div>');
            redirect(base_url('gratifikasi/update_gratifikasi/' . $id_gratifikasi));
        else:
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Gagal!</strong> File gagal dihapus.</div>');
            redirect(base_url('gratifikasi/update_gratifikasi/' . $id_gratifikasi));
        endif;
    }

    public function update() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $now = date('Y-m-d H:i:s');
        $id_gratifikasi = strip_tags(trim($_POST['id_gratifikasi']));
        $b = $this->common->filter(strip_tags(trim($_POST['nilai_nominal'])));
        $b = str_replace('Rp.', '', $b);
        $b = str_replace('.', '', $b);
        $nilai_nominal = str_replace('Rp.', '', $b);
        if (isset($_POST['send'])) {
            $jenis_dokumen = 'send';
            $status_dokumen = '2';
        }
        elseif (isset($_POST['revisi'])) {
            $jenis_dokumen = 'send';
            $status_dokumen = '4';
        }
        elseif (isset($_POST['save'])) {
            $jenis_dokumen = 'draft';
            $status_dokumen = '1';
        }
        if ($_POST['identitas'] == 'Sebagai Penerima Gratifikasi'):
            $data_non = array(
                'id_pelapor' => strip_tags(trim($this->session->userdata('id'))),
                'jenis_penerimaan' => strip_tags(trim($_POST['jenis_penerimaan'])),
                'uraian' => strip_tags(trim($_POST['uraian'])),
                'nilai_nominal' => $nilai_nominal,
                'kode_peristiwa' => strip_tags(trim($_POST['kode_peristiwa'])),
                'peristiwa_lainnya' => strip_tags(trim($_POST['peristiwa_lainnya'])),
                'tempat_penerimaan' => strip_tags(trim($_POST['tempat_penerimaan'])),
                'tanggal_penerimaan' => strip_tags(trim($_POST['tanggal_penerimaan'])),
                'nama_pemberi' => strip_tags(trim($_POST['nama_pemberi'])),
                'pekerjaan_pemberi' => strip_tags(trim($_POST['pekerjaan_pemberi'])),
                'alamat_pemberi' => strip_tags(trim($_POST['alamat_pemberi'])),
                'telepon_pemberi' => strip_tags(trim($_POST['telepon_pemberi'])),
                'faks_pemberi' => strip_tags(trim($_POST['faks_pemberi'])),
                'email_pemberi' => strip_tags(trim($_POST['email_pemberi'])),
                'hubungan_pemberi' => strip_tags(trim($_POST['hubungan_pemberi'])),
                'alasan_pemberian' => strip_tags(trim($_POST['alasan_pemberian'])),
                'kronologi_penerimaan' => strip_tags(trim($_POST['kronologi_penerimaan'])),
                'status_dokumen_dilampirkan' => strip_tags(trim($_POST['status_dokumen_dilampirkan'])),
                'dokumen_dilampirkan' => strip_tags(trim($_POST['dokumen_dilampirkan'])),
                'catatan_tambahan' => strip_tags(trim($_POST['catatan_tambahan'])),
                'tempat_laporan' => strip_tags(trim($_POST['tempat_laporan'])),
                'tgl_laporan' => $now,
                'jenis_dokumen' => $jenis_dokumen,
                'status_dokumen' => $status_dokumen,
            );
            $data = $this->security->xss_clean($data_non);
            $update = $this->Model_gratifikasi->update($id_gratifikasi,$data); 

            //tabel file
            if (!empty($_FILES['filesatu']['name'])):
                $file_name = $_FILES['filesatu']['name'];
                $file_tmp = $_FILES['filesatu']['tmp_name'];
                $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                $path = $this->config->item('path_file_gratifikasi');
                $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                move_uploaded_file($file_tmp, $lokasi);
                $datafilesatu_non = array(
                    'id_gratifikasi' => $id_gratifikasi,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                );
                $datafilesatu = $this->security->xss_clean($datafilesatu_non);
                $insert_file = $this->Model_gratifikasi->insert_file($datafilesatu);
            endif;

            if (!empty($_FILES['file']['name'])):
                for ($j = 0; $j < count($_FILES["file"]['name']); $j++):
                    $file_name = $_FILES['file']['name']["$j"];
                    $file_tmp = $_FILES['file']['tmp_name']["$j"];
                    $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                    $path = $this->config->item('path_file_gratifikasi');
                    $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                    move_uploaded_file($file_tmp, $lokasi);
                    $datafile_non = array(
                    'id_gratifikasi' => $id_gratifikasi,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                    );
                    $datafile = $this->security->xss_clean($datafile_non);
                    $insert_file = $this->Model_gratifikasi->insert_file($datafile);
                endfor;
            endif; 
        elseif ($_POST['identitas'] == 'Sebagai Pelapor Gratifikasi'):
            $id_penerima = strip_tags(trim($_POST['id']));
            $data_penerima_non = array(
                'name' => strip_tags(trim($_POST['name'])),
                'tempatlahir' => strip_tags(trim($_POST['tempatlahir'])),
                'tgllahir' => strip_tags(trim($_POST['tgllahir'])),
                'no_ktp' => strip_tags(trim($_POST['no_ktp'])),
                'jabatan' => strip_tags(trim($_POST['jabatan'])),
                'email' => strip_tags(trim($_POST['email'])),
                'nohp' => strip_tags(trim($_POST['nohp'])),
                'pin_bb' => strip_tags(trim($_POST['pin_bb'])),
                'inskerja' => strip_tags(trim($_POST['inskerja'])),
                'biro' => strip_tags(trim($_POST['biro'])),
                'bagian' => strip_tags(trim($_POST['bagian'])),
                'golongan' => strip_tags(trim($_POST['golongan'])),
                'pangkat' => strip_tags(trim($_POST['pangkat'])),
                'alamat_kantor' => strip_tags(trim($_POST['alamat_kantor'])),
                'provinsi_kantor' => strip_tags(trim($_POST['provinsi_kantor'])),
                'kota_kantor' => strip_tags(trim($_POST['kota_kantor'])),
                'kecamatan_kantor' => strip_tags(trim($_POST['kecamatan_kantor'])),
                'kelurahan_kantor' => strip_tags(trim($_POST['kelurahan_kantor'])),
                'kode_pos_kantor' => strip_tags(trim($_POST['kode_pos_kantor'])),
                'no_kantor' => strip_tags(trim($_POST['no_kantor'])),
                'alamat_rumah' => strip_tags(trim($_POST['alamat_rumah'])),
                'provinsi_rumah' => strip_tags(trim($_POST['provinsi_rumah'])),
                'kota_rumah' => strip_tags(trim($_POST['kota_rumah'])),
                'kecamatan_rumah' => strip_tags(trim($_POST['kecamatan_rumah'])),
                'kelurahan_rumah' => strip_tags(trim($_POST['kelurahan_rumah'])),
                'kode_pos_rumah' => strip_tags(trim($_POST['kode_pos_rumah'])),
                'no_rumah' => strip_tags(trim($_POST['no_rumah'])),
            );
            $data_penerima = $this->security->xss_clean($data_penerima_non);
            $update_penerima = $this->Model_profil->update($id_penerima, $data_penerima);

            $data_non = array(
                'jenis_penerimaan' => strip_tags(trim($_POST['jenis_penerimaan'])),
                'uraian' => strip_tags(trim($_POST['uraian'])),
                'nilai_nominal' => $nilai_nominal,
                'kode_peristiwa' => strip_tags(trim($_POST['kode_peristiwa'])),
                'peristiwa_lainnya' => strip_tags(trim($_POST['peristiwa_lainnya'])),
                'tempat_penerimaan' => strip_tags(trim($_POST['tempat_penerimaan'])),
                'tanggal_penerimaan' => strip_tags(trim($_POST['tanggal_penerimaan'])),
                'nama_pemberi' => strip_tags(trim($_POST['nama_pemberi'])),
                'pekerjaan_pemberi' => strip_tags(trim($_POST['pekerjaan_pemberi'])),
                'alamat_pemberi' => strip_tags(trim($_POST['alamat_pemberi'])),
                'telepon_pemberi' => strip_tags(trim($_POST['telepon_pemberi'])),
                'faks_pemberi' => strip_tags(trim($_POST['faks_pemberi'])),
                'email_pemberi' => strip_tags(trim($_POST['email_pemberi'])),
                'hubungan_pemberi' => strip_tags(trim($_POST['hubungan_pemberi'])),
                'alasan_pemberian' => strip_tags(trim($_POST['alasan_pemberian'])),
                'kronologi_penerimaan' => strip_tags(trim($_POST['kronologi_penerimaan'])),
                'status_dokumen_dilampirkan' => strip_tags(trim($_POST['status_dokumen_dilampirkan'])),
                'dokumen_dilampirkan' => strip_tags(trim($_POST['dokumen_dilampirkan'])),
                'catatan_tambahan' => strip_tags(trim($_POST['catatan_tambahan'])),
                'tempat_laporan' => strip_tags(trim($_POST['tempat_laporan'])),
                'tgl_laporan' => $now,
                'jenis_dokumen' => $jenis_dokumen,
                'status_dokumen' => $status_dokumen,
            );
            $data = $this->security->xss_clean($data_non);
            $update = $this->Model_gratifikasi->update($id_gratifikasi, $data);

            ///tabel file
            if (!empty($_FILES['filesatu']['name'])):
                $file_name = $_FILES['filesatu']['name'];
                $file_tmp = $_FILES['filesatu']['tmp_name'];
                $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                $path = $this->config->item('path_file_gratifikasi');
                $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                move_uploaded_file($file_tmp, $lokasi);
                $datafilesatu_non = array(
                    'id_gratifikasi' => $id_gratifikasi,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                );
                $datafilesatu = $this->security->xss_clean($datafilesatu_non);
                $insert_file = $this->Model_gratifikasi->insert_file($datafilesatu);
            endif;

            if (!empty($_FILES['file']['name'])):
                for ($j = 0; $j < count($_FILES["file"]['name']); $j++):
                    $file_name = $_FILES['file']['name']["$j"];
                    $file_tmp = $_FILES['file']['tmp_name']["$j"];
                    $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                    $path = $this->config->item('path_file_gratifikasi');
                    $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                    move_uploaded_file($file_tmp, $lokasi);
                    $datafile_non = array(
                        'id_gratifikasi' => $id_gratifikasi,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                    );
                    $datafile = $this->security->xss_clean($datafile_non);
                    $insert_file = $this->Model_gratifikasi->insert_file($datafile);
                endfor;
            endif;
        endif;

        if ($update == 'sukses') {
            if (isset($_POST['send'])):
                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Laporan Anda berhasil dikirim.</div>');
            elseif (isset($_POST['revisi'])):
                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Laporan Anda berhasil dikirim.</div>');
            elseif (isset($_POST['save'])):
                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Laporan Anda berhasil disimpan.</div>');
            endif;
            redirect('gratifikasi/riwayat_gratifikasi');
        } else {
            if (isset($_POST['send'])):
                $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Laporan Anda gagal dikirim.</div>');
            elseif (isset($_POST['revisi'])):
                $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Laporan Anda gagal dikirim.</div>');
            elseif (isset($_POST['save'])):
                $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Laporan Anda gagal disimpan.</div>');
            endif;
            redirect('gratifikasi/riwayat_gratifikasi');
        }
    }

    public function delete() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(3)));
        
        //tabel log_aktifitas
        $id_user = strip_tags(trim($this->session->userdata('id')));

        $file_gratifikasi = $this->Model_gratifikasi->getfile_gratifikasi($id_gratifikasi);
        if (count($file_gratifikasi) > 0) {
            $lokasi = $this->config->item('path_file_gratifikasi');
            foreach($file_gratifikasi as $row){
                $dokfile_gratifikasi = $row->files;
                @unlink($lokasi . $dokfile_gratifikasi);
                $id = $row->id;
                $this->Model_gratifikasi->deletefile_gratifikasi_byid($id);
            }
        }

        $resp1 = $this->Model_gratifikasi->delete_gratifikasi_by_id($id_gratifikasi);

        if ($resp1 == 'sukses'):
            $this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-ok"></i> Berhasil!</strong> Data berhasil dihapus.</div>');
            redirect('gratifikasi/riwayat_gratifikasi');
        else:
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-remove"></i> Gagal!</strong> Data gagal dihapus.</div>');
            redirect('gratifikasi/riwayat_gratifikasi');
        endif;
    }

    public function verifikasi_gratifikasi() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data['blm_verified'] = $this->Model_gratifikasi->get_data_blm_verified();
        $data['count_blm_verified'] = $this->Model_gratifikasi->get_count_blm_verified();
        $data['proses_revisi'] = $this->Model_gratifikasi->get_data_proses_revisi();
        $data['count_proses_revisi'] = $this->Model_gratifikasi->get_count_proses_revisi();
        $data['revisi'] = $this->Model_gratifikasi->get_data_revisi();
        $data['count_revisi'] = $this->Model_gratifikasi->get_count_revisi();
        //$data['verified'] = $this->Model_gratifikasi->get_data_verified();
        $data['verified'] = $this->Model_gratifikasi->get_data_telah_review();
        //$data['count_verified'] = $this->Model_gratifikasi->get_count_verified();
        $data['count_verified'] = $this->Model_gratifikasi->get_count_telah_review();
        $data['terkirim'] = $this->Model_gratifikasi->get_data_terkirim();
        $data['count_terkirim'] = $this->Model_gratifikasi->get_count_terkirim();
        $data['riwayat'] = $this->Model_gratifikasi->get_data_riwayat_gratifikasi();
        $data['count_riwayat'] = $this->Model_gratifikasi->get_count_riwayat();
        $data['tagmenu'] = 'verifikasi_gratifikasi';
        $data['body'] = 'gratifikasi/view_verifikasi_gratifikasi';
        $this->load->vars($data);
        $this->load->view('view_main');
    }

    public function detail_verifikasi() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(3)));
        $data['show_profil'] = $this->Model_gratifikasi->show_profil($id_gratifikasi)->row();
        $data['show_data'] = $this->Model_gratifikasi->show_data($id_gratifikasi)->row();
        $data['verifikasi'] = $this->Model_gratifikasi->get_verifikasi($id_gratifikasi);
        $data['file_verifikasi'] = $this->Model_gratifikasi->getfile_verifikasi($id_gratifikasi);
        $data['file_gratifikasi'] = $this->Model_gratifikasi->getfile_gratifikasi($id_gratifikasi);
        $data['file_rekomendasi'] = $this->Model_gratifikasi->getfile_rekomendasi($id_gratifikasi);
        $data['file_tindak_lanjut'] = $this->Model_gratifikasi->getfile_tindak_lanjut($id_gratifikasi);
        $data['tagmenu'] = 'riwayat_gratifikasi';
        $data['body'] = 'gratifikasi/view_detail_verifikasi';
        $this->load->vars($data);
        $this->load->view('view_main');
    }

    public function update_verifikasi() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(3)));
        $data['show_profil'] = $this->Model_gratifikasi->show_profil($id_gratifikasi)->row();
        $data['show_data'] = $this->Model_gratifikasi->show_data($id_gratifikasi)->row();
        $data['verifikasi'] = $this->Model_gratifikasi->get_verifikasi($id_gratifikasi);
        $data['id_gratifikasi'] = $id_gratifikasi;
        $data['file_gratifikasi'] = $this->Model_gratifikasi->getfile_gratifikasi($id_gratifikasi);
        $data['tagmenu'] = 'verifikasi_gratifikasi';
        $data['body'] = 'gratifikasi/view_update_verifikasi';
        $this->load->vars($data);
        $this->load->view('view_main');
    }

    public function send_verified() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(3)));
        $now = date('Y-m-d H:i:s');
        $tahun = date('Y');
        $nomor_laporan = "$id_gratifikasi/GRF/$tahun";
        $show_data = $this->Model_gratifikasi->show_data($id_gratifikasi)->row();
        $id_penerima = $show_data->id_penerima;
        $urutan = $this->Model_gratifikasi->get_sum_cetak_excel($id_penerima);
        $urutan_ke = $urutan + 1;

        $data_non = array(
            'nomor_laporan' => $nomor_laporan,
            'status_dokumen' => '5',
            'verifikasi_oleh' => $id_user,
            'tgl_verifikasi' => $now,
            'urutan' => $urutan_ke
        );
        $data = $this->security->xss_clean($data_non);
        $update = $this->Model_gratifikasi->update($id_gratifikasi,$data);

        if ($update == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Laporan berhasil direview.</div>');
            redirect('gratifikasi/verifikasi_gratifikasi');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Laporan gagal direview.</div>');
            redirect('gratifikasi/verifikasi_gratifikasi');
        }
    }

    public function download_verifikasi(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEG'));
        $this->load->helper('download');
        $id = strip_tags(trim($this->uri->segment(3)));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(4)));
        $show = $this->Model_gratifikasi->getfile_verifikasi_byid($id)->row();

        if (count($show) > 0) {
            $files = $show->files;
            $nama_files = $show->nama_files;
            $path = $this->config->item('path_file_verifikasi');
            ob_clean();
            $lokasi = file_get_contents($path . $files);
            force_download($nama_files, $lokasi);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-remove"></i>Gagal!</strong> Data tidak ditemukan.</div>');
            redirect('gratifikasi/detail_verifikasi/'.$id_gratifikasi);
        }
    }

    public function verified() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $now = date('Y-m-d H:i:s');
        $id_gratifikasi = strip_tags(trim($_POST['id_gratifikasi']));
        $tahun = date('Y');
        $show_data = $this->Model_gratifikasi->show_data($id_gratifikasi)->row();
        $id_penerima = $show_data->id_penerima;
        $urutan = $this->Model_gratifikasi->get_sum_cetak_excel($id_penerima);
        $urutan_ke = $urutan + 1;

        if (isset($_POST['verified'])):
            $status_dokumen = '5';
            $tgl_verifikasi = $now;
            $data['nomor_laporan'] = "$id_gratifikasi/GRF/$tahun";
            $data['verifikasi_oleh'] = $id_user;
        elseif (isset($_POST['revisi'])):
            $status_dokumen = '3';
            $tgl_verifikasi = '';
        endif;

        $data_non = array(
            'nomor_laporan' => $data['nomor_laporan'],
            'tgl_verifikasi' => $tgl_verifikasi,
            'verifikasi_oleh' => $data['verifikasi_oleh'],
            'status_dokumen' => $status_dokumen,
            'urutan' => $urutan_ke
        );
        $data = $this->security->xss_clean($data_non);
        $update = $this->Model_gratifikasi->update($id_gratifikasi,$data);

        if ($_POST['verifikasi']):
            $dataverifikasi_non = array(
                'id_gratifikasi' => $id_gratifikasi,
                'verifikasi' => strip_tags(trim($_POST['verifikasi'])),
                'oleh' => $id_user,
                'tanggal' => $now,
            );
            $dataverifikasi = $this->security->xss_clean($dataverifikasi_non);
            $update = $this->Model_gratifikasi->insert_verifikasi($dataverifikasi); 
        endif;

        //tabel file
        if (!empty($_FILES['filesatu']['name'])):
            $file_name = $_FILES['filesatu']['name'];
            $file_tmp = $_FILES['filesatu']['tmp_name'];
            $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
            $path = $this->config->item('path_file_verifikasi');
            $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
            move_uploaded_file($file_tmp, $lokasi);
            $datafilesatu_non = array(
                'id_gratifikasi' => $id_gratifikasi,
                'files' => $name,
                'nama_files' => $file_name,
                'tanggal_upload' => $now
            );
            $datafilesatu = $this->security->xss_clean($datafilesatu_non);
            $insert_file_verifikasi = $this->Model_gratifikasi->insert_file_verifikasi($datafilesatu);
        endif;

        if (!empty($_FILES['file']['name'])):
            for ($j = 0; $j < count($_FILES["file"]['name']); $j++):
                $file_name = $_FILES['file']['name']["$j"];
                $file_tmp = $_FILES['file']['tmp_name']["$j"];
                $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                $path = $this->config->item('path_file_verifikasi');
                $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                move_uploaded_file($file_tmp, $lokasi);
                $datafile_non = array(
                    'id_gratifikasi' => $id_gratifikasi,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                );
                $datafile = $this->security->xss_clean($datafile_non);
                $insert_file_verifikasi = $this->Model_gratifikasi->insert_file_verifikasi($datafile);
            endfor;
        endif;

        if ($update == 'sukses') {
            if (isset($_POST['verified'])):
                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Laporan berhasil direview.</div>');
            else:
                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Laporan berhasil dikirim untuk direvisi.</div>');
            endif;
            redirect('gratifikasi/verifikasi_gratifikasi');
        } else {
            if (isset($_POST['verified'])):
                $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Laporan gagal direview.</div>');
            else:
                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Laporan gagal dikirim untuk direvisi.</div>');
            endif;
            redirect('gratifikasi/verifikasi_gratifikasi');
        }
    }

    public function kirim_kpk() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $now = date('Y-m-d H:i:s');

        $data_non = array(
            'status_dokumen' => '6',
            'dikirim_oleh' => $id_user,
            'tgl_dikirim' => $now,
            'tgl_input_kirim' => $now
        );
        $data = $this->security->xss_clean($data_non);
        $update = $this->Model_gratifikasi->kirim_kpk($data);

        if ($update == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Laporan berhasil disimpan.</div>');
            redirect('gratifikasi/verifikasi_gratifikasi');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Laporan gagal disimpan.</div>');
            redirect('gratifikasi/verifikasi_gratifikasi');
        }
    }

    public function print_notifikasi() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(3)));
        $data['show_profil'] = $this->Model_gratifikasi->show_profil($id_gratifikasi)->row();
        $data['show_data'] = $this->Model_gratifikasi->show_data($id_gratifikasi)->row();
        $data['alamat_pengirim'] = $this->Model_gratifikasi->getalamat_pengirim();
        $data['tanggal'] = date('Y-m-d');
        $data['dategenerated'] = date('d-M-Y');

        $html = $this->load->view('view_print_notifikasi', $data, true); //render the view into HTML
        $this->load->library('Pdf');
        $pdf = $this->pdf->load();
        $pdf->AddPage('L', // L - landscape, P - portrait
                '', '', '', '',
                5, // margin_left
                5, // margin right
                5, // margin top
                5, // margin bottom
                5, // margin header
                5); // margin footer
        // $pdf->SetFooter(''.'|{PAGENO}|'.''); //Add a footer for good measure
        $pdf->WriteHTML($html); //write the HTML into PDF
        $pdf->Output('Bukti Pelaporan Gratifikasi ' . date('d-M-Y', strtotime($data['tanggal'])) .".pdf", 'I');
    }

     public function print_excel(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $this->load->library('excel');
        $dategenerated = date('d-m-Y');
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $show_profil = $this->Model_profil->show_profil($id_user)->row();
        $show_data = $this->Model_gratifikasi->cetak_excel()->result();
//var_dump($show_data); exit;
        //make excel
        $objPHPExcel = new PHPExcel();
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Laporan Gratifikasi Pegawai');
        
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "Tanggal Pelaporan");
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "Nama UPG");
        $objPHPExcel->getActiveSheet()->setCellValue('A3', "Instansi");
        $objPHPExcel->getActiveSheet()->setCellValue('A4', "Unit Kerja");
        $objPHPExcel->getActiveSheet()->setCellValue('A5', "Sub Unit Kerja");
        $objPHPExcel->getActiveSheet()->setCellValue('A6', "Alamat");
        $objPHPExcel->getActiveSheet()->setCellValue('A7', "Provinsi");
        $objPHPExcel->getActiveSheet()->setCellValue('A8', "Kabupaten/Kota");
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ": ".$dategenerated);
        $objPHPExcel->getActiveSheet()->setCellValue('B2', ": ".$show_profil->name);
        $objPHPExcel->getActiveSheet()->setCellValue('B3', ": ".$show_profil->inskerja);
        $objPHPExcel->getActiveSheet()->setCellValue('B4', ": ".$show_profil->biro);
        $objPHPExcel->getActiveSheet()->setCellValue('B5', ": ".$show_profil->bagian);
        $objPHPExcel->getActiveSheet()->setCellValue('B6', ": ".$show_profil->alamat_kantor." Kel:".$show_profil->kelurahan_kantor." Kec:".$show_profil->kecamatan_kantor);
        $objPHPExcel->getActiveSheet()->setCellValue('B7', ": ".$show_profil->nprovinsi_kantor);
        $objPHPExcel->getActiveSheet()->setCellValue('B8', ": ".$show_profil->kota_kantor);        
        $objPHPExcel->getActiveSheet()->getStyle('A1:A8')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('B1:B8')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:A8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,));
        $objPHPExcel->getActiveSheet()->getStyle('B1:B8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,));
        
        $objPHPExcel->getActiveSheet()->setCellValue('A11', "No");
        $objPHPExcel->getActiveSheet()->setCellValue('B11', "Tanggal Pelaporan ke UPG");
        $objPHPExcel->getActiveSheet()->setCellValue('C11', "Nama Lengkap");
        $objPHPExcel->getActiveSheet()->setCellValue('D11', "No NIK / KTP");
        $objPHPExcel->getActiveSheet()->setCellValue('E11', "Tempat Lahir");
        $objPHPExcel->getActiveSheet()->setCellValue('F11', "Tanggal Lahir");
        $objPHPExcel->getActiveSheet()->setCellValue('G11', "Email");
        $objPHPExcel->getActiveSheet()->setCellValue('H11', "No HP");
        $objPHPExcel->getActiveSheet()->setCellValue('I11', "Pangkat");
        $objPHPExcel->getActiveSheet()->setCellValue('J11', "Jabatan");
        $objPHPExcel->getActiveSheet()->setCellValue('K11', "Unit Kerja / Departemen");
        $objPHPExcel->getActiveSheet()->setCellValue('L11', "Sub Unit Kerja");
        $objPHPExcel->getActiveSheet()->setCellValue('M11', "Provinsi");
        $objPHPExcel->getActiveSheet()->setCellValue('N11', "Kabupaten/Kota");
        $objPHPExcel->getActiveSheet()->setCellValue('O11', "Alamat Rumah");
        $objPHPExcel->getActiveSheet()->setCellValue('P11', "Telp Rumah");
        $objPHPExcel->getActiveSheet()->setCellValue('Q11', "Telp Seluler");
        $objPHPExcel->getActiveSheet()->setCellValue('R11', "Jenis Pelaporan");
        $objPHPExcel->getActiveSheet()->setCellValue('S11', "Jenis Peristiwa");
        $objPHPExcel->getActiveSheet()->setCellValue('T11', "Jenis Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('U11', "Uraian Barang");
        $objPHPExcel->getActiveSheet()->setCellValue('V11', "Mata Uang");
        $objPHPExcel->getActiveSheet()->setCellValue('W11', "Nilai Nominal/Tafsiran");
        $objPHPExcel->getActiveSheet()->setCellValue('X11', "Tempat Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('Y11', "Tanggal Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('Z11', "Nama Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('AA11', "Pekerjaan/Jabatan Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('AB11', "Email Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('AC11', "Telp Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('AD11', "Hubungan dengan Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('AE11', "Alasan Pemberian");
        $objPHPExcel->getActiveSheet()->setCellValue('AF11', "Kronologis Pemberian");
        $objPHPExcel->getActiveSheet()->setCellValue('AG11', "Urutan Rincian ke-");
        $objPHPExcel->getActiveSheet()->getStyle('A11:AG11')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A11:AG11')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('FFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A11:AG11')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A11:AG11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A11:AG11')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));

        $rn = 12;
        $i  = 0;
        if (!empty($show_data)):
            foreach ($show_data as $row):
                $i++;
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, $i);
                //$objPHPExcel->getActiveSheet()->setCellValue('B'.$rn, date('d-m-Y',strtotime($row->tgl_laporan)));
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$rn, date('d/m/Y',strtotime($row->tgl_laporan)));
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$rn, $row->name);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$rn, $row->no_ktp, PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$rn, $row->tempatlahir);
                //$objPHPExcel->getActiveSheet()->setCellValue('F'.$rn, $row->tgllahir);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$rn, date('d-m-Y',strtotime($row->tgllahir)));
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$rn, $row->email);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$rn, $row->nohp);
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, $row->pangkat);
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$rn, $row->jabatan);
                $objPHPExcel->getActiveSheet()->setCellValue('K'.$rn, $row->biro);
                $objPHPExcel->getActiveSheet()->setCellValue('L'.$rn, $row->bagian);
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, $row->nprovinsi_kantor);
                $objPHPExcel->getActiveSheet()->setCellValue('N'.$rn, $row->kota_kantor);
                $objPHPExcel->getActiveSheet()->setCellValue('O'.$rn, $row->alamat_rumah." Kel: ".$row->kelurahan_rumah." Kec: ".$row->kecamatan_rumah.", ".$row->kota_rumah." ".$row->nprovinsi_rumah." ".$row->kode_pos_rumah);
                $objPHPExcel->getActiveSheet()->setCellValue('P'.$rn, $row->no_rumah);
                $objPHPExcel->getActiveSheet()->setCellValue('Q'.$rn, $row->nohp);
                if ($row->status_penerimaan == 'tolak'):
                $objPHPExcel->getActiveSheet()->setCellValue('R'.$rn, "Laporan Penolakan Gratifikasi");
                $objPHPExcel->getActiveSheet()->getStyle('R'.$rn)->getFont()->setBold(false)->setSize(11)->getColor()->setRGB('00A65A');
                else:
                $objPHPExcel->getActiveSheet()->setCellValue('R'.$rn, "Laporan Penerimaan Gratifikasi");
                $objPHPExcel->getActiveSheet()->getStyle('R'.$rn)->getFont()->setBold(false)->setSize(11)->getColor()->setRGB('FF0000');
                endif;
                if ($row->nperistiwa_penerimaan == 'Lainnya'):
                $objPHPExcel->getActiveSheet()->setCellValue('S'.$rn, $row->peristiwa_lainnya);
                else:
                $objPHPExcel->getActiveSheet()->setCellValue('S'.$rn, $row->nperistiwa_penerimaan);
                endif;
                $objPHPExcel->getActiveSheet()->setCellValue('T'.$rn, $row->njenis_penerimaan);
                $objPHPExcel->getActiveSheet()->setCellValue('U'.$rn, $row->uraian);
                $objPHPExcel->getActiveSheet()->setCellValue('V'.$rn, $row->mata_uang);
                //$objPHPExcel->getActiveSheet()->setCellValue('W'.$rn, "Rp. ".number_format($row->nilai_nominal, 2, ',', '.'));
                $objPHPExcel->getActiveSheet()->setCellValue('W'.$rn, $row->nilai_nominal);
                $objPHPExcel->getActiveSheet()->setCellValue('X'.$rn, $row->tempat_penerimaan);
                $objPHPExcel->getActiveSheet()->setCellValue('Y'.$rn, date('d-m-Y', strtotime($row->tanggal_penerimaan)));
                $objPHPExcel->getActiveSheet()->setCellValue('Z'.$rn, $row->nama_pemberi);
                $objPHPExcel->getActiveSheet()->setCellValue('AA'.$rn, $row->pekerjaan_pemberi);
                $objPHPExcel->getActiveSheet()->setCellValue('AB'.$rn, $row->email_pemberi);
                $objPHPExcel->getActiveSheet()->setCellValue('AC'.$rn, $row->telepon_pemberi);
                $objPHPExcel->getActiveSheet()->setCellValue('AD'.$rn, $row->hubungan_pemberi);
                $objPHPExcel->getActiveSheet()->setCellValue('AE'.$rn, $row->alasan_pemberian);
                $objPHPExcel->getActiveSheet()->setCellValue('AF'.$rn, $row->kronologi_penerimaan);
                $objPHPExcel->getActiveSheet()->setCellValue('AG'.$rn, $row->urutan);
                $objPHPExcel->getActiveSheet()->getStyle('A12:AG'.$rn)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('A12:AG'.$rn)->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
                $objPHPExcel->getActiveSheet()->getStyle('A12:AG'.$rn)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A12:AG'.$rn)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A12:AG'.$rn)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $rn++;
            endforeach;
        else:
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, "-")
                                          ->setCellValue('B'.$rn, "-")
                                          ->setCellValue('C'.$rn, "-")
                                          ->setCellValueExplicit('D'.$rn, "-")
                                          ->setCellValue('E'.$rn, "-")
                                          ->setCellValue('F'.$rn, "-")
                                          ->setCellValue('G'.$rn, "-")
                                          ->setCellValue('H'.$rn, "-")
                                          ->setCellValue('I'.$rn, "-")
                                          ->setCellValue('J'.$rn, "-")
                                          ->setCellValue('K'.$rn, "-")
                                          ->setCellValue('L'.$rn, "-")
                                          ->setCellValue('M'.$rn, "-")
                                          ->setCellValue('N'.$rn, "-")
                                          ->setCellValue('O'.$rn, "-")
                                          ->setCellValue('P'.$rn, "-")
                                          ->setCellValue('Q'.$rn, "-")
                                          ->setCellValue('R'.$rn, "-")
                                          ->setCellValue('S'.$rn, "-")
                                          ->setCellValue('T'.$rn, "-")
                                          ->setCellValue('U'.$rn, "-")
                                          ->setCellValue('V'.$rn, "-")
                                          ->setCellValue('W'.$rn, "-")
                                          ->setCellValue('X'.$rn, "-")
                                          ->setCellValue('Y'.$rn, "-")
                                          ->setCellValue('Z'.$rn, "-")
                                          ->setCellValue('AA'.$rn, "-")
                                          ->setCellValue('AB'.$rn, "-")
                                          ->setCellValue('AC'.$rn, "-")
                                          ->setCellValue('AD'.$rn, "-")
                                          ->setCellValue('AE'.$rn, "-")
                                          ->setCellValue('AF'.$rn, "-")
                                          ->setCellValue('AG'.$rn, "-");
            $objPHPExcel->getActiveSheet()->getStyle('A12:AG12')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('A12:AG12')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
            $objPHPExcel->getActiveSheet()->getStyle('A12:AG12')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A12:AG12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A12:AG12')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        endif;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(45);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(30);

        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Gratifikasi Pegawai '.$dategenerated.'.xlsx"');
        header('Cache-Control: max-age=0');
        
        
        // Do your stuff here
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        
        // This line will force the file to download
        $objWriter->save('php://output');
    }

    public function print_excel_inspektorat(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $this->load->library('excel');
        $dategenerated = date('d-M-Y');
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $show_profil = $this->Model_profil->show_profil($id_user)->row();
        //$show_data = $this->Model_gratifikasi->cetak_excel_inspektorat()->result();
        $show_data = $this->Model_gratifikasi->cetak_excel_telah_kirim_kpk()->result();
        
        //make excel
        $objPHPExcel = new PHPExcel();
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Laporan Gratifikasi Pegawai');
        
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "Tanggal Pelaporan");
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "Nama UPG");
        $objPHPExcel->getActiveSheet()->setCellValue('A3', "Instansi");
        $objPHPExcel->getActiveSheet()->setCellValue('A4', "Unit Kerja");
        $objPHPExcel->getActiveSheet()->setCellValue('A5', "Sub Unit Kerja");
        $objPHPExcel->getActiveSheet()->setCellValue('A6', "Alamat");
        $objPHPExcel->getActiveSheet()->setCellValue('A7', "Provinsi");
        $objPHPExcel->getActiveSheet()->setCellValue('A8', "Kabupaten/Kota");
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ": ".$dategenerated);
        $objPHPExcel->getActiveSheet()->setCellValue('B2', ": ".$show_profil->name);
        $objPHPExcel->getActiveSheet()->setCellValue('B3', ": ".$show_profil->inskerja);
        $objPHPExcel->getActiveSheet()->setCellValue('B4', ": ".$show_profil->biro);
        $objPHPExcel->getActiveSheet()->setCellValue('B5', ": ".$show_profil->bagian);
        $objPHPExcel->getActiveSheet()->setCellValue('B6', ": ".$show_profil->alamat_kantor." Kel:".$show_profil->kelurahan_kantor." Kec:".$show_profil->kecamatan_kantor);
        $objPHPExcel->getActiveSheet()->setCellValue('B7', ": ".$show_profil->nprovinsi_kantor);
        $objPHPExcel->getActiveSheet()->setCellValue('B8', ": ".$show_profil->kota_kantor);        
        $objPHPExcel->getActiveSheet()->getStyle('A1:A8')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('B1:B8')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:A8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,));
        $objPHPExcel->getActiveSheet()->getStyle('B1:B8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,));
        
        $objPHPExcel->getActiveSheet()->setCellValue('A11', "No");
        $objPHPExcel->getActiveSheet()->setCellValue('B11', "Tanggal Pelaporan ke UPG");
        $objPHPExcel->getActiveSheet()->setCellValue('C11', "Nama Lengkap");
        $objPHPExcel->getActiveSheet()->setCellValue('D11', "No NIK / KTP");
        $objPHPExcel->getActiveSheet()->setCellValue('E11', "Tempat Lahir");
        $objPHPExcel->getActiveSheet()->setCellValue('F11', "Tanggal Lahir");
        $objPHPExcel->getActiveSheet()->setCellValue('G11', "Email");
        $objPHPExcel->getActiveSheet()->setCellValue('H11', "No HP");
        $objPHPExcel->getActiveSheet()->setCellValue('I11', "Pangkat");
        $objPHPExcel->getActiveSheet()->setCellValue('J11', "Jabatan");
        $objPHPExcel->getActiveSheet()->setCellValue('K11', "Unit Kerja / Departemen");
        $objPHPExcel->getActiveSheet()->setCellValue('L11', "Sub Unit Kerja");
        $objPHPExcel->getActiveSheet()->setCellValue('M11', "Provinsi");
        $objPHPExcel->getActiveSheet()->setCellValue('N11', "Kabupaten/Kota");
        $objPHPExcel->getActiveSheet()->setCellValue('O11', "Alamat Rumah");
        $objPHPExcel->getActiveSheet()->setCellValue('P11', "Telp Rumah");
        $objPHPExcel->getActiveSheet()->setCellValue('Q11', "Telp Seluler");
        $objPHPExcel->getActiveSheet()->setCellValue('R11', "Jenis Pelaporan");
        $objPHPExcel->getActiveSheet()->setCellValue('S11', "Jenis Peristiwa");
        $objPHPExcel->getActiveSheet()->setCellValue('T11', "Jenis Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('U11', "Uraian Barang");
        $objPHPExcel->getActiveSheet()->setCellValue('V11', "Mata Uang");
        $objPHPExcel->getActiveSheet()->setCellValue('W11', "Nilai Nominal/Tafsiran");
        $objPHPExcel->getActiveSheet()->setCellValue('X11', "Tempat Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('Y11', "Tanggal Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('Z11', "Nama Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('AA11', "Pekerjaan/Jabatan Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('AB11', "Email Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('AC11', "Telp Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('AD11', "Hubungan dengan Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('AE11', "Alasan Pemberian");
        $objPHPExcel->getActiveSheet()->setCellValue('AF11', "Kronologis Pemberian");
        $objPHPExcel->getActiveSheet()->setCellValue('AG11', "Urutan Rincian ke-");
        $objPHPExcel->getActiveSheet()->setCellValue('AH11', "Status Dokumen");
        $objPHPExcel->getActiveSheet()->getStyle('A11:AH11')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A11:AH11')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('FFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A11:AH11')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A11:AH11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A11:AH11')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));

        $rn = 12;
        $i  = 0;
        if (!empty($show_data)):
            foreach ($show_data as $row):
                $i++;
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, $i);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$rn, date('d-m-Y',strtotime($row->tgl_laporan)));
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$rn, $row->name);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$rn, $row->no_ktp, PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$rn, $row->tempatlahir);
                //$objPHPExcel->getActiveSheet()->setCellValue('F'.$rn, $row->tgllahir);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$rn, date('d-m-Y',strtotime($row->tgllahir)));
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$rn, $row->email);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$rn, $row->nohp);
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, $row->pangkat);
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$rn, $row->jabatan);
                $objPHPExcel->getActiveSheet()->setCellValue('K'.$rn, $row->biro);
                $objPHPExcel->getActiveSheet()->setCellValue('L'.$rn, $row->bagian);
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, $row->nprovinsi_kantor);
                $objPHPExcel->getActiveSheet()->setCellValue('N'.$rn, $row->kota_kantor);
                $objPHPExcel->getActiveSheet()->setCellValue('O'.$rn, $row->alamat_rumah." Kel: ".$row->kelurahan_rumah." Kec: ".$row->kecamatan_rumah.", ".$row->kota_rumah." ".$row->nprovinsi_rumah." ".$row->kode_pos_rumah);
                $objPHPExcel->getActiveSheet()->setCellValue('P'.$rn, $row->no_rumah);
                $objPHPExcel->getActiveSheet()->setCellValue('Q'.$rn, $row->nohp);
                if ($row->status_penerimaan == 'tolak'):
                $objPHPExcel->getActiveSheet()->setCellValue('R'.$rn, "Laporan Penolakan Gratifikasi");
                $objPHPExcel->getActiveSheet()->getStyle('R'.$rn)->getFont()->setBold(false)->setSize(11)->getColor()->setRGB('00A65A');
                else:
                $objPHPExcel->getActiveSheet()->setCellValue('R'.$rn, "Laporan Penerimaan Gratifikasi");
                $objPHPExcel->getActiveSheet()->getStyle('R'.$rn)->getFont()->setBold(false)->setSize(11)->getColor()->setRGB('FF0000');
                endif;
                if ($row->nperistiwa_penerimaan == 'Lainnya'):
                $objPHPExcel->getActiveSheet()->setCellValue('S'.$rn, $row->peristiwa_lainnya);
                else:
                $objPHPExcel->getActiveSheet()->setCellValue('S'.$rn, $row->nperistiwa_penerimaan);
                endif;
                $objPHPExcel->getActiveSheet()->setCellValue('T'.$rn, $row->njenis_penerimaan);
                $objPHPExcel->getActiveSheet()->setCellValue('U'.$rn, $row->uraian);
                $objPHPExcel->getActiveSheet()->setCellValue('V'.$rn, $row->mata_uang);
                //$objPHPExcel->getActiveSheet()->setCellValue('W'.$rn, "Rp. ".number_format($row->nilai_nominal, 2, ',', '.'));
                $objPHPExcel->getActiveSheet()->setCellValue('W'.$rn, $row->nilai_nominal);
                $objPHPExcel->getActiveSheet()->setCellValue('X'.$rn, $row->tempat_penerimaan);
                $objPHPExcel->getActiveSheet()->setCellValue('Y'.$rn, date('d-m-Y', strtotime($row->tanggal_penerimaan)));
                $objPHPExcel->getActiveSheet()->setCellValue('Z'.$rn, $row->nama_pemberi);
                $objPHPExcel->getActiveSheet()->setCellValue('AA'.$rn, $row->pekerjaan_pemberi);
                $objPHPExcel->getActiveSheet()->setCellValue('AB'.$rn, $row->email_pemberi);
                $objPHPExcel->getActiveSheet()->setCellValue('AC'.$rn, $row->telepon_pemberi);
                $objPHPExcel->getActiveSheet()->setCellValue('AD'.$rn, $row->hubungan_pemberi);
                $objPHPExcel->getActiveSheet()->setCellValue('AE'.$rn, $row->alasan_pemberian);
                $objPHPExcel->getActiveSheet()->setCellValue('AF'.$rn, $row->kronologi_penerimaan);
                $objPHPExcel->getActiveSheet()->setCellValue('AG'.$rn, $row->urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('AH'.$rn, $row->nama_status_dokumen);
                $objPHPExcel->getActiveSheet()->getStyle('A12:AH'.$rn)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('A12:AH'.$rn)->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
                $objPHPExcel->getActiveSheet()->getStyle('A12:AH'.$rn)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A12:AH'.$rn)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A12:AH'.$rn)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $rn++;
            endforeach;
        else:
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, "-")
                                          ->setCellValue('B'.$rn, "-")
                                          ->setCellValue('C'.$rn, "-")
                                          ->setCellValueExplicit('D'.$rn, "-")
                                          ->setCellValue('E'.$rn, "-")
                                          ->setCellValue('F'.$rn, "-")
                                          ->setCellValue('G'.$rn, "-")
                                          ->setCellValue('H'.$rn, "-")
                                          ->setCellValue('I'.$rn, "-")
                                          ->setCellValue('J'.$rn, "-")
                                          ->setCellValue('K'.$rn, "-")
                                          ->setCellValue('L'.$rn, "-")
                                          ->setCellValue('M'.$rn, "-")
                                          ->setCellValue('N'.$rn, "-")
                                          ->setCellValue('O'.$rn, "-")
                                          ->setCellValue('P'.$rn, "-")
                                          ->setCellValue('Q'.$rn, "-")
                                          ->setCellValue('R'.$rn, "-")
                                          ->setCellValue('S'.$rn, "-")
                                          ->setCellValue('T'.$rn, "-")
                                          ->setCellValue('U'.$rn, "-")
                                          ->setCellValue('V'.$rn, "-")
                                          ->setCellValue('W'.$rn, "-")
                                          ->setCellValue('X'.$rn, "-")
                                          ->setCellValue('Y'.$rn, "-")
                                          ->setCellValue('Z'.$rn, "-")
                                          ->setCellValue('AA'.$rn, "-")
                                          ->setCellValue('AB'.$rn, "-")
                                          ->setCellValue('AC'.$rn, "-")
                                          ->setCellValue('AD'.$rn, "-")
                                          ->setCellValue('AE'.$rn, "-")
                                          ->setCellValue('AF'.$rn, "-")
                                          ->setCellValue('AG'.$rn, "-")
                                          ->setCellValue('AH'.$rn, "-");;
            $objPHPExcel->getActiveSheet()->getStyle('A12:AH12')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('A12:AH12')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
            $objPHPExcel->getActiveSheet()->getStyle('A12:AH12')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A12:AH12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A12:AH12')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        endif;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(45);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(30);

        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Daftar Laporan Gratifikasi Telah Kirim KPK '.$dategenerated.'.xlsx"');
        header('Cache-Control: max-age=0');
        
        
        // Do your stuff here
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        
        // This line will force the file to download
        $objWriter->save('php://output');
    }

    public function rekomendasi_gratifikasi() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEG'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data['rekomendasi_peg'] = $this->Model_gratifikasi->get_data_rekomendasi_peg($id_user);
        $data['count_rekomendasi_peg'] = $this->Model_gratifikasi->get_count_rekomendasi_peg($id_user);
        $data['rekomendasi'] = $this->Model_gratifikasi->get_data_rekomendasi();
        $data['count_rekomendasi'] = $this->Model_gratifikasi->get_count_rekomendasi();
        $data['tagmenu'] = 'rekomendasi_gratifikasi';
        $data['body'] = 'gratifikasi/view_rekomendasi_gratifikasi';
        $this->load->vars($data);
        $this->load->view('view_main');
    }

    public function rekomendasi() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $now = date('Y-m-d H:i:s');
        $id_gratifikasi = strip_tags(trim($_POST['id_gratifikasi']));

        $data_non = array(
            'tgl_rekomendasi' => $now,
            'status_dokumen' => '7',
        );
        $data = $this->security->xss_clean($data_non);
        $update = $this->Model_gratifikasi->update($id_gratifikasi,$data); 

        //tabel file
        if (!empty($_FILES['filesatu']['name'])):
            $file_name = $_FILES['filesatu']['name'];
            $file_tmp = $_FILES['filesatu']['tmp_name'];
            $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
            $path = $this->config->item('path_file_rekomendasi');
            $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
            move_uploaded_file($file_tmp, $lokasi);
            $datafilesatu_non = array(
                'id_gratifikasi' => $id_gratifikasi,
                'files' => $name,
                'nama_files' => $file_name,
                'tanggal_upload' => $now
            );
            $datafilesatu = $this->security->xss_clean($datafilesatu_non);
            $insert_file_rekomendasi = $this->Model_gratifikasi->insert_file_rekomendasi($datafilesatu);
        endif;

        if (!empty($_FILES['file']['name'])):
            for ($j = 0; $j < count($_FILES["file"]['name']); $j++):
                $file_name = $_FILES['file']['name']["$j"];
                $file_tmp = $_FILES['file']['tmp_name']["$j"];
                $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                $path = $this->config->item('path_file_rekomendasi');
                $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                move_uploaded_file($file_tmp, $lokasi);
                $datafile_non = array(
                    'id_gratifikasi' => $id_gratifikasi,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                );
                $datafile = $this->security->xss_clean($datafile_non);
                $insert_file_rekomendasi = $this->Model_gratifikasi->insert_file_rekomendasi($datafile);
            endfor;
        endif;

        if ($update == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Rekomendasi berhasil disimpan.</div>');
            redirect('gratifikasi/rekomendasi_gratifikasi');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Rekomendasi gagal disimpan.</div>');
            redirect('gratifikasi/rekomendasi_gratifikasi');
        }
    }

    public function download_rekomendasi(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEG'));
        $this->load->helper('download');
        $id = strip_tags(trim($this->uri->segment(3)));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(4)));
        $show = $this->Model_gratifikasi->getfile_rekomendasi_byid($id)->row();

        if (count($show) > 0) {
            $files = $show->files;
            $nama_files = $show->nama_files;
            $path = $this->config->item('path_file_rekomendasi');
            ob_clean();
            $lokasi = file_get_contents($path . $files);
            force_download($nama_files, $lokasi);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-remove"></i>Gagal!</strong> Data tidak ditemukan.</div>');
            redirect('gratifikasi/detail_verifikasi/'.$id_gratifikasi);
        }
    }

    public function tindak_lanjut_gratifikasi() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEG'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data['tindak_lanjut'] = $this->Model_gratifikasi->get_data_tindak_lanjut($id_user);
        $data['count_tindak_lanjut'] = $this->Model_gratifikasi->get_count_tindak_lanjut($id_user);
        $data['selesai_peg'] = $this->Model_gratifikasi->get_data_selesai_peg($id_user);
        $data['count_selesai_peg'] = $this->Model_gratifikasi->get_count_selesai_peg($id_user);
        $data['selesai'] = $this->Model_gratifikasi->get_data_selesai();
        $data['count_selesai'] = $this->Model_gratifikasi->get_count_selesai();
        $data['tagmenu'] = 'tindak_lanjut_gratifikasi';
        $data['body'] = 'gratifikasi/view_tindak_lanjut_gratifikasi';
        $this->load->vars($data);
        $this->load->view('view_main');
    }

    public function tindak_lanjut() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $now = date('Y-m-d H:i:s');
        $id_gratifikasi = strip_tags(trim($_POST['id_gratifikasi']));

        $data_non = array(
            'tindak_lanjut' => strip_tags(trim($_POST['tindak_lanjut'])),
            'tgl_tindak_lanjut' => $now,
            'status_dokumen' => '8',
        );
        $data = $this->security->xss_clean($data_non);
        $update = $this->Model_gratifikasi->update($id_gratifikasi,$data); 

        //tabel file
        if (!empty($_FILES['filesatu']['name'])):
            $file_name = $_FILES['filesatu']['name'];
            $file_tmp = $_FILES['filesatu']['tmp_name'];
            $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
            $path = $this->config->item('path_file_tindak_lanjut');
            $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
            move_uploaded_file($file_tmp, $lokasi);
            $datafilesatu_non = array(
                'id_gratifikasi' => $id_gratifikasi,
                'files' => $name,
                'nama_files' => $file_name,
                'tanggal_upload' => $now
            );
            $datafilesatu = $this->security->xss_clean($datafilesatu_non);
            $insert_file_tindak_lanjut = $this->Model_gratifikasi->insert_file_tindak_lanjut($datafilesatu);
        endif;

        if (!empty($_FILES['file']['name'])):
            for ($j = 0; $j < count($_FILES["file"]['name']); $j++):
                $file_name = $_FILES['file']['name']["$j"];
                $file_tmp = $_FILES['file']['tmp_name']["$j"];
                $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                $path = $this->config->item('path_file_tindak_lanjut');
                $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                move_uploaded_file($file_tmp, $lokasi);
                $datafile_non = array(
                    'id_gratifikasi' => $id_gratifikasi,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                );
                $datafile = $this->security->xss_clean($datafile_non);
                $insert_file_tindak_lanjut = $this->Model_gratifikasi->insert_file_tindak_lanjut($datafile);
            endfor;
        endif;

        if ($update == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Tindak Lanjut berhasil disimpan.</div>');
            redirect('gratifikasi/tindak_lanjut_gratifikasi');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Tindak Lanjut gagal disimpan.</div>');
            redirect('gratifikasi/tindak_lanjut_gratifikasi');
        }
    }

    public function download_tindak_lanjut(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEG'));
        $this->load->helper('download');
        $id = strip_tags(trim($this->uri->segment(3)));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(4)));
        $show = $this->Model_gratifikasi->getfile_tindak_lanjut_byid($id)->row();

        if (count($show) > 0) {
            $files = $show->files;
            $nama_files = $show->nama_files;
            $path = $this->config->item('path_file_tindak_lanjut');
            ob_clean();
            $lokasi = file_get_contents($path . $files);
            force_download($nama_files, $lokasi);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-remove"></i>Gagal!</strong> Data tidak ditemukan.</div>');
            redirect('gratifikasi/detail_verifikasi/'.$id_gratifikasi);
        }
    }

    public function ringkasan_laporan() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSMEN'));
        $data['total_laporan'] = $this->Model_gratifikasi->get_total_laporan();
        $data['total_laporan_penolakan'] = $this->Model_gratifikasi->get_total_laporan_penolakan();
        $data['total_laporan_penerimaan'] = $this->Model_gratifikasi->get_total_laporan_penerimaan();
        $data['total_tahap_penanganan'] = $this->Model_gratifikasi->get_total_tahap_penanganan();
        $data['sum_tahap_penanganan'] = $this->Model_gratifikasi->get_sum_total_tahap_penanganan();
        $data['nominal_tahap_penanganan'] = $this->Model_gratifikasi->get_nominal_tahap_penanganan();
        $data['sum_nominal_penanganan'] = $this->Model_gratifikasi->get_sum_nominal_tahap_penanganan();
        $data['tagmenu'] = 'ringkasan_laporan';
        $data['body'] = 'gratifikasi/view_ringkasan_laporan';
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function print_excel_riwayat(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $this->load->library('excel');
        $dategenerated = date('d-M-Y');
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $show_profil = $this->Model_profil->show_profil($id_user)->row();
        $show_riwayat = $this->Model_gratifikasi->cetak_excel_riwayat()->result();
		//var_dump($show_riwayat); exit;
        
        //make excel
        $objPHPExcel = new PHPExcel();
        //$this->excel->setActiveSheetIndex(0);
        //$this->excel->getActiveSheet()->setTitle('Laporan Riwayat Gratifikasi Pegawai');
        
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "Tanggal Pelaporan");
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "Nama UPG");
        $objPHPExcel->getActiveSheet()->setCellValue('A3', "Instansi");
        $objPHPExcel->getActiveSheet()->setCellValue('A4', "Unit Kerja");
        $objPHPExcel->getActiveSheet()->setCellValue('A5', "Sub Unit Kerja");
        $objPHPExcel->getActiveSheet()->setCellValue('A6', "Alamat");
        $objPHPExcel->getActiveSheet()->setCellValue('A7', "Provinsi");
        $objPHPExcel->getActiveSheet()->setCellValue('A8', "Kabupaten/Kota");
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ": ".$dategenerated);
        $objPHPExcel->getActiveSheet()->setCellValue('B2', ": ".$show_profil->name);
        $objPHPExcel->getActiveSheet()->setCellValue('B3', ": ".$show_profil->inskerja);
        $objPHPExcel->getActiveSheet()->setCellValue('B4', ": ".$show_profil->biro);
        $objPHPExcel->getActiveSheet()->setCellValue('B5', ": ".$show_profil->bagian);
        $objPHPExcel->getActiveSheet()->setCellValue('B6', ": ".$show_profil->alamat_kantor." Kel:".$show_profil->kelurahan_kantor." Kec:".$show_profil->kecamatan_kantor);
        $objPHPExcel->getActiveSheet()->setCellValue('B7', ": ".$show_profil->nprovinsi_kantor);
        $objPHPExcel->getActiveSheet()->setCellValue('B8', ": ".$show_profil->kota_kantor);        
        $objPHPExcel->getActiveSheet()->getStyle('A1:A8')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('B1:B8')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:A8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,));
        $objPHPExcel->getActiveSheet()->getStyle('B1:B8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,));
        
        $objPHPExcel->getActiveSheet()->setCellValue('A11', "No");
        $objPHPExcel->getActiveSheet()->setCellValue('B11', "Tanggal Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('C11', "Jenis Laporan");
        $objPHPExcel->getActiveSheet()->setCellValue('D11', "No Laporan");
        $objPHPExcel->getActiveSheet()->setCellValue('E11', "Nama Penerima");
        $objPHPExcel->getActiveSheet()->setCellValue('F11', "Jenis Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('G11', "Nilai Nominal (Rp.)");
        $objPHPExcel->getActiveSheet()->setCellValue('H11', "Nama Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('I11', "Status");
      
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('FFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));

        $rn = 12;
        $i  = 0;
        if (!empty($show_riwayat)):
            foreach ($show_riwayat as $row):
                $i++;
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, $i);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$rn, date('d-M-Y',strtotime($row->tanggal_penerimaan)));
                //$objPHPExcel->getActiveSheet()->setCellValue('C'.$rn, $row->status_penerimaan);
				if ($row->status_penerimaan == "tolak"):
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$rn, "Laporan Penolakan Gratifikasi");
				else:
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$rn, "Laporan Penerimaan Gratifikasi");
				endif;
				
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$rn, $row->nomor_laporan);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$rn, $row->nama_penerima);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$rn, $row->njenis_penerimaan);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$rn, $row->nilai_nominal);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$rn, $row->nama_pemberi);
                //$objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, $row->status_dokumen);
				if ($row->status_dokumen == 1):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Draft");
                elseif ($row->status_dokumen == 2):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Belum Direview");
				elseif ($row->status_dokumen == 3):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Revisi");
				elseif ($row->status_dokumen == 4):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Sudah Revisi");
				elseif ($row->status_dokumen == 5):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Telah Direview");
				elseif ($row->status_dokumen == 6):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Dikirim KPK");
				elseif ($row->status_dokumen == 7):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "SK Rekomendasi");
				else:
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Tindak Lanjut");
				endif;
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $rn++;
            endforeach;
        else:
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, "-")
                                          ->setCellValue('B'.$rn, "-")
                                          ->setCellValue('C'.$rn, "-")
                                          ->setCellValue('D'.$rn, "-")
                                          ->setCellValue('E'.$rn, "-")
                                          ->setCellValue('F'.$rn, "-")
                                          ->setCellValue('G'.$rn, "-")
                                          ->setCellValue('H'.$rn, "-")
                                          ->setCellValue('I'.$rn, "-");;
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        endif;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);

        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Inspektorat - Riwayat Gratifikasi '.$dategenerated.'.xlsx"');
        header('Cache-Control: max-age=0');
        
        
        // Do your stuff here
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        
        // This line will force the file to download
        $objWriter->save('php://output');
    }
	
	public function insert_laporan_tahunan() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $now = date('Y-m-d H:i:s');
        $id_laporan_terakhir = $this->Model_gratifikasi->get_id_laporan_terakhir();
        $id_gratifikasi = $id_laporan_terakhir + 1;
		$tahun = date('Y');
        $nomor_laporan = "$id_gratifikasi/LT/$tahun";
        
        $data_non = array(
                'id' => $id_gratifikasi,
                'id_user' => strip_tags(trim($this->session->userdata('id'))),
                'nmr_laporan' => $nomor_laporan,
                'status_laporan' => strip_tags(trim($_POST['status_laporan'])),
                'tempat_laporan' => strip_tags(trim($_POST['tempat_laporan'])),
                'tgl_laporan' => $now
            );
            $data = $this->security->xss_clean($data_non);
            $insert = $this->Model_gratifikasi->insert_laporan($data); 


        if ($insert == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Laporan Anda berhasil disimpan.</div>');
            redirect('beranda');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Laporan Anda gagal disimpan.</div>');
            redirect('beranda');
        }
    }
	
	public function detail_laporan_tahunan() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $data['status_penerimaan'] = strip_tags(trim($this->uri->segment(3)));
		$id_laporan = strip_tags(trim($this->uri->segment(3)));
        $id_user = strip_tags(trim($this->session->userdata('id', TRUE)));
        $data['show_profil'] = $this->Model_profil->show_profil($id_user)->row();
        $data['provinsi'] = $this->Model_profil->getprovinsi();
        $data['user'] = $this->Model_gratifikasi->getuser($id_user);
        $data['identitas'] = $this->Model_gratifikasi->getidentitas();
        $data['jenis_penerimaan'] = $this->Model_gratifikasi->getjenis_penerimaan();
        $data['peristiwa_penerimaan'] = $this->Model_gratifikasi->getperistiwa_penerimaan();
        $data['alamat_pengirim'] = $this->Model_gratifikasi->getalamat_pengirim();
		
		$data['show_riwayat'] = $this->Model_gratifikasi->laporan_tahunan()->result();
		$data['laporan'] = $this->Model_gratifikasi->get_data_laporan($id_user,$id_laporan);
		
        $data['tanggal'] = date('Y-m-d');
        $data['tagmenu'] = 'laporan_tahunan';
        $data['body'] = 'gratifikasi/view_detail_laporan_tahunan';
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function detail_laporan_tahunan_inspektorat() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSMEN'));
        $data['status_penerimaan'] = strip_tags(trim($this->uri->segment(3)));
		$id_laporan = strip_tags(trim($this->uri->segment(3)));
        $id_user = strip_tags(trim($this->session->userdata('id', TRUE)));
        //$data['show_profil'] = $this->Model_profil->show_profil($id_user)->row();
        $data['show_profil'] = $this->Model_profil->show_profil_inspektorat($id_laporan)->row();
        $data['provinsi'] = $this->Model_profil->getprovinsi();
        $data['user'] = $this->Model_gratifikasi->getuser($id_user);
        $data['identitas'] = $this->Model_gratifikasi->getidentitas();
        $data['jenis_penerimaan'] = $this->Model_gratifikasi->getjenis_penerimaan();
        $data['peristiwa_penerimaan'] = $this->Model_gratifikasi->getperistiwa_penerimaan();
        $data['alamat_pengirim'] = $this->Model_gratifikasi->getalamat_pengirim();
		
		$data['show_riwayat'] = $this->Model_gratifikasi->laporan_tahunan()->result();
		$data['laporan'] = $this->Model_gratifikasi->get_data_detail_laporan_inspektorat($id_laporan);
		
        $data['tanggal'] = date('Y-m-d');
        $data['tagmenu'] = 'laporan_tahunan';
        $data['body'] = 'gratifikasi/view_detail_laporan_tahunan_inspektorat';
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	public function riwayat_gratifikasi_inspektorat() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSMEN'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data['draft'] = $this->Model_gratifikasi->get_data_draft($id_user);
        $data['count_draft'] = $this->Model_gratifikasi->get_count_draft($id_user);
        $data['blm_verified'] = $this->Model_gratifikasi->get_data_blm_verified_peg($id_user);
        $data['count_blm_verified'] = $this->Model_gratifikasi->get_count_blm_verified_peg($id_user);
        $data['revisi'] = $this->Model_gratifikasi->get_data_revisi_peg($id_user);
        $data['count_revisi'] = $this->Model_gratifikasi->get_count_revisi_peg($id_user);
        $data['verified'] = $this->Model_gratifikasi->get_data_verified_peg($id_user);
        $data['count_verified'] = $this->Model_gratifikasi->get_count_verified_peg($id_user);
		
		$data['laporan'] = $this->Model_gratifikasi->get_data_laporan_inspektorat($id_user);
        $data['count_laporan'] = $this->Model_gratifikasi->get_count_laporan_inspektorat($id_user);
		
        $data['tagmenu'] = 'riwayat_gratifikasi';
        $data['body'] = 'gratifikasi/view_riwayat_gratifikasi_inspektorat';
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function verifikasi_gratifikasi_inspektorat() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data['blm_verified'] = $this->Model_gratifikasi->get_data_blm_verified();
        $data['count_blm_verified'] = $this->Model_gratifikasi->get_count_blm_verified();
        $data['proses_revisi'] = $this->Model_gratifikasi->get_data_proses_revisi();
        $data['count_proses_revisi'] = $this->Model_gratifikasi->get_count_proses_revisi();
        $data['revisi'] = $this->Model_gratifikasi->get_data_revisi();
        $data['count_revisi'] = $this->Model_gratifikasi->get_count_revisi();
        $data['verified'] = $this->Model_gratifikasi->get_data_verified();
        $data['count_verified'] = $this->Model_gratifikasi->get_count_verified();
        $data['terkirim'] = $this->Model_gratifikasi->get_data_terkirim();
        $data['count_terkirim'] = $this->Model_gratifikasi->get_count_terkirim();
        $data['riwayat'] = $this->Model_gratifikasi->get_data_riwayat_all();
        $data['count_riwayat'] = $this->Model_gratifikasi->get_count_riwayat_all();
        $data['tagmenu'] = 'verifikasi_gratifikasi';
        $data['body'] = 'gratifikasi/view_verifikasi_gratifikasi_inspektorat';
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function print_excel_riwayat_inspektorat(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $this->load->library('excel');
        $dategenerated = date('d-M-Y');
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $show_profil = $this->Model_profil->show_profil($id_user)->row();
        $show_riwayat = $this->Model_gratifikasi->cetak_excel_riwayat()->result();
		//var_dump($show_riwayat); exit;
        
        //make excel
        $objPHPExcel = new PHPExcel();
        //$this->excel->setActiveSheetIndex(0);
        //$this->excel->getActiveSheet()->setTitle('Laporan Riwayat Gratifikasi Pegawai');
        
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "Tanggal Pelaporan");
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "Nama UPG");
        $objPHPExcel->getActiveSheet()->setCellValue('A3', "Instansi");
        $objPHPExcel->getActiveSheet()->setCellValue('A4', "Unit Kerja");
        $objPHPExcel->getActiveSheet()->setCellValue('A5', "Sub Unit Kerja");
        $objPHPExcel->getActiveSheet()->setCellValue('A6', "Alamat");
        $objPHPExcel->getActiveSheet()->setCellValue('A7', "Provinsi");
        $objPHPExcel->getActiveSheet()->setCellValue('A8', "Kabupaten/Kota");
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ": ".$dategenerated);
        $objPHPExcel->getActiveSheet()->setCellValue('B2', ": ".$show_profil->name);
        $objPHPExcel->getActiveSheet()->setCellValue('B3', ": ".$show_profil->inskerja);
        $objPHPExcel->getActiveSheet()->setCellValue('B4', ": ".$show_profil->biro);
        $objPHPExcel->getActiveSheet()->setCellValue('B5', ": ".$show_profil->bagian);
        $objPHPExcel->getActiveSheet()->setCellValue('B6', ": ".$show_profil->alamat_kantor." Kel:".$show_profil->kelurahan_kantor." Kec:".$show_profil->kecamatan_kantor);
        $objPHPExcel->getActiveSheet()->setCellValue('B7', ": ".$show_profil->nprovinsi_kantor);
        $objPHPExcel->getActiveSheet()->setCellValue('B8', ": ".$show_profil->kota_kantor);        
        $objPHPExcel->getActiveSheet()->getStyle('A1:A8')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('B1:B8')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:A8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,));
        $objPHPExcel->getActiveSheet()->getStyle('B1:B8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,));
        
        $objPHPExcel->getActiveSheet()->setCellValue('A11', "No");
        $objPHPExcel->getActiveSheet()->setCellValue('B11', "Tanggal Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('C11', "Jenis Laporan");
        $objPHPExcel->getActiveSheet()->setCellValue('D11', "No Laporan");
        $objPHPExcel->getActiveSheet()->setCellValue('E11', "Nama Penerima");
        $objPHPExcel->getActiveSheet()->setCellValue('F11', "Jenis Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('G11', "Nilai Nominal (Rp.)");
        $objPHPExcel->getActiveSheet()->setCellValue('H11', "Nama Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('I11', "Status");
      
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('FFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));

        $rn = 12;
        $i  = 0;
        if (!empty($show_riwayat)):
            foreach ($show_riwayat as $row):
                $i++;
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, $i);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$rn, date('d-M-Y',strtotime($row->tanggal_penerimaan)));
                //$objPHPExcel->getActiveSheet()->setCellValue('C'.$rn, $row->status_penerimaan);
				if ($row->status_penerimaan == "tolak"):
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$rn, "Laporan Penolakan Gratifikasi");
				else:
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$rn, "Laporan Penerimaan Gratifikasi");
				endif;
				
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$rn, $row->nomor_laporan);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$rn, $row->nama_penerima);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$rn, $row->njenis_penerimaan);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$rn, $row->nilai_nominal);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$rn, $row->nama_pemberi);
                //$objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, $row->status_dokumen);
				if ($row->status_dokumen == 1):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Draft");
                elseif ($row->status_dokumen == 2):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Belum Direview");
				elseif ($row->status_dokumen == 3):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Revisi");
				elseif ($row->status_dokumen == 4):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Sudah Revisi");
				elseif ($row->status_dokumen == 5):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Telah Direview");
				elseif ($row->status_dokumen == 6):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Dikirim KPK");
				elseif ($row->status_dokumen == 7):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "SK Rekomendasi");
				else:
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Tindak Lanjut");
				endif;
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $rn++;
            endforeach;
        else:
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, "-")
                                          ->setCellValue('B'.$rn, "-")
                                          ->setCellValue('C'.$rn, "-")
                                          ->setCellValue('D'.$rn, "-")
                                          ->setCellValue('E'.$rn, "-")
                                          ->setCellValue('F'.$rn, "-")
                                          ->setCellValue('G'.$rn, "-")
                                          ->setCellValue('H'.$rn, "-")
                                          ->setCellValue('I'.$rn, "-");;
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        endif;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);

        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Inspektorat - Riwayat Gratifikasi '.$dategenerated.'.xlsx"');
        header('Cache-Control: max-age=0');
        
        
        // Do your stuff here
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        
        // This line will force the file to download
        $objWriter->save('php://output');
    }
	
	public function verifikasi_kpk_inspektorat() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data['blm_verified'] = $this->Model_gratifikasi->get_data_blm_verified();
        $data['count_blm_verified'] = $this->Model_gratifikasi->get_count_blm_verified();
        $data['proses_revisi'] = $this->Model_gratifikasi->get_data_proses_revisi();
        $data['count_proses_revisi'] = $this->Model_gratifikasi->get_count_proses_revisi();
        $data['revisi'] = $this->Model_gratifikasi->get_data_revisi();
        $data['count_revisi'] = $this->Model_gratifikasi->get_count_revisi();
        $data['verified'] = $this->Model_gratifikasi->get_data_verified();
        $data['count_verified'] = $this->Model_gratifikasi->get_count_verified();
        $data['terkirim'] = $this->Model_gratifikasi->get_data_terkirim();
        $data['count_terkirim'] = $this->Model_gratifikasi->get_count_terkirim();
        $data['riwayat'] = $this->Model_gratifikasi->get_data_riwayat();
        $data['count_riwayat'] = $this->Model_gratifikasi->get_count_riwayat();
        $data['count_riwayat_verifikasi'] = $this->Model_gratifikasi->get_count_riwayat_verifikasi();
        $data['tagmenu'] = 'verifikasi_gratifikasi';
        $data['body'] = 'gratifikasi/view_verifikasi_kpk_inspektorat';
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function detail_verifikasi_kpk() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(3)));
		$nomor_laporan = strip_tags(trim($this->uri->segment(3))).'/'.strip_tags(trim($this->uri->segment(4))).'/'.strip_tags(trim($this->uri->segment(5)));
		
        $data['id_gratifikasi'] = $id_gratifikasi;
        $data['show_profil'] = $this->Model_gratifikasi->show_pelapor($id_gratifikasi)->row();
        $data['show_data'] = $this->Model_gratifikasi->show_data($id_gratifikasi)->row();
        $data['file_gratifikasi'] = $this->Model_gratifikasi->getfile_gratifikasi($id_gratifikasi);
        $data['verifikasi'] = $this->Model_gratifikasi->get_verifikasi($id_gratifikasi);
        $data['file_verifikasi'] = $this->Model_gratifikasi->getfile_verifikasi($id_gratifikasi);
        $data['file_rekomendasi'] = $this->Model_gratifikasi->getfile_rekomendasi($id_gratifikasi);
        $data['file_tindak_lanjut'] = $this->Model_gratifikasi->getfile_tindak_lanjut($id_gratifikasi);
        $data['kronologis_verifikasi'] = $this->Model_gratifikasi->get_kronologis_verifikasi($nomor_laporan);
		
        $data['tagmenu'] = 'riwayat_gratifikasi';
        $data['body'] = 'gratifikasi/view_form_update_verifikasi_kpk';
        $this->load->vars($data);
        $this->load->view('view_main');
    }

	public function update_verifikasi_kpk() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $now = date('Y-m-d H:i:s');
        $id_verifikasi_terakhir = $this->Model_gratifikasi->get_id_verifikasi_terakhir();
        $id_verifikasi = $id_verifikasi_terakhir + 1;
        
        $data_non = array(
                'id' => $id_verifikasi,
                'no_laporan_gratifikasi' => strip_tags(trim($_POST['nomor_laporan'])),
                'kronologis' => strip_tags(trim($_POST['kronologis_verifikasi'])),
                'tgl_input_verifikasi' => $now
            );
            $data = $this->security->xss_clean($data_non);
            $insert = $this->Model_gratifikasi->insert_verifikasi_kpk($data); 

            //tabel file
            if (!empty($_FILES['filesatu']['name'])):
                $file_name = $_FILES['filesatu']['name'];
                $file_tmp = $_FILES['filesatu']['tmp_name'];
                $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                //$path = $this->config->item('path_file_gratifikasi');
                $path = $this->config->item('path_file_verifikasi_kpk');
                $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                move_uploaded_file($file_tmp, $lokasi);
                $datafilesatu_non = array(
                    'id_verifikasi' => $id_verifikasi,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                );
                $datafilesatu = $this->security->xss_clean($datafilesatu_non);
                $insert_file = $this->Model_gratifikasi->insert_file_verifikasi_kpk($datafilesatu);
            endif;

            if (!empty($_FILES['file']['name'])):
                for ($j = 0; $j < count($_FILES["file"]['name']); $j++):
                    $file_name = $_FILES['file']['name']["$j"];
                    $file_tmp = $_FILES['file']['tmp_name']["$j"];
                    $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                    //$path = $this->config->item('path_file_gratifikasi');
                    $path = $this->config->item('path_file_verifikasi_kpk');
                    $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                    move_uploaded_file($file_tmp, $lokasi);
                    $datafile_non = array(
                    'id_verifikasi' => $id_verifikasi,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                    );
                    $datafile = $this->security->xss_clean($datafile_non);
                    $insert_file = $this->Model_gratifikasi->insert_file_verifikasi_kpk($datafile);
                endfor;
            endif; 

        if ($insert == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Data Anda berhasil disimpan.</div>');
            redirect('gratifikasi/verifikasi_kpk_inspektorat');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Data Anda gagal disimpan.</div>');
            redirect('gratifikasi/verifikasi_kpk_inspektorat');
        }
    }
	
	public function print_excel_riwayat_peg(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_PEGAWAI'));
        $this->load->library('excel');
        $dategenerated = date('d-M-Y');
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $show_profil = $this->Model_profil->show_profil($id_user)->row();
        $show_riwayat = $this->Model_gratifikasi->cetak_excel_riwayat()->result();
		//var_dump($show_riwayat); exit;
        
        //make excel
        $objPHPExcel = new PHPExcel();
        //$this->excel->setActiveSheetIndex(0);
        //$this->excel->getActiveSheet()->setTitle('Laporan Riwayat Gratifikasi Pegawai');
        
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "Tanggal Download");
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "Nama");
        $objPHPExcel->getActiveSheet()->setCellValue('A3', "Instansi");
        $objPHPExcel->getActiveSheet()->setCellValue('A4', "Unit Kerja");
        $objPHPExcel->getActiveSheet()->setCellValue('A5', "Sub Unit Kerja");
        $objPHPExcel->getActiveSheet()->setCellValue('A6', "Alamat Kantor");
        $objPHPExcel->getActiveSheet()->setCellValue('A7', "Provinsi");
        $objPHPExcel->getActiveSheet()->setCellValue('A8', "Kabupaten/Kota");
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ": ".$dategenerated);
        $objPHPExcel->getActiveSheet()->setCellValue('B2', ": ".$show_profil->name);
        $objPHPExcel->getActiveSheet()->setCellValue('B3', ": ".$show_profil->inskerja);
        $objPHPExcel->getActiveSheet()->setCellValue('B4', ": ".$show_profil->biro);
        $objPHPExcel->getActiveSheet()->setCellValue('B5', ": ".$show_profil->bagian);
        $objPHPExcel->getActiveSheet()->setCellValue('B6', ": ".$show_profil->alamat_kantor." Kel:".$show_profil->kelurahan_kantor." Kec:".$show_profil->kecamatan_kantor);
        $objPHPExcel->getActiveSheet()->setCellValue('B7', ": ".$show_profil->nprovinsi_kantor);
        $objPHPExcel->getActiveSheet()->setCellValue('B8', ": ".$show_profil->kota_kantor);        
        $objPHPExcel->getActiveSheet()->getStyle('A1:A8')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('B1:B8')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:A8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,));
        $objPHPExcel->getActiveSheet()->getStyle('B1:B8')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,));
        
        $objPHPExcel->getActiveSheet()->setCellValue('A11', "No");
        $objPHPExcel->getActiveSheet()->setCellValue('B11', "Tanggal Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('C11', "Jenis Laporan");
        $objPHPExcel->getActiveSheet()->setCellValue('D11', "No Laporan");
        $objPHPExcel->getActiveSheet()->setCellValue('E11', "Nama Penerima");
        $objPHPExcel->getActiveSheet()->setCellValue('F11', "Jenis Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('G11', "Nilai Nominal (Rp.)");
        $objPHPExcel->getActiveSheet()->setCellValue('H11', "Nama Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('I11', "Status");
      
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('FFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A11:I11')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));

        $rn = 12;
        $i  = 0;
        if (!empty($show_riwayat)):
            foreach ($show_riwayat as $row):
                $i++;
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, $i);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$rn, date('d-M-Y',strtotime($row->tanggal_penerimaan)));
                //$objPHPExcel->getActiveSheet()->setCellValue('C'.$rn, $row->status_penerimaan);
				if ($row->status_penerimaan == "tolak"):
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$rn, "Laporan Penolakan Gratifikasi");
				else:
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$rn, "Laporan Penerimaan Gratifikasi");
				endif;
				
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$rn, $row->nomor_laporan);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$rn, $row->nama_penerima);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$rn, $row->njenis_penerimaan);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$rn, $row->nilai_nominal);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$rn, $row->nama_pemberi);
                //$objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, $row->status_dokumen);
				if ($row->status_dokumen == 1):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Draft");
                elseif ($row->status_dokumen == 2):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Belum Direview");
				elseif ($row->status_dokumen == 3):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Revisi");
				elseif ($row->status_dokumen == 4):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Sudah Revisi");
				elseif ($row->status_dokumen == 5):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Telah Direview");
				elseif ($row->status_dokumen == 6):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Dikirim KPK");
				elseif ($row->status_dokumen == 7):
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "SK Rekomendasi");
				else:
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, "Tindak Lanjut");
				endif;
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A12:I'.$rn)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $rn++;
            endforeach;
        else:
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, "-")
                                          ->setCellValue('B'.$rn, "-")
                                          ->setCellValue('C'.$rn, "-")
                                          ->setCellValue('D'.$rn, "-")
                                          ->setCellValue('E'.$rn, "-")
                                          ->setCellValue('F'.$rn, "-")
                                          ->setCellValue('G'.$rn, "-")
                                          ->setCellValue('H'.$rn, "-")
                                          ->setCellValue('I'.$rn, "-");;
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A12:I12')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        endif;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);

        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="User - Riwayat Gratifikasi '.$dategenerated.'.xlsx"');
        header('Cache-Control: max-age=0');
        
        
        // Do your stuff here
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        
        // This line will force the file to download
        $objWriter->save('php://output');
    }
	
	public function download_verifikasi_kpk(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEG'));
        $this->load->helper('download');
        $id = strip_tags(trim($this->uri->segment(3)));
        //$id_gratifikasi = strip_tags(trim($this->uri->segment(4)));
		$nama_files = strip_tags(trim($this->uri->segment(3)));
		$nomor_laporan = strip_tags(trim($this->uri->segment(3))).'/'.strip_tags(trim($this->uri->segment(4))).'/'.strip_tags(trim($this->uri->segment(5)));
		
        $show = $this->Model_gratifikasi->getfile_verifikasi_byid($nama_files)->row();

        if (count($show) > 0) {
            $files = $show->files;
            $nama_files = $show->nama_files;
            $path = $this->config->item('path_file_verifikasi_kpk');
            ob_clean();
            $lokasi = file_get_contents($path . $files);
            force_download($nama_files, $lokasi);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-remove"></i>Gagal!</strong> Data tidak ditemukan.</div>');
            redirect('gratifikasi/detail_verifikasi_kpk/'.$nomor_laporan);
        }
    }
	
	public function info_pasca_tindak_lanjut() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data['blm_verified'] = $this->Model_gratifikasi->get_data_blm_verified();
        $data['count_blm_verified'] = $this->Model_gratifikasi->get_count_blm_verified();
        $data['proses_revisi'] = $this->Model_gratifikasi->get_data_proses_revisi();
        $data['count_proses_revisi'] = $this->Model_gratifikasi->get_count_proses_revisi();
        $data['revisi'] = $this->Model_gratifikasi->get_data_revisi();
        $data['count_revisi'] = $this->Model_gratifikasi->get_count_revisi();
        $data['verified'] = $this->Model_gratifikasi->get_data_verified();
        $data['count_verified'] = $this->Model_gratifikasi->get_count_verified();
        $data['terkirim'] = $this->Model_gratifikasi->get_data_terkirim();
        $data['count_terkirim'] = $this->Model_gratifikasi->get_count_terkirim();
        $data['riwayat'] = $this->Model_gratifikasi->get_data_riwayat();
        $data['count_riwayat'] = $this->Model_gratifikasi->get_count_riwayat();
        $data['count_riwayat_verifikasi'] = $this->Model_gratifikasi->get_count_riwayat_verifikasi();
        $data['count_pasca_tindak_lanjut'] = $this->Model_gratifikasi->get_count_pasca_tindak_lanjut();
        $data['tagmenu'] = 'verifikasi_gratifikasi';
        $data['body'] = 'gratifikasi/view_info_pasca_tindak_lanjut';
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function detail_tindak_lanjut() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $id_gratifikasi = strip_tags(trim($this->uri->segment(3)));
		$nomor_laporan = strip_tags(trim($this->uri->segment(3))).'/'.strip_tags(trim($this->uri->segment(4))).'/'.strip_tags(trim($this->uri->segment(5)));
		
        $data['id_gratifikasi'] = $id_gratifikasi;
        $data['show_profil'] = $this->Model_gratifikasi->show_pelapor($id_gratifikasi)->row();
        $data['show_data'] = $this->Model_gratifikasi->show_data($id_gratifikasi)->row();
        $data['file_gratifikasi'] = $this->Model_gratifikasi->getfile_gratifikasi($id_gratifikasi);
        $data['verifikasi'] = $this->Model_gratifikasi->get_verifikasi($id_gratifikasi);
        $data['file_verifikasi'] = $this->Model_gratifikasi->getfile_verifikasi($id_gratifikasi);
        $data['file_rekomendasi'] = $this->Model_gratifikasi->getfile_rekomendasi($id_gratifikasi);
        $data['file_tindak_lanjut'] = $this->Model_gratifikasi->getfile_tindak_lanjut($id_gratifikasi);
        $data['kronologis_verifikasi'] = $this->Model_gratifikasi->get_info_pasca_tindak_lanjut($nomor_laporan);
		
        $data['tagmenu'] = 'riwayat_gratifikasi';
        $data['body'] = 'gratifikasi/view_form_tindak_lanjut';
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function update_pasca_tindak_lanjut() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_INSPEKTORAT'));
        $now = date('Y-m-d H:i:s');
        $id_pasca_terakhir = $this->Model_gratifikasi->get_id_pascatindaklanjut_terakhir();
        $id_pasca = $id_pasca_terakhir + 1;
        
        $data_non = array(
                'id' => $id_pasca,
                'no_laporan_gratifikasi' => strip_tags(trim($_POST['nomor_laporan'])),
                'catatan' => strip_tags(trim($_POST['catatan'])),
                'tgl_input' => $now
            );
            $data = $this->security->xss_clean($data_non);
            $insert = $this->Model_gratifikasi->insert_pasca_tindak_lanjut($data); 

            //tabel file
            if (!empty($_FILES['filesatu']['name'])):
                $file_name = $_FILES['filesatu']['name'];
                $file_tmp = $_FILES['filesatu']['tmp_name'];
                $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                //$path = $this->config->item('path_file_gratifikasi');
                $path = $this->config->item('path_file_pasca_tindak_lanjut');
                $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                move_uploaded_file($file_tmp, $lokasi);
                $datafilesatu_non = array(
                    'id_pasca_tindak_lanjut' => $id_pasca,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                );
                $datafilesatu = $this->security->xss_clean($datafilesatu_non);
                $insert_file = $this->Model_gratifikasi->insert_file_pasca_tindak_lanjut($datafilesatu);
            endif;

            if (!empty($_FILES['file']['name'])):
                for ($j = 0; $j < count($_FILES["file"]['name']); $j++):
                    $file_name = $_FILES['file']['name']["$j"];
                    $file_tmp = $_FILES['file']['tmp_name']["$j"];
                    $name = date('Y-m-d') . '_' . time() . '_' . $file_name;
                    //$path = $this->config->item('path_file_gratifikasi');
                    $path = $this->config->item('path_file_pasca_tindak_lanjut');
                    $lokasi = $path . date('Y-m-d') . '_' . time() . '_' . $file_name;
                    move_uploaded_file($file_tmp, $lokasi);
                    $datafile_non = array(
                    'id_pasca_tindak_lanjut' => $id_pasca,
                    'files' => $name,
                    'nama_files' => $file_name,
                    'tanggal_upload' => $now
                    );
                    $datafile = $this->security->xss_clean($datafile_non);
                    $insert_file = $this->Model_gratifikasi->insert_file_pasca_tindak_lanjut($datafile);
                endfor;
            endif; 

        if ($insert == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Data Anda berhasil disimpan.</div>');
            redirect('gratifikasi/info_pasca_tindak_lanjut');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Data Anda gagal disimpan.</div>');
            redirect('gratifikasi/info_pasca_tindak_lanjut');
        }
    }
	
	public function detail_penolakan_gratifikasi_menteri() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_MENTERI'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data['blm_verified'] = $this->Model_gratifikasi->get_data_blm_verified();
        $data['count_blm_verified'] = $this->Model_gratifikasi->get_count_blm_verified();
        $data['proses_revisi'] = $this->Model_gratifikasi->get_data_proses_revisi();
        $data['count_proses_revisi'] = $this->Model_gratifikasi->get_count_proses_revisi();
        $data['revisi'] = $this->Model_gratifikasi->get_data_revisi();
        $data['count_revisi'] = $this->Model_gratifikasi->get_count_revisi();
        $data['verified'] = $this->Model_gratifikasi->get_data_verified();
        $data['count_verified'] = $this->Model_gratifikasi->get_count_verified();
        $data['terkirim'] = $this->Model_gratifikasi->get_data_terkirim();
        $data['count_terkirim'] = $this->Model_gratifikasi->get_count_terkirim();
        $data['riwayat'] = $this->Model_gratifikasi->get_data_riwayat_penolakan();
        $data['count_riwayat_penolakan'] = $this->Model_gratifikasi->get_count_riwayat_penolakan();
        $data['tagmenu'] = 'detail_gratifikasi';
        $data['body'] = 'gratifikasi/view_detail_penolakan_gratifikasi_menteri';
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function detail_penerimaan_gratifikasi_menteri() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_MENTERI'));
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $data['blm_verified'] = $this->Model_gratifikasi->get_data_blm_verified();
        $data['count_blm_verified'] = $this->Model_gratifikasi->get_count_blm_verified();
        $data['proses_revisi'] = $this->Model_gratifikasi->get_data_proses_revisi();
        $data['count_proses_revisi'] = $this->Model_gratifikasi->get_count_proses_revisi();
        $data['revisi'] = $this->Model_gratifikasi->get_data_revisi();
        $data['count_revisi'] = $this->Model_gratifikasi->get_count_revisi();
        $data['verified'] = $this->Model_gratifikasi->get_data_verified();
        $data['count_verified'] = $this->Model_gratifikasi->get_count_verified();
        $data['terkirim'] = $this->Model_gratifikasi->get_data_terkirim();
        $data['count_terkirim'] = $this->Model_gratifikasi->get_count_terkirim();
        $data['riwayat'] = $this->Model_gratifikasi->get_data_riwayat_penerimaan();
        $data['count_riwayat_penerimaan'] = $this->Model_gratifikasi->get_count_riwayat_penerimaan();
        $data['tagmenu'] = 'detail_gratifikasi';
        $data['body'] = 'gratifikasi/view_detail_penerimaan_gratifikasi_menteri';
        $this->load->vars($data);
        $this->load->view('view_main');
    }
	
	public function print_excel_detail_penolakan_menteri(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_MENTERI'));
        $this->load->library('excel');
        $dategenerated = date('d-M-Y');
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $show_profil = $this->Model_profil->show_profil($id_user)->row();
        $show_riwayat = $this->Model_gratifikasi->cetak_excel_riwayat_penolakan()->result();
		
        //make excel
        $objPHPExcel = new PHPExcel();
        
        
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "Rekapitulasi Laporan Penolakan");
        $objPHPExcel->getActiveSheet()->mergeCells('A1:M1');
                
        $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
		$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getFont()->setBold(true)->setSize(14)->getColor()->setRGB('000000');
        
        $objPHPExcel->getActiveSheet()->setCellValue('A3', "No");
        $objPHPExcel->getActiveSheet()->setCellValue('B3', "Jenis Laporan");
        $objPHPExcel->getActiveSheet()->setCellValue('C3', "Nomor Laporan");
        $objPHPExcel->getActiveSheet()->setCellValue('D3', "Tanggal Laporan");
        $objPHPExcel->getActiveSheet()->setCellValue('E3', "Nama Penerima");
        $objPHPExcel->getActiveSheet()->setCellValue('F3', "Nama Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('G3', "Nilai Nominal (Rp.)");
        $objPHPExcel->getActiveSheet()->setCellValue('H3', "Jenis Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('I3', "Tanggal Review");
        $objPHPExcel->getActiveSheet()->setCellValue('J3', "Tanggal Dikirim KPK");
        $objPHPExcel->getActiveSheet()->setCellValue('K3', "Tanggal Rekomendasi");
        $objPHPExcel->getActiveSheet()->setCellValue('L3', "Tanggal Tindak Lanjut");
        $objPHPExcel->getActiveSheet()->setCellValue('M3', "Status");
      
        $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('FFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));

        $rn = 4;
        $i  = 0;
        if (!empty($show_riwayat)):
            foreach ($show_riwayat as $row):
                $i++;
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, $i);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$rn, $row->status_penerimaan);
				if ($row->status_penerimaan == "tolak"):
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$rn, "Laporan Penolakan Gratifikasi");
				else:
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$rn, "Laporan Penerimaan Gratifikasi");
				endif;
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$rn, $row->nomor_laporan);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$rn, date('d-M-Y',strtotime($row->tanggal_penerimaan)));
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$rn, $row->nama_penerima);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$rn, $row->nama_pemberi);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$rn, $row->nilai_nominal);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$rn, $row->njenis_penerimaan);
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, $row->tgl_verifikasi);
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$rn, $row->tgl_dikirim);
                $objPHPExcel->getActiveSheet()->setCellValue('K'.$rn, $row->tgl_rekomendasi);
                $objPHPExcel->getActiveSheet()->setCellValue('L'.$rn, $row->tgl_tindak_lanjut);            
                
				if ($row->status_dokumen == 1):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Draft");
                elseif ($row->status_dokumen == 2):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Belum Direview");
				elseif ($row->status_dokumen == 3):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Revisi");
				elseif ($row->status_dokumen == 4):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Sudah Revisi");
				elseif ($row->status_dokumen == 5):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Telah Direview");
				elseif ($row->status_dokumen == 6):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Dikirim KPK");
				elseif ($row->status_dokumen == 7):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "SK Rekomendasi");
				else:
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Tindak Lanjut");
				endif;
                $objPHPExcel->getActiveSheet()->getStyle('A4:M4'.$rn)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('A4:M4'.$rn)->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
                $objPHPExcel->getActiveSheet()->getStyle('A4:M4'.$rn)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A4:M4'.$rn)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A4:M4'.$rn)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $rn++;
            endforeach;
        else:
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, "-")
                                          ->setCellValue('B'.$rn, "-")
                                          ->setCellValue('C'.$rn, "-")
                                          ->setCellValue('D'.$rn, "-")
                                          ->setCellValue('E'.$rn, "-")
                                          ->setCellValue('F'.$rn, "-")
                                          ->setCellValue('G'.$rn, "-")
                                          ->setCellValue('H'.$rn, "-")
                                          ->setCellValue('I'.$rn, "-")
                                          ->setCellValue('J'.$rn, "-")
                                          ->setCellValue('K'.$rn, "-")
                                          ->setCellValue('L'.$rn, "-")
                                          ->setCellValue('M'.$rn, "-");
            $objPHPExcel->getActiveSheet()->getStyle('A4:M4')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('A4:M4')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
            $objPHPExcel->getActiveSheet()->getStyle('A4:M4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A4:M4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A4:M4')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        endif;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);

        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Detail Laporan Penolakan Gratifikasi '.$dategenerated.'.xlsx"');
        header('Cache-Control: max-age=0');
        
        
        // Do your stuff here
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        
        // This line will force the file to download
        $objWriter->save('php://output');
    }
	
	public function print_excel_detail_penerimaan_menteri(){
        $this->auth->isprivilegectrl($this->config->item('AKSES_MENTERI'));
        $this->load->library('excel');
        $dategenerated = date('d-M-Y');
        $id_user = strip_tags(trim($this->session->userdata('id')));
        $show_profil = $this->Model_profil->show_profil($id_user)->row();
        $show_riwayat = $this->Model_gratifikasi->cetak_excel_riwayat_penerimaan()->result();
		
        //make excel
        $objPHPExcel = new PHPExcel();
        
        
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "Rekapitulasi Laporan Penerimaan");
        $objPHPExcel->getActiveSheet()->mergeCells('A1:M1');
                
        $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
		$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getFont()->setBold(true)->setSize(14)->getColor()->setRGB('000000');
        
        $objPHPExcel->getActiveSheet()->setCellValue('A3', "No");
        $objPHPExcel->getActiveSheet()->setCellValue('B3', "Jenis Laporan");
        $objPHPExcel->getActiveSheet()->setCellValue('C3', "Nomor Laporan");
        $objPHPExcel->getActiveSheet()->setCellValue('D3', "Tanggal Laporan");
        $objPHPExcel->getActiveSheet()->setCellValue('E3', "Nama Penerima");
        $objPHPExcel->getActiveSheet()->setCellValue('F3', "Nama Pemberi");
        $objPHPExcel->getActiveSheet()->setCellValue('G3', "Nilai Nominal (Rp.)");
        $objPHPExcel->getActiveSheet()->setCellValue('H3', "Jenis Penerimaan");
        $objPHPExcel->getActiveSheet()->setCellValue('I3', "Tanggal Review");
        $objPHPExcel->getActiveSheet()->setCellValue('J3', "Tanggal Dikirim KPK");
        $objPHPExcel->getActiveSheet()->setCellValue('K3', "Tanggal Rekomendasi");
        $objPHPExcel->getActiveSheet()->setCellValue('L3', "Tanggal Tindak Lanjut");
        $objPHPExcel->getActiveSheet()->setCellValue('M3', "Status");
      
        $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('FFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));

        $rn = 4;
        $i  = 0;
        if (!empty($show_riwayat)):
            foreach ($show_riwayat as $row):
                $i++;
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, $i);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$rn, $row->status_penerimaan);
				if ($row->status_penerimaan == "tolak"):
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$rn, "Laporan Penolakan Gratifikasi");
				else:
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$rn, "Laporan Penerimaan Gratifikasi");
				endif;
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$rn, $row->nomor_laporan);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$rn, date('d-M-Y',strtotime($row->tanggal_penerimaan)));
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$rn, $row->nama_penerima);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$rn, $row->nama_pemberi);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$rn, $row->nilai_nominal);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$rn, $row->njenis_penerimaan);
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$rn, $row->tgl_verifikasi);
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$rn, $row->tgl_dikirim);
                $objPHPExcel->getActiveSheet()->setCellValue('K'.$rn, $row->tgl_rekomendasi);
                $objPHPExcel->getActiveSheet()->setCellValue('L'.$rn, $row->tgl_tindak_lanjut);            
                
				if ($row->status_dokumen == 1):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Draft");
                elseif ($row->status_dokumen == 2):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Belum Direview");
				elseif ($row->status_dokumen == 3):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Revisi");
				elseif ($row->status_dokumen == 4):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Sudah Revisi");
				elseif ($row->status_dokumen == 5):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Telah Direview");
				elseif ($row->status_dokumen == 6):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Dikirim KPK");
				elseif ($row->status_dokumen == 7):
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "SK Rekomendasi");
				else:
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$rn, "Tindak Lanjut");
				endif;
                $objPHPExcel->getActiveSheet()->getStyle('A4:M4'.$rn)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('A4:M4'.$rn)->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
                $objPHPExcel->getActiveSheet()->getStyle('A4:M4'.$rn)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A4:M4'.$rn)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A4:M4'.$rn)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $rn++;
            endforeach;
        else:
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rn, "-")
                                          ->setCellValue('B'.$rn, "-")
                                          ->setCellValue('C'.$rn, "-")
                                          ->setCellValue('D'.$rn, "-")
                                          ->setCellValue('E'.$rn, "-")
                                          ->setCellValue('F'.$rn, "-")
                                          ->setCellValue('G'.$rn, "-")
                                          ->setCellValue('H'.$rn, "-")
                                          ->setCellValue('I'.$rn, "-")
                                          ->setCellValue('J'.$rn, "-")
                                          ->setCellValue('K'.$rn, "-")
                                          ->setCellValue('L'.$rn, "-")
                                          ->setCellValue('M'.$rn, "-");
            $objPHPExcel->getActiveSheet()->getStyle('A4:M4')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('A4:M4')->getFont()->setBold(true)->setSize(11)->getColor()->setRGB('000000');
            $objPHPExcel->getActiveSheet()->getStyle('A4:M4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A4:M4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A4:M4')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        endif;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);

        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Detail Laporan Penerimaan Gratifikasi '.$dategenerated.'.xlsx"');
        header('Cache-Control: max-age=0');
        
        
        // Do your stuff here
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        
        // This line will force the file to download
        $objWriter->save('php://output');
    }
}
