<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_gratifikasi extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_data_draft_beranda($id_user){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.jenis_dokumen = ? and a.id_penerima = ? and a.identitas = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('draft', $id_user, 'Sebagai Pelapor Gratifikasi'));
        return $Q->result();
    }

    function get_data_draft($id_user){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.jenis_dokumen = ? and a.id_penerima = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('draft', $id_user));
        return $Q->result();
    }

    function get_count_draft($id_user){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ? AND id_penerima = ?";

        $fetch = $this->db->query($queryString, array('draft', array('1'), '0', $id_user));
        return $fetch->row(); 
    }

    function get_data_blm_verified(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', '2'));
        return $Q->result();
    }

    function get_count_blm_verified(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ?";

        $fetch = $this->db->query($queryString, array('send', array('2'), '0'));
        return $fetch->row();  
    }

    function get_data_blm_verified_peg($id_user){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen IN ? and a.id_penerima = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('2','4'), $id_user));
        return $Q->result();
    }

    function get_count_blm_verified_peg($id_user){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ? AND id_penerima = ?";

        $fetch = $this->db->query($queryString, array('send', array('2','4'), '0', $id_user));
        return $fetch->row(); 
    }

    function get_data_revisi_peg($id_user){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen = ? and a.id_penerima = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('3'), $id_user));
        return $Q->result();
    }

    function get_count_revisi_peg($id_user){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ? AND id_penerima = ?";

        $fetch = $this->db->query($queryString, array('send', array('3'), '0', $id_user));
        return $fetch->row(); 
    }

    function get_data_proses_revisi(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('3')));
        return $Q->result();
    }

    function get_count_proses_revisi(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ?";

        $fetch = $this->db->query($queryString, array('send', array('3'), '0'));
        return $fetch->row(); 
    }

    function get_data_revisi(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('4')));
        return $Q->result();
    }

    function get_count_revisi(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ?";

        $fetch = $this->db->query($queryString, array('send', array('4'), '0'));
        return $fetch->row(); 
    }

    function get_data_verified_peg($id_user){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen = ? and a.id_penerima = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('5'), $id_user));
        return $Q->result();
    }

    function get_count_verified_peg($id_user){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ? AND id_penerima = ?";

        $fetch = $this->db->query($queryString, array('send', array('5'), '0', $id_user));
        return $fetch->row(); 
    }

    function get_data_verified(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('5')));
        return $Q->result();
    }

    function get_count_verified(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ?";

        $fetch = $this->db->query($queryString, array('send', array('5'), '0'));
        return $fetch->row(); 
    }

    function get_data_verified_today($now){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen = ? and a.tgl_verifikasi = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('5'), $now));
        return $Q->result();
    }

    /*function get_data_terkirim(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('6')));
        return $Q->result();
    }*/
	
	function get_data_terkirim(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = 'send' and a.status_dokumen = '6'
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('6')));
        return $Q->result();
    }

    function get_count_terkirim(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ?";

        $fetch = $this->db->query($queryString, array('send', array('6'), '0'));
        return $fetch->row(); 
    }

    function get_data_rekomendasi_peg($id_user){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen = ? and a.id_penerima = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('6'), $id_user));
        return $Q->result();
    }

    function get_count_rekomendasi_peg($id_user){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ? AND id_penerima = ?";

        $fetch = $this->db->query($queryString, array('send', array('6'), '0', $id_user));
        return $fetch->row(); 
    }

    function get_data_rekomendasi(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('7')));
        return $Q->result();
    }

    function get_count_rekomendasi(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ?";

        $fetch = $this->db->query($queryString, array('send', array('7'), '0'));
        return $fetch->row(); 
    }

    function get_data_tindak_lanjut($id_user){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen = ? and a.id_penerima = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('7'), $id_user));
        return $Q->result();
    }

    function get_count_tindak_lanjut($id_user){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ? and id_penerima = ?";

        $fetch = $this->db->query($queryString, array('send', array('7'), '0', $id_user));
        return $fetch->row();  
    }

    function get_data_selesai_peg($id_user){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen = ? and a.id_penerima = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('8'), $id_user));
        return $Q->result();
    }

    function get_count_selesai_peg($id_user){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ? and id_penerima = ?";

        $fetch = $this->db->query($queryString, array('send', array('8'), '0', $id_user));
        return $fetch->row(); 
    }


    function get_data_selesai(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null and a.jenis_dokumen = ? and a.status_dokumen = ?
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('8')));
        return $Q->result();
    }

    function get_count_selesai(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE jenis_dokumen = ? and status_dokumen IN ? AND baca = ?";

        $fetch = $this->db->query($queryString, array('send', array('8'), '0'));
        return $fetch->row(); 
    }

    function show_data($a){
        $this->db->select('a.*, a.id as id_gratifikasi, b.*, c.*, c.id as id_penerima, d.nama as nalamat_pengiriman, e.jenis_penerimaan as njenis_penerimaan, f.peristiwa_penerimaan as nperistiwa_penerimaan, g.nama as nidentitas, h.name as nprovinsi_kantor, i.name as nprovinsi_rumah');
        $this->db->from('grf_gratifikasi a');
        $this->db->join('grf_file_gratifikasi b','a.id = b.id_gratifikasi','LEFT');
        $this->db->join('conf_user c','a.id_penerima = c.id','LEFT');
        $this->db->join('mst_alamat_pengirim d','d.id = c.alamat_pengiriman','LEFT');
        $this->db->join('mst_jenis_penerimaan e','a.jenis_penerimaan = e.id','LEFT');
        $this->db->join('mst_peristiwa_penerimaan f','a.kode_peristiwa = f.id','LEFT');
        $this->db->join('mst_identitas g','a.identitas = g.id','LEFT');
        $this->db->join('mst_provinsi h','c.provinsi_kantor = h.id','LEFT');
        $this->db->join('mst_provinsi i','c.provinsi_rumah = i.id','LEFT');
        $this->db->where('a.id', $a);

        return $this->db->get();
    }

    function show_profil($a){
        $this->db->select('a.*, b.nama, c.name as nprovinsi_kantor, d.name as nprovinsi_rumah');
        $this->db->from('conf_user a');
        $this->db->join('mst_alamat_pengirim b','a.alamat_pengiriman = b.id','LEFT');
        $this->db->join('mst_provinsi c','a.provinsi_kantor = c.id','LEFT');
        $this->db->join('mst_provinsi d','a.provinsi_rumah = d.id','LEFT');
        $this->db->join('grf_gratifikasi e','a.id = e.id_penerima','LEFT');
        $this->db->where('e.id', $a);

        return $this->db->get();
    }

    function show_pelapor($a){
        $this->db->select('a.*, b.nama, c.name as nprovinsi_kantor, d.name as nprovinsi_rumah');
        $this->db->from('conf_user a');
        $this->db->join('mst_alamat_pengirim b','a.alamat_pengiriman = b.id','LEFT');
        $this->db->join('mst_provinsi c','a.provinsi_kantor = c.id','LEFT');
        $this->db->join('mst_provinsi d','a.provinsi_rumah = d.id','LEFT');
        $this->db->join('grf_gratifikasi e','a.id = e.id_pelapor','LEFT');
        $this->db->where('e.id', $a);

        return $this->db->get();
    }

    function show_penerima($id){
        $sql = "SELECT a.*, b.nama, c.name as nprovinsi_kantor, d.name as nprovinsi_rumah
                FROM conf_user a
                LEFT JOIN mst_alamat_pengirim b ON a.alamat_pengiriman = b.id
                LEFT JOIN mst_provinsi c ON a.provinsi_kantor = c.id
                LEFT JOIN mst_provinsi d ON a.provinsi_kantor = d.id
                WHERE a.id = ? LIMIT 1";
       
        $Q = $this->db->query($sql, array($id));
        return $Q->result();
        
    }

    public function getfile_gratifikasi($id){
        $sql = "SELECT * FROM grf_file_gratifikasi WHERE id_gratifikasi = ?";
       
        $fetch = $this->db->query($sql, array($id));
        return $fetch ? $fetch->result() : NULL;
    }

    function getfile_gratifikasi_byid($id)
    {
        $this->db->select('*');
        $this->db->from('grf_file_gratifikasi');
        $this->db->where('id', $id);

        return $this->db->get();
    }

     public function get_verifikasi($id){
        $sql = "SELECT * FROM grf_ctt_verifikasi WHERE id_gratifikasi = ?";
       
        $fetch = $this->db->query($sql, array($id));
        return $fetch ? $fetch->result() : NULL;
    }

    public function getfile_verifikasi($id){
        $sql = "SELECT * FROM grf_file_verifikasi WHERE id_gratifikasi = ?";
       
        $fetch = $this->db->query($sql, array($id));
        return $fetch ? $fetch->result() : NULL;
    }

    function getfile_verifikasi_byid($id)
    {
        $this->db->select('*');
        $this->db->from('grf_file_verifikasi');
        $this->db->where('id', $id);

        return $this->db->get();
    }

    public function getfile_rekomendasi($id){
       $sql = "SELECT * FROM grf_file_rekomendasi WHERE id_gratifikasi = ?";
       
        $fetch = $this->db->query($sql, array($id));
        return $fetch ? $fetch->result() : NULL;
    }

    function getfile_rekomendasi_byid($id)
    {
        $this->db->select('*');
        $this->db->from('grf_file_rekomendasi');
        $this->db->where('id', $id);

        return $this->db->get();
    }

    public function getfile_tindak_lanjut($id){
        $sql = "SELECT * FROM grf_file_tindak_lanjut WHERE id_gratifikasi = ?";
       
        $fetch = $this->db->query($sql, array($id));
        return $fetch ? $fetch->result() : NULL;
    }

    function getfile_tindak_lanjut_byid($id)
    {
        $this->db->select('*');
        $this->db->from('grf_file_tindak_lanjut');
        $this->db->where('id', $id);

        return $this->db->get();
    }

    function getalamat_pengirim() {
        $data = array();

        $sql = "SELECT * FROM mst_alamat_pengirim ";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }

    function getuser($id_user) {
        $data = array();

        $sql = "SELECT * FROM conf_user WHERE isldap = '1' AND username != 'admin' AND id != '$id_user' ORDER BY username ASC";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }
    
   function getidentitas() {
        $data = array();

        $sql = "SELECT * FROM mst_identitas ";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }

    function getjenis_penerimaan() {
        $data = array();

        $sql = "SELECT * FROM mst_jenis_penerimaan ";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }

    function getperistiwa_penerimaan() {
        $data = array();

        $sql = "SELECT * FROM mst_peristiwa_penerimaan ";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }

    function getpenilaian() {
        $data = array();

        $sql = "SELECT * FROM mst_penilaian ";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }

    function get_id_terakhir(){
        $data = array();
        $sql = "SELECT id FROM grf_gratifikasi ORDER BY id DESC limit 1";
        $q = $this->db->query($sql);
        
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        } else{
            $data['id'] = 0;
        }
        $q->free_result();
        return $data['id'];
    }

    function insert($data){
        $res = $this->db->insert('grf_gratifikasi', $data);
        if (!$res) {
            return 'proses gagal';
        } else {
            return 'sukses';
        }
    }

    function insert_file($data){
        $res = $this->db->insert('grf_file_gratifikasi', $data);
        if (!$res) {
            return 'proses gagal';
        } else {
            return 'sukses';
        }
    }

    function insert_verifikasi($data){
        $res = $this->db->insert('grf_ctt_verifikasi', $data);
        if (!$res) {
            return 'proses gagal';
        } else {
            return 'sukses';
        }
    }

    function insert_file_verifikasi($data){
        $res = $this->db->insert('grf_file_verifikasi', $data);
        if (!$res) {
            return 'proses gagal';
        } else {
            return 'sukses';
        }
    }

    function insert_file_rekomendasi($data){
        $res = $this->db->insert('grf_file_rekomendasi', $data);

        if (!$res) {
            return 'proses gagal';
        } else {
            return 'sukses';
        }
    }

    function insert_file_tindak_lanjut($data){
        $res = $this->db->insert('grf_file_tindak_lanjut', $data);

        if (!$res) {
            return 'proses gagal';
        } else {
            return 'sukses';
        }
    }

    function update($id_gratifikasi,$data){
        $res = $this->db->where('id', $id_gratifikasi)
                        ->update('grf_gratifikasi', $data);

        if (!$res):
            return 'proses gagal';
        else:
            return 'sukses';
        endif;
    }

    function update_penerima($id_gratifikasi,$data){
        $res = $this->db->where('id_gratifikasi', $id_gratifikasi)
                        ->update('grf_penerima', $data);

        if (!$res):
            return 'proses gagal';
        else:
            return 'sukses';
        endif;
    }

    function kirim_kpk($data){
        $res = $this->db->where('status_dokumen', '5')
                        ->update('grf_gratifikasi', $data);

        if (!$res):
            return 'proses gagal';
        else:
            return 'sukses';
        endif;
    }

    public function deletefile_gratifikasi_byid($id) {
        $res = $this->db->delete('grf_file_gratifikasi', array('id' => $id));

        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }

    public function delete_gratifikasi_by_id($id) {
        $res = $this->db->delete('grf_gratifikasi', array('id' => $id));
        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }

    public function delete_penerima_byid($id) {
        $res = $this->db->delete('grf_penerima', array('id_gratifikasi' => $id));
        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }

    //chart
    /*function get_total_laporan_penolakan(){
        $sql = "SELECT id, COUNT('id') AS jumlah, YEAR(tgl_laporan) as tahun
                        FROM grf_gratifikasi
                        WHERE id is not null AND status_penerimaan = ? AND status_dokumen IN ?
                        GROUP BY YEAR(tgl_laporan)";
       
        $Q = $this->db->query($sql, array('tolak', array('6','7','8')));
        return $Q->result();
    }*/
	function get_total_laporan_penolakan(){
        $sql = "SELECT id, COUNT('id') AS jumlah, YEAR(tgl_laporan) as tahun, status_penerimaan as status
                        FROM grf_gratifikasi
                        WHERE id is not null AND status_penerimaan = 'tolak'
                        GROUP BY YEAR(tgl_laporan)";
       
        $Q = $this->db->query($sql, array('tolak', array('6','7','8')));
        return $Q->result();
    }

    /*function get_total_laporan_penerimaan(){
        $sql = "SELECT id, COUNT('id') AS jumlah, YEAR(tgl_laporan) as tahun
                        FROM grf_gratifikasi
                        WHERE id is not null AND status_penerimaan = ? AND status_dokumen IN ?
                        GROUP BY YEAR(tgl_laporan)";
       
        $Q = $this->db->query($sql, array('terima', array('6','7','8')));
        return $Q->result();
    }*/
	function get_total_laporan_penerimaan(){
        $sql = "SELECT id, COUNT('id') AS jumlah, YEAR(tgl_laporan) as tahun, status_penerimaan as status
                        FROM grf_gratifikasi
                        WHERE id is not null AND status_penerimaan = 'terima'
                        GROUP BY YEAR(tgl_laporan)";
       
        $Q = $this->db->query($sql, array('terima', array('6','7','8')));
        return $Q->result();
    }

    /*function get_total_tahap_penanganan(){
        $sql = "SELECT *
                FROM(
                    SELECT a.id, a.nama_status_dokumen, COALESCE(COUNT(c.id),0) AS jumlah
                    FROM mst_status_dokumen a
                    LEFT JOIN grf_gratifikasi c on a.id = c.status_dokumen
                    GROUP BY a.id
                    UNION
                    SELECT a.id, a.nama_status_dokumen, 0 as jumlah
                    FROM mst_status_dokumen a
                    LEFT JOIN grf_gratifikasi c on a.id = c.status_dokumen) asd
                WHERE asd.id IS NOT NULL AND asd.id NOT IN ?
                GROUP BY asd.id
                ORDER BY asd.id ASC";
       
        $Q = $this->db->query($sql, array(array('3')));
        return $Q->result();
    }*/
	function get_total_tahap_penanganan(){
        $sql = "SELECT *
                FROM(
                    SELECT a.id, a.nama_status_dokumen, COALESCE(COUNT(c.id),0) AS jumlah
                    FROM mst_status_dokumen a
                    LEFT JOIN grf_gratifikasi c on a.id = c.status_dokumen
                    GROUP BY a.id
                    UNION
                    SELECT a.id, a.nama_status_dokumen, 0 as jumlah
                    FROM mst_status_dokumen a
                    LEFT JOIN grf_gratifikasi c on a.id = c.status_dokumen) asd
                WHERE asd.id IS NOT NULL
                GROUP BY asd.id
                ORDER BY asd.id ASC";
       
        $Q = $this->db->query($sql, array(array('3')));
        return $Q->result();
    }

    /*function get_sum_total_tahap_penanganan(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE status_dokumen != ?";

        $fetch = $this->db->query($queryString, array(array('3')));
        return $fetch->row(); 
    }*/
	function get_sum_total_tahap_penanganan(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi";

        $fetch = $this->db->query($queryString, array(array('3')));
        return $fetch->row(); 
    }

    /* function get_nominal_tahap_penanganan(){
        $sql = "SELECT *
                FROM(
                    SELECT a.id, a.nama_status_dokumen, COALESCE(SUM(c.nilai_nominal), 0) AS jumlah
                    FROM mst_status_dokumen a
                    LEFT JOIN grf_gratifikasi c on a.id = c.status_dokumen
                    GROUP BY a.id
                    UNION
                    SELECT a.id, a.nama_status_dokumen, 0 as jumlah
                    FROM mst_status_dokumen a
                    LEFT JOIN grf_gratifikasi c on a.id = c.status_dokumen) asd
                WHERE asd.id IS NOT NULL AND asd.id NOT IN ?
                GROUP BY asd.id
                ORDER BY asd.id ASC";
       
        $Q = $this->db->query($sql, array(array('3')));
        return $Q->result();
    } */
	function get_nominal_tahap_penanganan(){
        $sql = "SELECT *
                FROM(
                    SELECT a.id, a.nama_status_dokumen, COALESCE(SUM(c.nilai_nominal), 0) AS jumlah
                    FROM mst_status_dokumen a
                    LEFT JOIN grf_gratifikasi c on a.id = c.status_dokumen
                    GROUP BY a.id
                    UNION
                    SELECT a.id, a.nama_status_dokumen, 0 as jumlah
                    FROM mst_status_dokumen a
                    LEFT JOIN grf_gratifikasi c on a.id = c.status_dokumen) asd
                WHERE asd.id IS NOT NULL
                GROUP BY asd.id
                ORDER BY asd.id ASC";
       
        $Q = $this->db->query($sql, array(array('3')));
        return $Q->result();
    }


    /* function get_sum_nominal_tahap_penanganan(){
       $queryString = "SELECT SUM(nilai_nominal) as jumlah 
                       FROM grf_gratifikasi WHERE status_dokumen != ?";

        $fetch = $this->db->query($queryString, array(array('3')));
        return $fetch->row(); 
    } */
	function get_sum_nominal_tahap_penanganan(){
       $queryString = "SELECT SUM(nilai_nominal) as jumlah 
                       FROM grf_gratifikasi";

        $fetch = $this->db->query($queryString, array(array('3')));
        return $fetch->row(); 
    }

    function cetak_excel(){
        $this->db->select('a.*, a.id as id_gratifikasi, b.*, c.*, c.id as id_penerima, d.nama as nalamat_pengiriman, e.jenis_penerimaan as njenis_penerimaan, f.peristiwa_penerimaan as nperistiwa_penerimaan, g.nama as nidentitas, h.name as nprovinsi_kantor, i.name as nprovinsi_rumah');
        $this->db->from('grf_gratifikasi a');
        $this->db->join('grf_file_gratifikasi b','a.id = b.id_gratifikasi','LEFT');
        $this->db->join('conf_user c','a.id_penerima = c.id','LEFT');
        $this->db->join('mst_alamat_pengirim d','d.id = c.alamat_pengiriman','LEFT');
        $this->db->join('mst_jenis_penerimaan e','a.jenis_penerimaan = e.id','LEFT');
        $this->db->join('mst_peristiwa_penerimaan f','a.kode_peristiwa = f.id','LEFT');
        $this->db->join('mst_identitas g','a.identitas = g.id','LEFT');
        $this->db->join('mst_provinsi h','c.provinsi_kantor = h.id','LEFT');
        $this->db->join('mst_provinsi i','c.provinsi_rumah = i.id','LEFT');
        $this->db->where('a.status_dokumen', '5');
		$this->db->group_by('a.id');
        $this->db->order_by('a.urutan','a.id');

        return $this->db->get();
    }

    function cetak_excel_inspektorat(){
        $stat_dok = array('2','4','5','6','7','8');
        $this->db->select('a.*, a.id as id_gratifikasi, b.*, c.*, c.id as id_penerima, d.nama as nalamat_pengiriman, e.jenis_penerimaan as njenis_penerimaan, f.peristiwa_penerimaan as nperistiwa_penerimaan, g.nama as nidentitas, h.name as nprovinsi_kantor, i.name as nprovinsi_rumah, j.nama_status_dokumen');
        $this->db->from('grf_gratifikasi a');
        $this->db->join('grf_file_gratifikasi b','a.id = b.id_gratifikasi','LEFT');
        $this->db->join('conf_user c','a.id_penerima = c.id','LEFT');
        $this->db->join('mst_alamat_pengirim d','d.id = c.alamat_pengiriman','LEFT');
        $this->db->join('mst_jenis_penerimaan e','a.jenis_penerimaan = e.id','LEFT');
        $this->db->join('mst_peristiwa_penerimaan f','a.kode_peristiwa = f.id','LEFT');
        $this->db->join('mst_identitas g','a.identitas = g.id','LEFT');
        $this->db->join('mst_provinsi h','c.provinsi_kantor = h.id','LEFT');
        $this->db->join('mst_provinsi i','c.provinsi_rumah = i.id','LEFT');
        $this->db->join('mst_status_dokumen j','a.status_dokumen = j.id','LEFT');
        $this->db->where_in('a.status_dokumen', $stat_dok);
		$this->db->group_by('a.id');
        $this->db->order_by('a.urutan');
		
        return $this->db->get();
    }

    function get_sum_cetak_excel($id_penerima){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE id_penerima = ? AND status_dokumen IN ? GROUP BY id_penerima ORDER BY id DESC";
        
        $fetch = $this->db->query($queryString, array($id_penerima, array('5','6','7','8')));
        return $fetch->row(); 
    }
	
	function get_data_riwayat(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null AND a.status_dokumen >= 6
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', '2'));
        return $Q->result();
    }
	
	function get_data_riwayat_pegawai($id){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id_pelapor = '$id'
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', '2'));
        return $Q->result();
    }
	
	function get_data_riwayat_gratifikasi(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null AND a.status_dokumen >= 2 AND a.status_dokumen <= 6
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', '2'));
        return $Q->result();
    }

    function get_count_riwayat(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE id is not null AND status_dokumen >= 2 AND status_dokumen <= 6";

        $fetch = $this->db->query($queryString, array('send', array('2'), '0'));
        return $fetch->row();  
    }
	
	function get_count_riwayat_pegawai($id){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE id_pelapor = '$id' ";

        $fetch = $this->db->query($queryString, array('send', array('2'), '0'));
        return $fetch->row();  
    }
	
	function cetak_excel_riwayat(){
 
		$this->db->select('a.*, b.jenis_penerimaan as njenis_penerimaan, c.name as nama_penerima');
        $this->db->from('grf_gratifikasi a');
        $this->db->join('mst_jenis_penerimaan b','a.jenis_penerimaan = b.id','LEFT');
		$this->db->join('conf_user c','a.id_penerima = c.id','LEFT');		
        
		
        $this->db->where('a.id is not null');
		$this->db->group_by('a.id');
        $this->db->order_by('a.tgl_laporan','DESC');

        return $this->db->get();
    }
	
	function laporan_tahunan(){
 
		$this->db->select('a.*, b.jenis_penerimaan as njenis_penerimaan, c.name as nama_penerima, d.name as nama_pelapor');
        $this->db->from('grf_gratifikasi a');
        $this->db->join('mst_jenis_penerimaan b','a.jenis_penerimaan = b.id','LEFT');
		$this->db->join('conf_user c','a.id_penerima = c.id','LEFT');		
		$this->db->join('conf_user d','a.id_pelapor = d.id','LEFT');		
        
		
        $this->db->where('a.id is not null');
		$this->db->group_by('a.id');
        $this->db->order_by('a.tgl_laporan','DESC');

        return $this->db->get();
    }
	
	function laporan_tahunan_pegawai($id){
 
		$this->db->select('a.*, b.jenis_penerimaan as njenis_penerimaan, c.name as nama_penerima, d.name as nama_pelapor');
        $this->db->from('grf_gratifikasi a');
        $this->db->join('mst_jenis_penerimaan b','a.jenis_penerimaan = b.id','LEFT');
		$this->db->join('conf_user c','a.id_penerima = c.id','LEFT');		
		$this->db->join('conf_user d','a.id_pelapor = d.id','LEFT');		
        
		
        $this->db->where('a.id_pelapor', $id);
		$this->db->group_by('a.id');
        $this->db->order_by('a.tgl_laporan','DESC');

        return $this->db->get();
    }
	
	function insert_laporan($data){
        $res = $this->db->insert('lap_tahunan', $data);
        if (!$res) {
            return 'proses gagal';
        } else {
            return 'sukses';
        }
    }
	
	function get_id_laporan_terakhir(){
        $data = array();
        $sql = "SELECT id FROM lap_tahunan ORDER BY id DESC limit 1";
        $q = $this->db->query($sql);
        
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        } else{
            $data['id'] = 0;
        }
        $q->free_result();
        return $data['id'];
    }
	
	function get_data_laporan_peg($id_user){
        $sql = "SELECT * FROM lap_tahunan 
                WHERE id_user = '$id_user'
                ORDER BY tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('5'), $id_user));
        return $Q->result();
    }

    function get_count_laporan_peg($id_user){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM lap_tahunan WHERE id_user = '$id_user'";

        $fetch = $this->db->query($queryString, array('send', array('5'), '0', $id_user));
        return $fetch->row(); 
    }
	
	function get_data_laporan($id_user,$id_laporan){
        $sql = "SELECT * FROM lap_tahunan 
                WHERE id_user = '$id_user' and id = '$id_laporan'
                ORDER BY tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', array('5'), $id_user));
        return $Q->result();
    }
	
	function get_data_laporan_inspektorat(){
        $sql = "SELECT a.*, b.name, b.jabatan FROM lap_tahunan a 
				LEFT JOIN conf_user b
				ON a.id_user = b.id
                ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array());
        return $Q->result();
    }

    function get_count_laporan_inspektorat(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM lap_tahunan";

        $fetch = $this->db->query($queryString, array());
        return $fetch->row(); 
    }
	
	function get_data_detail_laporan_inspektorat($a){
        $sql = "SELECT * FROM lap_tahunan
				WHERE id = '$a'
                ORDER BY tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array());
        return $Q->result();
    }
	
	function insert_verifikasi_kpk($data){
        $res = $this->db->insert('verifikasi_kpk', $data);
        if (!$res) {
            return 'proses gagal';
        } else {
            return 'sukses';
        }
    }
	
	function get_id_verifikasi_terakhir(){
        $data = array();
        $sql = "SELECT id FROM verifikasi_kpk ORDER BY id DESC limit 1";
        $q = $this->db->query($sql);
        
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        } else{
            $data['id'] = 0;
        }
        $q->free_result();
        return $data['id'];
    }
	
	function getmata_uang() {
        $data = array();

        $sql = "SELECT * FROM mst_mata_uang ORDER BY negara ASC";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }
	
	function insert_file_verifikasi_kpk($data){
        $res = $this->db->insert('file_verifikasi_kpk', $data);
        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }
	
	function get_count_riwayat_verifikasi(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE status_dokumen >= 6";

        $fetch = $this->db->query($queryString, array('send', array('2'), '0'));
        return $fetch->row();  
    }
	
	function get_kronologis_verifikasi($a){
        $sql = "SELECT * FROM verifikasi_kpk a
				LEFT JOIN file_verifikasi_kpk b ON a.id = b.id_verifikasi
				WHERE a.no_laporan_gratifikasi = '$a'
                ORDER BY a.tgl_input_verifikasi DESC";
       
        $Q = $this->db->query($sql, array());
        return $Q->result();
    }
	
	/*function getfile_verifikasi_byid($id)
    {
        $this->db->select('*');
        $this->db->from('file_verifikasi_kpk');
        $this->db->where('files', $id);

        return $this->db->get();
    }*/
	
	function get_count_pasca_tindak_lanjut(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE status_dokumen >= 8";

        $fetch = $this->db->query($queryString, array('send', array('2'), '0'));
        return $fetch->row();  
    }
	
	function get_id_pascatindaklanjut_terakhir(){
        $data = array();
        $sql = "SELECT id FROM info_pasca_tindak_lanjut ORDER BY id DESC limit 1";
        $q = $this->db->query($sql);
        
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        } else{
            $data['id'] = 0;
        }
        $q->free_result();
        return $data['id'];
    }
	
	function insert_pasca_tindak_lanjut($data){
        $res = $this->db->insert('info_pasca_tindak_lanjut', $data);
        if (!$res) {
            return 'proses gagal';
        } else {
            return 'sukses';
        }
    }
	
	function insert_file_pasca_tindak_lanjut($data){
        $res = $this->db->insert('file_info_pasca_tindak_lanjut', $data);
        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }
	
	function get_info_pasca_tindak_lanjut($a){
        $sql = "SELECT * FROM info_pasca_tindak_lanjut a
				LEFT JOIN file_info_pasca_tindak_lanjut b ON a.id = b.id_pasca_tindak_lanjut
				WHERE a.no_laporan_gratifikasi = '$a'
                ORDER BY a.tgl_input DESC";
       
        $Q = $this->db->query($sql, array());
        return $Q->result();
    }
	
	function get_total_laporan(){
        $sql = "SELECT status_penerimaan, count(*) as jumlah from grf_gratifikasi GROUP BY status_penerimaan";
       
        $Q = $this->db->query($sql, array(array('3')));
        return $Q->result();
    }
	
	function get_count_riwayat_penolakan(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE status_penerimaan = 'tolak'";

        $fetch = $this->db->query($queryString, array('send', array('2'), '0'));
        return $fetch->row();  
    }
	
	function get_data_riwayat_penolakan(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null AND a.status_penerimaan = 'tolak'
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', '2'));
        return $Q->result();
    }
	
	function get_count_riwayat_penerimaan(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE status_penerimaan = 'terima'";

        $fetch = $this->db->query($queryString, array('send', array('2'), '0'));
        return $fetch->row();  
    }
	
	function get_data_riwayat_penerimaan(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null AND a.status_penerimaan = 'terima'
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', '2'));
        return $Q->result();
    }
	
	function cetak_excel_riwayat_penolakan(){
 
		$this->db->select('a.*, b.jenis_penerimaan as njenis_penerimaan, c.name as nama_penerima');
        $this->db->from('grf_gratifikasi a');
        $this->db->join('mst_jenis_penerimaan b','a.jenis_penerimaan = b.id','LEFT');
		$this->db->join('conf_user c','a.id_penerima = c.id','LEFT');		   
		
        $this->db->where('a.id is not null');
        $this->db->where('a.status_penerimaan', 'tolak');
		$this->db->group_by('a.id');
        $this->db->order_by('a.tgl_laporan','DESC');

        return $this->db->get();
    }
	
	function cetak_excel_riwayat_penerimaan(){
 
		$this->db->select('a.*, b.jenis_penerimaan as njenis_penerimaan, c.name as nama_penerima');
        $this->db->from('grf_gratifikasi a');
        $this->db->join('mst_jenis_penerimaan b','a.jenis_penerimaan = b.id','LEFT');
		$this->db->join('conf_user c','a.id_penerima = c.id','LEFT');		   
		
        $this->db->where('a.id is not null');
        $this->db->where('a.status_penerimaan', 'terima');
		$this->db->group_by('a.id');
        $this->db->order_by('a.tgl_laporan','DESC');

        return $this->db->get();
    }
	
	function get_data_telah_review(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null AND a.status_dokumen = '5'
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', '2'));
        return $Q->result();
    }
	
	function get_count_telah_review(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE status_dokumen = '5'";

        $fetch = $this->db->query($queryString, array('send', array('2'), '0'));
        return $fetch->row();  
    }
	
	function cetak_excel_telah_kirim_kpk(){
        $stat_dok = array('2','4','5','6','7','8');
        $this->db->select('a.*, a.id as id_gratifikasi, b.*, c.*, c.id as id_penerima, d.nama as nalamat_pengiriman, e.jenis_penerimaan as njenis_penerimaan, f.peristiwa_penerimaan as nperistiwa_penerimaan, g.nama as nidentitas, h.name as nprovinsi_kantor, i.name as nprovinsi_rumah, j.nama_status_dokumen');
        $this->db->from('grf_gratifikasi a');
        $this->db->join('grf_file_gratifikasi b','a.id = b.id_gratifikasi','LEFT');
        $this->db->join('conf_user c','a.id_penerima = c.id','LEFT');
        $this->db->join('mst_alamat_pengirim d','d.id = c.alamat_pengiriman','LEFT');
        $this->db->join('mst_jenis_penerimaan e','a.jenis_penerimaan = e.id','LEFT');
        $this->db->join('mst_peristiwa_penerimaan f','a.kode_peristiwa = f.id','LEFT');
        $this->db->join('mst_identitas g','a.identitas = g.id','LEFT');
        $this->db->join('mst_provinsi h','c.provinsi_kantor = h.id','LEFT');
        $this->db->join('mst_provinsi i','c.provinsi_rumah = i.id','LEFT');
        $this->db->join('mst_status_dokumen j','a.status_dokumen = j.id','LEFT');
        $this->db->where_in('a.status_dokumen', '6');
		$this->db->group_by('a.id');
        $this->db->order_by('a.urutan');
		
        return $this->db->get();
    }
	
	function get_count_riwayat_all(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi";

        $fetch = $this->db->query($queryString, array('send', array('2'), '0'));
        return $fetch->row();  
    }
	
	function get_data_riwayat_all(){
        $sql = "SELECT a.*, b.jenis_penerimaan as njenis_penerimaan, c.peristiwa_penerimaan, d.nama as nidentitas, e.name as nama_penerima, f.name as nama_pelapor
                                FROM grf_gratifikasi a 
                                LEFT JOIN mst_jenis_penerimaan b ON a.jenis_penerimaan = b.id
                                LEFT JOIN mst_peristiwa_penerimaan c ON a.kode_peristiwa = c.id
                                LEFT JOIN mst_identitas d ON a.identitas = d.id
                                LEFT JOIN conf_user e ON a.id_penerima = e.id
                                LEFT JOIN conf_user f ON a.id_pelapor = f.id
                                WHERE a.id is not null
                                GROUP BY a.id ORDER BY a.tgl_laporan DESC";
       
        $Q = $this->db->query($sql, array('send', '2'));
        return $Q->result();
    }
}
