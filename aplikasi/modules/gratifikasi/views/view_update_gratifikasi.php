<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
      <h1>Update Gratifikasi</h1>
      <div class="content-form-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="alert alert-info display-hide">
              <i class="fa fa-spin fa-spinner"></i> Silahkan tunggu. Proses penyimpanan sedang berjalan.
            </div>
            <div class="alert alert-danger display-hide">
              <button class="close" data-close="alert"></button>
              <i class="fa fa-ban"></i> <strong>Peringatan!</strong> Formulir belum lengkap. Data gagal ditambahkan. 
            </div>
            <?= $this->session->flashdata('message'); ?>
            <?php if($show_data->identitas == 'Sebagai Penerima Gratifikasi'): ?>
              <form method="post" id="update_penerima" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('gratifikasi/update'); ?>">
                <?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>
              <fieldset>
                <legend style="color: #65aed9">Detail Laporan</legend>
                <div class="form-group">
                  <div class="row static-info">
                      <div class="col-md-3 name">
                        Identitas
                      </div>
                      <div class="col-md-8">
                        : <?php cetak($show_data->identitas) ?>
                        <input type="text" class="hidden" id="identitas" name="identitas" value="<?php cetak($show_data->identitas) ?>">
                        <input type="text" class="hidden" id="id_gratifikasi" name="id_gratifikasi" value="<?php cetak($id_gratifikasi) ?>">
                      </div>
                  </div>
                  <div class="row static-info">
                      <div class="col-md-3 name">
                        Jenis Laporan
                      </div>
                      <div class="col-md-8">
                      <?= $show_data->status_penerimaan == 'tolak' ? ': Laporan Penolakan Gratifikasi' : ': Laporan Penerimaan Gratifikasi'; ?>
                      </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend style="color: #65aed9">Identitas Pelapor Gratifikasi</legend>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         Nama Lengkap
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->name) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         Tempat &amp; Tanggal Lahir 
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->tempatlahir.", ".$show_profil->tgllahir) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         No. KTP (NIK)
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->no_ktp) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         Jabatan/Pangkat/Golongan
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->jabatan." ".$show_profil->pangkat ." ".$show_profil->golongan) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Alamat Email
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->email) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Nomor Telepon Seluler
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->nohp) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Pin BB/WA
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->pin_bb) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Nama Instansi
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->inskerja) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Unit Kerja/Sub Unit Kerja
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->biro." ".$show_profil->bagian) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Alamat rumah
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->alamat_kantor) ?><br>&nbsp;&nbsp;<?php cetak($show_profil->kelurahan_kantor." ".$show_profil->kecamatan_kantor.", ".$show_profil->kode_pos_kantor) ?><br>&nbsp;&nbsp;<?php cetak($show_profil->kota_kantor.", ".$show_profil->nprovinsi_kantor) ?><br>&nbsp;&nbsp;Telp: <?php cetak($show_profil->no_kantor) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Alamat Rumah
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->alamat_rumah) ?><br>&nbsp;&nbsp;<?php cetak($show_profil->kelurahan_rumah." ".$show_profil->kecamatan_rumah.", ".$show_profil->kode_pos_rumah) ?><br>&nbsp;&nbsp;<?php cetak($show_profil->kota_rumah.", ".$show_profil->nprovinsi_rumah) ?><br>&nbsp;&nbsp;Telp: <?php cetak($show_profil->no_rumah) ?>
                    </div>
                </div>
              </fieldset>
              <hr>
              <fieldset>
                <?php if ($show_data->status_penerimaan == 'tolak'): ?>
                <legend style="color: #65aed9">Data Penolakan Gratifikasi</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Jenis Penolakan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="jenis_penerimaan" id="jenis_penerimaan" class="form-control">
                        <?php foreach ($jenis_penerimaan as $row): ?>
                          <option value="<?php cetak($row['id']) ?>" <?php if ($row['id'] == $show_data->jenis_penerimaan): echo 'selected="selected"'; endif; ?>><?php cetak($row['jenis_penerimaan']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <!--<label>Uraian Gratifikasi yang Ditolak (max. 250 karakter) <span class="require">*</span></label>-->
                    <label>Uraian Gratifikasi yang Ditolak <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <!--<textarea class="form-control" name="uraian" maxlength="250"><?php cetak($show_data->uraian) ?></textarea>-->
                      <textarea class="form-control" name="uraian"><?php cetak($show_data->uraian) ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Harga/Nilai Nominal/Taksiran <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="nilai_nominal" name="nilai_nominal" value="<?php cetak($show_data->nilai_nominal) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Kode Peristiwa Penolakan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="kode_peristiwa" id="kode_peristiwa" class="form-control" onchange="Check1(this);">
                        <?php foreach ($peristiwa_penerimaan as $row): ?>
                          <option value="<?php cetak($row['id']) ?>" <?php if ($row['id'] == $show_data->kode_peristiwa): echo 'selected="selected"'; endif; ?>><?php cetak($row['peristiwa_penerimaan']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <?php if ($show_data->kode_peristiwa == '7'): ?>
                <div class="row static-info form-group" id="peristiwa_lainnya_check">
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="peristiwa_lainnya" name="peristiwa_lainnya" placeholder="Peristiwa Penerimaan Lainnya" value="<?php cetak($show_data->peristiwa_lainnya) ?>">
                    </div>
                  </div>
                </div>
                <?php else: ?>
                <div class="row static-info form-group" id="peristiwa_lainnya_check" style='display:none;'>
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="peristiwa_lainnya" name="peristiwa_lainnya" placeholder="Peristiwa Penerimaan Lainnya">
                    </div>
                  </div>
                </div>
                <?php endif; ?>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Tempat &amp; Tanggal Penolakan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="tempat_penerimaan" name="tempat_penerimaan" value="<?php cetak($show_data->tempat_penerimaan) ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <div class="input-group">    
                          <div class="input-icon right">
                              <i class="fa"></i>
                              <input class="form-control date-picker input-sm" onkeyup="checkonlynumber10(this);" placeholder="yyyy-mm-dd" size="16" type="text" id="tanggal_penerimaan" name="tanggal_penerimaan" value="<?php cetak($show_data->tanggal_penerimaan) ?>"/>
                          </div>
                          <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </span>
                      </div>
                    </div>
                  </div>
                </div>
                <?php else: ?>
                <legend style="color: #65aed9">Data Penerima Gratifikasi</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Jenis Penerimaan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="jenis_penerimaan" id="jenis_penerimaan" class="form-control">
                        <?php foreach ($jenis_penerimaan as $row): ?>
                          <option value="<?php cetak($row['id']) ?>" <?php if ($row['id'] == $show_data->jenis_penerimaan): echo 'selected="selected"'; endif; ?>><?php cetak($row['jenis_penerimaan']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <!--<label>Uraian Gratifikasi yang Diterima (max. 250 karakter) <span class="require">*</span></label>-->
                    <label>Uraian Gratifikasi yang Diterima <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <!--<textarea class="form-control" name="uraian" maxlength="250"><?php cetak($show_data->uraian) ?></textarea>-->
                      <textarea class="form-control" name="uraian"><?php cetak($show_data->uraian) ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Harga/Nilai Nominal/Taksiran <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="nilai_nominal" name="nilai_nominal" value="<?php cetak($show_data->nilai_nominal) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Kode Peristiwa Penerimaan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="kode_peristiwa" id="kode_peristiwa" class="form-control" onchange="Check1(this);">
                        <?php foreach ($peristiwa_penerimaan as $row): ?>
                          <option value="<?php cetak($row['id']) ?>" <?php if ($row['id'] == $show_data->kode_peristiwa): echo 'selected="selected"'; endif; ?>><?php cetak($row['peristiwa_penerimaan']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <?php if ($show_data->kode_peristiwa == '7'): ?>
                <div class="row static-info form-group" id="peristiwa_lainnya_check">
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="peristiwa_lainnya" name="peristiwa_lainnya" placeholder="Peristiwa Penerimaan Lainnya" value="<?php cetak($show_data->peristiwa_lainnya) ?>">
                    </div>
                  </div>
                </div>
                <?php else: ?>
                <div class="row static-info form-group" id="peristiwa_lainnya_check" style='display:none;'>
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="peristiwa_lainnya" name="peristiwa_lainnya" placeholder="Peristiwa Penerimaan Lainnya">
                    </div>
                  </div>
                </div>
                <?php endif; ?>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Tempat &amp; Tanggal Penerimaan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="tempat_penerimaan" name="tempat_penerimaan" value="<?php cetak($show_data->tempat_penerimaan) ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <div class="input-group">    
                          <div class="input-icon right">
                              <i class="fa"></i>
                              <input class="form-control date-picker input-sm" onkeyup="checkonlynumber10(this);" placeholder="yyyy-mm-dd" size="16" type="text" id="tanggal_penerimaan" name="tanggal_penerimaan" value="<?php cetak($show_data->tanggal_penerimaan) ?>"/>
                          </div>
                          <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </span>
                      </div>
                    </div>
                  </div>
                </div>
                <?php endif; ?>
              </fieldset>
              <hr>
              <fieldset>
                <legend style="color: #65aed9">Data Pemberi Gratifikasi</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Nama <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="nama_pemberi" name="nama_pemberi" value="<?php cetak($show_data->nama_pemberi) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Pekerjaan dan Jabatan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="pekerjaan_pemberi" name="pekerjaan_pemberi" value="<?php cetak($show_data->pekerjaan_pemberi) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Alamat <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="alamat_pemberi" name="alamat_pemberi"><?php cetak($show_data->alamat_pemberi) ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label</label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber8(this);" class="form-control" id="telepon_pemberi" name="telepon_pemberi" placeholder="Nomor Telepon" value="<?php cetak($show_data->telepon_pemberi) ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber9(this);" class="form-control" id="faks_pemberi" name="faks_pemberi" placeholder="Faks" value="<?php cetak($show_data->faks_pemberi) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Email </label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="email_pemberi" name="email_pemberi" value="<?php cetak($show_data->email_pemberi) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Hubungan dengan Pemberi <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="hubungan_pemberi" name="hubungan_pemberi" value="<?php cetak($show_data->hubungan_pemberi) ?>">
                    </div>
                  </div>
                </div>
              </fieldset>
              <hr>
              <fieldset>
                <legend style="color: #65aed9">Alasan dan Kronologi</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Alasan Pemberian <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="alasan_pemberian" name="alasan_pemberian"><?php cetak($show_data->alasan_pemberian) ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Kronologi <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="kronologi_penerimaan" name="kronologi_penerimaan"><?php cetak($show_data->kronologi_penerimaan) ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Dokumen yang dilampirkan<span class="require">*</span></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select class="form-control" name="status_dokumen_dilampirkan" id="status_dokumen_dilampirkan" onchange="Check2(this);">
                        <option value="tidak" <?= $show_data->status_dokumen_dilampirkan == 'tidak' ? 'selected' : ''; ?>>Tidak Ada</option>
                        <option value="ada" <?= $show_data->status_dokumen_dilampirkan == 'ada' ? 'selected' : ''; ?>>Ada</option>
                      </select>
                    </div>
                  </div>
                </div>
                <?php if ($show_data->status_dokumen_dilampirkan == 'ada'): ?>
                <div class="row static-info form-group" id="dokumen_dilampirkan_check">
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="dokumen_dilampirkan" name="dokumen_dilampirkan" placeholder="Yaitu"><?php cetak($show_data->dokumen_dilampirkan) ?></textarea>
                    </div>
                  </div>
                </div>
                <?php else: ?>
                <div class="row static-info form-group" id="dokumen_dilampirkan_check" style='display:none;'>
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="dokumen_dilampirkan" name="dokumen_dilampirkan" placeholder="Yaitu"></textarea>
                    </div>
                  </div>
                </div>
                <?php endif; ?>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Catatan Tambahan (bila perlu) </label>
                  </div>
                  <div class="col-md-8">
                    <textarea class="form-control" id="catatan_tambahan" name="catatan_tambahan"><?php cetak($show_data->catatan_tambahan) ?></textarea>
                  </div>
                </div>
              </fieldset>
              <hr>
              <fieldset>
                <legend style="color: #65aed9">Data Tambahan</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Upload File</label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <div class="input-group">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn default btn-file">
                                <span class="fileinput-new">
                                    Pilih File </span>
                                <span class="fileinput-exists">
                                    Ubah </span>
                                <input type="file" name="filesatu" id="filesatu" onchange="document.getElementById('moreFileLink').style.display = 'block';">
                            </span>
                            <span class="fileinput-filename">
                            </span>
                            &nbsp; <a href="#" class="close fileinput-exists" data-dismiss="fileinput">
                            </a>
                        </div>
                        <div id="moreFile"></div>
                        <div id="moreFileLink" style="display:none;"><a href="javascript:addFile();">Tambah File Lain</a></div>
                        <span class="help-block">File yang diupload :</span>
                        <?php if (!empty($file_gratifikasi)): ?>
                          <?php foreach ($file_gratifikasi as $row): ?>
                              <a target="_blank" href="<?= site_url('gratifikasi/download_file/'.$row->id.'/'.$row->id_gratifikasi); ?>"><?php cetak($row->nama_files) ?></a>&nbsp;&nbsp;
                              <?php if (!empty($row->files)): ?>
                                  <a href="<?php echo site_url('gratifikasi/delete_file/'.$row->id); ?>" class="btn btn-circle btn-xs red" title="Hapus File" data-placement="bottom" rel="tooltip" onclick="return confirm('Apakah anda yakin menghapus file ini?');">
                                      <i class="glyphicon glyphicon-trash "></i>
                                  </a><br>
                              <?php endif; ?>
                          <?php endforeach; ?>
                        <?php else: ?>
                            <span class="help-block">Belum ada dokumen yang diupload</span>
                        <?php endif; ?>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
              <hr>
              <fieldset>
                <div class="row static-info form-group">
                  <div class="col-md-12 name">
                    <span class="help-block">
                      <label class="checkbox-inline" style="text-align: justify;">
                        <div class="checker" id="uniform-inlineCheckbox1"><span><input id="persyaratan" name="persyaratan" value="option1" type="checkbox" checked="checked"></span></div> Laporan Gratifikasi ini saya sampaikan dengan sebenar-benarnya. Apabila ada yang sengaja tidak saya laporkan atau saya laporkan kepada Komisi Pemberantasan Korupsi secara tidak benar, maka saya bersedia mempertanggungjawabkannya secara hukum sesuai dengan peraturan perundang-undangan yang berlaku dan saya bersedia memberikan keterangan selanjutnya.
                      </label>
                    </span>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-8 name">
                    <label></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control input-inline input-sm" id="tempat_laporan" name="tempat_laporan" placeholder="Tempat" value="<?php cetak($show_data->tempat_laporan) ?>"><span class="help-inline">, <?php cetak(date('d-M-Y', strtotime($tanggal))) ?></span>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-9 name">
                    <label></label>
                  </div>
                  <div class="col-md-3">
                    <label><?php cetak($this->session->userdata('name')) ?></label>
                  </div>
                </div>
              </fieldset>
              <br><br>
              <div class="row">
                <div class="col-md-offset-4 col-md-9">
                <?php if($show_data->jenis_dokumen == 'draft'): ?>
                  <button id="submit" name="send" type="submit" onclick="return confirm('Apakah anda yakin mengirimkan data ini?');" class="btn btn-sm blue-dark"><i class="fa fa-paper-plane-o"></i>&nbsp;&nbsp;Kirim Review</button>
                <?php endif; ?>
                <?php if($show_data->status_dokumen == '3'): ?>
                  <button id="submit" name="revisi" type="submit" onclick="return confirm('Apakah anda yakin mengirimkan data ini?');" class="btn btn-sm blue-dark"><i class="fa fa-paper-plane-o"></i>&nbsp;&nbsp;Kirim Revisi</button>
                <?php endif; ?>
                <?php if ($show_data->jenis_dokumen == 'draft'): ?>
                  <button id="submit" name="save" type="submit" class="btn btn-sm green-seagreen"><i class="fa fa-paperclip"></i>&nbsp;&nbsp;Save as Draft</button>
                <?php endif; ?>
                  <a href="<?php echo site_url('gratifikasi/riwayat_gratifikasi'); ?>" class="btn btn-sm grey"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</a>
                </div>
              </div>
            </form>
            <?php else: ?>
            <form method="post" id="update_pelapor" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('gratifikasi/update'); ?>">
              <?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>
              <fieldset>
                <legend style="color: #65aed9">Detail Laporan</legend>
                <div class="form-group">
                  <div class="row static-info">
                      <div class="col-md-3 name">
                        Identitas
                      </div>
                      <div class="col-md-8">
                        : <?php cetak($show_data->identitas) ?>
                        <input type="text" class="hidden" id="identitas" name="identitas" value="<?php cetak($show_data->identitas) ?>">
                        <input type="text" class="hidden" id="id_gratifikasi" name="id_gratifikasi" value="<?php cetak($id_gratifikasi) ?>">
                      </div>
                  </div>
                  <div class="row static-info">
                      <div class="col-md-3 name">
                        Jenis Laporan
                      </div>
                      <div class="col-md-8">
                       <?= $show_data->status_penerimaan == 'tolak' ? ': Laporan Penolakan Gratifikasi' : ': Laporan Penerimaan Gratifikasi'; ?>
                      </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend style="color: #65aed9">Identitas Pelapor Gratifikasi</legend>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         Nama Lengkap
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_pelapor->name) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         Tempat &amp; Tanggal Lahir 
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_pelapor->tempatlahir.", ".$show_pelapor->tgllahir) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         No. KTP (NIK)
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_pelapor->no_ktp) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         Jabatan/Pangkat/Golongan
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_pelapor->jabatan." ".$show_pelapor->pangkat ." ".$show_pelapor->golongan) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Alamat Email
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_pelapor->email) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Nomor Telepon Seluler
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_pelapor->nohp) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Pin BB/WA
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_pelapor->pin_bb) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Nama Instansi
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_pelapor->inskerja) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Unit Kerja/Sub Unit Kerja
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_pelapor->biro." ".$show_pelapor->bagian) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Alamat Kantor
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_pelapor->alamat_kantor) ?><br>&nbsp;&nbsp;<?php cetak($show_pelapor->kelurahan_kantor." ".$show_pelapor->kecamatan_kantor.", ".$show_pelapor->kode_pos_kantor) ?><br>&nbsp;&nbsp;<?php cetak($show_pelapor->kota_kantor.", ".$show_pelapor->nprovinsi_kantor) ?><br>&nbsp;&nbsp;Telp: <?php cetak($show_pelapor->no_kantor) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Alamat Rumah
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_pelapor->alamat_rumah) ?><br>&nbsp;&nbsp;<?php cetak($show_pelapor->kelurahan_rumah." ".$show_pelapor->kecamatan_rumah.", ".$show_pelapor->kode_pos_rumah) ?><br>&nbsp;&nbsp;<?php cetak($show_pelapor->kota_rumah.", ".$show_pelapor->nprovinsi_rumah) ?><br>&nbsp;&nbsp;Telp: <?php cetak($show_pelapor->no_rumah) ?>
                    </div>
                </div>
              </fieldset>
              <hr>
              <fieldset>
                <legend style="color: #65aed9">Identitas Penerima Gratifikasi</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                      <label >Nama Lengkap <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="name" name="name" value="<?php cetak($show_data->name) ?>">
                      <input type="hidden" class="form-control" id="id" name="id" value="<?php cetak($show_data->id) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                      <label >Tempat &amp; Tanggal Lahir <span class="require">*</span></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="tempatlahir" name="tempatlahir" value="<?php cetak($show_data->tempatlahir) ?>">
                    </div>
                  </div>
                   <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <div class="input-group">    
                          <div class="input-icon right">
                              <i class="fa"></i>
                              <input class="form-control date-picker input-sm" onkeyup="checkonlynumber11(this);" placeholder="yyyy-mm-dd" size="16" type="text" id="tgllahir" name="tgllahir" value="<?php cetak($show_data->tgllahir) ?>"/>
                          </div>
                          <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </span>
                        </div>
                      </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label >No. KTP (NIK) <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber(this);" class="form-control" id="no_ktp" name="no_ktp" value="<?php cetak($show_data->no_ktp) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label >Alamat Email <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="email" name="email" value="<?php cetak($show_data->email) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label >Nomor Telepon Seluler <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber2(this);" class="form-control" id="nohp" name="nohp" value="<?php cetak($show_data->nohp) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label >Pin BB/WA </label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="pin_bb" name="pin_bb" value="<?php cetak($show_data->pin_bb) ?>">
                    </div>
                  </div>
                </div> 
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label >Nama Instansi <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="inskerja" name="inskerja" value="<?php cetak($show_data->inskerja) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label >Unit Kerja <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="biro" name="biro"><?php cetak($show_data->biro) ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label >Bagian </label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="bagian" name="bagian" value="<?php cetak($show_data->bagian) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label >Jabatan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="jabatan" name="jabatan" value="<?php cetak($show_data->jabatan) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label >Golongan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="golongan" name="golongan" value="<?php cetak($show_data->golongan) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label >Pangkat <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="pangkat" name="pangkat" value="<?php cetak($show_data->pangkat) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Alamat Kantor <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="alamat_kantor" name="alamat_kantor"><?php cetak($show_data->alamat_kantor) ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label</label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="kelurahan_kantor" name="kelurahan_kantor" placeholder="Kel/Desa" value="<?php cetak($show_data->kelurahan_kantor) ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="kecamatan_kantor" name="kecamatan_kantor" placeholder="Kecamatan" value="<?php cetak($show_data->kecamatan_kantor) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label</label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="kota_kantor" name="kota_kantor" placeholder="Kota/Kabupaten" value="<?php cetak($show_data->kota_kantor) ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="provinsi_kantor" id="provinsi_kantor" class="form-control">
                        <option value="">-- Silahkan Pilih --</option>
                        <?php foreach ($provinsi as $row): ?>
                          <option value="<?php cetak($row['id']) ?>" <?php if ($row['id'] == $show_data->provinsi_kantor): echo 'selected="selected"'; endif; ?>><?php cetak($row['name']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label</label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber3(this);" class="form-control" id="kode_pos_kantor" name="kode_pos_kantor" placeholder="Kode Pos" value="<?php cetak($show_data->kode_pos_kantor) ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber4(this);" class="form-control" id="no_kantor" name="no_kantor" placeholder="Nomor Telepon kantor" value="<?php cetak($show_data->no_kantor) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Alamat Rumah <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="alamat_rumah" name="alamat_rumah"><?php cetak($show_data->alamat_rumah) ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label</label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="kelurahan_rumah" name="kelurahan_rumah" placeholder="Kel/Desa" value="<?php cetak($show_data->kelurahan_rumah) ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="kecamatan_rumah" name="kecamatan_rumah" placeholder="Kecamatan" value="<?php cetak($show_data->kecamatan_rumah) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label</label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="kota_rumah" name="kota_rumah" placeholder="Kota/Kabupaten" value="<?php cetak($show_data->kota_rumah) ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="provinsi_rumah" id="provinsi_rumah" class="form-control">
                        <option value="">-- Silahkan Pilih --</option>
                        <?php foreach ($provinsi as $row): ?>
                          <option value="<?php cetak($row['id']) ?>" <?php if ($row['id'] == $show_data->provinsi_rumah): echo 'selected="selected"'; endif; ?>><?php cetak($row['name']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label</label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber5(this);" class="form-control" id="kode_pos_rumah" name="kode_pos_rumah" placeholder="Kode Pos" value="<?php cetak($show_data->kode_pos_rumah) ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber6(this);" class="form-control" id="no_rumah" name="no_rumah" placeholder="Nomor Telepon rumah" value="<?php cetak($show_data->no_rumah) ?>">
                    </div>
                  </div>
                </div>
              </fieldset>
              <hr>
              <fieldset>
                <?php if ($show_data->status_penerimaan == 'tolak'): ?>
                <legend style="color: #65aed9">Data Penolakan Gratifikasi</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Jenis Penolakan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="jenis_penerimaan" id="jenis_penerimaan" class="form-control">
                        <?php foreach ($jenis_penerimaan as $row): ?>
                          <option value="<?php cetak($row['id']) ?>" <?php if ($row['id'] == $show_data->jenis_penerimaan): echo 'selected="selected"'; endif; ?>><?php cetak($row['jenis_penerimaan']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <!--<label>Uraian Gratifikasi yang Ditolak (max. 250 karakter) <span class="require">*</span></label>-->
                    <label>Uraian Gratifikasi yang Ditolak<span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <!--<textarea class="form-control" name="uraian" maxlength="250"><?php cetak($show_data->uraian) ?></textarea>-->
                      <textarea class="form-control" name="uraian"><?php cetak($show_data->uraian) ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Harga/Nilai Nominal/Taksiran <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="nilai_nominal" name="nilai_nominal" value="<?php cetak($show_data->nilai_nominal) ?>">
                    </div>
                  </div>
                </div> 
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Kode Peristiwa Penolakan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="kode_peristiwa" id="kode_peristiwa" class="form-control" onchange="Check1(this);">
                        <?php foreach ($peristiwa_penerimaan as $row): ?>
                          <option value="<?php cetak($row['id']) ?>" <?php if ($row['id'] == $show_data->kode_peristiwa): echo 'selected="selected"'; endif; ?>><?php cetak($row['peristiwa_penerimaan']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <?php if ($show_data->kode_peristiwa == '7'): ?>
                <div class="row static-info form-group" id="peristiwa_lainnya_check">
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="peristiwa_lainnya" name="peristiwa_lainnya" placeholder="Peristiwa Penerimaan Lainnya" value="<?php cetak($show_data->peristiwa_lainnya) ?>">
                    </div>
                  </div>
                </div>
                <?php else: ?>
                <div class="row static-info form-group" id="peristiwa_lainnya_check" style='display:none;'>
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="peristiwa_lainnya" name="peristiwa_lainnya" placeholder="Peristiwa Penerimaan Lainnya">
                    </div>
                  </div>
                </div>
                <?php endif; ?>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Tempat &amp; Tanggal Penolakan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="tempat_penerimaan" name="tempat_penerimaan" value="<?php cetak($show_data->tempat_penerimaan) ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <div class="input-group">    
                          <div class="input-icon right">
                              <i class="fa"></i>
                              <input class="form-control date-picker input-sm" onkeyup="checkonlynumber12(this);" placeholder="yyyy-mm-dd" size="16" type="text" id="tanggal_penerimaan" name="tanggal_penerimaan" value="<?php cetak($show_data->tanggal_penerimaan) ?>"/>
                          </div>
                          <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </span>
                      </div>
                    </div>
                  </div>
                </div>
                <?php else: ?>
                <legend style="color: #65aed9">Data Penerima Gratifikasi</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Jenis Penerimaan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="jenis_penerimaan" id="jenis_penerimaan" class="form-control">
                        <?php foreach ($jenis_penerimaan as $row): ?>
                          <option value="<?php cetak($row['id']) ?>" <?php if ($row['id'] == $show_data->jenis_penerimaan): echo 'selected="selected"'; endif; ?>><?php cetak($row['jenis_penerimaan']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <!--<label>Uraian Gratifikasi yang Diterima (max. 250 karakter) <span class="require">*</span></label>-->
                    <label>Uraian Gratifikasi yang Diterima<span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <!--<textarea class="form-control" name="uraian" maxlength="250"><?php cetak($show_data->uraian) ?></textarea>-->
                      <textarea class="form-control" name="uraian"><?php cetak($show_data->uraian) ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Harga/Nilai Nominal/Taksiran <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="nilai_nominal" name="nilai_nominal" value="<?php cetak($show_data->nilai_nominal) ?>">
                    </div>
                  </div>
                </div> 
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Kode Peristiwa Penerimaan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="kode_peristiwa" id="kode_peristiwa" class="form-control" onchange="Check1(this);">
                        <?php foreach ($peristiwa_penerimaan as $row): ?>
                          <option value="<?php cetak($row['id']) ?>" <?php if ($row['id'] == $show_data->kode_peristiwa): echo 'selected="selected"'; endif; ?>><?php cetak($row['peristiwa_penerimaan']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <?php if ($show_data->kode_peristiwa == '7'): ?>
                <div class="row static-info form-group" id="peristiwa_lainnya_check">
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="peristiwa_lainnya" name="peristiwa_lainnya" placeholder="Peristiwa Penerimaan Lainnya" value="<?php cetak($show_data->peristiwa_lainnya) ?>">
                    </div>
                  </div>
                </div>
                <?php else: ?>
                <div class="row static-info form-group" id="peristiwa_lainnya_check" style='display:none;'>
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="peristiwa_lainnya" name="peristiwa_lainnya" placeholder="Peristiwa Penerimaan Lainnya">
                    </div>
                  </div>
                </div>
                <?php endif; ?>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Tempat &amp; Tanggal Penerimaan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="tempat_penerimaan" name="tempat_penerimaan" value="<?php cetak($show_data->tempat_penerimaan) ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <div class="input-group">    
                          <div class="input-icon right">
                              <i class="fa"></i>
                              <input class="form-control date-picker input-sm" onkeyup="checkonlynumber12(this);" placeholder="yyyy-mm-dd" size="16" type="text" id="tanggal_penerimaan" name="tanggal_penerimaan" value="<?php cetak($show_data->tanggal_penerimaan) ?>"/>
                          </div>
                          <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </span>
                      </div>
                    </div>
                  </div>
                </div>
                <?php endif; ?>
              </fieldset>
              <hr>
              <fieldset>
                <legend style="color: #65aed9">Data Pemberi Gratifikasi</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Nama <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="nama_pemberi" name="nama_pemberi" value="<?php cetak($show_data->nama_pemberi) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Pekerjaan dan Jabatan <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="pekerjaan_pemberi" name="pekerjaan_pemberi" value="<?php cetak($show_data->pekerjaan_pemberi) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Alamat <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="alamat_pemberi" name="alamat_pemberi"><?php cetak($show_data->alamat_pemberi) ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label</label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber8(this);" class="form-control" id="telepon_pemberi" name="telepon_pemberi" placeholder="Nomor Telepon" value="<?php cetak($show_data->telepon_pemberi) ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber9(this);" class="form-control" id="faks_pemberi" name="faks_pemberi" placeholder="Faks" value="<?php cetak($show_data->faks_pemberi) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Email </label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="email_pemberi" name="email_pemberi" value="<?php cetak($show_data->email_pemberi) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Hubungan dengan Pemberi <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="hubungan_pemberi" name="hubungan_pemberi" value="<?php cetak($show_data->hubungan_pemberi) ?>">
                    </div>
                  </div>
                </div>
              </fieldset>
              <hr>
              <fieldset>
                <legend style="color: #65aed9">Alasan dan Kronologi</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Alasan Pemberian <span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="alasan_pemberian" name="alasan_pemberian"><?php cetak($show_data->alasan_pemberian) ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Kronologi<span class="require">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="kronologi_penerimaan" name="kronologi_penerimaan"><?php cetak($show_data->kronologi_penerimaan) ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Dokumen yang dilampirkan<span class="require">*</span></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select class="form-control" name="status_dokumen_dilampirkan" id="status_dokumen_dilampirkan" onchange="Check2(this);">
                        <option value="tidak" <?= $show_data->status_dokumen_dilampirkan == 'tidak' ? 'selected' : ''; ?>>Tidak Ada</option>
                        <option value="ada" <?= $show_data->status_dokumen_dilampirkan == 'ada' ? 'selected' : ''; ?>>Ada</option>
                      </select>
                    </div>
                  </div>
                </div>
                <?php if ($show_data->status_dokumen_dilampirkan == 'ada'): ?>
                <div class="row static-info form-group" id="dokumen_dilampirkan_check" >
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="dokumen_dilampirkan" name="dokumen_dilampirkan" placeholder="Yaitu"><?php cetak($show_data->dokumen_dilampirkan) ?></textarea>
                    </div>
                  </div>
                </div>
                <?php else: ?>
                <div class="row static-info form-group" id="dokumen_dilampirkan_check" style='display:none;'>
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="dokumen_dilampirkan" name="dokumen_dilampirkan" placeholder="Yaitu"></textarea>
                    </div>
                  </div>
                </div>
                <?php endif; ?>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Catatan Tambahan (bila perlu) </label>
                  </div>
                  <div class="col-md-8">
                    <textarea class="form-control" id="catatan_tambahan" name="catatan_tambahan"><?php cetak($show_data->catatan_tambahan) ?></textarea>
                  </div>
                </div>
              </fieldset>
              <hr>
              <fieldset>
                <legend style="color: #65aed9">Data Tambahan</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Upload File</label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <div class="input-group">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn default btn-file">
                                <span class="fileinput-new">
                                    Pilih File </span>
                                <span class="fileinput-exists">
                                    Ubah </span>
                                <input type="file" name="filesatu" id="filesatu" onchange="document.getElementById('moreFileLink').style.display = 'block';">
                            </span>
                            <span class="fileinput-filename">
                            </span>
                            &nbsp; <a href="#" class="close fileinput-exists" data-dismiss="fileinput">
                            </a>
                        </div>
                        <div id="moreFile"></div>
                        <div id="moreFileLink" style="display:none;"><a href="javascript:addFile();">Tambah File Lain</a></div>
                        <span class="help-block">File yang diupload :</span>
                        <?php if (!empty($file_gratifikasi)): ?>
                          <?php foreach ($file_gratifikasi as $row): ?>
                              <a target="_blank" href="<?= site_url('gratifikasi/download_file/'.$row->id.'/'.$row->id_gratifikasi); ?>"><?php cetak($row->nama_files) ?></a>&nbsp;&nbsp;
                              <?php if (!empty($row->files)): ?>
                                  <a href="<?php echo site_url('gratifikasi/delete_file/'.$row->id); ?>" class="btn btn-circle btn-xs red" title="Hapus File" data-placement="bottom" rel="tooltip" onclick="return confirm('Apakah anda yakin menghapus file ini?');">
                                      <i class="glyphicon glyphicon-trash "></i>
                                  </a><br>
                              <?php endif; ?>
                          <?php endforeach; ?>
                        <?php else: ?>
                            <span class="help-block">Belum ada dokumen yang diupload</span>
                        <?php endif; ?>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
              <hr>
              <fieldset>
                <div class="row static-info form-group">
                  <div class="col-md-12 name">
                    <span class="help-block">
                      <label class="checkbox-inline" style="text-align: justify;">
                        <div class="checker" id="uniform-inlineCheckbox1"><span><input id="persyaratan" name="persyaratan" value="option1" type="checkbox" checked="checked"></span></div> Laporan Gratifikasi ini saya sampaikan dengan sebenar-benarnya. Apabila ada yang sengaja tidak saya laporkan atau saya laporkan kepada Komisi Pemberantasan Korupsi secara tidak benar, maka saya bersedia mempertanggungjawabkannya secara hukum sesuai dengan peraturan perundang-undangan yang berlaku dan saya bersedia memberikan keterangan selanjutnya.
                      </label>
                    </span>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-8 name">
                    <label></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control input-inline input-sm" id="tempat_laporan" name="tempat_laporan" placeholder="Tempat" value="<?php cetak($show_data->tempat_laporan) ?>"><span class="help-inline">, <?php cetak(date('d-M-Y', strtotime($tanggal))) ?></span>
                    </div>
                  </div>
                </div>
                <div class="row static-info">
                  <div class="col-md-9 name">
                    <label></label>
                  </div>
                  <div class="col-md-3">
                    <label><?php cetak($this->session->userdata('name')) ?></label>
                  </div>
                </div>
              </fieldset>
              <br><br>
              <div class="row">
                <div class="col-md-offset-4 col-md-9">
                <?php if($show_data->jenis_dokumen == 'draft'): ?>
                  <button id="submit" name="send" type="submit" onclick="return confirm('Apakah anda yakin mengirimkan data ini?');" class="btn btn-sm blue-dark"><i class="fa fa-paper-plane-o"></i>&nbsp;&nbsp;Kirim Review</button>
                <?php endif; ?>
                <?php if($show_data->status_dokumen == '3'): ?>
                  <button id="submit" name="revisi" type="submit" onclick="return confirm('Apakah anda yakin mengirimkan data ini?');" class="btn btn-sm blue-dark"><i class="fa fa-paper-plane-o"></i>&nbsp;&nbsp;Kirim Revisi</button>
                <?php endif; ?>
                <?php if ($show_data->jenis_dokumen == 'draft'): ?>
                  <button id="submit" name="save" type="submit" class="btn btn-sm green-seagreen"><i class="fa fa-paperclip"></i>&nbsp;&nbsp;Save as Draft</button>
                <?php endif; ?>
                  <a href="<?php echo site_url('gratifikasi/riwayat_gratifikasi'); ?>" class="btn btn-sm grey"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</a>
                </div>
              </div>
            </form>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
</div>

<script src="<?= site_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?= site_url('assets/frontend/pages/scripts/jquery.price_format.1.8.min.js'); ?>"></script>
<script src="<?= site_url('assets/frontend/pages/scripts/charcount.js'); ?>"></script>
<script>
  jQuery(function($) {
    $("#nilai_nominal").priceFormat({
        prefix: 'Rp. ',
        centsLimit: 0,
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
  });

  function checkonlynumber(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber1(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber2(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber3(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber4(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber5(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber6(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber7(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber8(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber9(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber10(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber11(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber12(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }

  function Check1(that) {
      if (that.value == "7"){
          document.getElementById("peristiwa_lainnya_check").style.display = "block";
      }
  }

  function Check2(that) {
      if (that.value == "ada"){
          document.getElementById("dokumen_dilampirkan_check").style.display = "block";
      }
  }

  var upload_number = 2;

  function addFile() {
        var br = document.createElement("br");
        var br2 = document.createElement("br");
        var d = document.createElement("div");
        d.setAttribute("class", "fileinput fileinput-new");
        d.setAttribute("data-provides", "fileinput");

        var s = document.createElement("span");
        s.setAttribute("class", "btn default btn-file");
        d.appendChild(s);


        var s2 = document.createElement("span");
        s2.setAttribute("class", "fileinput-new");
        s.appendChild(s2);

        var p = document.createTextNode("Pilih File");

        s2.appendChild(p);

        var s3 = document.createElement("span");
        s3.setAttribute("class", "fileinput-exists");
        s.appendChild(s3);

        var u = document.createTextNode("Ubah");
        s3.appendChild(u);

        var file = document.createElement("input");
        file.setAttribute("type", "file");
        file.setAttribute("name", "file[]");
        file.setAttribute("id", "file");
        file.setAttribute("onchange", "document.getElementById('moreFileLink').style.display = 'block';");
        s.appendChild(file);

        var s4 = document.createElement("span");
        s4.setAttribute("class", "fileinput-filename");
        d.appendChild(s4);

        var spasi = document.createTextNode("\u00A0");
        d.appendChild(spasi);

        var s5 = document.createElement("a");
        s5.setAttribute("href", "#");
        s5.setAttribute("class", "close fileinput-exists");
        s5.setAttribute("data-dismiss", "fileinput");
        d.appendChild(s5);

        document.getElementById("moreFile").appendChild(br);
        document.getElementById("moreFile").appendChild(d);
        document.getElementById("moreFile").appendChild(br2);
        upload_number++;
    }
</script>