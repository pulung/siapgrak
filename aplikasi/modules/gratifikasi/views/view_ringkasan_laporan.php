<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
      <h1>Dashboard Pengendalian Gratifikasi</h1><br>
      <div class="content-form-page">
        <div class="row">
          <!--<div class="col-md-6 col-sm-6">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-hand-paper-o"></i>Total Laporan Penolakan Gratifikasi
                </div>
              </div>
              <div class="portlet-body">
                <div id="echart_pie" style="height:385px;"></div>
                <table class="table table-bordered table-hover">
                  <tbody>
                    <tr class="info">
                        <th style="text-align:center;">Tahun</th>
                        <th style="text-align:center;">Jumlah Laporan Penolakan Gratifikasi</th>
                    </tr>
                    <?php //foreach ($total_laporan_penolakan as $row):?>
                    <tr>
                        <td align="center"><?php //cetak($row->tahun) ?></td>
                        <td align="center"><?php //cetak($row->jumlah) ?></td>
                    </tr>
                    <?php //endforeach;?>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-gift"></i>Total Laporan Penerimaan Gratifikasi
                </div>
              </div>
              <div class="portlet-body">
                <div id="echart_pie1" style="height:385px;"></div>
				<table class="table table-bordered table-hover">
                  <tbody>
                    <tr class="info">
                        <th style="text-align:center;">Tahun</th>
                        <th style="text-align:center;">Jumlah Laporan Penerimaan Gratifikasi</th>
                    </tr>
                    <?php //foreach ($total_laporan_penerimaan as $row):?>
                    <tr>
                        <td align="center"><?php //cetak($row->tahun) ?></td>
                        <td align="center"><?php //cetak($row->jumlah) ?></td>
                    </tr>
                    <?php //endforeach;?>
                    
                  </tbody>
                </table>
                
              </div>
            </div>
          </div>
        </div>-->
		<div class="col-md-12 col-sm-6">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-hand-paper-o"></i>Total Laporan Gratifikasi
                </div>
              </div>
              <div class="portlet-body">
                <div id="echart_pie" style="height:385px;"></div>
                <table class="table table-bordered table-hover">
                  <tbody>
                    <tr class="info">
                        <th style="text-align:center;">Tahun</th>
                        <th style="text-align:center;">Jenis Laporan</th>
                        <th style="text-align:center;">Jumlah</th>
                        <th style="text-align:center;">Aksi</th>
                    </tr>
                    <?php foreach ($total_laporan_penolakan as $row):?>
                    <tr>
                        <td align="center"><?php cetak($row->tahun) ?></td>
                        <td align="center">
						<?php 
							if ($row->status == 'tolak'){
								echo "Laporan Penolakan Gratifikasi"; 
							}else{
								echo "Laporan Penerimaan Gratifikasi";
							} 
						?>
						</td>
                        <td align="center"><?php cetak($row->jumlah) ?></td>
                        <td align="center"><a href="<?= site_url()?>gratifikasi/detail_penolakan_gratifikasi_menteri" style="color:blue;">Lihat Detail</a></td>
                    </tr>
                    <?php endforeach;?>
					<?php foreach ($total_laporan_penerimaan as $row):?>
                    <tr>
                        <td align="center"><?php cetak($row->tahun) ?></td>
                        <td align="center">
						<?php 
							if ($row->status == 'terima'){
								echo "Laporan Penerimaan Gratifikasi"; 
							}else{
								echo "Laporan Penolakan Gratifikasi";
							} 
						?>
						</td>
                        <td align="center"><?php cetak($row->jumlah) ?></td>
						<td align="center"><a href="<?= site_url()?>gratifikasi/detail_penerimaan_gratifikasi_menteri" style="color:blue;">Lihat Detail</a></td>
                    </tr>
                    <?php endforeach;?>
                    
                  </tbody>
                </table>
              </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-signal"></i>Tahap Penanganan Laporan Gratifikasi
                </div>
              </div>
              <div class="portlet-body">
                <div id="echart_pie2" style="height:385px;"></div>
                <table class="table table-bordered table-hover">
                  <tbody>
                    <tr class="info">
                        <th style="text-align:center;">Tahapan</th>
                        <th style="text-align:center;">Jumlah Laporan</th>
                    </tr>
                    <?php foreach ($total_tahap_penanganan as $row):?>
                    <tr>
                        <td><?php cetak($row->nama_status_dokumen) ?></td>
                        <td align="center"><?php cetak($row->jumlah) ?></td>
                    </tr>
                    <?php endforeach;?>
                    <tr>
                        <th style="text-align:center;">Total</th>
                        <th style="text-align:center;"><?php cetak($sum_tahap_penanganan->jumlah) ?></th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-signal"></i>Nilai Nominal Laporan Gratifikasi
                </div>
              </div>
              <div class="portlet-body">
                <div id="echart_pie3" style="height:385px;"></div>
                <table class="table table-bordered table-hover">
                  <tbody>
                    <tr class="info">
                        <th style="text-align:center;">Tahapan</th>
                        <th style="text-align:center;">Nilai Nominal (Rupiah)</th>
                    </tr>
                    <?php foreach ($nominal_tahap_penanganan as $row):?>
                    <tr>
                        <td><?php cetak($row->nama_status_dokumen) ?></td>
                        <td align="right"><?php cetak(number_format($row->jumlah, 2, ',', '.')) ?></td>
                    </tr>
                    <?php endforeach;?>
                    <tr>
                        <th style="text-align:center;">Total</th>
                        <th style="text-align:right;"><?php cetak(number_format($sum_nominal_penanganan->jumlah, 2, ',', '.')) ?></th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
</div>
<script src="<?= site_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?= site_url('assets/global/plugins/e_chart/echarts-all.js') ?>"></script>
<script src="<?= site_url('assets/global/plugins/e_chart/green.js') ?>"></script>
<script>    
    /*var myChart = echarts.init(document.getElementById('echart_pie'), theme);
    myChart.setTheme({color:['#649EFF','#4188FF','#00719C','#02344D', '#FF5E00' ,'#FAA20D','#77BE9E']});
    myChart.setOption({
    tooltip: {
    trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
            legend: {
            //orient: 'vertical',
            //x: 'left',
            x: 'center',
                    y: 'bottom',
                    data: []
            },
            toolbox: {
            show: false,
                    feature: {
                    magicType: {
                    show: true,
                            type: ['pie', 'funnel'],
                            option: {
                            funnel: {
                            x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1548
                            }
                            }
                    },
                            restore: {
                            show: true
                            },
                            saveAsImage: {
                            show: true
                            }
                    }
            },
            calculable: true,
            series: [{
            name: 'Grafik Jumlah Penolakan Gratifikasi',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '48%'], //left,top
                    data: [
                        <?php foreach ($total_laporan_penolakan as $row): ?>
                            {
                                value: <?= $row->jumlah; ?>,
                                name: '<?= $row->tahun; ?>'
                            },
                        <?php endforeach; ?>
                    ]
            }]
    });

    var myChart = echarts.init(document.getElementById('echart_pie1'), theme);
    myChart.setTheme({color:['#649EFF','#4188FF','#00719C','#02344D', '#FF5E00' ,'#FAA20D','#77BE9E']});
    myChart.setOption({
    tooltip: {
    trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
            legend: {
            //orient: 'vertical',
            //x: 'left',
            x: 'center',
                    y: 'bottom',
                    data: []
            },
            toolbox: {
            show: false,
                    feature: {
                    magicType: {
                    show: true,
                            type: ['pie', 'funnel'],
                            option: {
                            funnel: {
                            x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1548
                            }
                            }
                    },
                            restore: {
                            show: true
                            },
                            saveAsImage: {
                            show: true
                            }
                    }
            },
            calculable: true,
            series: [{
            name: 'Grafik Jumlah Penerimaan Gratifikasi',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '48%'], //left,top
                    data: [
                    <?php foreach ($total_laporan_penerimaan as $row): ?>
                        {
                            value: <?= $row->jumlah; ?>,
                            name: '<?= $row->tahun; ?>'
                        },
                    <?php endforeach; ?>
                    ]
            }]
    });*/
	
	var myChart = echarts.init(document.getElementById('echart_pie'), theme);
    myChart.setTheme({color:['#FAA20D','#649EFF','#4188FF','#00719C','#02344D', '#FF5E00','#77BE9E']});
    myChart.setOption({
    tooltip: {
    trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
            legend: {
            //orient: 'vertical',
            //x: 'left',
            x: 'center',
                    y: 'bottom',
                    data: []
            },
            toolbox: {
            show: false,
                    feature: {
                    magicType: {
                    show: true,
                            type: ['pie', 'funnel'],
                            option: {
                            funnel: {
                            x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1548
                            }
                            }
                    },
                            restore: {
                            show: true
                            },
                            saveAsImage: {
                            show: true
                            }
                    }
            },
            calculable: true,
            series: [{
            name: 'Grafik Gratifikasi',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '48%'], //left,top
                    data: [
                    <?php foreach ($total_laporan as $row):?>
                        {
                            value: <?= $row->jumlah ?>,
                            name: '<?php
							if ($row->status_penerimaan == 'terima'){
								echo "Laporan Penerimaan Gratifikasi"; 
							}else{
								echo "Laporan Penolakan Gratifikasi";
							}  ?>',
                        },
                    <?php endforeach;?>
                    ]
            }]
    });

    var myChart = echarts.init(document.getElementById('echart_pie2'), theme);
    myChart.setTheme({color:['#649EFF','#4188FF','#00719C','#02344D', '#FF5E00' ,'#FAA20D','#77BE9E']});
    myChart.setOption({
    tooltip: {
    trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
            legend: {
            //orient: 'vertical',
            //x: 'left',
            x: 'center',
                    y: 'bottom',
                    data: []
            },
            toolbox: {
            show: false,
                    feature: {
                    magicType: {
                    show: true,
                            type: ['pie', 'funnel'],
                            option: {
                            funnel: {
                            x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1548
                            }
                            }
                    },
                            restore: {
                            show: true
                            },
                            saveAsImage: {
                            show: true
                            }
                    }
            },
            calculable: true,
            series: [{
            name: 'Grafik Gratifikasi',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '48%'], //left,top
                    data: [
                    <?php foreach ($total_tahap_penanganan as $row):?>
                        {
                            value: <?= $row->jumlah ?>,
                            name: '<?= $row->nama_status_dokumen ?>',
                        },
                    <?php endforeach;?>
                    ]
            }]
    });

    var myChart = echarts.init(document.getElementById('echart_pie3'), theme);
    myChart.setTheme({color:['#649EFF','#4188FF','#00719C','#02344D', '#FF5E00' ,'#FAA20D','#77BE9E']});
    myChart.setOption({
    tooltip: {
    trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
            legend: {
            //orient: 'vertical',
            //x: 'left',
            x: 'center',
                    y: 'bottom',
                    data: []
            },
            toolbox: {
            show: false,
                    feature: {
                    magicType: {
                    show: true,
                            type: ['pie', 'funnel'],
                            option: {
                            funnel: {
                            x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1548
                            }
                            }
                    },
                            restore: {
                            show: true
                            },
                            saveAsImage: {
                            show: true
                            }
                    }
            },
            calculable: true,
            series: [{
            name: 'Grafik Gratifikasi',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '48%'], //left,top
                    data: [
                     <?php foreach ($nominal_tahap_penanganan as $row):?>
                        {
                            value: <?= $row->jumlah ?>,
                            name: '<?= $row->nama_status_dokumen ?>',
                        },
                     <?php endforeach;?>
                    ]
            }]
    });
</script>