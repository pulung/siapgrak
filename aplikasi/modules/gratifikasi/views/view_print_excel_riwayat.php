<?php
$filename = 'Inspektorat - Riwayat Gratifikasi '; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.' '.date('d-M-Y', strtotime($dategenerated)).'.xls'.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel6');
//$objWriter->save('php://output');
?>

<div style="margin-top: 6px;"></div>
<table cellpadding="0">
    <tr>
        <td valign="top">Tanggal Pelaporan</td>
        <td align="left">: <?= $dategenerated ?></td>
    </tr>
    <tr>
        <td valign="top">Nama UPG</td>
        <td><?php if (!empty($show_profil->name)): ?> : <?= $show_profil->name ?> <?php else: ?> : <?php endif; ?></td>
    </tr>
    <tr>
        <td valign="top">Instansi</td>
        <td><?php if (!empty($show_profil->inskerja)): ?> : <?= $show_profil->inskerja ?> <?php else: ?> : <?php endif; ?></td>
    </tr>
    <tr>
        <td valign="top">Unit Kerja</td>
        <td><?php if (!empty($show_profil->biro)): ?> : <?= $show_profil->biro ?> <?php else: ?> : <?php endif; ?></td>
    </tr>
    <tr>
        <td valign="top">Sub Unit Kerja</td>
        <td><?php if (!empty($show_profil->bagian)): ?> : <?= $show_profil->bagian ?> <?php else: ?> : <?php endif; ?></td>
    </tr>
    <tr>
        <td valign="top">Alamat</td>
        <td>: <?= $show_profil->alamat_kantor ?> Kel: <?= $show_profil->kelurahan_kantor ?> Kec: <?= $show_profil->kecamatan_kantor ?></td>
    </tr>
    <tr>
        <td valign="top">Provinsi</td>
        <td><?php if (!empty($show_profil->provinsi_kantor)): ?> : <?= $show_profil->nprovinsi_kantor ?> <?php else: ?> : <?php endif; ?></td>
    </tr>
    <tr>
        <td valign="top">Kabupaten/Kota</td>
        <td><?php if (!empty($show_profil->kota_kantor)): ?> : <?= $show_profil->kota_kantor ?> <?php else: ?> : <?php endif; ?></td>
    </tr>
</table>
<div style="margin-top: 6px;"></div>

<div style="margin-top: 6px;"></div><br><br>
<table cellpadding="0" border="1" style="border-spacing: 0px;">
    <thead>               
        <tr style="background-color: #000000; color: #ffffff">
            <td width="50" align="center" valign="top"><b>No</b></td>
            <td align="center" width="150" valign="top"><b>Tanggal Penerimaan</b></td>
            <td align="center" width="100" valign="top"><b>Jenis Laporan</b></td>
            <td align="center" width="200" valign="top"><b>Nomor Laporan</b></td>
            <td align="center" width="150" valign="top"><b>Nama Penerima</b></td>
            <td align="center" width="150" valign="top"><b>Jenis Penerimaan</b></td>
            <td align="center" width="200" valign="top"><b>Nilai Nominal (Rp.)</b></td>
            <td align="center" width="150" valign="top"><b>Nama Pemberi</b></td>
            <td align="center" width="100" valign="top"><b>Status</b></td>
           
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($show_riwayat)): ?>
            <?php
            $i = 0;
            foreach ($show_riwayat as $row):
                $i++;
                ?>
                <tr>
                    <td width="50" align="center" valign="top"><?php echo $i; ?></td>
                    <td align="center" width="150" valign="top"><b><?= date('d-m-Y', strtotime($row->tanggal_penerimaan)); ?></b></td>
                    <td align="center" width="100" valign="top"><b><?= $row->status_penerimaan == 'tolak' ? 'Laporan Penolakan Gratifikasi' : 'Laporan Penerimaan Gratifikasi'; ?></b></td>
                    <td align="center" width="200" valign="top"><b><?= $row->nomor_laporan ?></b></td>
                    <td align="center" width="150" valign="top"><b><?= $row->nama_penerima ?></b></td>
                    <td align="center" width="150" valign="top"><b><?= $row->njenis_penerimaan ?></b></td>
                    <td align="center" width="200" valign="top"><b><?= $row->nilai_nominal ?></b></td>
                    <td align="center" width="150" valign="top"><b><?= $row->nama_pemberi ?></b></td>
                    <td align="center" width="150" valign="top"><b><?= $row->status_dokumen ?></b></td>
					
                    
                </tr>
            <?php
            endforeach;
            ?>
        <?php else : ?>
            <tr style="vertical-align:middle;">
                <td width="50" align="center"><b>-</b></td>
                <td align="center" width="150" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="200" ><b>-</b></td>
                <td align="center" width="150" ><b>-</b></td>
                <td align="center" width="150" ><b>-</b></td>
                <td align="center" width="200" ><b>-</b></td>
                <td align="center" width="150" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                
            </tr>
        <?php endif; ?>
    </tbody>
</table>