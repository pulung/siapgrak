<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
      <!--<h1>Verifikasi Gratifikasi</h1>-->
      <h1>Review Gratifikasi</h1>
      <div class="content-form-page">
        <!-- BEGIN TABS AND TESTIMONIALS -->
        <div class="row mix-block margin-bottom-40">
          <!-- TABS -->
          <div class="col-md-12 tab-style-1">
            <ul class="nav nav-tabs">
              <!--<li class="active"><a href="#tab-1" data-toggle="tab">Belum Verifikasi &nbsp;<span class="badge badge-danger"><?php cetak($count_blm_verified->jumlah) ?></a></li>-->
              <li class="active"><a href="#tab-1" data-toggle="tab">Belum Direview &nbsp;<span class="badge badge-danger"><?php cetak($count_blm_verified->jumlah) ?></a></li>
                <li><a href="#tab-2" data-toggle="tab">Proses Revisi &nbsp;<span class="badge badge-danger"><?php cetak($count_proses_revisi->jumlah) ?></a></li>
              <li><a href="#tab-3" data-toggle="tab">Hasil Revisi &nbsp;<span class="badge badge-danger"><?php cetak($count_revisi->jumlah) ?></a></li>
              <!--<li><a href="#tab-4" data-toggle="tab">Terverifikasi &nbsp;<span class="badge badge-danger"><?php cetak($count_verified->jumlah) ?></a></li>-->
              <li><a href="#tab-4" data-toggle="tab">Telah Direview &nbsp;<span class="badge badge-danger"><?php cetak($count_verified->jumlah) ?></a></li>
              <li><a href="#tab-5" data-toggle="tab">Sudah Kirim KPK &nbsp;<span class="badge badge-danger"><?php cetak($count_terkirim->jumlah) ?></a></li>
			  <li><a href="#tab-6" data-toggle="tab">Riwayat Data &nbsp;<span class="badge badge-danger"><?php cetak($count_riwayat->jumlah) ?></a></li>
              
            </ul>
            <div class="tab-content">
              <div class="tab-pane fade in active" id="tab-1">
                <!--<h1 style="color: #65aed9">Daftar Gratifikasi Belum Verifikasi</h1>-->
                <h1 style="color: #65aed9">Daftar Gratifikasi Belum Review</h1>
                <?= $this->session->flashdata('message'); ?>
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                    <tr class="info">
                      <th style="font-size:12px; text-align:center; width: 5%" >No</th>
                      <th style="font-size:12px; text-align:center;" >Tanggal Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Nama Penerima</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Nilai Nominal (Rp.)</th>
                      <th style="font-size:12px; text-align:center;" >Nama Pemberi</th>
                      <th style="font-size:12px; text-align:center; width: 5%;" >Aksi</th>
                      <th style="font-size:12px; text-align:center; width: 11%;" >Review</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($blm_verified as $row):?>
                    <tr class="odd gradeX">
                      <td style="font-size:11px; text-align:center;"><?php cetak($i++) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tanggal_penerimaan))) ?></td>
                      <td style="font-size:11px; text-align:left;"><?= $row->status_penerimaan == 'tolak' ? 'Laporan Penolakan Gratifikasi' : 'Laporan Penerimaan Gratifikasi'; ?></td>
                      <td style="font-size:11px; text-align:left;"><?= $row->identitas == 'Sebagai Penerima Gratifikasi' ? cetak($row->nama_pelapor) : cetak($row->nama_penerima); ?></td>
                      <td style="font-size:11px; text-align:left;"><?php cetak($row->njenis_penerimaan) ?></td>
                      <td style="font-size:11px; text-align:right;"><?php cetak(number_format($row->nilai_nominal, 2, ',', '.')) ?></td>
                      <td style="font-size:11px; text-align:left;"><?php cetak($row->nama_pemberi) ?></td>
                      <td style="font-size:11px; text-align:center;">
                        <a href="<?= site_url('gratifikasi/update_verifikasi/'.$row->id); ?>" class="btn btn-xs green-seagreen" title="Edit"><i class="fa fa-edit"></i></a> 
                      <td style="font-size:11px; text-align:center;">
                        <a href="<?php echo site_url('gratifikasi/send_verified/'.$row->id); ?>" onclick="return confirm('Apakah anda yakin telah mereview data ini?');" class="btn btn-xs grey-gallery" style="font-size:11px;" title="Review Data"><i class="fa fa-check"></i></a>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <div class="tab-pane fade" id="tab-2">
                <h1 style="color: #65aed9">Daftar Gratifikasi Proses Revisi</h1>
                <?= $this->session->flashdata('message'); ?>
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                  <thead>
                    <tr class="info">
                      <th style="font-size:12px; text-align:center; width: 5%" >No</th>
                      <th style="font-size:12px; text-align:center;" >Tanggal Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Nama Penerima</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Nilai Nominal (Rp.)</th>
                      <th style="font-size:12px; text-align:center;" >Nama Pemberi</th>
                      <th style="font-size:12px; text-align:center; width: 5%;" >Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($proses_revisi as $row):?>
                    <tr class="odd gradeX">
                      <td style="font-size:11px; text-align:center;"><?php cetak($i++) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tanggal_penerimaan))) ?></td>
                      <td style="font-size:11px; text-align:left;"><?= $row->status_penerimaan == 'tolak' ? 'Laporan Penolakan Gratifikasi' : 'Laporan Penerimaan Gratifikasi'; ?></td>
                      <td style="font-size:11px; text-align:left;"><?= $row->identitas == 'Sebagai Penerima Gratifikasi' ? cetak($row->nama_pelapor) : cetak($row->nama_penerima); ?></td>
                      <td style="font-size:11px; text-align:left;"><?php cetak($row->njenis_penerimaan) ?></td>
                      <td style="font-size:11px; text-align:right;"><?php cetak(number_format($row->nilai_nominal, 2, ',', '.')) ?></td>
                      <td style="font-size:11px; text-align:left;"><?php cetak($row->nama_pemberi) ?></td>
                      <td style="font-size:11px; text-align:center;">
                        <a href="<?= site_url('gratifikasi/detail_verifikasi/'.$row->id); ?>" class="btn btn-xs blue-steel" title="Detail"><i class="fa fa-eye"></i></a>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <div class="tab-pane fade" id="tab-3">
                <h1 style="color: #65aed9">Daftar Gratifikasi Sudah Revisi</h1>
                <?= $this->session->flashdata('message'); ?>
                <table class="table table-striped table-bordered table-hover" id="sample_3">
                  <thead>
                    <tr class="info">
                      <th style="font-size:12px; text-align:center; width: 5%" >No</th>
                      <th style="font-size:12px; text-align:center;" >Tanggal Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Nama Penerima</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Nilai Nominal (Rp.)</th>
                      <th style="font-size:12px; text-align:center;" >Nama Pemberi</th>
                      <th style="font-size:12px; text-align:center; width: 5%;" >Aksi</th>
                      <th style="font-size:12px; text-align:center; width: 11%;" >Review</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($revisi as $row):?>
                    <tr class="odd gradeX">
                      <td style="font-size:11px; text-align:center;"><?php cetak($i++) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tanggal_penerimaan))) ?></td>
                      <td style="font-size:11px; text-align:left;"><?= $row->status_penerimaan == 'tolak' ? 'Laporan Penolakan Gratifikasi' : 'Laporan Penerimaan Gratifikasi'; ?></td>
                      <td style="font-size:11px; text-align:left;"><?= $row->identitas == 'Sebagai Penerima Gratifikasi' ? cetak($row->nama_pelapor) : cetak($row->nama_penerima); ?></td>
                      <td style="font-size:11px; text-align:left;"><?php cetak($row->njenis_penerimaan) ?></td>
                      <td style="font-size:11px; text-align:right;"><?php cetak(number_format($row->nilai_nominal, 2, ',', '.')) ?></td>
                      <td style="font-size:11px; text-align:left;"><?php cetak($row->nama_pemberi) ?></td>
                      <td style="font-size:11px; text-align:center;">
                        <a href="<?= site_url('gratifikasi/update_verifikasi/'.$row->id); ?>" class="btn btn-xs green-seagreen" title="Edit"><i class="fa fa-edit"></i></a> 
                      <td style="font-size:11px; text-align:center;">
                        <a href="<?php echo site_url('gratifikasi/send_verified/'.$row->id); ?>" onclick="return confirm('Apakah anda yakin telah mereview data ini?');" class="btn btn-xs grey-gallery" style="font-size:11px;" title="Review Data"><i class="fa fa-check"></i></a>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <div class="tab-pane fade" id="tab-4">
                <h1 style="color: #65aed9">Daftar Gratifikasi Telah Direview</h1>
                <?= $this->session->flashdata('message'); ?>
                <div align="right">
                <a href="<?= site_url('gratifikasi/print_excel') ?>" class="btn btn-sm yellow-casablanca">
                    <i class="fa fa-file-excel-o"></i> Ekspor Excel
                </a>
                <a href="#konfirmasisatu" class="btn btn-sm blue-steel" data-toggle="modal">
                    <i class="fa fa-check-square-o"></i> Konfirmasi Ekspor dan Email
                </a></div><br>
                <table class="table table-striped table-bordered table-hover" id="sample_4">
                  <thead>
                    <tr class="info">
                      <th style="font-size:12px; text-align:center; width: 5%" >No</th>
                      <th style="font-size:12px; text-align:center;" >Tanggal Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Nomor Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Nama Penerima</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Nilai Nominal (Rp.)</th>
                      <th style="font-size:12px; text-align:center;" >Nama Pemberi</th>
                      <!-- <th style="font-size:12px; text-align:center; width: 12%;" >Aksi</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($verified as $row):?>
                    <tr class="odd gradeX">
                      <td style="font-size:11px; text-align:center;"><?php cetak($i++) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tanggal_penerimaan))) ?></td>
                      <td style="font-size:11px; text-align:left;"><?= $row->status_penerimaan == 'tolak' ? 'Laporan Penolakan Gratifikasi' : 'Laporan Penerimaan Gratifikasi'; ?></td>
                      <td style="font-size:11px; text-align:left;"><?php cetak($row->nomor_laporan) ?></td>
                      <td style="font-size:11px; text-align:left;"><?= $row->identitas == 'Sebagai Penerima Gratifikasi' ? cetak($row->nama_pelapor) : cetak($row->nama_penerima); ?></td>
                      <td style="font-size:11px; text-align:left;"><?php cetak($row->njenis_penerimaan) ?></td>
                      <td style="font-size:11px; text-align:right;"><?php cetak(number_format($row->nilai_nominal, 2, ',', '.')) ?></td>
                      <td style="font-size:11px; text-align:left;"><?php cetak($row->nama_pemberi) ?></td>
                      <!-- <td style="font-size:11px; text-align:center;">
                        <a href="<?= site_url('gratifikasi/update_verifikasi/'.$row->id); ?>" class="btn btn-xs green-seagreen" title="Edit"><i class="fa fa-edit"></i></a>
                      </td> -->
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <div class="tab-pane fade" id="tab-5">
                <h1 style="color: #65aed9">Daftar Gratifikasi Sudah Kirim KPK</h1>
                <?= $this->session->flashdata('message'); ?>
                <div align="right">
                <a href="<?= site_url('gratifikasi/print_excel_inspektorat') ?>" class="btn btn-sm yellow-casablanca">
                    <i class="fa fa-file-excel-o"></i> Ekspor Excel
                </a></div><br>
                <table class="table table-striped table-bordered table-hover" id="sample_5">
                  <thead>
                    <tr class="info">
                      <th style="font-size:12px; text-align:center; width: 5%" >No</th>
                      <th style="font-size:12px; text-align:center;" >Tanggal Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Nomor Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Nama Penerima</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Nilai Nominal (Rp.)</th>
                      <th style="font-size:12px; text-align:center;" >Nama Pemberi</th>
                      <th style="font-size:12px; text-align:center; width: 12%;" >Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($terkirim as $row):?>
                    <tr class="odd gradeX">
                      <td style="font-size:11px; text-align:center;"><?php cetak($i++) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tanggal_penerimaan))) ?></td>
                      <td style="font-size:11px; text-align:center;"><?= $row->status_penerimaan == 'tolak' ? 'Laporan Penolakan Gratifikasi' : 'Laporan Penerimaan Gratifikasi'; ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->nomor_laporan) ?></td>
                      <td style="font-size:11px; text-align:center;"><?= $row->identitas == 'Sebagai Penerima Gratifikasi' ? cetak($row->nama_pelapor) : cetak($row->nama_penerima); ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->njenis_penerimaan) ?></td>
                      <td style="font-size:11px; text-align:right;"><?php cetak(number_format($row->nilai_nominal, 2, ',', '.')) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->nama_pemberi) ?></td>
                      <td style="font-size:11px; text-align:center;">
                        <a href="<?= site_url('gratifikasi/detail_verifikasi/'.$row->id); ?>" class="btn btn-xs blue-steel" title="Detail"><i class="fa fa-eye"></i></a>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
			  <div class="tab-pane fade" id="tab-6">
                <h1 style="color: #65aed9">Riwayat Data</h1>
                <?= $this->session->flashdata('message'); ?>
                <div align="right">
                <a href="<?= site_url('gratifikasi/print_excel_riwayat') ?>" class="btn btn-sm yellow-casablanca">
                    <i class="fa fa-file-excel-o"></i> Ekspor Excel
                </a></div><br>
                <table class="table table-striped table-bordered table-hover" id="sample_6">
                  <thead>
                    <tr class="info">
                      <th style="font-size:12px; text-align:center; width: 5%" >No</th>
                      <th style="font-size:12px; text-align:center;" >Tanggal Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Nomor Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Nama Penerima</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Nilai Nominal (Rp.)</th>
                      <th style="font-size:12px; text-align:center;" >Nama Pemberi</th>
                      <th style="font-size:12px; text-align:center; width: 12%;" >Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($riwayat as $row):?>
                    <tr class="odd gradeX">
                      <td style="font-size:11px; text-align:center;"><?php cetak($i++) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tanggal_penerimaan))) ?></td>
                      <td style="font-size:11px; text-align:center;"><?= $row->status_penerimaan == 'tolak' ? 'Laporan Penolakan Gratifikasi' : 'Laporan Penerimaan Gratifikasi'; ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->nomor_laporan) ?></td>
                      <td style="font-size:11px; text-align:center;"><?= $row->identitas == 'Sebagai Penerima Gratifikasi' ? cetak($row->nama_pelapor) : cetak($row->nama_penerima); ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->njenis_penerimaan) ?></td>
                      <td style="font-size:11px; text-align:right;"><?php cetak(number_format($row->nilai_nominal, 2, ',', '.')) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->nama_pemberi) ?></td>
                      <td style="font-size:11px; text-align:center;">
						<?php  
							if($row->status_dokumen == '1'){
								echo 'Draft';
							}elseif($row->status_dokumen == '2'){
								echo 'Belum Direview';
							}elseif($row->status_dokumen == '3'){
								echo 'Revisi';
							}elseif($row->status_dokumen == '4'){
								echo 'Sudah Revisi';
							}elseif($row->status_dokumen == '5'){
								echo 'Telah Direview';
							}elseif($row->status_dokumen == '6'){
								echo 'Dikirim KPK';
							}elseif($row->status_dokumen == '7'){
								echo 'SK Rekomendasi';
							}else{
								echo 'Tindak Lanjut';
							}
						?>
					  
					  </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- END TABS -->
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
</div>
<div id="konfirmasisatu" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>
           Apakah Anda sudah mengekspor ke excel?
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default"><i class="fa fa-times"></i> Batal</button>
        <a href="#konfirmasidua" class="btn btn-sm blue-steel" data-dismiss="modal" data-toggle="modal">
          <i class="fa fa-check"></i> Sudah
        </a>
      </div>
    </div>
  </div>
</div>
<div id="konfirmasidua" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>
           Apakah Anda sudah mengemailkan ke KPK?
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default"><i class="fa fa-times"></i> Batal</button>
        <a href="<?= site_url('gratifikasi/kirim_kpk'); ?>" class="btn btn-sm blue-steel">
          <i class="fa fa-check"></i> Sudah
        </a>
      </div>
    </div>
  </div>
</div>
<script src="<?= site_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
     $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust();
     });   
  });
</script>