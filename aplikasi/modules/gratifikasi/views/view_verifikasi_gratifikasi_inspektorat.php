<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
      <!--<h1>Verifikasi Gratifikasi</h1>-->
      <h1>Riwayat</h1>
      <div class="content-form-page">
        <!-- BEGIN TABS AND TESTIMONIALS -->
        <div class="row mix-block margin-bottom-40">
          <!-- TABS -->
          <div class="col-md-12 tab-style-1">
            <ul class="nav nav-tabs">
              <!--<li class="active"><a href="#tab-1" data-toggle="tab">Belum Verifikasi &nbsp;<span class="badge badge-danger"><?php cetak($count_blm_verified->jumlah) ?></a></li>-->
              <li><a href="#tab-6" data-toggle="tab">Riwayat Data &nbsp;<span class="badge badge-danger"><?php cetak($count_riwayat->jumlah) ?></a></li>
            </ul>
            <div class="tab-content">
             
			  <div class="tab-pane fade in active" id="tab-6">
                <!--<h1 style="color: #65aed9">Daftar Riwayat Data Review Gratifikasi</h1>-->
                <?= $this->session->flashdata('message'); ?>
                <div align="right">
                <a href="<?= site_url('gratifikasi/print_excel_riwayat_inspektorat') ?>" class="btn btn-sm yellow-casablanca">
                    <i class="fa fa-file-excel-o"></i> Eksport Excel
                </a></div><br>
                <table class="table table-striped table-bordered table-hover" id="sample_6">
                  <thead>
                    <tr class="info">
                      <th style="font-size:12px; text-align:center; width: 5%" >No</th>
                      <th style="font-size:12px; text-align:center;" >Tanggal Penerimaan</th>
                      <!--<th style="font-size:12px; text-align:center;" >Jenis Laporan</th>-->
                      <th style="font-size:12px; text-align:center;" >Nomor Laporan</th>
					  <th style="font-size:12px; text-align:center; width: 12%;" >Tanggal Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Nama Penerima</th>
                      <!--<th style="font-size:12px; text-align:center;" >Jenis Penerimaan</th>-->
                      <th style="font-size:12px; text-align:center;" >Nilai Nominal (Rp.)</th>
                      <!--<th style="font-size:12px; text-align:center;" >Nama Pemberi</th>-->
					  
					  <th style="font-size:12px; text-align:center; width: 12%;" >Tanggal Review</th>
					  <th style="font-size:12px; text-align:center; width: 12%;" >Tanggal Dikirim KPK</th>
					  <th style="font-size:12px; text-align:center; width: 12%;" >Tanggal Rekomendasi</th>
					  <th style="font-size:12px; text-align:center; width: 12%;" >Tanggal Tindak Lanjut</th>
					  <th style="font-size:12px; text-align:center; width: 12%;" >Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($riwayat as $row):?>
                    <tr class="odd gradeX">
                      <td style="font-size:11px; text-align:center;"><?php cetak($i++) ?></td>
                      <!--<td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tanggal_penerimaan))) ?></td>-->
                      <td style="font-size:11px; text-align:center;"><?= $row->status_penerimaan == 'tolak' ? 'Laporan Penolakan Gratifikasi' : 'Laporan Penerimaan Gratifikasi'; ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->nomor_laporan) ?></td>
					  <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tgl_laporan))) ?>
                      <td style="font-size:11px; text-align:center;"><?= $row->identitas == 'Sebagai Penerima Gratifikasi' ? cetak($row->nama_pelapor) : cetak($row->nama_penerima); ?></td>
                      <!--<td style="font-size:11px; text-align:center;"><?php cetak($row->njenis_penerimaan) ?></td>-->
                      <td style="font-size:11px; text-align:right;"><?php cetak(number_format($row->nilai_nominal, 2, ',', '.')) ?></td>
                      <!--<td style="font-size:11px; text-align:center;"><?php cetak($row->nama_pemberi) ?></td>-->
                      
					  <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tgl_verifikasi))) ?></td>
					  <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tgl_dikirim))) ?></td>
					  <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tgl_rekomendasi))) ?></td>
					  <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tgl_tindak_lanjut))) ?></td>
                      <td style="font-size:11px; text-align:center;">
						<?php  
							if($row->status_dokumen == '1'){
								echo 'Draft';
							}elseif($row->status_dokumen == '2'){
								echo 'Belum Direview';
							}elseif($row->status_dokumen == '3'){
								echo 'Revisi';
							}elseif($row->status_dokumen == '4'){
								echo 'Sudah Revisi';
							}elseif($row->status_dokumen == '5'){
								echo 'Telah Direview';
							}elseif($row->status_dokumen == '6'){
								echo 'Dikirim KPK';
							}elseif($row->status_dokumen == '7'){
								echo 'SK Rekomendasi';
							}else{
								echo 'Tindak Lanjut';
							}
						?>
					  
					  </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- END TABS -->
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
</div>
<div id="konfirmasisatu" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>
           Apakah Anda sudah mengeksport ke excel?
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default"><i class="fa fa-times"></i> Batal</button>
        <a href="#konfirmasidua" class="btn btn-sm blue-steel" data-dismiss="modal" data-toggle="modal">
          <i class="fa fa-check"></i> Sudah
        </a>
      </div>
    </div>
  </div>
</div>
<div id="konfirmasidua" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>
           Apakah Anda sudah mengemailkan ke KPK?
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default"><i class="fa fa-times"></i> Batal</button>
        <a href="<?= site_url('gratifikasi/kirim_kpk'); ?>" class="btn btn-sm blue-steel">
          <i class="fa fa-check"></i> Sudah
        </a>
      </div>
    </div>
  </div>
</div>
<script src="<?= site_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
     $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust();
     });   
  });
</script>