<html>
	<title>Bukti Pelaporan Gratifikasi</title>
	<body>
	  <div class="row">
        <div class="col-md-12 col-sm-12">
            <table width="100%">
            	<tr style="background-color: #848587;">
            		<td align="center" style="font-size: 18pt; font-weight: bold; font-family: Arial;"><font color="#fff">Penyampaian Laporan Gratifikasi Online<br>&copy; Inspektorat Sekretariat Negara RI</font></td>
            	</tr>
            	<tr style="background-color: #FFFFCC;">
            		<td align="center" style="font-size: 12pt; font-family: Arial;">Berikut ini adalah Bukti Pelaporan Gratifikasi Anda.<br>---------------------------------------------------</td>
            	</tr>
            	<tr style="background-color: #FFFFCC;">
            		<td align="center" style="font-size: 12pt; font-family: Arial;">
            			Nama : <?php cetak($show_profil->name) ?><br>
            			Nomor Laporan : <?php cetak($show_data->nomor_laporan) ?><br>
            			Tanggal Laporan : <?php cetak(date('d-M-Y', strtotime($show_data->tgl_laporan))) ?><br>
            			Jenis Laporan : <?= $show_data->status_penerimaan == 'tolak' ? 'Laporan Penolakan Gratifikasi' : 'Laporan Penerimaan Gratifikasi'; ?><br>
            			Jenis Penerimaan : <?php cetak($show_data->njenis_penerimaan) ?><br>
            			Nominal : Rp. <?php cetak(number_format($show_data->nilai_nominal, 2, ',', '.')) ?><br>
            			Tempat, Tanggal Penerimaan : <?php cetak($show_data->tempat_penerimaan.", ".date('d-M-Y', strtotime($show_data->tanggal_penerimaan))) ?><br><br>
                              <b>Terima kasih telah menyampaikan Laporan Gratifikasi Anda.</b><br><br>&nbsp;
            		</td>
            	</tr>
            </table>
        </div>
      </div>
	</body>
</html>
