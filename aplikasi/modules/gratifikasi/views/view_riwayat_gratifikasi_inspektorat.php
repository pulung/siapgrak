<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
      <h1>Laporan Tahunan</h1>
      <div class="content-form-page">
        <!-- BEGIN TABS AND TESTIMONIALS -->
        <div class="row mix-block margin-bottom-40">
          <!-- TABS -->
          <div class="col-md-12 tab-style-1">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab-5" data-toggle="tab">Laporan Tahunan &nbsp;<span class="badge badge-danger"><?php cetak($count_laporan->jumlah) ?></span></a></li>
            </ul>
            <div class="tab-content">
              
			  <div class="tab-pane fade in active" id="tab-5">
                <h1 style="color: #65aed9">Daftar Laporan Tahunan</h1>
                <?= $this->session->flashdata('message'); ?>
                <table class="table table-striped table-bordered table-hover" id="sample_5">
                  <thead>
                    <tr class="info">
                      <th style="font-size:12px; text-align:center; width: 5%" >No</th>
                      <th style="font-size:12px; text-align:center;" >Nama Pegawai</th>
                      <th style="font-size:12px; text-align:center;" >Jabatan</th>
                      <th style="font-size:12px; text-align:center;" >Nomor Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Status Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Tanggal Laporan</th>
                      <th style="font-size:12px; text-align:center; width: 12%;" >Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($laporan as $row):?>
                    <tr class="odd gradeX">
                      <td style="font-size:11px; text-align:center;"><?php cetak($i++) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->name) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->jabatan) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->nmr_laporan) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->status_laporan) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->tgl_laporan) ?></td>
                      <td style="font-size:11px; text-align:center;">
                        <a href="<?= site_url('gratifikasi/detail_laporan_tahunan_inspektorat/'. $row->id); ?>" class="btn btn-xs blue-steel" title="Detail"><i class="fa fa-eye"></i></a>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- END TABS -->
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
</div>
<script src="<?= site_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
     $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust();
     });   
  });
</script>