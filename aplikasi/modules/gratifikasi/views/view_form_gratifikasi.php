<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
      <h1>Formulir Gratifikasi</h1>
      <div class="content-form-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="alert alert-info display-hide">
              <i class="fa fa-spin fa-spinner"></i> Silahkan tunggu. Proses penyimpanan sedang berjalan.
            </div>
            <div class="alert alert-danger display-hide">
              <button class="close" data-close="alert"></button>
              <i class="fa fa-ban"></i> <strong>Peringatan!</strong> Formulir belum lengkap. Data gagal ditambahkan. 
            </div>
            <?= $this->session->flashdata('message'); ?>
            <fieldset>
              <legend style="color: #65aed9">Identitas</legend>
              <div class="form-group">
				<?php cetak($row['id']) == '1'; ?>
                <?php foreach ($identitas as $row): ?>
                <div class="col-lg-3">
                  <label class="radio-inline">
                    <span><input id="identiti" name="identiti" value="<?php cetak($row['id']) ?>" type="radio" onchange="Check(this);"></span><?php //cetak($row['nama']) ?>
					<?php 
					if ($status_penerimaan == 'tolak'){
					if(($row['id']) == '1'){
						echo "Sebagai Penolak Gratifikasi"; 
					}else{
						echo "Sebagai Pembantu Penolak Gratifikasi";
					}
					}else{
					if(($row['id']) == '1'){
						echo "Sebagai Penerima Gratifikasi"; 
					}else{
						echo "Sebagai Pembantu Penerima Gratifikasi";
					}
					}
					?>
                  </label>
                </div>
                <?php endforeach; ?>
              </div>
            </fieldset>
            <form method="post" id="form_penerima" style='display:none;' role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('gratifikasi/insert'); ?>">
              <?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>
              <fieldset>
                <legend style="color: #65aed9">Identitas Pelapor Gratifikasi</legend>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         Nama Lengkap
                    </div>
					<div class="col-md-2" style="width:1px;">:</div>
                    <div class="col-md-6">
                         <?php cetak($show_profil->name) ?>
                    </div>
                    <input type="text" class="hidden" id="status_penerimaan" name="status_penerimaan" value="<?php cetak($status_penerimaan) ?>">
                    <input type="text" class="hidden" id="identitas" name="identitas" value="Sebagai Penerima Gratifikasi">
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         Tempat &amp; Tanggal Lahir 
                    </div>
					<div class="col-md-2" style="width:1px;">:</div>
                    <div class="col-md-6">
                         <?php cetak($show_profil->tempatlahir.", ".$show_profil->tgllahir) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         No. KTP (NIK)
                    </div>
					<div class="col-md-2" style="width:1px;">:</div>
                    <div class="col-md-6">
                         <?php cetak($show_profil->no_ktp) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         Jabatan/Pangkat/Golongan
                    </div>
					<div class="col-md-2" style="width:1px;">:</div>
					<div class="col-md-6">
                         <?php cetak($show_profil->jabatan ." ". $show_profil->pangkat ." ". $show_profil->golongan) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Alamat Email
                    </div>
					<div class="col-md-2" style="width:1px;">:</div>
                    <div class="col-md-6">
                         <?php cetak($show_profil->email) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Nomor Telepon Seluler
                    </div>
					<div class="col-md-2" style="width:1px;">:</div>
                    <div class="col-md-6">
                         <?php cetak($show_profil->nohp) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Pin BB/WA
                    </div>
					<div class="col-md-2" style="width:1px;">:</div>
                    <div class="col-md-6">
                         <?php cetak($show_profil->pin_bb) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Nama Instansi
                    </div>
					<div class="col-md-2" style="width:1px;">:</div>
                    <div class="col-md-6">
                         <?php cetak($show_profil->inskerja) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Unit Kerja/Sub Unit Kerja
                    </div>
					<div class="col-md-2" style="width:1px;">:</div>
                    <div class="col-md-6">
                         <?php cetak($show_profil->biro." ".$show_profil->bagian) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Alamat Kantor
                    </div>
					<div class="col-md-2" style="width:1px;">:</div>
                    <div class="col-md-6">
                         <?php cetak($show_profil->alamat_kantor) ?><br><?php cetak($show_profil->kelurahan_kantor." ".$show_profil->kecamatan_kantor.", ".$show_profil->kode_pos_kantor) ?><br><?php cetak($show_profil->kota_kantor.", ".$show_profil->nprovinsi_kantor) ?><br>Telp: <?php cetak($show_profil->no_kantor) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Alamat Rumah
                    </div>
					<div class="col-md-2" style="width:1px;">:</div>
                    <div class="col-md-6">
                         <?php cetak($show_profil->alamat_rumah) ?><br><?php cetak($show_profil->kelurahan_rumah." ".$show_profil->kecamatan_rumah.", ".$show_profil->kode_pos_rumah) ?><br><?php cetak($show_profil->kota_rumah.", ".$show_profil->nprovinsi_rumah) ?><br>Telp: <?php cetak($show_profil->no_rumah) ?>
                    </div>
                </div>
              </fieldset>
              <hr>
              <fieldset>
                <?php if ($status_penerimaan == 'tolak'): ?>
                <legend style="color: #65aed9">Data Penolakan Gratifikasi</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Jenis Penolakan <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="jenis_penerimaan" id="jenis_penerimaan" class="form-control">
                        <?php foreach ($jenis_penerimaan as $row): ?>
                          <option value="<?php cetak($row['id']) ?>"><?php cetak($row['jenis_penerimaan']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <!--<label>Uraian Gratifikasi yang Ditolak (max. 250 karakter) <span class="require">*</span></label>-->
                    <label>Uraian Gratifikasi yang Ditolak <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <!--<textarea class="form-control" name="uraian" maxlength="250"></textarea>-->
                      <textarea class="form-control" name="uraian"></textarea>
                    </div>
                  </div>
                </div>
				<div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Jenis Mata Uang yang Diterima <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="mata_uang" id="mata_uang" class="form-control">
                        <?php foreach ($mata_uang as $row): ?>
                          <option value="<?php cetak($row['kode']) ?>"><?php cetak($row['negara']) ?> <?php echo " - "; ?><?php cetak($row['kode']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Harga/Nilai Nominal/Taksiran <b> dalam Rupiah </b> <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="nilai_nominal_pen" name="nilai_nominal">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Kode Peristiwa Penolakan <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="kode_peristiwa" id="kode_peristiwa" class="form-control" onchange="Check1a(this);">
                        <?php foreach ($peristiwa_penerimaan as $row): ?>
                          <option value="<?php cetak($row['id']) ?>"><?php cetak($row['peristiwa_penerimaan']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group" id="peristiwa_lainnya_check_pen" style='display:none;'>
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="peristiwa_lainnya" name="peristiwa_lainnya" placeholder="Peristiwa Penerimaan Lainnya">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Tempat &amp; Tanggal Penolakan <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="tempat_penerimaan" name="tempat_penerimaan">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <div class="input-group">    
                          <div class="input-icon right">
                              <i class="fa"></i>
                              <input class="form-control date-picker input-sm" onkeyup="checkonlynumber11(this);"size="16" placeholder="yyyy-mm-dd" type="text" id="tanggal_penerimaan" name="tanggal_penerimaan"/>
                          </div>
                          <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </span>
                      </div>
                    </div>
                  </div>
                </div>
                <?php else: ?>
                <legend style="color: #65aed9">Data Penerimaan Gratifikasi</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Jenis Penerimaan <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="jenis_penerimaan" id="jenis_penerimaan" class="form-control">
                        <?php foreach ($jenis_penerimaan as $row): ?>
                          <option value="<?php cetak($row['id']) ?>"><?php cetak($row['jenis_penerimaan']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <!--<label>Uraian Gratifikasi yang Diterima (max. 250 karakter) <span class="require">*</span></label>-->
                    <label>Uraian Gratifikasi yang Diterima <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <!--<textarea class="form-control" name="uraian" maxlength="250"></textarea>-->
                      <textarea class="form-control" name="uraian"></textarea>
                    </div>
                  </div>
                </div>
				<div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Jenis Mata Uang yang Diterima <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="mata_uang" id="mata_uang" class="form-control">
                        <?php foreach ($mata_uang as $row): ?>
                          <option value="<?php cetak($row['kode']) ?>"><?php cetak($row['negara']) ?> <?php echo " - "; ?><?php cetak($row['kode']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Harga/Nilai Nominal/Taksiran <b> dalam Rupiah </b> <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="nilai_nominal_pen" name="nilai_nominal">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Kode Peristiwa Penerimaan <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="kode_peristiwa" id="kode_peristiwa" class="form-control" onchange="Check1a(this);">
                        <?php foreach ($peristiwa_penerimaan as $row): ?>
                          <option value="<?php cetak($row['id']) ?>"><?php cetak($row['peristiwa_penerimaan']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group" id="peristiwa_lainnya_check_pen" style='display:none;'>
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="peristiwa_lainnya" name="peristiwa_lainnya" placeholder="Peristiwa Penerimaan Lainnya">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Tempat &amp; Tanggal Penerimaan <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="tempat_penerimaan" name="tempat_penerimaan">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <div class="input-group">    
                          <div class="input-icon right">
                              <i class="fa"></i>
                              <input class="form-control date-picker input-sm" onkeyup="checkonlynumber13(this);" placeholder="yyyy-mm-dd" size="16" type="text" id="tanggal_penerimaan" name="tanggal_penerimaan"/>
                          </div>
                          <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </span>
                      </div>
                    </div>
                  </div>
                </div>
                <?php endif; ?>
              </fieldset>
              <hr>
              <fieldset>
                <legend style="color: #65aed9">Data Pemberi Gratifikasi</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Nama <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="nama_pemberi" name="nama_pemberi">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Pekerjaan dan Jabatan <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="pekerjaan_pemberi" name="pekerjaan_pemberi">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Alamat <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="alamat_pemberi" name="alamat_pemberi"></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label</label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber8(this);" class="form-control" id="telepon_pemberi" name="telepon_pemberi" placeholder="Nomor Telepon">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" onkeyup="checkonlynumber9(this);" class="form-control" id="faks_pemberi" name="faks_pemberi" placeholder="Faks">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Email </label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="email" class="form-control" id="email_pemberi" name="email_pemberi">
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Hubungan dengan Pemberi <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" id="hubungan_pemberi" name="hubungan_pemberi">
                    </div>
                  </div>
                </div>
              </fieldset>
              <hr>
              <fieldset>
                <legend style="color: #65aed9">Alasan dan Kronologi</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Alasan Pemberian <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="alasan_pemberian" name="alasan_pemberian"></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Kronologi <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="kronologi_penerimaan" name="kronologi_penerimaan"></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Dokumen yang dilampirkan <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select class="form-control" name="status_dokumen_dilampirkan" id="status_dokumen_dilampirkan" onchange="Check2(this);">
                        <option value="tidak">Tidak Ada</option>
                        <option value="ada">Ada</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group" id="dokumen_dilampirkan_check_pen" style='display:none;'>
                  <div class="col-md-3 name">
                    <label></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="dokumen_dilampirkan" name="dokumen_dilampirkan" placeholder="Yaitu"></textarea>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Catatan Tambahan (bila perlu) </label>
                  </div>
                  <div class="col-md-8">
                    <textarea class="form-control" id="catatan_tambahan" name="catatan_tambahan"></textarea>
                  </div>
                </div>
              </fieldset>
              <hr>
              <fieldset>
                <legend style="color: #65aed9">Data Tambahan</legend>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Upload File</label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <div class="input-group">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn default btn-file">
                                <span class="fileinput-new">
                                    Pilih File </span>
                                <span class="fileinput-exists">
                                    Ubah </span>
                                <input type="file" name="filesatu" id="filesatu" onchange="document.getElementById('moreFileLink_pen').style.display = 'block';">
                            </span>
                            <span class="fileinput-filename">
                            </span>
                            &nbsp; <a href="#" class="close fileinput-exists" data-dismiss="fileinput">
                            </a>
                        </div>
                        <div id="moreFile_pen"></div>
                        <div id="moreFileLink_pen" style="display:none;"><a href="javascript:addFile_pen();">Tambah File Lain</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
              <hr>
              <fieldset>
                <div class="row static-info form-group">
                  <div class="col-md-12 name">
                    <span class="help-block">
                      <label class="checkbox-inline" style="text-align: justify;">
                        <div class="checker" id="uniform-inlineCheckbox1"><span><input id="persyaratan" name="persyaratan" value="option1" type="checkbox"></span></div> Laporan Gratifikasi ini saya sampaikan dengan sebenar-benarnya. Apabila ada yang sengaja tidak saya laporkan atau saya laporkan kepada Komisi Pemberantasan Korupsi secara tidak benar, maka saya bersedia mempertanggungjawabkannya secara hukum sesuai dengan peraturan perundang-undangan yang berlaku dan saya bersedia memberikan keterangan selanjutnya.
                      </label>
                    </span>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-8 name">
                    <label></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control input-inline input-sm" id="tempat_laporan" name="tempat_laporan" placeholder="Tempat"><span class="help-inline">, <?php cetak(date('d-M-Y', strtotime($tanggal))) ?></span>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-9 name">
                    <label></label>
                  </div>
                  <div class="col-md-3">
                    <label><?php cetak($this->session->userdata('name')) ?></label>
                  </div>
                </div>
              </fieldset>
              <br><br>
              <div class="row">
                <div class="col-md-offset-4 col-md-9">
                  <button id="submit" name="send" type="submit" onclick="return confirm('Apakah anda yakin mengirimkan data ini?');" class="btn btn-sm blue-dark"><i class="fa fa-paper-plane-o"></i>&nbsp;&nbsp;Kirim UPG</button>
                  <button id="submit" name="save" type="submit" class="btn btn-sm green-seagreen"><i class="fa fa-paperclip"></i>&nbsp;&nbsp;Save as Draft</button>
                  <a href="<?php echo site_url('beranda'); ?>" class="btn btn-sm grey"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</a>
                </div>
              </div>
            </form>
            <form method="post" id="form_pelapor" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('gratifikasi/insert'); ?>">
              <?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>
              <div id="pelapor_check" style='display:none;'>
                <fieldset>
                  <legend style="color: #65aed9">Identitas Pelapor Gratifikasi</legend>
                  <div class="row static-info">
                      <div class="col-md-3 name">
                           Nama Lengkap
                      </div>
					  <div class="col-md-2" style="width:1px;">:</div>
                      <div class="col-md-6">
                           <?php cetak($show_profil->name) ?>
                      </div>
                      <input type="text" class="hidden" id="status_penerimaan" name="status_penerimaan" value="<?php cetak($status_penerimaan) ?>">
                      <input type="text" class="hidden" id="identitas" name="identitas" value="Sebagai Pelapor Gratifikasi">
                  </div>
                  <div class="row static-info">
                      <div class="col-md-3 name">
                           Tempat &amp; Tanggal Lahir 
                      </div>
					  <div class="col-md-2" style="width:1px;">:</div>
                      <div class="col-md-6">
                           <?php cetak($show_profil->tempatlahir.", ".$show_profil->tgllahir) ?>
                      </div>
                  </div>
                  <div class="row static-info">
                      <div class="col-md-3 name">
                           No. KTP (NIK)
                      </div>
					  <div class="col-md-2" style="width:1px;">:</div>
                      <div class="col-md-6">
                           <?php cetak($show_profil->no_ktp) ?>
                      </div>
                  </div>
                  <div class="row static-info">
                      <div class="col-md-3 name">
                           Jabatan/Pangkat/Golongan
                      </div>
					  <div class="col-md-2" style="width:1px;">:</div>
                      <div class="col-md-6">
                           <?php cetak($show_profil->jabatan ." ". $show_profil->pangkat ." ". $show_profil->golongan) ?>
                      </div>
                  </div>
                  <div class="row static-info">
                      <div class="col-md-3 name">
                          Alamat Email
                      </div>
					  <div class="col-md-2" style="width:1px;">:</div>
                      <div class="col-md-6">
                           <?php cetak($show_profil->email) ?>
                      </div>
                  </div>
                  <div class="row static-info">
                      <div class="col-md-3 name">
                          Nomor Telepon Seluler
                      </div>
					  <div class="col-md-2" style="width:1px;">:</div>
                      <div class="col-md-6">
                           <?php cetak($show_profil->nohp) ?>
                      </div>
                  </div>
                  <div class="row static-info">
                      <div class="col-md-3 name">
                          Pin BB/WA
                      </div>
					  <div class="col-md-2" style="width:1px;">:</div>
                      <div class="col-md-6">
                           <?php cetak($show_profil->pin_bb) ?>
                      </div>
                  </div>
                  <div class="row static-info">
                      <div class="col-md-3 name">
                          Nama Instansi
                      </div>
					  <div class="col-md-2" style="width:1px;">:</div>
                      <div class="col-md-6">
                           <?php cetak($show_profil->inskerja) ?>
                      </div>
                  </div>
                  <div class="row static-info">
                      <div class="col-md-3 name">
                          Unit Kerja/Sub Unit Kerja
                      </div>
					  <div class="col-md-2" style="width:1px;">:</div>
                      <div class="col-md-6">
                           <?php cetak($show_profil->biro." ".$show_profil->bagian) ?>
                      </div>
                  </div>
                  <div class="row static-info">
                      <div class="col-md-3 name">
                          Alamat kantor
                      </div>
					  <div class="col-md-2" style="width:1px;">:</div>
                      <div class="col-md-6">
                           <?php cetak($show_profil->alamat_kantor) ?><br><?php cetak($show_profil->kelurahan_kantor." ".$show_profil->kecamatan_kantor.", ".$show_profil->kode_pos_kantor) ?><br><?php cetak($show_profil->kota_kantor.", ".$show_profil->nprovinsi_kantor) ?><br>Telp: <?php cetak($show_profil->no_kantor) ?>
                      </div>
                  </div>
                  <div class="row static-info">
                      <div class="col-md-3 name">
                          Alamat Rumah
                      </div>
					  <div class="col-md-2" style="width:1px;">:</div>
                      <div class="col-md-6">
                           <?php cetak($show_profil->alamat_rumah) ?><br><?php cetak($show_profil->kelurahan_rumah." ".$show_profil->kecamatan_rumah.", ".$show_profil->kode_pos_rumah) ?><br><?php cetak($show_profil->kota_rumah.", ".$show_profil->nprovinsi_rumah) ?><br>Telp: <?php cetak($show_profil->no_rumah) ?>
                      </div>
                  </div>
                </fieldset>
                <hr>
              </div>
              <div id="identitas_check" style='display:none;'>
                <fieldset>
                  <legend style="color: #65aed9">Identitas Penerima Gratifikasi</legend>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                        <label>NIP/Username SSO <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <select id="id" name="id" class="form-control select2me" data-placeholder="-- Silahkan Pilih --">
                        <option value="">-- Silahkan Pilih --</option>
                        <?php foreach ($user as $row) { ?>
                          <option value="<?php cetak($row['id']) ?>"><?php cetak($row['username']); ?> <?php echo " - "; ?><?php cetak($row['name']); ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label >Nama Lengkap <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="name" name="name">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                        <label >Tempat &amp; Tanggal Lahir <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="tempatlahir" name="tempatlahir">
                      </div>
                    </div>
                     <div class="col-md-4">
                        <div class="input-icon right">
                          <i class="fa"></i>
                          <div class="input-group">    
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input class="form-control date-picker input-sm" onkeyup="checkonlynumber12(this);" placeholder="yyyy-mm-dd" size="16" type="text" id="tgllahir" name="tgllahir"/>
                            </div>
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label >No. KTP (NIK) <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" onkeyup="checkonlynumber(this);" class="form-control" id="no_ktp" name="no_ktp">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label >Alamat Email <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="email" class="form-control" id="email" name="email">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label >Nomor Telepon Seluler <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" onkeyup="checkonlynumber2(this);" class="form-control" id="nohp" name="nohp">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label >Pin BB/WA </label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="pin_bb" name="pin_bb">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label >Nama Instansi <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="inskerja" name="inskerja">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Unit Kerja <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <textarea class="form-control" id="biro" name="biro"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Bagian </label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="bagian" name="bagian">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label >Jabatan <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="jabatan" name="jabatan">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label >Golongan <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="golongan" name="golongan">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label >Pangkat <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="pangkat" name="pangkat">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Alamat Kantor <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <textarea class="form-control" id="alamat_kantor" name="alamat_kantor"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label</label>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="kelurahan_kantor" name="kelurahan_kantor" placeholder="Kel/Desa">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="kecamatan_kantor" name="kecamatan_kantor" placeholder="Kecamatan">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label</label>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="kota_kantor" name="kota_kantor" placeholder="Kota/Kabupaten">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <select name="provinsi_kantor" id="provinsi_kantor" class="form-control">
                          <option value="">-- Silahkan Pilih --</option>
                          <?php foreach ($provinsi as $row): ?>
                            <option value="<?php cetak($row['id']) ?>"><?php cetak($row['name']) ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label</label>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" onkeyup="checkonlynumber3(this);" class="form-control" id="kode_pos_kantor" name="kode_pos_kantor" placeholder="Kode Pos">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" onkeyup="checkonlynumber4(this);" class="form-control" id="no_kantor" name="no_kantor" placeholder="Nomor Telepon kantor">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Alamat Rumah <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <textarea class="form-control" id="alamat_rumah" name="alamat_rumah"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label</label>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="kelurahan_rumah" name="kelurahan_rumah" placeholder="Kel/Desa">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="kecamatan_rumah" name="kecamatan_rumah" placeholder="Kecamatan">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label</label>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="kota_rumah" name="kota_rumah" placeholder="Kota/Kabupaten">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <select name="provinsi_rumah" id="provinsi_rumah" class="form-control">
                          <option value="">-- Silahkan Pilih --</option>
                          <?php foreach ($provinsi as $row): ?>
                            <option value="<?php cetak($row['id']) ?>"><?php cetak($row['name']) ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label</label>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" onkeyup="checkonlynumber5(this);" class="form-control" id="kode_pos_rumah" name="kode_pos_rumah" placeholder="Kode Pos">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" onkeyup="checkonlynumber6(this);" class="form-control" id="no_rumah" name="no_rumah" placeholder="Nomor Telepon rumah">
                      </div>
                    </div>
                  </div>
                </fieldset>
                <hr>
              </div>
              <div id="data_penerima_check" style='display:none;'>
                <fieldset>
                  <?php if ($status_penerimaan == 'tolak'): ?>
                  <legend style="color: #65aed9">Data Penolakan Gratifikasi</legend>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Jenis Penolakan <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <select name="jenis_penerimaan" id="jenis_penerimaan" class="form-control">
                          <?php foreach ($jenis_penerimaan as $row): ?>
                            <option value="<?php cetak($row['id']) ?>"><?php cetak($row['jenis_penerimaan']) ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <!--<label>Uraian Gratifikasi yang Ditolak (max. 250 karakter) <span class="require">*</span></label>-->
                      <label>Uraian Gratifikasi yang Ditolak <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <!--<textarea class="form-control" name="uraian" maxlength="250"></textarea>-->
                        <textarea class="form-control" name="uraian"></textarea>
                      </div>
                    </div>
                  </div>
				  <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Jenis Mata Uang yang Diterima <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="mata_uang" id="mata_uang" class="form-control">
                        <?php foreach ($mata_uang as $row): ?>
                          <option value="<?php cetak($row['kode']) ?>"><?php cetak($row['negara']) ?> <?php echo " - "; ?><?php cetak($row['kode']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Harga/Nilai Nominal/Taksiran <b> dalam Rupiah </b> <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="nilai_nominal_pel" name="nilai_nominal">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Kode Peristiwa Penolakan <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <select name="kode_peristiwa" id="kode_peristiwa" class="form-control" onchange="Check1b(this);">
                          <?php foreach ($peristiwa_penerimaan as $row): ?>
                            <option value="<?php cetak($row['id']) ?>"><?php cetak($row['peristiwa_penerimaan']) ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group" id="peristiwa_lainnya_check_pel" style='display:none;'>
                    <div class="col-md-3 name">
                      <label></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="peristiwa_lainnya" name="peristiwa_lainnya" placeholder="Peristiwa Penerimaan Lainnya">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Tempat &amp; Tanggal Penolakan <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="tempat_penerimaan" name="tempat_penerimaan">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <div class="input-group">    
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input class="form-control date-picker input-sm" onkeyup="checkonlynumber13(this);" placeholder="yyyy-mm-dd" size="16" type="text" id="tanggal_penerimaan" name="tanggal_penerimaan"/>
                            </div>
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php else: ?>
                  <legend style="color: #65aed9">Data Penerimaan Gratifikasi</legend>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Jenis Penerimaan <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <select name="jenis_penerimaan" id="jenis_penerimaan" class="form-control">
                          <?php foreach ($jenis_penerimaan as $row): ?>
                            <option value="<?php cetak($row['id']) ?>"><?php cetak($row['jenis_penerimaan']) ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <!--<label>Uraian Gratifikasi yang Diterima (max. 250 karakter) <span class="require">*</span></label>-->
                      <label>Uraian Gratifikasi yang Diterima <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <!--<textarea class="form-control" name="uraian" maxlength="250"></textarea>-->
                        <textarea class="form-control" name="uraian"></textarea>
                      </div>
                    </div>
                  </div>
				  <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Jenis Mata Uang yang Diterima <span class="require" style="color:red;">*</span></label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <select name="mata_uang" id="mata_uang" class="form-control">
                        <?php foreach ($mata_uang as $row): ?>
                          <option value="<?php cetak($row['kode']) ?>"><?php cetak($row['negara']) ?> <?php echo " - "; ?><?php cetak($row['kode']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Harga/Nilai Nominal/Taksiran <b> dalam Rupiah </b> <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="nilai_nominal_pel" name="nilai_nominal">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Kode Peristiwa Penerimaan <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <select name="kode_peristiwa" id="kode_peristiwa" class="form-control" onchange="Check1b(this);">
                          <?php foreach ($peristiwa_penerimaan as $row): ?>
                            <option value="<?php cetak($row['id']) ?>"><?php cetak($row['peristiwa_penerimaan']) ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group" id="peristiwa_lainnya_check_pel" style='display:none;'>
                    <div class="col-md-3 name">
                      <label></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="peristiwa_lainnya" name="peristiwa_lainnya" placeholder="Peristiwa Penerimaan Lainnya">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Tempat &amp; Tanggal Penerimaan <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="tempat_penerimaan" name="tempat_penerimaan">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <div class="input-group">    
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input class="form-control date-picker input-sm" onkeyup="checkonlynumber14(this);" placeholder="yyyy-mm-dd" size="16" type="text" id="tanggal_penerimaan" name="tanggal_penerimaan"/>
                            </div>
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php endif; ?>
                </fieldset>
                <hr>
              </div>
              <div id="data_pemberi_check" style='display:none;'>
                <fieldset>
                  <legend style="color: #65aed9">Data Pemberi Gratifikasi</legend>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Nama <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="nama_pemberi" name="nama_pemberi">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Pekerjaan dan Jabatan <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="pekerjaan_pemberi" name="pekerjaan_pemberi">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Alamat <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <textarea class="form-control" id="alamat_pemberi" name="alamat_pemberi"></textarea>
                      </div>
                    </div>
                  </div> 
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label</label>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" onkeyup="checkonlynumber8(this);" class="form-control" id="telepon_pemberi" name="telepon_pemberi" placeholder="Nomor Telepon">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" onkeyup="checkonlynumber9(this);" class="form-control" id="faks_pemberi" name="faks_pemberi" placeholder="Faks">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Email </label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="email" class="form-control" id="email_pemberi" name="email_pemberi">
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Hubungan dengan Pemberi <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="text" class="form-control" id="hubungan_pemberi" name="hubungan_pemberi">
                      </div>
                    </div>
                  </div>
                </fieldset>
                <hr>
              </div>
              <div id="alasan_check" style='display:none;'>
                <fieldset>
                  <legend style="color: #65aed9">Alasan dan Kronologi</legend>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Alasan Pemberian <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <textarea class="form-control" id="alasan_pemberian" name="alasan_pemberian"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Kronologi <span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <textarea class="form-control" id="kronologi_penerimaan" name="kronologi_penerimaan"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Dokumen yang dilampirkan<span class="require" style="color:red;">*</span></label>
                    </div>
                    <div class="col-md-4">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <select class="form-control" name="status_dokumen_dilampirkan" id="status_dokumen_dilampirkan" onchange="Check3(this);">
                          <option value="tidak">Tidak Ada</option>
                          <option value="ada">Ada</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group" id="dokumen_dilampirkan_check_pel" style='display:none;'>
                    <div class="col-md-3 name">
                      <label></label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <textarea class="form-control" id="dokumen_dilampirkan" name="dokumen_dilampirkan" placeholder="Yaitu"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Catatan Tambahan (bila perlu) </label>
                    </div>
                    <div class="col-md-8">
                      <textarea class="form-control" id="catatan_tambahan" name="catatan_tambahan"></textarea>
                    </div>
                  </div>
                </fieldset>
                <hr>
              </div>
              <div id="data_tambahan_check" style='display:none;'>
                <fieldset>
                  <legend style="color: #65aed9">Data Tambahan</legend>
                  <div class="row static-info form-group">
                    <div class="col-md-3 name">
                      <label>Upload File</label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-icon right">
                        <i class="fa"></i>
                        <div class="input-group">
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                              <span class="btn default btn-file">
                                  <span class="fileinput-new">
                                      Pilih File </span>
                                  <span class="fileinput-exists">
                                      Ubah </span>
                                  <input type="file" name="filesatu" id="filesatu" onchange="document.getElementById('moreFileLink_pel').style.display = 'block';">
                              </span>
                              <span class="fileinput-filename">
                              </span>
                              &nbsp; <a href="#" class="close fileinput-exists" data-dismiss="fileinput">
                              </a>
                          </div>
                          <div id="moreFile_pel"></div>
                          <div id="moreFileLink_pel" style="display:none;"><a href="javascript:addFile_pel();">Tambah File Lain</a></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </fieldset>
                <hr>
              </div>
              <fieldset id="persyaratan_check" style='display:none;'>
                <div class="row static-info form-group">
                  <div class="col-md-12 name">
                    <span class="help-block">
                      <label class="checkbox-inline" style="text-align: justify;">
                        <div class="checker" id="uniform-inlineCheckbox1"><span><input id="persyaratan" name="persyaratan" value="option1" type="checkbox"></span></div> Laporan Gratifikasi ini saya sampaikan dengan sebenar-benarnya. Apabila ada yang sengaja tidak saya laporkan atau saya laporkan kepada Komisi Pemberantasan Korupsi secara tidak benar, maka saya bersedia mempertanggungjawabkannya secara hukum sesuai dengan peraturan perundang-undangan yang berlaku dan saya bersedia memberikan keterangan selanjutnya.
                      </label>
                    </span>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-8 name">
                    <label></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control input-inline input-sm" id="tempat_laporan" name="tempat_laporan" placeholder="Tempat"><span class="help-inline">, <?php cetak(date('d-M-Y', strtotime($tanggal))) ?></span>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-8 name">
                    <label></label>
                  </div>
                  <div class="col-md-4">
                    <input type="text" class="form-control" id="nama_penerima" name="nama_penerima" readonly>
                  </div>
                </div>
              </fieldset>
              <br><br>
              <div class="row" id="button_check" style='display:none;'>
                <div class="col-md-offset-3 col-md-9">
                  <button id="submit" name="save" type="submit" class="btn btn-sm green-seagreen"><i class="fa fa-paperclip"></i>&nbsp;&nbsp;Save as Draft</button>
                  <a href="<?php echo site_url('beranda'); ?>" class="btn btn-sm grey"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
</div>
<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Informasi Gratifikasi</h4>
      </div>
      <div class="modal-body">
         <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption" style="font-size: 14px">
              <i class="icon-bulb "></i> UU Nomor 20 Tahun 2001, tentang Perubahan atas UU Nomor 31 Tahun 1999 tentang Pemberantasan Tindak Pidana Korupsi
            </div>
          </div>
          <div class="portlet-body">
            <p style="font-size: 10px">
              <b>Pasal 12B</b><br>
              (1) Setiap gratifikasi kepada pegawai negeri atau penyelenggara negara dianggap pemberi suap, apabila berhubungan dengan jabatannya dan yang berlawan dengan kewajibannya atau tugasnya, dengan ketentuan sebagai berikut:<br>
              &nbsp;&nbsp;&nbsp;&nbsp; a. Yang nilainya Rp 10.000.000,00 (sepuluh juta rupiah) atau lebih, pembuktian bahwa gratifikasi tersebut bukan merupakan suap dilakukan oleh penerima gratifikasi;<br>
              &nbsp;&nbsp;&nbsp;&nbsp; b. Yang nilainya kurang dari Rp 10.000.000,00 (sepuluh juta rupiah), pembuktian bahwa gratifikasi tersebut suap dilakukan oleh penuntut hukum;<br>
              (2) Pidana bagi pegawai negeri atau penyelenggara negara sebagaimana dimaksud dalam ayat (1) adalah pidana penjara seumur hidup atau pidana penjara paling singkat 4 (empat) tahun dan paling lama 20 (dua puluh) tahun, dan pidana denda paling sedikit Rp 200.000.000,00 (dua ratus juta rupiah) dan paling banyak Rp 1.000.000.000,00 (satu miyar rupiah).<br>
              <b>Pasal 12C ayat (1):</b> Ketentuan sebagaimana dimaksud dalam Pasal 12B ayat 1 tidak berlaku jika penerima melaporkan gratifikasi yang diterimanya kepada Komisi Pemberantasan Tindak Pidana Korupsi.<br>
              <b>Pasal 12C ayat (2):</b> Penyampaian laporan sebagaimana dimaksud dalam ayat (1) wajib dilakukan oleh penerima gratifikasi paling lambat 30 (tiga puluh) hari kerja terhitung sejak tanggal gratifikasi tersebut diterima.<br>
            </p>
          </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Saya Mengerti</button>
        <button type="button" class="btn default" data-dismiss="modal" onclick="history.back();">Batal</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script src="<?= site_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?= site_url('assets/frontend/pages/scripts/jquery.price_format.1.8.min.js'); ?>"></script>
<script src="<?= site_url('assets/frontend/pages/scripts/charcount.js'); ?>"></script>
<script>
  $(window).on('load',function(){
      $('#static').modal('show');
  });

  jQuery(document).ready(function () {
      $("#id").change(function () {
          var id = {"id": $('#id').val()};

          $.ajax({
              type: "POST",
              data: id,
              url: "<?= site_url('gratifikasi/get_data_user') ?>",
              success: function (data) {
                  result = jQuery.parseJSON(data);
                  $('#name').val(result[0].name);
                  $('#tempatlahir').val(result[0].tempatlahir);
                  $('#tgllahir').val(result[0].tgllahir);
                  $('#no_ktp').val(result[0].no_ktp);
                  $('#jabatan').val(result[0].jabatan);
                  $('#email').val(result[0].email);
                  $('#nohp').val(result[0].nohp);
                  $('#pin_bb').val(result[0].pin_bb);
                  $('#inskerja').val(result[0].inskerja);
                  $('#biro').val(result[0].biro);
                  $('#bagian').val(result[0].bagian);
                  $('#golongan').val(result[0].golongan);
                  $('#pangkat').val(result[0].pangkat);
                  $('#alamat_kantor').val(result[0].alamat_kantor);
                  $('#provinsi_kantor').val(result[0].provinsi_kantor);
                  $('#kota_kantor').val(result[0].kota_kantor);
                  $('#kecamatan_kantor').val(result[0].kecamatan_kantor);
                  $('#kelurahan_kantor').val(result[0].kelurahan_kantor);
                  $('#kode_pos_kantor').val(result[0].kode_pos_kantor);
                  $('#no_kantor').val(result[0].no_kantor);
                  $('#alamat_rumah').val(result[0].alamat_rumah);
                  $('#provinsi_rumah').val(result[0].provinsi_rumah);
                  $('#kota_rumah').val(result[0].kota_rumah);
                  $('#kecamatan_rumah').val(result[0].kecamatan_rumah);
                  $('#kelurahan_rumah').val(result[0].kelurahan_rumah);
                  $('#kode_pos_rumah').val(result[0].kode_pos_rumah);
                  $('#no_rumah').val(result[0].no_rumah);
                  $('#nama_penerima').val(result[0].name);
              }
          });
      });
  });

  /*jQuery(function($) {
    $("#nilai_nominal_pen").priceFormat({
        prefix: 'Rp. ',
        centsLimit: 0,
        //centsLimit: 0,
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
    $("#nilai_nominal_pel").priceFormat({
        prefix: 'Rp. ',
        centsLimit: 0,
        centsLimit: 0
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
  }); */

  //var maxAmount = 250;
  var maxAmount = 100000;
  function textCounter(textField, showCountField) {
    if (textField.value.length > maxAmount) {
      textField.value = textField.value.substring(0, maxAmount);
    } else { 
      showCountField.value = maxAmount - textField.value.length;
    }
  }

  function checkonlynumber(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber1(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber2(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber3(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber4(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber5(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber6(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber7(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber8(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber9(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  /*function checkonlynumber10(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }*/
  function checkonlynumber11(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber12(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber13(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber14(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber15(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }

  function Check(that) {
      if (that.value == "1"){
        document.getElementById("form_penerima").style.display = "block";
        document.getElementById("pelapor_check").style.display = "none";
        document.getElementById("identitas_check").style.display = "none";
        document.getElementById("data_penerima_check").style.display = "none";
        document.getElementById("data_pemberi_check").style.display = "none";
        document.getElementById("alasan_check").style.display = "none";
        document.getElementById("data_tambahan_check").style.display = "none";
        document.getElementById("persyaratan_check").style.display = "none";
        document.getElementById("button_check").style.display = "none";
      } else if (that.value == "2"){
        document.getElementById("pelapor_check").style.display = "block";
        document.getElementById("identitas_check").style.display = "block";
        document.getElementById("data_penerima_check").style.display = "block";
        document.getElementById("data_pemberi_check").style.display = "block";
        document.getElementById("alasan_check").style.display = "block";
        document.getElementById("data_tambahan_check").style.display = "block";
        document.getElementById("button_check").style.display = "block";
        document.getElementById("persyaratan_check").style.display = "block";
        document.getElementById("form_penerima").style.display = "none";
      }
  }

  function Check1a(that) {
      if (that.value == "7"){
          document.getElementById("peristiwa_lainnya_check_pen").style.display = "block";
      } else {
        document.getElementById("peristiwa_lainnya_check_pen").style.display = "none";
      }
  }

  function Check1b(that) {
      if (that.value == "7"){
          document.getElementById("peristiwa_lainnya_check_pel").style.display = "block";
      } else {
        document.getElementById("peristiwa_lainnya_check_pel").style.display = "none";
      }
  }

  function Check2(that) {
      if (that.value == "ada"){
        document.getElementById("dokumen_dilampirkan_check_pen").style.display = "block";
      } else {
        document.getElementById("dokumen_dilampirkan_check_pen").style.display = "none";
      }
  }

  function Check3(that) {
      if (that.value == "ada"){
          document.getElementById("dokumen_dilampirkan_check_pel").style.display = "block";
      } else {
        document.getElementById("dokumen_dilampirkan_check_pel").style.display = "none";
      }
  }

  var upload_number = 2;

  function addFile_pen() {
        var br = document.createElement("br");
        var br2 = document.createElement("br");
        var d = document.createElement("div");
        d.setAttribute("class", "fileinput fileinput-new");
        d.setAttribute("data-provides", "fileinput");

        var s = document.createElement("span");
        s.setAttribute("class", "btn default btn-file");
        d.appendChild(s);


        var s2 = document.createElement("span");
        s2.setAttribute("class", "fileinput-new");
        s.appendChild(s2);

        var p = document.createTextNode("Pilih File");

        s2.appendChild(p);

        var s3 = document.createElement("span");
        s3.setAttribute("class", "fileinput-exists");
        s.appendChild(s3);

        var u = document.createTextNode("Ubah");
        s3.appendChild(u);

        var file = document.createElement("input");
        file.setAttribute("type", "file");
        file.setAttribute("name", "file[]");
        file.setAttribute("id", "file");
        file.setAttribute("onchange", "document.getElementById('moreFileLink_pen').style.display = 'block';");
        s.appendChild(file);

        var s4 = document.createElement("span");
        s4.setAttribute("class", "fileinput-filename");
        d.appendChild(s4);

        var spasi = document.createTextNode("\u00A0");
        d.appendChild(spasi);

        var s5 = document.createElement("a");
        s5.setAttribute("href", "#");
        s5.setAttribute("class", "close fileinput-exists");
        s5.setAttribute("data-dismiss", "fileinput");
        d.appendChild(s5);

        document.getElementById("moreFile_pen").appendChild(br);
        document.getElementById("moreFile_pen").appendChild(d);
        document.getElementById("moreFile_pen").appendChild(br2);
        upload_number++;
    }
  function addFile_pel() {
        var br = document.createElement("br");
        var br2 = document.createElement("br");
        var d = document.createElement("div");
        d.setAttribute("class", "fileinput fileinput-new");
        d.setAttribute("data-provides", "fileinput");

        var s = document.createElement("span");
        s.setAttribute("class", "btn default btn-file");
        d.appendChild(s);


        var s2 = document.createElement("span");
        s2.setAttribute("class", "fileinput-new");
        s.appendChild(s2);

        var p = document.createTextNode("Pilih File");

        s2.appendChild(p);

        var s3 = document.createElement("span");
        s3.setAttribute("class", "fileinput-exists");
        s.appendChild(s3);

        var u = document.createTextNode("Ubah");
        s3.appendChild(u);

        var file = document.createElement("input");
        file.setAttribute("type", "file");
        file.setAttribute("name", "file[]");
        file.setAttribute("id", "file");
        file.setAttribute("onchange", "document.getElementById('moreFileLink_pel').style.display = 'block';");
        s.appendChild(file);

        var s4 = document.createElement("span");
        s4.setAttribute("class", "fileinput-filename");
        d.appendChild(s4);

        var spasi = document.createTextNode("\u00A0");
        d.appendChild(spasi);

        var s5 = document.createElement("a");
        s5.setAttribute("href", "#");
        s5.setAttribute("class", "close fileinput-exists");
        s5.setAttribute("data-dismiss", "fileinput");
        d.appendChild(s5);

        document.getElementById("moreFile_pel").appendChild(br);
        document.getElementById("moreFile_pel").appendChild(d);
        document.getElementById("moreFile_pel").appendChild(br2);
        upload_number++;
    }
</script>
<!--<script>
   $(document).ready(function(){
    $("#tanggal_penerimaan").datepicker({
		dateFormat: 'yyyy-mm-dd'
    })
   })
  </script>-->