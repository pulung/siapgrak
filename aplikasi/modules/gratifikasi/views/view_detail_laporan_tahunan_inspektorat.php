<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
      <h1>Detail Laporan Tahunan Gratifikasi</h1>
      <div class="content-form-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="alert alert-info display-hide">
              <i class="fa fa-spin fa-spinner"></i> Silahkan tunggu. Proses penyimpanan sedang berjalan.
            </div>
            <div class="alert alert-danger display-hide">
              <button class="close" data-close="alert"></button>
              <i class="fa fa-ban"></i> <strong>Peringatan!</strong> Formulir belum lengkap. Data gagal ditambahkan. 
            </div>
            <?= $this->session->flashdata('message'); ?>
            
            <form method="post" id="form_penerima" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('gratifikasi/insert_laporan_tahunan'); ?>">
              <?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>
              <fieldset>
                <div class="row static-info form-group">
                  <div class="col-md-12 name">
                    <span class="help-block">
                      <label class="checkbox-inline" style="text-align: justify;">
                        Yang bertanda tangan di bawah ini, saya :
                      </label>
                    </span>
                  </div>
                </div>
			  </fieldset>
			  <fieldset>
                
                <div class="row static-info">
                    <div class="col-md-3 name">
                         Nama Lengkap
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->name) ?>
                    </div>
                    <input type="text" class="hidden" id="status_penerimaan" name="status_penerimaan" value="<?php cetak($status_penerimaan) ?>">
                    <input type="text" class="hidden" id="identitas" name="identitas" value="Sebagai Penerima Gratifikasi">
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         Tempat &amp; Tanggal Lahir 
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->tempatlahir.", ".$show_profil->tgllahir) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         No. KTP (NIK)
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->no_ktp) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                         Jabatan/Pangkat/Golongan
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->jabatan." ".$show_profil->pangkat ." ".$show_profil->golongan) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Alamat Email
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->email) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Nomor Telepon Seluler
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->nohp) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Pin BB/WA
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->pin_bb) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Nama Instansi
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->inskerja) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Unit Kerja/Sub Unit Kerja
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->biro." ".$show_profil->bagian) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Alamat Kantor
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->alamat_kantor) ?><br>&nbsp;&nbsp;<?php cetak($show_profil->kelurahan_kantor." ".$show_profil->kecamatan_kantor.", ".$show_profil->kode_pos_kantor) ?><br>&nbsp;&nbsp;<?php cetak($show_profil->kota_kantor.", ".$show_profil->nprovinsi_kantor) ?><br>&nbsp;&nbsp;Telp: <?php cetak($show_profil->no_kantor) ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name">
                        Alamat Rumah
                    </div>
                    <div class="col-md-8">
                         : <?php cetak($show_profil->alamat_rumah) ?><br>&nbsp;&nbsp;<?php cetak($show_profil->kelurahan_rumah." ".$show_profil->kecamatan_rumah.", ".$show_profil->kode_pos_rumah) ?><br>&nbsp;&nbsp;<?php cetak($show_profil->kota_rumah.", ".$show_profil->nprovinsi_rumah) ?><br>&nbsp;&nbsp;Telp: <?php cetak($show_profil->no_rumah) ?>
                    </div>
                </div>
              </fieldset>
              <hr>
			  <fieldset>
                <div class="row static-info form-group">
                  <div class="col-md-12 name">
                    <span class="help-block">
                      <label class="checkbox-inline" style="text-align: justify;">
                        Menyatakan bahwa pada tahun ini laporan tahunan gratifikasi saya adalah
                      </label>
                    </span>
                  </div>
                </div>
			  </fieldset>
			  <?php if(!empty($show_riwayat)):?>
				  <div style="display:none"><input id="status_laporan" name="status_laporan" type="text" value="Tidak Nihil"></div>
			  <?php else: ?>
				  <div style="display:none"><input id="status_laporan" name="status_laporan" type="text" value="Nihil"></div>
			  <?php endif; ?>
			  
			  <?php if(!empty($show_riwayat)): ?>
              <fieldset>
				<div class="row static-info">
                    
                <div class="col-md-12 name">
                  <table class="table table-striped table-bordered table-hover" id="sample_6">
                  <thead>
                    <tr class="info">
                      <th style="font-size:12px; text-align:center; width: 5%" >No</th>
                      <th style="font-size:12px; text-align:center;" >Tanggal Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Nomor Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Nama Penerima/Pelapor</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Nilai Nominal (Rp.)</th>
                      <th style="font-size:12px; text-align:center;" >Nama Pemberi</th>
                      <th style="font-size:12px; text-align:center; width: 12%;" >Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($show_riwayat as $row):?>
                    <tr class="odd gradeX">
                      <td style="font-size:11px; text-align:center;"><?php cetak($i++) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tanggal_penerimaan))) ?></td>
                      <td style="font-size:11px; text-align:center;"><?= $row->status_penerimaan == 'tolak' ? 'Laporan Penolakan Gratifikasi' : 'Laporan Penerimaan Gratifikasi'; ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->nomor_laporan) ?></td>
                      <td style="font-size:11px; text-align:center;"><?= $row->identitas == 'Sebagai Penerima Gratifikasi' ? cetak($row->nama_pelapor) : cetak($row->nama_penerima); ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->njenis_penerimaan) ?></td>
                      <td style="font-size:11px; text-align:right;"><?php cetak(number_format($row->nilai_nominal, 2, ',', '.')) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->nama_pemberi) ?></td>
                      <td style="font-size:11px; text-align:center;">
						<?php  
							if($row->status_dokumen == '1'){
								echo 'Draft';
							}elseif($row->status_dokumen == '2'){
								echo 'Belum Review';
							}elseif($row->status_dokumen == '3'){
								echo 'Revisi';
							}elseif($row->status_dokumen == '4'){
								echo 'Sudah Revisi';
							}elseif($row->status_dokumen == '5'){
								echo 'Telah Direview';
							}elseif($row->status_dokumen == '6'){
								echo 'Dikirim KPK';
							}elseif($row->status_dokumen == '7'){
								echo 'SK Rekomendasi';
							}else{
								echo 'Tindak Lanjut';
							}
						?>
					  
					  </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table> 
                    </div>
                </div>
			  </fieldset>
			  <?php else: ?>
			  <fieldset>
                <div class="row static-info form-group">
                  <div class="col-md-12 name">
                    <span class="help-block">
                      <label class="checkbox-inline" style="text-align: justify;">
                        <center>NIHIL</center>
                      </label>
                    </span>
                  </div>
                </div>
			  </fieldset>
			  <?php endif; ?>
              <hr>
              <fieldset>
                <div class="row static-info form-group">
                  <div class="col-md-12 name">
                    <span class="help-block">
                      <label class="checkbox-inline" style="text-align: justify;">
                        <div class="checker" id="uniform-inlineCheckbox1"><span></span></div> Laporan Tahunan ini saya sampaikan dengan sebenar-benarnya. Apabila ada yang sengaja tidak saya laporkan atau saya laporkan kepada UPG secara tidak benar, maka saya bersedia mempertanggungjawabkannya secara hukum sesuai dengan peraturan perundang-undangan yang berlaku dan saya bersedia memberikan keterangan selanjutnya.
                      </label>
                    </span>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-8 name">
                    <label></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right" style="text-align: center">
                      
					  <?php foreach($laporan as $row):?>
                      <?php cetak($row->tempat_laporan); ?>, <?php cetak(date('d-M-Y', strtotime($row->tgl_laporan))) ?>
					  <?php endforeach; ?>
                    </div>
                  </div>
                </div>
                <div class="row static-info form-group">
                  <div class="col-md-9 name">
                    <label></label>
                  </div>
                  <div class="col-md-3">
                    <!--<label><?php cetak($this->session->userdata('name')) ?></label>-->
                    <label><?php cetak($show_profil->name) ?></label>
                  </div>
                </div>
              </fieldset>
              <br><br>
              <div class="row">
                <div class="col-md-offset-4 col-md-9">
                  <a href="<?php echo site_url('gratifikasi/riwayat_gratifikasi_inspektorat'); ?>" class="btn btn-sm grey" style="background-color:orange;"><i class="fa fa-times"></i>&nbsp;&nbsp;Tutup</a>
				</div>
              </div>
            </form>
			

          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
</div>


<script src="<?= site_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?= site_url('assets/frontend/pages/scripts/jquery.price_format.1.8.min.js'); ?>"></script>
<script src="<?= site_url('assets/frontend/pages/scripts/charcount.js'); ?>"></script>
<script>

  jQuery(document).ready(function () {
      $("#id").change(function () {
          var id = {"id": $('#id').val()};

          $.ajax({
              type: "POST",
              data: id,
              url: "<?= site_url('gratifikasi/get_data_user') ?>",
              success: function (data) {
                  result = jQuery.parseJSON(data);
                  $('#name').val(result[0].name);
                  $('#tempatlahir').val(result[0].tempatlahir);
                  $('#tgllahir').val(result[0].tgllahir);
                  $('#no_ktp').val(result[0].no_ktp);
                  $('#jabatan').val(result[0].jabatan);
                  $('#email').val(result[0].email);
                  $('#nohp').val(result[0].nohp);
                  $('#pin_bb').val(result[0].pin_bb);
                  $('#inskerja').val(result[0].inskerja);
                  $('#biro').val(result[0].biro);
                  $('#bagian').val(result[0].bagian);
                  $('#golongan').val(result[0].golongan);
                  $('#pangkat').val(result[0].pangkat);
                  $('#alamat_kantor').val(result[0].alamat_kantor);
                  $('#provinsi_kantor').val(result[0].provinsi_kantor);
                  $('#kota_kantor').val(result[0].kota_kantor);
                  $('#kecamatan_kantor').val(result[0].kecamatan_kantor);
                  $('#kelurahan_kantor').val(result[0].kelurahan_kantor);
                  $('#kode_pos_kantor').val(result[0].kode_pos_kantor);
                  $('#no_kantor').val(result[0].no_kantor);
                  $('#alamat_rumah').val(result[0].alamat_rumah);
                  $('#provinsi_rumah').val(result[0].provinsi_rumah);
                  $('#kota_rumah').val(result[0].kota_rumah);
                  $('#kecamatan_rumah').val(result[0].kecamatan_rumah);
                  $('#kelurahan_rumah').val(result[0].kelurahan_rumah);
                  $('#kode_pos_rumah').val(result[0].kode_pos_rumah);
                  $('#no_rumah').val(result[0].no_rumah);
                  $('#nama_penerima').val(result[0].name);
              }
          });
      });
  });

  jQuery(function($) {
    $("#nilai_nominal_pen").priceFormat({
        //prefix: 'Rp. ',
        //centsLimit: 0,
        centsLimit: 0
        //centsSeparator: ',',
        //thousandsSeparator: '.'
    });
    $("#nilai_nominal_pel").priceFormat({
        //prefix: 'Rp. ',
        //centsLimit: 0,
        centsLimit: 0
        //centsSeparator: ',',
        //thousandsSeparator: '.'
    });
  });

  //var maxAmount = 250;
  var maxAmount = 100000;
  function textCounter(textField, showCountField) {
    if (textField.value.length > maxAmount) {
      textField.value = textField.value.substring(0, maxAmount);
    } else { 
      showCountField.value = maxAmount - textField.value.length;
    }
  }

  function checkonlynumber(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber1(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber2(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber3(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber4(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber5(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber6(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber7(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber8(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber9(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber10(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber11(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber12(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber13(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }
  function checkonlynumber14(ob) {
      var validChars = /[^0-9-+]/gi;
      if (validChars.test(ob.value)) {
          ob.value = ob.value.replace(validChars, "");
      }
  }

  function Check(that) {
      if (that.value == "1"){
        document.getElementById("form_penerima").style.display = "block";
        document.getElementById("pelapor_check").style.display = "none";
        document.getElementById("identitas_check").style.display = "none";
        document.getElementById("data_penerima_check").style.display = "none";
        document.getElementById("data_pemberi_check").style.display = "none";
        document.getElementById("alasan_check").style.display = "none";
        document.getElementById("data_tambahan_check").style.display = "none";
        document.getElementById("persyaratan_check").style.display = "none";
        document.getElementById("button_check").style.display = "none";
      } else if (that.value == "2"){
        document.getElementById("pelapor_check").style.display = "block";
        document.getElementById("identitas_check").style.display = "block";
        document.getElementById("data_penerima_check").style.display = "block";
        document.getElementById("data_pemberi_check").style.display = "block";
        document.getElementById("alasan_check").style.display = "block";
        document.getElementById("data_tambahan_check").style.display = "block";
        document.getElementById("button_check").style.display = "block";
        document.getElementById("persyaratan_check").style.display = "block";
        document.getElementById("form_penerima").style.display = "none";
      }
  }

  function Check1a(that) {
      if (that.value == "7"){
          document.getElementById("peristiwa_lainnya_check_pen").style.display = "block";
      } else {
        document.getElementById("peristiwa_lainnya_check_pen").style.display = "none";
      }
  }

  function Check1b(that) {
      if (that.value == "7"){
          document.getElementById("peristiwa_lainnya_check_pel").style.display = "block";
      } else {
        document.getElementById("peristiwa_lainnya_check_pel").style.display = "none";
      }
  }

  function Check2(that) {
      if (that.value == "ada"){
        document.getElementById("dokumen_dilampirkan_check_pen").style.display = "block";
      } else {
        document.getElementById("dokumen_dilampirkan_check_pen").style.display = "none";
      }
  }

  function Check3(that) {
      if (that.value == "ada"){
          document.getElementById("dokumen_dilampirkan_check_pel").style.display = "block";
      } else {
        document.getElementById("dokumen_dilampirkan_check_pel").style.display = "none";
      }
  }

  var upload_number = 2;

  function addFile_pen() {
        var br = document.createElement("br");
        var br2 = document.createElement("br");
        var d = document.createElement("div");
        d.setAttribute("class", "fileinput fileinput-new");
        d.setAttribute("data-provides", "fileinput");

        var s = document.createElement("span");
        s.setAttribute("class", "btn default btn-file");
        d.appendChild(s);


        var s2 = document.createElement("span");
        s2.setAttribute("class", "fileinput-new");
        s.appendChild(s2);

        var p = document.createTextNode("Pilih File");

        s2.appendChild(p);

        var s3 = document.createElement("span");
        s3.setAttribute("class", "fileinput-exists");
        s.appendChild(s3);

        var u = document.createTextNode("Ubah");
        s3.appendChild(u);

        var file = document.createElement("input");
        file.setAttribute("type", "file");
        file.setAttribute("name", "file[]");
        file.setAttribute("id", "file");
        file.setAttribute("onchange", "document.getElementById('moreFileLink_pen').style.display = 'block';");
        s.appendChild(file);

        var s4 = document.createElement("span");
        s4.setAttribute("class", "fileinput-filename");
        d.appendChild(s4);

        var spasi = document.createTextNode("\u00A0");
        d.appendChild(spasi);

        var s5 = document.createElement("a");
        s5.setAttribute("href", "#");
        s5.setAttribute("class", "close fileinput-exists");
        s5.setAttribute("data-dismiss", "fileinput");
        d.appendChild(s5);

        document.getElementById("moreFile_pen").appendChild(br);
        document.getElementById("moreFile_pen").appendChild(d);
        document.getElementById("moreFile_pen").appendChild(br2);
        upload_number++;
    }
  function addFile_pel() {
        var br = document.createElement("br");
        var br2 = document.createElement("br");
        var d = document.createElement("div");
        d.setAttribute("class", "fileinput fileinput-new");
        d.setAttribute("data-provides", "fileinput");

        var s = document.createElement("span");
        s.setAttribute("class", "btn default btn-file");
        d.appendChild(s);


        var s2 = document.createElement("span");
        s2.setAttribute("class", "fileinput-new");
        s.appendChild(s2);

        var p = document.createTextNode("Pilih File");

        s2.appendChild(p);

        var s3 = document.createElement("span");
        s3.setAttribute("class", "fileinput-exists");
        s.appendChild(s3);

        var u = document.createTextNode("Ubah");
        s3.appendChild(u);

        var file = document.createElement("input");
        file.setAttribute("type", "file");
        file.setAttribute("name", "file[]");
        file.setAttribute("id", "file");
        file.setAttribute("onchange", "document.getElementById('moreFileLink_pel').style.display = 'block';");
        s.appendChild(file);

        var s4 = document.createElement("span");
        s4.setAttribute("class", "fileinput-filename");
        d.appendChild(s4);

        var spasi = document.createTextNode("\u00A0");
        d.appendChild(spasi);

        var s5 = document.createElement("a");
        s5.setAttribute("href", "#");
        s5.setAttribute("class", "close fileinput-exists");
        s5.setAttribute("data-dismiss", "fileinput");
        d.appendChild(s5);

        document.getElementById("moreFile_pel").appendChild(br);
        document.getElementById("moreFile_pel").appendChild(d);
        document.getElementById("moreFile_pel").appendChild(br2);
        upload_number++;
    }
</script>