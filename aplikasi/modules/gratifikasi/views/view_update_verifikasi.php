<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
      <h1>Detail Gratifikasi</h1>
      <div class="content-form-page">
        <div class="row">
          <div class="alert alert-info display-hide">
            <i class="fa fa-spin fa-spinner"></i> Silahkan tunggu. Proses penyimpanan sedang berjalan.
          </div>
          <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <i class="fa fa-ban"></i> <strong>Peringatan!</strong> Formulir belum lengkap. Data gagal ditambahkan. 
          </div>
          <?= $this->session->flashdata('message'); ?>
          <div class="col-md-12 col-sm-12">
            <legend style="color: #65aed9">Detail Laporan</legend>
            <div class="col-md-11 col-sm-11">
              <?php if ($show_data->status_dokumen == '5' || $show_data->status_dokumen == '6' || $show_data->status_dokumen == '7' || $show_data->status_dokumen == '8'): ?>
              <div class="row static-info">
                <div class="col-md-3 name">
                  Nomor Laporan
                </div>
                <div class="col-md-8">
                  : <?php cetak($show_data->nomor_laporan) ?>
                </div>
              </div>
              <?php endif; ?>
              <div class="row static-info">
                <div class="col-md-3 name">
                  Jenis Laporan
                </div>
                <div class="col-md-8">
                <?= $show_data->status_penerimaan == 'tolak' ? ': Laporan Penolakan Gratifikasi' : ': Laporan Penerimaan Gratifikasi'; ?>
                </div>
              </div>
              <div class="row static-info">
                <div class="col-md-3 name">
                  Tempat &amp; Tanggal Pelaporan
                </div>
                <div class="col-md-8">
                : <?php cetak($show_data->tempat_laporan.", ".date('d-M-Y', strtotime($show_data->tgl_laporan))) ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-sm-12">
            <legend style="color: #65aed9">Identitas Pelapor Gratifikasi</legend>
            <div class="col-md-11 col-sm-11">
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Nama Lengkap
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_profil->name) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Tempat &amp; Tanggal Lahir 
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_profil->tempatlahir.", ".$show_profil->tgllahir) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       No. KTP (NIK)
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_profil->no_ktp) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Jabatan/Pangkat/Golongan
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_profil->jabatan." ".$show_profil->pangkat ." ".$show_profil->golongan) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                      Alamat Email
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_profil->email) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                      Nomor Telepon Seluler
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_profil->nohp) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                      Pin BB/WA
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_profil->pin_bb) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                      Nama Instansi
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_profil->inskerja) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                      Unit Kerja/Sub Unit Kerja
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_profil->biro." ".$show_profil->bagian) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                      Alamat Kantor
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_profil->alamat_kantor) ?><br>&nbsp;&nbsp;<?php cetak($show_profil->kelurahan_kantor." ".$show_profil->kecamatan_kantor.", ".$show_profil->kode_pos_kantor) ?><br>&nbsp;&nbsp;<?php cetak($show_profil->kota_kantor.", ".$show_profil->nprovinsi_kantor) ?><br>&nbsp;&nbsp;Telp: <?php cetak($show_profil->no_kantor) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                      Alamat Rumah
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_profil->alamat_rumah) ?><br>&nbsp;&nbsp;<?php cetak($show_profil->kelurahan_rumah." ".$show_profil->kecamatan_rumah.", ".$show_profil->kode_pos_rumah) ?><br>&nbsp;&nbsp;<?php cetak($show_profil->kota_rumah.", ".$show_profil->nprovinsi_rumah) ?><br>&nbsp;&nbsp;Telp: <?php cetak($show_profil->no_rumah) ?>
                  </div>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-sm-12">
            <?php if ($show_data->status_penerimaan == 'tolak'): ?>
            <legend style="color: #65aed9">Data Penolakan Gratifikasi</legend>
            <div class="col-md-11 col-sm-11">
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Jenis Penolakan
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->njenis_penerimaan) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Uraian Gratifikasi yang Ditolak
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->uraian) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Harga/Nilai Nominal/Taksiran
                  </div>
                  <div class="col-md-6">
                       : Rp. <?php cetak(number_format($show_data->nilai_nominal, 2, ',', '.')) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Kode Peristiwa Penolakan
                  </div>
                  <div class="col-md-6">
                       : <?= $show_data->nperistiwa_penerimaan == 'Lainnya'? cetak($show_data->peristiwa_lainnya) : cetak($show_data->nperistiwa_penerimaan); ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                      Tempat &amp; Tanggal Penolakan
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->tempat_penerimaan.", ".date('d-M-Y', strtotime($show_data->tanggal_penerimaan))) ?>
                  </div>
              </div>
            </div>
            <?php else: ?>
            <legend style="color: #65aed9">Data Penerima Gratifikasi</legend>
            <div class="col-md-11 col-sm-11">
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Jenis Penerimaan
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->njenis_penerimaan) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Uraian
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->uraian) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Harga/Nilai Nominal/Taksiran
                  </div>
                  <div class="col-md-6">
                       : Rp. <?php cetak(number_format($show_data->nilai_nominal, 2, ',', '.')) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Kode Peristiwa Penerimaan
                  </div>
                  <div class="col-md-6">
                       : <?= $show_data->nperistiwa_penerimaan == 'Lainnya'? cetak($show_data->peristiwa_lainnya) : cetak($show_data->nperistiwa_penerimaan); ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                      Tempat &amp; Tanggal Penerimaan
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->tempat_penerimaan.", ".date('d-M-Y', strtotime($show_data->tanggal_penerimaan))) ?>
                  </div>
              </div>
            </div>
            <?php endif; ?>
          </div>
          <div class="col-md-12 col-sm-12">
            <legend style="color: #65aed9">Data Pemberi Gratifikasi</legend>
            <div class="col-md-11 col-sm-11">
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Nama
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->nama_pemberi) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                      Pekerjaan dan Jabatan 
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->pekerjaan_pemberi) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Alamat
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->alamat_pemberi) ?><br>&nbsp;&nbsp;Telp: <?php cetak($show_data->telepon_pemberi) ?>, Faks: <?php cetak($show_data->faks_pemberi) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Email
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->email_pemberi) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                      Hubungan dengan Pemberi
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->hubungan_pemberi) ?>
                  </div>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-sm-12">
            <legend style="color: #65aed9">Alasan dan Kronologi</legend>
            <div class="col-md-11 col-sm-11">
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Alasan Pemberian
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->alasan_pemberian) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Kronologi
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->kronologi_penerimaan) ?>
                  </div>
              </div>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Dokumen yang dilampirkan
                  </div>
                  <div class="col-md-6">
                       : <?= $show_data->status_dokumen_dilampirkan == 'tidak' ? 'Tidak ada' : 'Ada, yaitu ' . cetak($show_data->dokumen_dilampirkan)?>
                  </div>
              </div>
              <?php if(!empty($show_data->catatan_tambahan)): ?>
              <div class="row static-info">
                  <div class="col-md-3 name">
                       Catatan Tambahan
                  </div>
                  <div class="col-md-6">
                       : <?php cetak($show_data->catatan_tambahan) ?>
                  </div>
              </div>
              <?php endif; ?>
            </div>
          </div>
          <div class="col-md-12 col-sm-12">
            <legend style="color: #65aed9">Data Tambahan</legend>
            <div class="col-md-11 col-sm-11">
              <div class="row static-info">
                  <div class="col-md-3 name">
                       File
                  </div>
                  <div class="col-md-6">
                    <?php if (!empty($file_gratifikasi)): ?>
                      <?php foreach ($file_gratifikasi as $row): ?>
                         : <a target="_blank" href="<?= site_url('gratifikasi/download_file/'.$row->id.'/'.$row->id_gratifikasi); ?>"><?php cetak($row->nama_files) ?></a><br>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <span class="help-block">: Belum ada dokumen yang diupload</span>
                    <?php endif; ?>  
                  </div>
              </div>
            </div>
          </div>
          <?php if($show_data->status_dokumen == '2'): ?>
          <div class="col-md-12 col-sm-12">
            <legend style="color: #65aed9">Review</legend>
            <div class="col-md-11 col-sm-11">
              <form id="form_verifikasi" method="post" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('gratifikasi/verified'); ?>">
                <?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>
                <div class="row static-info form-group">
                  <div class="col-md-3 name">
                    <label>Catatan Review</label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="verifikasi" name="verifikasi"></textarea>
                      <input type="text" class="hidden" id="id_gratifikasi" name="id_gratifikasi" value="<?php cetak($id_gratifikasi) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info">
                  <div class="col-md-3 name">
                    <label>Upload File</label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <div class="input-group">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn default btn-file">
                                <span class="fileinput-new">
                                    Pilih File </span>
                                <span class="fileinput-exists">
                                    Ubah </span>
                                <input type="file" name="filesatu" id="filesatu" onchange="document.getElementById('moreFileLink').style.display = 'block';">
                            </span>
                            <span class="fileinput-filename">
                            </span>
                            &nbsp; <a href="#" class="close fileinput-exists" data-dismiss="fileinput">
                            </a>
                        </div>
                        <span id="name-error" class="help-block help-block-error"></span>
                        <div id="moreFile"></div>
                        <div id="moreFileLink" style="display:none;"><a href="javascript:addFile();">Tambah File Lain</a></div>
                      </div>
                    </div>
                  </div>
                </div>
                <br><br><br><br>
                <div class="row" >
                  <div class="col-md-offset-4 col-md-9">
                    <button formnovalidate="formnovalidate" id="submit" name="verified" type="submit" onclick="return confirm('Apakah anda yakin telah mereview data ini?');" class="btn btn-sm blue-dark"><i class="fa fa-check"></i>&nbsp;&nbsp;Telah Direview</button>
                    <button id="submit" name="revisi" type="submit" class="btn btn-sm green-seagreen"><i class="fa fa-edit"></i>&nbsp;&nbsp;Revisi</button>
                    <a href="<?php echo site_url('gratifikasi/verifikasi_gratifikasi'); ?>" class="btn btn-sm grey"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <?php elseif($show_data->status_dokumen == '4'): ?>
          <div class="col-md-12 col-sm-12">
            <legend style="color: #65aed9">Review</legend>
            <div class="col-md-11 col-sm-11">
              <div class="row static-info">
                <div class="col-md-3 name">
                    Catatan Review
                </div>
                <div class="col-md-6">
                     :<?php if (!empty($verifikasi)): ?>
                        <?php foreach ($verifikasi as $row): ?>
                          <?php cetak($row->verifikasi) ?><span class="help-inline" style="font-size: 7pt;"><?php cetak(date('d-M-Y H:i', strtotime($row->tanggal))) ?></span><br>&nbsp;
                        <?php endforeach; ?>
                      <?php else: ?>
                        Tidak Ada Catatan
                      <?php endif; ?>
                </div>
              </div>
              <div class="row static-info">
                <div class="col-md-3 name">
                     File Review
                </div>
                <div class="col-md-6">
                  <?php if (!empty($file_verifikasi)): ?>
                    <?php foreach ($file_verifikasi as $row): ?>
                       : <a target="_blank" href="<?= site_url('gratifikasi/download_verifikasi/'.$row->id.'/'.$row->id_gratifikasi); ?>"><?php cetak($row->nama_files) ?></a><span class="help-inline" style="font-size: 7pt;"><?php cetak(date('d-M-Y H:i', strtotime($row->tanggal_upload))) ?></span>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <span class="help-block">: Belum ada dokumen yang diupload</span>
                  <?php endif; ?>  
                </div>
              </div>
              <form method="post" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('gratifikasi/verified'); ?>">
                <?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>
                <div class="row static-info">
                  <div class="col-md-3 name">
                    <label>Catatan Review</label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="form-control" id="verifikasi" name="verifikasi"></textarea>
                      <input type="text" class="hidden" id="id_gratifikasi" name="id_gratifikasi" value="<?php cetak($id_gratifikasi) ?>">
                    </div>
                  </div>
                </div>
                <div class="row static-info">
                  <div class="col-md-3 name">
                    <label>Upload File</label>
                  </div>
                  <div class="col-md-8">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <div class="input-group">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn default btn-file">
                                <span class="fileinput-new">
                                    Pilih File </span>
                                <span class="fileinput-exists">
                                    Ubah </span>
                                <input type="file" name="filesatu" id="filesatu" onchange="document.getElementById('moreFileLink').style.display = 'block';">
                            </span>
                            <span class="fileinput-filename">
                            </span>
                            &nbsp; <a href="#" class="close fileinput-exists" data-dismiss="fileinput">
                            </a>
                        </div>
                        <span id="name-error" class="help-block help-block-error"></span>
                        <div id="moreFile"></div>
                        <div id="moreFileLink" style="display:none;"><a href="javascript:addFile();">Tambah File Lain</a></div>
                      </div>
                    </div>
                  </div>
                </div>
                <br><br><br><br>
                <div class="row" >
                  <div class="col-md-offset-4 col-md-9">
                    <button id="submit" name="verified" type="submit" onclick="return confirm('Apakah anda yakin telah mereview data ini?');" class="btn btn-sm blue-dark"><i class="fa fa-check"></i>&nbsp;&nbsp;Telah Direview</button>
                    <button id="submit" name="revisi" type="submit" class="btn btn-sm green-seagreen"><i class="fa fa-edit"></i>&nbsp;&nbsp;Revisi</button>
                    <a href="<?php echo site_url('gratifikasi/verifikasi_gratifikasi'); ?>" class="btn btn-sm grey"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <?php elseif($show_data->status_dokumen == '5'): ?>
          <div class="col-md-12 col-sm-12">
            <legend style="color: #65aed9">Review</legend>
            <div class="col-md-11 col-sm-11">
              <div class="row static-info">
                <div class="col-md-3 name">
                    Catatan Review
                </div>
                <div class="col-md-6">
                     :<?php if (!empty($verifikasi)): ?>
                        <?php foreach ($verifikasi as $row): ?>
                          <?php cetak($row->verifikasi) ?><span class="help-inline" style="font-size: 7pt;"><?php cetak(date('d-M-Y H:i', strtotime($row->tanggal))) ?></span><br>&nbsp;
                        <?php endforeach; ?>
                      <?php else: ?>
                        Tidak Ada Catatan
                      <?php endif; ?>
                </div>
              </div>
              <div class="row static-info">
                <div class="col-md-3 name">
                     File Review
                </div>
                <div class="col-md-6">
                  <?php if (!empty($file_verifikasi)): ?>
                    <?php foreach ($file_verifikasi as $row): ?>
                       : <a target="_blank" href="<?= site_url('gratifikasi/download_verifikasi/'.$row->id.'/'.$row->id_gratifikasi); ?>"><?php cetak($row->nama_files) ?></a><span class="help-inline" style="font-size: 7pt;"><?php cetak(date('d-M-Y H:i', strtotime($row->tanggal_upload))) ?></span>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <span class="help-block">: Belum ada dokumen yang diupload</span>
                  <?php endif; ?>  
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-sm-12">
            <legend style="color: #65aed9">Tanggal Kirim</legend>
            <div class="col-md-11 col-sm-11">
              <form method="post" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('gratifikasi/kirim_kpk'); ?>">
                <?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>
                <div class="row static-info">
                  <div class="col-md-3 name">
                    <label>Tanggal Kirim ke KPK <span class="require">*</span></label>
                  </div>
                  <div class="col-md-4">
                    <div class="input-icon right">
                      <i class="fa"></i>
                      <div class="input-group">    
                          <div class="input-icon right">
                              <i class="fa"></i>
                              <input class="form-control date-picker input-sm" onkeyup="checkonlynumber10(this);" placeholder="yyyy-mm-dd" size="16" type="text" id="tanggal_dikirim" name="tanggal_dikirim"/>
                              <input type="text" class="hidden" id="id_gratifikasi" name="id_gratifikasi" value="<?php cetak($id_gratifikasi) ?>">
                          </div>
                          <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </span>
                      </div>
                    </div>
                  </div>
                </div>
                <br><br><br><br>
                <div class="row" >
                  <div class="col-md-offset-4 col-md-9">
                    <button id="submit" type="submit" class="btn btn-sm blue-dark"><i class="fa fa-check"></i>&nbsp;&nbsp;Simpan</button>
                    <a href="<?php echo site_url('gratifikasi/verifikasi_gratifikasi'); ?>" class="btn btn-sm grey"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
</div>
<script>
  var upload_number = 2;

  function addFile() {
        var br = document.createElement("br");
        var br2 = document.createElement("br");
        var d = document.createElement("div");
        d.setAttribute("class", "fileinput fileinput-new");
        d.setAttribute("data-provides", "fileinput");

        var s = document.createElement("span");
        s.setAttribute("class", "btn default btn-file");
        d.appendChild(s);


        var s2 = document.createElement("span");
        s2.setAttribute("class", "fileinput-new");
        s.appendChild(s2);

        var p = document.createTextNode("Pilih File");

        s2.appendChild(p);

        var s3 = document.createElement("span");
        s3.setAttribute("class", "fileinput-exists");
        s.appendChild(s3);

        var u = document.createTextNode("Ubah");
        s3.appendChild(u);

        var file = document.createElement("input");
        file.setAttribute("type", "file");
        file.setAttribute("name", "file[]");
        file.setAttribute("id", "file");
        file.setAttribute("onchange", "document.getElementById('moreFileLink').style.display = 'block';");
        s.appendChild(file);

        var s4 = document.createElement("span");
        s4.setAttribute("class", "fileinput-filename");
        d.appendChild(s4);

        var spasi = document.createTextNode("\u00A0");
        d.appendChild(spasi);

        var s5 = document.createElement("a");
        s5.setAttribute("href", "#");
        s5.setAttribute("class", "close fileinput-exists");
        s5.setAttribute("data-dismiss", "fileinput");
        d.appendChild(s5);

        document.getElementById("moreFile").appendChild(br);
        document.getElementById("moreFile").appendChild(d);
        document.getElementById("moreFile").appendChild(br2);
        upload_number++;
    }
</script>