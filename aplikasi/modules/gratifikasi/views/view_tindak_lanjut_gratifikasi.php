<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
      <h1>Data Tindak Lanjut Gratifikasi</h1>
      <div class="content-form-page">
        <!-- BEGIN TABS AND TESTIMONIALS -->
        <div class="row mix-block margin-bottom-40">
          <!-- TABS -->
          <div class="col-md-12 tab-style-1">
            <ul class="nav nav-tabs">
              <?php if($this->session->userdata('role') == '0' || $this->session->userdata('role') == '1'): ?>
              <li class="active"><a href="#tab-1" data-toggle="tab">Update Tindak Lanjut &nbsp;<span class="badge badge-danger"><?php cetak($count_tindak_lanjut->jumlah) ?></span></a></li>
              <li><a href="#tab-2" data-toggle="tab">Selesai Tindak Lanjut &nbsp;<span class="badge badge-danger"><?php cetak($count_selesai_peg->jumlah) ?></span></a></li>
              <?php elseif($this->session->userdata('role') == '0'): ?>
              <li><a href="#tab-3" data-toggle="tab">Tindak Lanjut &nbsp;<span class="badge badge-danger"><?php cetak($count_selesai->jumlah) ?></span></a></li>
              <?php elseif($this->session->userdata('role') == '2'): ?>
              <li class="active"><a href="#tab-3" data-toggle="tab">Tindak Lanjut &nbsp;<span class="badge badge-danger"><?php cetak($count_selesai->jumlah) ?></span></a></li>
              <?php endif ?>
            </ul>
            <div class="tab-content">
              <?php if($this->session->userdata('role') == '0' || $this->session->userdata('role') == '1'): ?>
              <div class="tab-pane fade in active" id="tab-1">
                <h1 style="color: #65aed9">Daftar Update Tindak Lanjut</h1>
                <?= $this->session->flashdata('message'); ?>
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                    <tr class="info">
                      <th style="font-size:12px; text-align:center; width: 5%" >No</th>
                      <th style="font-size:12px; text-align:center;" >Tanggal Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Nomor Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Nilai Nominal (Rp.)</th>
                      <th style="font-size:12px; text-align:center;" >Nama Pemberi</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Laporan</th>
                      <th style="font-size:12px; text-align:center; width: 12%;" >Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($tindak_lanjut as $row):?>
                    <tr class="odd gradeX">
                      <td style="font-size:11px; text-align:center;"><?php cetak($i++) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tanggal_penerimaan))) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->nomor_laporan) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->njenis_penerimaan) ?></td>
                      <td style="font-size:11px; text-align:right;"><?php cetak(number_format($row->nilai_nominal, 2, ',', '.')) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->nama_pemberi) ?></td>
                      <td style="font-size:11px; text-align:center;"><?= $row->status_penerimaan == 'tolak' ? 'Laporan Penolakan Gratifikasi' : 'Laporan Penerimaan Gratifikasi'; ?></td>
                      <td style="font-size:11px; text-align:center;">
                        <a href="<?= site_url('gratifikasi/detail_gratifikasi/'.$row->id); ?>" class="btn btn-xs green-seagreen" title="Edit"><i class="fa fa-edit"></i></a>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <div class="tab-pane fade" id="tab-2">
                <h1 style="color: #65aed9">Selesai Tindak Lanjut</h1>
                <?= $this->session->flashdata('message'); ?>
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                  <thead>
                    <tr class="info">
                      <th style="font-size:12px; text-align:center; width: 5%" >No</th>
                      <th style="font-size:12px; text-align:center;" >Tanggal Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Nomor Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Nilai Nominal (Rp.)</th>
                      <th style="font-size:12px; text-align:center;" >Nama Pemberi</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Laporan</th>
                      <th style="font-size:12px; text-align:center; width: 12%;" >Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($selesai_peg as $row):?>
                    <tr class="odd gradeX">
                      <td style="font-size:11px; text-align:center;"><?php cetak($i++) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tanggal_penerimaan))) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->nomor_laporan) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->njenis_penerimaan) ?></td>
                      <td style="font-size:11px; text-align:right;"><?php cetak(number_format($row->nilai_nominal, 2, ',', '.')) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->nama_pemberi) ?></td>
                      <td style="font-size:11px; text-align:center;"><?= $row->status_penerimaan == 'tolak' ? 'Laporan Penolakan Gratifikasi' : 'Laporan Penerimaan Gratifikasi'; ?></td>
                      <td style="font-size:11px; text-align:center;">
                        <a href="<?= site_url('gratifikasi/detail_gratifikasi/'.$row->id); ?>" class="btn btn-xs green-seagreen" title="Lihat"><i class="fa fa-eye"></i></a>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <?php elseif($this->session->userdata('role') == '0' || $this->session->userdata('role') == '2'): ?>
              <?php if ($this->session->userdata('role') == '0'): ?> 
              <div class="tab-pane fade" id="tab-3">
              <?php elseif($this->session->userdata('role') == '2'): ?>
              <div class="tab-pane fade in active" id="tab-3">
              <?php endif; ?>
                <h1 style="color: #65aed9">Tindak Lanjut</h1>
                <?= $this->session->flashdata('message'); ?>
                <table class="table table-striped table-bordered table-hover" id="sample_3">
                  <thead>
                    <tr class="info">
                      <th style="font-size:12px; text-align:center; width: 5%" >No</th>
                      <th style="font-size:12px; text-align:center;" >Tanggal Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Nama Penerima</th>
                      <th style="font-size:12px; text-align:center;" >Nomor Laporan</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Penerimaan</th>
                      <th style="font-size:12px; text-align:center;" >Nilai Nominal (Rp.)</th>
                      <th style="font-size:12px; text-align:center;" >Nama Pemberi</th>
                      <th style="font-size:12px; text-align:center;" >Jenis Laporan</th>
                      <th style="font-size:12px; text-align:center; width: 12%;" >Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($selesai as $row):?>
                    <tr class="odd gradeX">
                      <td style="font-size:11px; text-align:center;"><?php cetak($i++) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tanggal_penerimaan))) ?></td>
                      <td style="font-size:11px; text-align:left;"><?= $row->identitas == 'Sebagai Penerima Gratifikasi' ? cetak($row->nama_pelapor) : cetak($row->nama_penerima); ?></td>
                      <td style="font-size:11px; text-align:left;"><?php cetak($row->nomor_laporan) ?></td>
                      <td style="font-size:11px; text-align:left;"><?php cetak($row->njenis_penerimaan) ?></td>
                      <td style="font-size:11px; text-align:right;"><?php cetak(number_format($row->nilai_nominal, 2, ',', '.')) ?></td>
                      <td style="font-size:11px; text-align:left;"><?php cetak($row->nama_pemberi) ?></td>
                      <td style="font-size:11px; text-align:left;"><?= $row->status_penerimaan == 'tolak' ? 'Laporan Penolakan Gratifikasi' : 'Laporan Penerimaan Gratifikasi'; ?></td>
                      <td style="font-size:11px; text-align:center;">
                        <a href="<?= site_url('gratifikasi/detail_verifikasi/'.$row->id); ?>" class="btn btn-xs green-seagreen" title="Lihat"><i class="fa fa-eye"></i></a>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <?php endif; ?>
            </div>
          </div>
          <!-- END TABS -->
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
</div>
<script src="<?= site_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
     $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust();
     });   
  });
</script>