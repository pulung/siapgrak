<?php
$filename = 'Inspektorat - Laporan Gratifikasi 2'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.' '.date('d-M-Y', strtotime($dategenerated)).'.xls'.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//$objWriter->save('php://output');
?>

<div style="margin-top: 6px;"></div>
<table cellpadding="0">
    <tr>
        <td valign="top">Tanggal Pelaporan</td>
        <td align="left">: <?= $dategenerated ?></td>
    </tr>
    <tr>
        <td valign="top">Nama UPG</td>
        <td><?php if (!empty($show_profil->name)): ?> : <?= $show_profil->name ?> <?php else: ?> : <?php endif; ?></td>
    </tr>
    <tr>
        <td valign="top">Instansi</td>
        <td><?php if (!empty($show_profil->inskerja)): ?> : <?= $show_profil->inskerja ?> <?php else: ?> : <?php endif; ?></td>
    </tr>
    <tr>
        <td valign="top">Unit Kerja</td>
        <td><?php if (!empty($show_profil->biro)): ?> : <?= $show_profil->biro ?> <?php else: ?> : <?php endif; ?></td>
    </tr>
    <tr>
        <td valign="top">Sub Unit Kerja</td>
        <td><?php if (!empty($show_profil->bagian)): ?> : <?= $show_profil->bagian ?> <?php else: ?> : <?php endif; ?></td>
    </tr>
    <tr>
        <td valign="top">Alamat</td>
        <td>: <?= $show_profil->alamat_kantor ?> Kel: <?= $show_profil->kelurahan_kantor ?> Kec: <?= $show_profil->kecamatan_kantor ?></td>
    </tr>
    <tr>
        <td valign="top">Provinsi</td>
        <td><?php if (!empty($show_profil->provinsi_kantor)): ?> : <?= $show_profil->nprovinsi_kantor ?> <?php else: ?> : <?php endif; ?></td>
    </tr>
    <tr>
        <td valign="top">Kabupaten/Kota</td>
        <td><?php if (!empty($show_profil->kota_kantor)): ?> : <?= $show_profil->kota_kantor ?> <?php else: ?> : <?php endif; ?></td>
    </tr>
</table>
<div style="margin-top: 6px;"></div>

<div style="margin-top: 6px;"></div><br><br>
<table cellpadding="0" border="1" style="border-spacing: 0px;">
    <thead>               
        <tr style="background-color: #000000; color: #ffffff">
            <td width="50" align="center" valign="top"><b>No</b></td>
            <td align="center" width="150" valign="top"><b>Tanggal Pelaporan ke UPG</b></td>
            <td align="center" width="100" valign="top"><b>Nama Lengkap</b></td>
            <td align="center" width="200" valign="top"><b>No NIK / KTP</b></td>
            <td align="center" width="150" valign="top"><b>Tempat Lahir</b></td>
            <td align="center" width="150" valign="top"><b>Tanggal Lahir</b></td>
            <td align="center" width="200" valign="top"><b>Email</b></td>
            <td align="center" width="150" valign="top"><b>No HP</b></td>
            <td align="center" width="100" valign="top"><b>Pangkat</b></td>
            <td align="center" width="200" valign="top"><b>Jabatan</b></td>
            <td align="center" width="200" valign="top"><b>Unit Kerja / Departemen</b></td>
            <td align="center" width="150" valign="top"><b>Sub Unit Kerja</b></td>
            <td align="center" width="150" valign="top"><b>Provinsi</b></td>
            <td align="center" width="150" valign="top"><b>Kabupaten/Kota</b></td>
            <td align="center" width="250" valign="top"><b>Alamat Rumah</b></td>
            <td align="center" width="250" valign="top"><b>Telp Rumah</b></td>
            <td align="center" width="250" valign="top"><b>Telp Seluler</b></td>
            <td align="center" width="100" valign="top"><b>Jenis Pelaporan</b></td>
            <td align="center" width="100" valign="top"><b>Jenis Peristiwa</b></td>
            <td align="center" width="100" valign="top"><b>Jenis Penerimaan</b></td>
            <td align="center" width="100" valign="top"><b>Uraian Barang</b></td>
            <td align="center" width="100" valign="top"><b>Mata Uang</b></td>
            <td align="center" width="100" valign="top"><b>Nilai Nominal/Tafsiran</b></td>
            <td align="center" width="100" valign="top"><b>Tempat Penerimaan</b></td>
            <td align="center" width="100" valign="top"><b>Tanggal Penerimaan</b></td>
            <td align="center" width="100" valign="top"><b>Nama Pemberi</b></td>
            <td align="center" width="100" valign="top"><b>Pekerjaan/Jabatan Pemberi</b></td>
            <td align="center" width="250" valign="top"><b>Email Pemberi</b></td>
            <td align="center" width="250" valign="top"><b>Telp Pemberi</b></td>
            <td align="center" width="250" valign="top"><b>Hubungan dengan Pemberi</b></td>
            <td align="center" width="100" valign="top"><b>Alasan Pemberian</b></td>
            <td align="center" width="250" valign="top"><b>Kronologis Pemberian</b></td>
            <td align="center" width="250" valign="top"><b>Urutan Rincian ke-</b></td>
            <td align="center" width="250" valign="top"><b>Status Dokumen</b></td>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($show_data)): ?>
            <?php
            $i = 0;
            foreach ($show_data as $row):
                $i++;
                ?>
                <tr>
                    <td width="50" align="center" valign="top"><?php echo $i; ?></td>
                    <td align="center" width="150" valign="top"><b><?= date('d-m-Y', strtotime($row->tgl_laporan)); ?></b></td>
                    <td align="center" width="100" valign="top"><b><?= $row->name ?></b></td>
                    <td align="center" width="200" valign="top"><b><?= $row->no_ktp ?></b></td>
                    <td align="center" width="150" valign="top"><b><?= $row->tempatlahir ?></b></td>
                    <td align="center" width="150" valign="top"><b><?= $row->tgllahir ?></b></td>
                    <td align="center" width="200" valign="top"><b><?= $row->email ?></b></td>
                    <td align="center" width="150" valign="top"><b><?= $row->nohp ?></b></td>
                    <td align="center" width="100" valign="top"><b><?php if (!empty($row->pangkat)): ?> <?= $row->pangkat ?> <?php else: ?>&nbsp;<?php endif; ?></b></td>
                    <td align="center" width="200" valign="top"><b><?php if (!empty($row->jabatan)): ?> <?= $row->jabatan ?> <?php else: ?>&nbsp;<?php endif; ?></b></td>
                    <td align="center" width="200" valign="top"><b><?php if (!empty($row->biro)): ?> <?= $row->biro ?> <?php else: ?>&nbsp;<?php endif; ?></b></td>
                    <td align="center" width="150" valign="top"><b><?php if (!empty($row->bagian)): ?> <?= $row->bagian ?> <?php else: ?>&nbsp;<?php endif; ?></b></td>
                    <td align="center" width="150" valign="top"><b><?php if (!empty($row->provinsi_kantor)): ?> <?= $row->nprovinsi_kantor ?> <?php else: ?>&nbsp;<?php endif; ?></b></td>
                    <td align="center" width="150" valign="top"><b><?php if (!empty($row->kota_kantor)): ?> <?= $row->kota_kantor ?> <?php else: ?>&nbsp;<?php endif; ?></b></td>
                    <td align="center" width="250" valign="top"><b><?= $row->alamat_rumah ?> Kel: <?= $row->kelurahan_rumah ?> Kec: <?= $row->kecamatan_rumah ?>, <?= $row->kota_rumah ?> <?= $row->nprovinsi_rumah ?> <?= $row->kode_pos_rumah ?></b></td>
                    <td align="center" width="250" valign="top"><b><?= $row->no_rumah ?></b></td>
                    <td align="center" width="250" valign="top"><b><?= $row->nohp ?></b></td>
                    <?= $row->status_penerimaan == 'tolak' ? '<td align="center" width="100" valign="top" style="color: #00A65A"><b> Laporan Penolakan Gratifikasi </b></td>' : '<td align="center" width="100" valign="top" style="color: #FF0000"><b> Laporan Penerimaan Gratifikasi </b></td>'; ?>
                    <td align="center" width="100" valign="top"><b><?php if ($row->nperistiwa_penerimaan == 'Lainnya'): ?><?= $row->peristiwa_lainnya ?>
                          <?php else: ?><?= $row->nperistiwa_penerimaan ?><?php endif ?></b></td>
                    <td align="center" width="100" valign="top"><b><?= $row->njenis_penerimaan ?></b></td>
                    <td align="center" width="100" valign="top"><b><?= $row->uraian ?></b></td>
                    <td align="center" width="100" valign="top"><b>Rupiah</b></td>
                    <td align="center" width="100" valign="top"><b>Rp. <?= number_format($row->nilai_nominal, 2, ',', '.') ?></b></td>
                    <td align="center" width="100" valign="top"><b><?= $row->tempat_penerimaan ?></b></td>
                    <td align="center" width="100" valign="top"><b><?= date('d-m-Y', strtotime($row->tanggal_penerimaan)); ?></b></td>
                    <td align="center" width="100" valign="top"><b><?= $row->nama_pemberi ?></b></td>
                    <td align="center" width="100" valign="top"><b><?= $row->pekerjaan_pemberi ?></b></td>
                    <td align="center" width="250" valign="top"><b><?php if (!empty($row->email_pemberi)): ?> <?= $row->email_pemberi ?> <?php else: ?>&nbsp;<?php endif; ?></b></td>
                    <td align="center" width="250" valign="top"><b><?php if (!empty($row->telepon_pemberi)): ?> <?= $row->telepon_pemberi ?> <?php else: ?>&nbsp;<?php endif; ?></b></td>
                    <td align="center" width="250" valign="top"><b><?= $row->hubungan_pemberi ?></b></td>
                    <td align="center" width="100" valign="top"><b><?= $row->alasan_pemberian ?></b></td>
                    <td align="center" width="250" valign="top"><b><?= $row->kronologi_penerimaan ?></b></td>
                    <td align="center" width="250" valign="top"><b><?= $row->urutan ?></b></td>
                    <td align="center" width="250" valign="top"><b><?= $row->nama_status_dokumen ?></b></td>
                </tr>
            <?php
            endforeach;
            ?>
        <?php else : ?>
            <tr style="vertical-align:middle;">
                <td width="50" align="center"><b>-</b></td>
                <td align="center" width="150" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="200" ><b>-</b></td>
                <td align="center" width="150" ><b>-</b></td>
                <td align="center" width="150" ><b>-</b></td>
                <td align="center" width="200" ><b>-</b></td>
                <td align="center" width="150" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="200" ><b>-</b></td>
                <td align="center" width="200" ><b>-</b></td>
                <td align="center" width="150" ><b>-</b></td>
                <td align="center" width="150" ><b>-</b></td>
                <td align="center" width="150" ><b>-</b></td>
                <td align="center" width="250" ><b>-</b></td>
                <td align="center" width="250" ><b>-</b></td>
                <td align="center" width="250" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="250" ><b>-</b></td>
                <td align="center" width="250" ><b>-</b></td>
                <td align="center" width="250" ><b>-</b></td>
                <td align="center" width="100" ><b>-</b></td>
                <td align="center" width="250" ><b>-</b></td>
                <td align="center" width="250" ><b>-</b></td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>