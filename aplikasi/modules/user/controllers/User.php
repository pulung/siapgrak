<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('Model_user','profil/Model_profil'));
        if (!$this->session->userdata('username')):
            redirect('login');
            return;
        endif;
        @date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        /*$id_user = $this->session->userdata('id');
        $data['count_riwayat_peg'] = $this->Model_beranda->get_count_riwayat_peg($id_user);
        $data['count_rekomendasi_peg'] = $this->Model_beranda->get_count_rekomendasi_peg($id_user);
        $data['count_tindak_lanjut_peg'] = $this->Model_beranda->get_count_tindak_lanjut_peg($id_user);
        $data['count_verifikasi'] = $this->Model_beranda->get_count_verifikasi();
        $data['count_rekomendasi'] = $this->Model_beranda->get_count_rekomendasi();
        $data['count_tindak_lanjut'] = $this->Model_beranda->get_count_tindak_lanjut();
        $data['draft'] = $this->Model_gratifikasi->get_data_draft_beranda($id_user);
        $data['show_profil'] = $this->Model_profil->show_profil($id_user)->row();*/
        $data['tagmenu'] = 'user';
        $data['body'] = 'user/view_user';
		
		$data['upg'] = $this->Model_user->getupg();
		$data['user'] = $this->Model_user->getuser();
		$data['data_hirarki'] = $this->Model_user->getdatahirarki();
		
        $this->load->vars($data);																									
        $this->load->view('view_main');
    }
	
	public function insert() {
		//var_dump(date('Y')); exit;
		$this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $now = date('Y-m-d H:i:s');
                
        $data_non = array(
                'id_hirarki' => '',
                'level1' => strip_tags(trim($_POST['inputLevel1'])),
                'level2' => strip_tags(trim($_POST['inputLevel2'])),
                'level3' => strip_tags(trim($_POST['inputLevel3'])),
                'level4' => strip_tags(trim($_POST['inputLevel4'])),
                'level5' => strip_tags(trim($_POST['inputLevel5'])),
                'level6' => strip_tags(trim($_POST['inputLevel6'])),
                'level7' => strip_tags(trim($_POST['inputLevel7'])),
                'level8' => strip_tags(trim($_POST['inputLevel8'])),
                'tgl_upload' => $now
            );
            $data = $this->security->xss_clean($data_non);
            $insert = $this->Model_user->insert_hirarki($data); 


        if ($insert == 'sukses') {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-check"></i><strong> Berhasil! </strong></h4> Data berhasil disimpan.</div>');
            redirect('user');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="fa fa-ban"></i><strong> Peringatan! </strong></h4> Data gagal disimpan.</div>');
            redirect('user');
        }
    }
	
	public function hapus_hirarki($id = ""){
        
        $hapus = $this->Model_user->deletehirarki($id);
        if ($hapus == 'sukses') {
            $this->session->set_flashdata('message', '<div><div class="alert alert-success"><i class="fa fa-check"></i> Pengguna berhasil dihapus.</div></div>');
            redirect('user');
        } else {
            $this->session->set_flashdata('message', '<div><div class="alert alert-danger"><i class="fa fa-times"></i> Pengguna gagal dihapus.</div></div>');
            redirect('user');
        }
    }
	
	public function edit_hirarki($id = "") {
        $data['tagmenu'] = 'user';
        $data['body'] 	 = 'user/view_edit_user';
				
		$data['gethirarki'] = $this->Model_user->gethirarki($id);
       
        $this->load->vars($data);
		$this->load->view('view_main');
        
			
   }
   
   public function update_hirarki($id = "") {
	   
		$now = date('Y-m-d H:i:s');
		$data_form = array(
                'id_hirarki' => $id,
                'level1' => strip_tags(trim($_POST['inputLevel1'])),
                'level2' => strip_tags(trim($_POST['inputLevel2'])),
                'level3' => strip_tags(trim($_POST['inputLevel3'])),
                'level4' => strip_tags(trim($_POST['inputLevel4'])),
                'level5' => strip_tags(trim($_POST['inputLevel5'])),
                'level6' => strip_tags(trim($_POST['inputLevel6'])),
                'level7' => strip_tags(trim($_POST['inputLevel7'])),
                'level8' => strip_tags(trim($_POST['inputLevel8'])),
                'tgl_upload' => $now
                
            );
        
		$data_hirarki = $this->security->xss_clean($data_form);
		 
		$ubah_hirarki = $this->Model_user->updatehirarki($id,$data_hirarki); 
		
		 
		if($ubah_hirarki == 'sukses') {
			$this->session->set_flashdata('message', '<div><div class="alert alert-success"><i class="fa fa-check"></i> Data berhasil diubah. </div></div>');
			redirect('user');
		}else{
			$this->session->set_flashdata('message', '<div><div class="alert alert-danger"><i class="fa fa-times"></i> Data gagal diubah. </div></div>');
			redirect('user');
		} 
	}

}
