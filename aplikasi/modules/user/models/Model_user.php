<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_user extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	function getupg() {
        $data = array();

        $sql = "SELECT * FROM upg";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }
	
	function getuser() {
        $data = array();

        $sql = "SELECT * FROM conf_user";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }
	
	function insert_hirarki($data){
        $res = $this->db->insert('hirarki', $data);
        if (!$res) {
            return 'proses gagal';
        } else {
            return 'sukses';
        }
    }
	
	function getdatahirarki() {
        $data = array();

        $sql = "SELECT * FROM hirarki";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }
	
	public function deletehirarki($id) {
        $res = $this->db->delete('hirarki', array('id_hirarki' => $id));

        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }
	
	function updatehirarki($id,$data){
        $res = $this->db->where('id_hirarki', $id)
                        ->update('hirarki', $data);

        if (!$res):
            return 'gagal';
        else:
            return 'sukses';
        endif;
    }
	
	function gethirarki($id) {
        $data = array();

        $sql = "SELECT * FROM hirarki WHERE id_hirarki = $id";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }
}
