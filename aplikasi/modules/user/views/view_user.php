<div class="main">
	<div class="container">
		<!-- BEGIN CONTENT -->
		<div class="col-md-12">
		  <h1>Manajemen Hirarki</h1>
		</div>	
	</div>
	<br>
	<div class="container">
		<div class="col-md-12">
			<?= $this->session->flashdata('message'); ?>
			<form method="post" id="form_hirarki" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('user/insert'); ?>">
              <?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>
              
			  <div class="form-group row">
				<label for="staticEmail" class="col-sm-2 col-form-label">Level 1</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel1" name="inputLevel1" placeholder="Level 1">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 50px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 2</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel2" name="inputLevel2" placeholder="Level 2">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 100px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 3</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel3" name="inputLevel3" placeholder="Level 3">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 150px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 4</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel4" name="inputLevel4" placeholder="Level 4">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 200px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 5</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel5" name="inputLevel5" placeholder="Level 5">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 250px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 6</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel6" name="inputLevel6" placeholder="Level 6">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 300px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 7</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel7" name="inputLevel7" placeholder="Level 7">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 350px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 8</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel8" name="inputLevel8" placeholder="Level 8">
				</div>
			  </div>
			  <br><br>
			  
			  <div class="row">
                <div class="col-md-offset-6 col-md-2">
                  
				  <button id="submit" name="send" type="submit" class="btn btn-sm blue-dark"><i class="fa fa-paper-plane-o"></i>&nbsp;&nbsp;Simpan</button>
                  
                </div>
              </div>
			  
			</form>
			
	<form method="post" id="form_pengguna" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('pengguna/tambah_pengguna'); ?>">
	 <div class="col-md-12">
	  <br><br>
      <h1>Daftar Hirarki</h1>
      <div class="content-page">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="tab-content" style="padding:0; background: #fff;">
				<table id="tabel-data" name="tabel-data" class="table table-striped table-bordered" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>No.</th>
							<th>Level 1</th>
							<th>Level 2</th>
							<th>Level 3</th>
							<th>Level 4</th>
							<th>Level 5</th>
							<th>Level 6</th>
							<th>Level 7</th>
							<th>Level 8</th>
							<th>Aksi</th>
						</tr>
					</thead>
					
					<tbody>
					<?php $no = 1; ?>
					<?php foreach ($data_hirarki as $row): ?>
                          
						<tr>
						
							<td><?php echo $no; ?></td>
							<td><?php cetak($row['level1']); ?></td>
							<td><?php cetak($row['level2']); ?></td>
							<td><?php cetak($row['level3']); ?></td>
							<td><?php cetak($row['level4']); ?></td>
							<td><?php cetak($row['level5']); ?></td>
							<td><?php cetak($row['level6']); ?></td>
							<td><?php cetak($row['level7']); ?></td>
							<td><?php cetak($row['level8']); ?></td>
							
							<td>
								  <?  var_dump(cetak($row['id'])); exit; ?>
								  <a href="<?= site_url()?>user/edit_hirarki/<?php cetak($row['id_hirarki']); ?>" title="Edit">
									  <span class="btn btn-xs btn-primary">
											<i class="fa fa-pencil"></i> 
									  </span>
								  </a>
								  <a href="<?= site_url()?>user/hapus_hirarki/<?php cetak($row['id_hirarki']); ?>" onclick="return window.confirm('Apakah Anda yakin ingin menghapus data tersebut?');"  title="Hapus">
									   <span class="btn btn-xs btn-danger">
											<i class="fa fa-remove"></i> 
									   </span>
								  </a>
								  
							</td>
						</tr>
						
					<?php $no++; endforeach; ?>
					<?php //} ?>						   
					</tbody>
				</table>
				
			</div>
		  </div>
		</div>
	  </div>
	 </div>
	</form>
		</div>
	</div>
	
	
</div>

