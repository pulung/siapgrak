<div class="main">
	<div class="container">
		<!-- BEGIN CONTENT -->
		<div class="col-md-12">
		  <h1>Manajemen Hirarki</h1>
		</div>	
	</div>
	<br>
	<div class="container">
		<div class="col-md-12">
			<div class="content-page" style="border-style:solid; border-color: grey;">
				<input type="hidden" name="count" value="1" />
				<div class="control-group" id="fields">
					<label class="control-label" for="field1"><h3><b>Level 1</b></h3></label>
					<hr>
					<div class="controls" id="profs"> 
						<form class="input-append">
							<br><div id="field">
							<input autocomplete="off" class="input form-control" id="field1" name="prof1" type="text" placeholder="Jabatan" data-items="8"/>
							
								<!--<select class="form-control select2" onchange="myFunctionAgenda()" id="field1" name="prof1">
								<option value="" disabled selected hidden>Pilih</option>
								<?php foreach ($user as $u) { ?>
									<option value="<?php echo $u['id'] ?>"<?php echo $u['id'] ? 'selected' : ''; ?> ><?= $u['name']. ' - '. $u['jabatan']; ?></option>
								<?php } ?>
								</select>-->
							
							<br><br>
							<button id="b1" class="btn add-more" type="button">+ Tambah</button></div>
						</form>
					<br>
					<small>Klik tombol + Tambah untuk menambah field</small>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="container">
		<div class="col-md-12">
			<div class="content-page" style="border-style:solid; border-color: grey;">
				<input type="hidden" name="count" value="1" />
				<div class="control-group" id="fields">
					<label class="control-label" for="field1"><h3><b>Level 2</b></h3></label>
					<hr>
					<div class="controls" id="profs2"> 
						<form class="input-append">
							<br><div id="fieldb"><input autocomplete="off" class="input form-control" id="fieldb1" name="prof2" type="text" placeholder="Jabatan" data-items="8"/><br><br>
							<button id="b2" class="btn add-more2" type="button">+ Tambah</button></div>
						</form>
					<br>
					<small>Klik tombol + Tambah untuk menambah field</small>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="container">
		<div class="col-md-12">
			<div class="content-page" style="border-style:solid; border-color: grey;">
				<input type="hidden" name="count" value="1" />
				<div class="control-group" id="fields">
					<label class="control-label" for="field1"><h3><b>Level 3</b></h3></label>
					<hr>
					<div class="controls" id="profs3"> 
						<form class="input-append">
							<br><div id="fielda"><input autocomplete="off" class="input form-control" id="fielda1" name="prof3" type="text" placeholder="Jabatan" data-items="8"/><br><br>
							<button id="b1" class="btn add-more3" type="button">+ Tambah</button></div>
						</form>
					<br>
					<small>Klik tombol + Tambah untuk menambah field</small>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="container">
		<div class="col-md-12">
			<div class="content-page" style="border-style:solid; border-color: grey;">
				<input type="hidden" name="count" value="1" />
				<div class="control-group" id="fields">
					<label class="control-label" for="field1"><h3><b>Level 4</b></h3></label>
					<hr>
					<div class="controls" id="profs4"> 
						<form class="input-append">
							<br><div id="fieldc"><input autocomplete="off" class="input form-control" id="fieldc1" name="prof4" type="text" placeholder="Jabatan" data-items="8"/><br><br>
							<button id="b1" class="btn add-more4" type="button">+ Tambah</button></div>
						</form>
					<br>
					<small>Klik tombol + Tambah untuk menambah field</small>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="container">
		<div class="col-md-12">
			<div class="content-page" style="border-style:solid; border-color: grey;">
				<input type="hidden" name="count" value="1" />
				<div class="control-group" id="fields">
					<label class="control-label" for="field1"><h3><b>Level 5</b></h3></label>
					<hr>
					<div class="controls" id="profs5"> 
						<form class="input-append">
							<br><div id="fieldd"><input autocomplete="off" class="input form-control" id="fieldd1" name="prof5" type="text" placeholder="Jabatan" data-items="8"/><br><br>
							<button id="b1" class="btn add-more5" type="button">+ Tambah</button></div>
						</form>
					<br>
					<small>Klik tombol + Tambah untuk menambah field</small>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="container">
		<div class="col-md-12">
			<div class="content-page" style="border-style:solid; border-color: grey;">
				<input type="hidden" name="count" value="1" />
				<div class="control-group" id="fields">
					<label class="control-label" for="field1"><h3><b>Level 6</b></h3></label>
					<hr>
					<div class="controls" id="profs6"> 
						<form class="input-append">
							<br><div id="fielde"><input autocomplete="off" class="input form-control" id="fielde1" name="prof6" type="text" placeholder="Jabatan" data-items="8"/><br><br>
							<button id="b1" class="btn add-more6" type="button">+ Tambah</button></div>
						</form>
					<br>
					<small>Klik tombol + Tambah untuk menambah field</small>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="container">
		<div class="col-md-12">
			<div class="content-page" style="border-style:solid; border-color: grey;">
				<input type="hidden" name="count" value="1" />
				<div class="control-group" id="fields">
					<label class="control-label" for="field1"><h3><b>Level 7</b></h3></label>
					<hr>
					<div class="controls" id="profs7"> 
						<form class="input-append">
							<br><div id="fieldf"><input autocomplete="off" class="input form-control" id="fieldf1" name="prof7" type="text" placeholder="Jabatan" data-items="8"/><br><br>
							<button id="b1" class="btn add-more7" type="button">+ Tambah</button></div>
						</form>
					<br>
					<small>Klik tombol + Tambah untuk menambah field</small>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="container">
		<div class="col-md-12">
			<div class="content-page" style="border-style:solid; border-color: grey;">
				<input type="hidden" name="count" value="1" />
				<div class="control-group" id="fields">
					<label class="control-label" for="field1"><h3><b>Level 8</b></h3></label>
					<hr>
					<div class="controls" id="profs8"> 
						<form class="input-append">
							<br><div id="fieldg"><input autocomplete="off" class="input form-control" id="fieldg1" name="prof8" type="text" placeholder="Jabatan" data-items="8"/><br><br>
							<button id="b1" class="btn add-more8" type="button">+ Tambah</button></div>
						</form>
					<br>
					<small>Klik tombol + Tambah untuk menambah field</small>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="container">
		<div class="col-md-12">
			<div class="content-page" style="border-style:solid; border-color: grey;">
				<input type="submit" id="kirim" class="btn btn-success" placeholder="Kirim" style="padding:4px"></input>
			</div>
		</div>
	</div>
	
</div>

