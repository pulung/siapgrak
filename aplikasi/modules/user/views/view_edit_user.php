<div class="main">
	<div class="container">
		<!-- BEGIN CONTENT -->
		<div class="col-md-12">
		  <h1>Manajemen Hirarki</h1>
		</div>	
	</div>
	<br>
	<div class="container">
		<div class="col-md-12">
			<?= $this->session->flashdata('message'); ?>
			<?php foreach ($gethirarki as $row): ?>
			<form method="post" id="form_hirarki" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= base_url()?>user/update_hirarki/<?php cetak($row['id_hirarki']); ?>">
              <?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>
              
			  <div class="form-group row">
				<label for="staticEmail" class="col-sm-2 col-form-label">Level 1</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel1" name="inputLevel1" value="<?php cetak($row['level1']); ?>">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 50px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 2</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel2" name="inputLevel2" value="<?php cetak($row['level2']); ?>">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 100px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 3</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel3" name="inputLevel3" value="<?php cetak($row['level3']); ?>">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 150px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 4</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel4" name="inputLevel4" value="<?php cetak($row['level4']); ?>">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 200px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 5</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel5" name="inputLevel5" value="<?php cetak($row['level5']); ?>">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 250px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 6</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel6" name="inputLevel6" value="<?php cetak($row['level6']); ?>">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 300px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 7</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel7" name="inputLevel7" value="<?php cetak($row['level7']); ?>">
				</div>
			  </div>
			  <div class="form-group row" style="padding-left: 350px;">
				<label for="inputPassword" class="col-sm-2 col-form-label">Level 8</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="inputLevel8" name="inputLevel8" value="<?php cetak($row['level8']); ?>">
				</div>
			  </div>
			  <br><br>
			  
			  <div class="row">
                <div class="col-md-offset-6 col-md-6">
                  
				  <button id="submit" name="send" type="submit" class="btn btn-sm blue-dark"><i class="fa fa-paper-plane-o"></i>&nbsp;&nbsp;Update</button>
				  <a href="<?php echo site_url('user'); ?>" class="btn btn-sm blue-dark" style="background-color:#588759;"><i class="fa fa-times"></i>&nbsp;&nbsp;Kembali</a>
            
                </div>
				
              </div>
			  
			</form>
			<?php endforeach; ?>
		</div>
	</div>
	
	
</div>

