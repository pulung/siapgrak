<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->load->model(array('Model_kontak','profil/Model_profil'));
        if (!$this->session->userdata('username')):
            redirect('login');
            return;
        endif;
        @date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        $this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $data['tagmenu'] = 'kontak';
        $data['body'] = 'kontak/view_kontak';
		
		$data['kontak'] = $this->Model_kontak->getupg();
		
        $this->load->vars($data);
        $this->load->view('view_main');
    }

}
