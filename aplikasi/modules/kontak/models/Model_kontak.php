<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_kontak extends CI_Model {

	private $table_upg;

    function __construct() {
        parent::__construct();
		$this->table_upg = "upg";
    }

	function getupg() {
        $data = array();

        $sql = "SELECT * FROM upg";
                

        $Q = $this->db->query($sql);
        if ($Q->num_rows() > 0) {
            $data = $Q->result_array();
        }

        $Q->free_result();
        return $data;
    }
	
	function tambah_pengguna($data){
        $res = $this->db->insert('conf_user_2', $data);

        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }
	
	public function get($id) {
        $query = $this->db->get_where($this->table_pengguna, array('id' => $id), 1, 0);
        $result = $query->result();
        return $result ? $result[0] : NULL;
    }
	
	public function delete_pengguna($id) {
		$res = $this->db->delete('conf_user_2', array('id' => $id));
        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }
	
	function update_pengguna($id,$data){
        $res = $this->db->where('id', $id)
                        ->update('conf_user_2', $data);

        if (!$res):
            return 'gagal';
        else:
            return 'sukses';
        endif;
    }
	
 
}
