<div class="main">
  <div class="container">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12">
      <!--<h1>Kontak Kami</h1>-->
	  <?php foreach($kontak as $row) :?>
      <div class="content-page">
        <div class="row">
          <div class="col-md-5 col-sm-5" style="font-size: 15px;">
            <address style="font-size: 20px;">
              <strong><?= cetak($row['nama_upg']); ?></strong>
            </address>  
             <address>
              <?= cetak($row['alamat']); ?>
            </address>
			<address>
              Telp : <?= cetak($row['no_telp']); ?>
            </address> 
			<address>
              Faks : <?= cetak($row['fax']); ?>
            </address>
			<address>
              Email : <?= cetak($row['email']); ?>
            </address>
          </div>
		  
          <div class="col-md-7">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.712745297031!2d<?= cetak($row['longitudes']); ?>!3d<?= cetak($row['latitudes']); ?>!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5d11a555a99%3A0x6769c3cbd3657c8e!2sInspektorat+Kementerian+Sekretariat+Negara!5e0!3m2!1sid!2s!4v1504063956914" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
      </div><br><br>
	  <?php endforeach; ?>
    </div>
    <!-- END CONTENT -->
  </div>
</div>