<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_beranda extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_count_riwayat_peg($id_user){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE status_dokumen IN ? AND baca = ? AND id_penerima = ?";

        $fetch = $this->db->query($queryString, array(array('1','2','3','4','5'), '0',$id_user));
        return $fetch->row();   
    }

    function get_count_verifikasi(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE status_dokumen IN ? AND baca = ?";

        $fetch = $this->db->query($queryString, array(array('2','3','4','5','6'), '0'));
        return $fetch->row();  
    }

    function get_count_rekomendasi_peg($id_user){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE status_dokumen IN ? AND baca = ? AND id_penerima = ?";

        $fetch = $this->db->query($queryString, array(array('6'), '0', $id_user));
        return $fetch->row();   
    }

    function get_count_rekomendasi(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE status_dokumen IN ? AND baca = ?";

        $fetch = $this->db->query($queryString, array(array('7'), '0'));
        return $fetch->row();  
    }

    function get_count_tindak_lanjut_peg($id_user){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE status_dokumen IN ? AND baca = ? AND id_penerima = ?";

        $fetch = $this->db->query($queryString, array(array('7','8'),'0',$id_user));
        return $fetch->row();  
    }

    function get_count_tindak_lanjut(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM grf_gratifikasi WHERE status_dokumen IN ? AND baca = ?";

        $fetch = $this->db->query($queryString, array(array('8'), '0'));
        return $fetch->row();  
    }
	
	function get_count_riwayat_inspektorat(){
        $queryString = "SELECT COALESCE(COUNT(id),0) as jumlah 
                        FROM lap_tahunan";

        $fetch = $this->db->query($queryString, array());
        return $fetch->row();   
    }
}
