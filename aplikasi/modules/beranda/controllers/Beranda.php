<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('Model_beranda','profil/Model_profil','gratifikasi/Model_gratifikasi'));
        if (!$this->session->userdata('username')):
            redirect('login');
            return;
        endif;
        @date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
		//var_dump($this->auth->isprivilegectrl($this->config->item('AKSES_ALL')));exit;
		$this->auth->isprivilegectrl($this->config->item('AKSES_ALL'));
        $id_user = $this->session->userdata('id');
        $data['count_riwayat_peg'] = $this->Model_beranda->get_count_riwayat_peg($id_user);
        $data['count_rekomendasi_peg'] = $this->Model_beranda->get_count_rekomendasi_peg($id_user);
        $data['count_tindak_lanjut_peg'] = $this->Model_beranda->get_count_tindak_lanjut_peg($id_user);
        $data['count_verifikasi'] = $this->Model_beranda->get_count_verifikasi();
        $data['count_rekomendasi'] = $this->Model_beranda->get_count_rekomendasi();
        $data['count_tindak_lanjut'] = $this->Model_beranda->get_count_tindak_lanjut();
		
		$data['count_riwayat_inspektorat'] = $this->Model_beranda->get_count_riwayat_inspektorat();
		$data['count_riwayat'] = $this->Model_gratifikasi->get_count_riwayat_all();
		$data['count_riwayat_verifikasi'] = $this->Model_gratifikasi->get_count_riwayat_verifikasi();
		//$data['cek_eselon'] = $this->Model_gratifikasi->get_cek_eselon();
		$data['count_pasca_tindak_lanjut'] = $this->Model_gratifikasi->get_count_pasca_tindak_lanjut();
		
        $data['draft'] = $this->Model_gratifikasi->get_data_draft_beranda($id_user);
        $data['show_profil'] = $this->Model_profil->show_profil($id_user)->row();
        $data['tagmenu'] = 'beranda';
        $data['body'] = 'beranda/view_beranda';
        $this->load->vars($data);
        $this->load->view('view_main');
    }

}
