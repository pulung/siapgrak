<div class="main">
  <div class="container">
    <?= $this->session->flashdata('message'); ?>
    <?php if(!empty($show_profil->name) && !empty($show_profil->tempatlahir) && !empty($show_profil->tgllahir) && !empty($show_profil->email) && !empty($show_profil->no_ktp) && !empty($show_profil->nohp) && !empty($show_profil->inskerja) && !empty($show_profil->biro) && !empty($show_profil->pangkat) && !empty($show_profil->jabatan) && !empty($show_profil->alamat_kantor) && !empty($show_profil->kelurahan_kantor) && !empty($show_profil->kota_kantor) && !empty($show_profil->provinsi_kantor) && !empty($show_profil->kode_pos_kantor) && !empty($show_profil->no_kantor) && !empty($show_profil->alamat_rumah) && !empty($show_profil->kelurahan_rumah) && !empty($show_profil->kota_rumah) && !empty($show_profil->provinsi_rumah)): ?>
      <div class="row">
        <div class="col-md-8">
          <div class="services-block content content-center" id="services">
          <?php if($this->session->userdata('role') == '0' || $this->session->userdata('role') == '1'): ?>
            <a href="<?= site_url('gratifikasi/form_gratifikasi/tolak') ?>">
              <div class="col-md-3 col-sm-3 item">
                <i class="fa fa-hand-paper-o"></i>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;Laporan<br>Penolakan</h3>
              </div>
            </a>
            <a href="<?= site_url('gratifikasi/form_gratifikasi/terima') ?>">
              <div class="col-md-3 col-sm-3 item">
                <i class="fa fa-gift"></i>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;Laporan<br>Penerimaan</h3>
              </div>
            </a>
            <a href="<?= site_url('gratifikasi/riwayat_gratifikasi') ?>">
              <div class="col-md-3 col-sm-3 item">
                <i class="fa fa-book"></i><span style="margin-bottom: 95px;" class="badge badge-danger"><?php cetak($count_riwayat_peg->jumlah) ?></span>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Progress<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Laporan </h3>
              </div>
            </a>
            <a href="<?= site_url('gratifikasi/rekomendasi_gratifikasi') ?>">
            <div class="col-md-3 col-sm-3 item">
              <i class="fa fa-university"></i><span style="margin-bottom: 95px;" class="badge badge-danger"><?php cetak($count_rekomendasi_peg->jumlah) ?></span>
              <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Update<br>Rekomendasi<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KPK</h3>
            </div>
            </a>
            <a href="<?= site_url('gratifikasi/tindak_lanjut_gratifikasi') ?>">
              <div class="col-md-3 col-sm-3 item">
                <i class="fa fa-external-link"></i><span style="margin-bottom: 95px;" class="badge badge-danger"><?php cetak($count_tindak_lanjut_peg->jumlah) ?></span>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Update<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tindak<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lanjut</h3>
              </div>
            </a>
			<?php if(($this->session->userdata('eselon') != NULL) || ($this->session->userdata('eselon') != 'NULL') || ($this->session->userdata('eselon') != '')):?>
			<a href="<?= site_url('gratifikasi/laporan_tahunan') ?>"><?php //var_dump($this->session->userdata('eselon')); exit; ?>
              <div class="col-md-3 col-sm-3 col-xs-12 item">
                <i class="fa fa-pencil"></i>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;Laporan<br>&nbsp;&nbsp;&nbsp;Tahunan</h3>
              </div>
            </a>
			<?php endif;?>
          <?php elseif($this->session->userdata('role') == '0' || $this->session->userdata('role') == '2'): ?>
            <a href="<?= site_url('gratifikasi/verifikasi_gratifikasi') ?>">
              <div class="col-md-3 col-sm-3 item">
                <i class="fa fa-check-square-o"></i><span style="margin-bottom: 95px;" class="badge badge-danger"><?php cetak($count_verifikasi->jumlah) ?></span>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;Review</h3>
              </div>
            </a>
			<a href="<?= site_url('gratifikasi/verifikasi_kpk_inspektorat') ?>">
              <div class="col-md-3 col-sm-3 item">
                <i class="fa fa-users"></i><span style="margin-bottom: 95px;" class="badge badge-danger"><?php cetak($count_riwayat_verifikasi->jumlah) ?></span>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Catatan<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Verifikasi<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KPK </h3>
              </div>
            </a>
            <a href="<?= site_url('gratifikasi/rekomendasi_gratifikasi') ?>">
              <div class="col-md-3 col-sm-3 item">
                <i class="fa fa-university"></i><span style="margin-bottom: 95px;" class="badge badge-danger"><?php cetak($count_rekomendasi->jumlah) ?></span>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data<br>Rekomendasi<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KPK</h3>
              </div>
            </a>
            <a href="<?= site_url('gratifikasi/tindak_lanjut_gratifikasi') ?>">
              <div class="col-md-3 col-sm-3 item">
                <i class="fa fa-external-link"></i><span style="margin-bottom: 95px;" class="badge badge-danger"><?php cetak($count_tindak_lanjut->jumlah) ?></span>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tindak<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lanjut</h3>
              </div>
            </a>
            <a href="<?= site_url('gratifikasi/ringkasan_laporan') ?>">
              <div class="col-md-3 col-sm-3 col-xs-12 item">
                <i class="fa fa-bar-chart"></i>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;Dashboard<br>Pengendalian<br>&nbsp;&nbsp;&nbsp;Gratifikasi</h3>
              </div>
            </a>
			<a href="<?= site_url('gratifikasi/riwayat_gratifikasi_inspektorat') ?>">
              <div class="col-md-3 col-sm-3 item">
                <i class="fa fa-pencil"></i><span style="margin-bottom: 95px;" class="badge badge-danger"><?php cetak($count_riwayat_inspektorat->jumlah) ?></span>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Laporan<br>&nbsp;&nbsp;&nbsp;&nbsp;Tahunan </h3>
              </div>
            </a>
			<a href="<?= site_url('gratifikasi/verifikasi_gratifikasi_inspektorat') ?>">
              <div class="col-md-3 col-sm-3 item">
                <i class="fa fa-book"></i><span style="margin-bottom: 95px;" class="badge badge-danger"><?php cetak($count_riwayat->jumlah) ?></span>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Riwayat </h3>
              </div>
            </a>
			<a href="<?= site_url('gratifikasi/info_pasca_tindak_lanjut') ?>">
              <div class="col-md-3 col-sm-3 item">
                <i class="fa fa-users"></i><span style="margin-bottom: 95px;" class="badge badge-danger"><?php cetak($count_pasca_tindak_lanjut->jumlah) ?></span>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;Tuntas<br>Tindak Lanjut </h3>
              </div>
            </a>
          <?php elseif($this->session->userdata('role') == '0' || $this->session->userdata('role') == '4'): ?>
            <a href="<?= site_url('gratifikasi/ringkasan_laporan') ?>">
              <div class="col-md-3 col-sm-3 col-xs-12 item">
                <i class="fa fa-bar-chart"></i>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;Dashboard</h3>
              </div>
            </a>
			<a href="<?= site_url('gratifikasi/riwayat_gratifikasi_inspektorat') ?>">
              <div class="col-md-3 col-sm-3 item">
                <i class="fa fa-pencil"></i><span style="margin-bottom: 95px;" class="badge badge-danger"><?php cetak($count_riwayat_inspektorat->jumlah) ?></span>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Laporan<br>&nbsp;&nbsp;&nbsp;&nbsp;Tahunan</h3>
              </div>
            </a>
		  
		  <?php elseif($this->session->userdata('role') == '2' || $this->session->userdata('role') == '4'): ?>
            <a href="<?= site_url('bantuan') ?>">
              <div class="col-md-3 col-sm-3 col-xs-12 item">
                <center>
				<i class="fa fa-book"></i>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;Petunjuk<br>Penggunaan<br>&nbsp;&nbsp;&nbsp;Aplikasi</h3>
				</center>
              </div>
            </a>
          <?php //endif; ?>
		  <?php elseif($this->session->userdata('role') == '5'): ?>
            <a href="<?= site_url('pengguna') ?>">
              <div class="col-md-3 col-sm-3 col-xs-12 item">
                <center>
				<i class="fa fa-user"></i>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;Manajemen<br>User</h3>
				</center>
              </div>
            </a>
			<a href="<?= site_url('user') ?>">
              <div class="col-md-3 col-sm-3 col-xs-12 item">
                <center>
				<i class="fa fa-sitemap"></i>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;Manajemen<br>Hirarki</h3>
				</center>
              </div>
            </a>
			
			<a href="<?= site_url('upg') ?>">
              <div class="col-md-3 col-sm-3 col-xs-12 item">
                <center>
				<i class="fa fa-building"></i>
                <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;Manajemen<br>UPG</h3>
				</center>
              </div>
            </a>
          <?php endif; ?>
          </div>
        </div>
        <?php if($this->session->userdata('role') == '0' || $this->session->userdata('role') == '1'): ?>
        <div class="col-md-4">
          <div class="portlet box blue-madison">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-check"></i>Draft Laporan
              </div>
            </div>
            <div class="portlet-body">
              <div class="scroller" style="height: 250px;" data-always-visible="1" data-rail-visible="0">
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr class="active">
                      <th style="font-size:12px; text-align:center;">No</th>
                      <th style="font-size:12px; text-align:center;">Tanggal Penerimaan</th>
                      <th style="font-size:12px; text-align:center;">Nama Pemberi</th>
                      <th style="font-size:12px; text-align:center;" width="25%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php  if(!empty($draft)): ?>
                    <?php $i = 1; ?>
                    <?php foreach($draft as $row):?>
                    <tr>
                      <td style="font-size:11px; text-align:center;"><?php cetak($i++) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak(date('d-M-Y', strtotime($row->tanggal_penerimaan))) ?></td>
                      <td style="font-size:11px; text-align:center;"><?php cetak($row->nama_pemberi) ?></td>
                      <td style="font-size:11px; text-align:center;">
                        <a href="<?= site_url('gratifikasi/update_gratifikasi/'.$row->id); ?>" class="btn btn-xs green-seagreen" title="Edit"><i class="fa fa-edit"></i></a> 
                        <a href="<?php echo site_url('gratifikasi/send_to/'.$row->id); ?>" title="Kirim Review" onclick="return confirm('Apakah anda yakin mengirimkan data ini?');" class="btn btn-xs grey-gallery" style="font-size:11px;"><i class="fa fa-paper-plane-o"></i></a>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <th style="font-size:12px; text-align:center;" colspan="4">Tidak ada data</th>
                    </tr>
                  <?php endif; ?>
                  </tbody>
                </table>
              </div>
              <div class="scroller-footer">
                <div class="btn-arrow-link pull-right">
                  <a href="<?= site_url('gratifikasi/riwayat_gratifikasi') ?>">Lihat Semua Riwayat</a>
                  <i class="icon-arrow-right"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php endif; ?>
      </div>
    <?php else: ?>
      <div class="services-block content content-center" id="services">
        <div class="row">
          <a href="<?= site_url('profil') ?>">
            <div class="col-md-3 col-sm-3 col-xs-12 item">
              <i class="fa fa fa-address-card-o"></i>
              <h3 style="font-family: Lato; font-size: 15px; color: #3E4D5C; font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Profil<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Saya</h3>
            </div>
          </a>
          <div class="col-md-6">
            <div class="note note-info">
              <h4 class="block">Info!</h4>
              <p>
                 Sebelum melaporkan gratifikasi, harap melengkapi Profil Saya terlebih dahulu.
              </p>
            </div>
          </div>
        </div>
      </div>
      <br><br><br>
    <?php endif; ?>
  </div>
</div>