﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <meta charset="utf-8">
  <title>Lapor Gratifikasi | Sign In</title>

  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="Metronic Shop UI description" name="description">
  <meta content="Metronic Shop UI keywords" name="keywords">
  <meta content="keenthemes" name="author">

  <meta property="og:site_name" content="-CUSTOMER VALUE-">
  <meta property="og:title" content="-CUSTOMER VALUE-">
  <meta property="og:description" content="-CUSTOMER VALUE-">
  <meta property="og:type" content="website">
  <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
  <meta property="og:url" content="-CUSTOMER VALUE-">

  <link rel="shortcut icon" href="favicon.ico">

  <!-- Fonts START -->
  <link href="<?= site_url('assets/global/css/open-sans.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?= site_url('assets/global/css/sans-pro.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?= site_url('assets/global/css/lato.css'); ?>" rel="stylesheet" type="text/css">
  <!-- Fonts END -->

  <!-- Global styles START -->          
  <link href="<?= site_url('assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?= site_url('assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <!-- Global styles END --> 
   
  <!-- Page level plugin styles START -->
  <link href="<?= site_url('assets/global/plugins/fancybox/source/jquery.fancybox.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/global/plugins/uniform/css/uniform.default.css'); ?>" rel="stylesheet" type="text/css">
  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <link href="<?= site_url('assets/global/css/components.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/frontend/layout/css/style.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/frontend/layout/css/style-responsive.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/frontend/layout/css/themes/blue.css'); ?>" rel="stylesheet" id="style-color">
  <link href="<?= site_url('assets/frontend/layout/css/custom.css'); ?>" rel="stylesheet">
  <!-- Theme styles END -->
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate" oncontextmenu="return false">
    <div class="wrapper" style="background-color:#23e62f">
        <!--<img width="100%" src="<?php echo site_url('assets/images/logo/banner-4.jpg'); ?>"/>-->
        <img width="100%" src="<?php echo site_url('assets/images/logo/siapgrak-logo2.png' ); ?>" style="
    width: 690px;
    padding-left: 90px;
"/>
		<hr>
    </div>
    <br><br><br><br>
    <div class="main">
      <div class="container" >
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <div class="col-md-6 col-sm-6">
            <!--<h1 style="font-family: Segoe UI; font-size: 40px; font-weight: bold;">SIAPGraK</h1>
            <p style="font-family: Segoe UI; font-size: 25px;">Sistem Informasi Administrasi <br>dan Pelaporan Gratifikasi Kementerian</p><br><br>-->
            <!--<h1 style="font-family: Segoe UI; font-size: 40px; font-weight: bold;">SIAPGraK</h1>-->
            <p style="font-family: Segoe UI; font-size: 25px;"><b>Sistem Informasi Administrasi Pengendalian Gratifikasi Kementerian</b></p><br><br>
            <?= $this->session->flashdata('message'); ?>
            <form method="post" role="form" enctype="multipart/form-data" autocomplete="off" action="<?= site_url('login/proseslogin'); ?>">
              <!--<?=  form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash());  ?>-->
              <div class="row static-info">
                <div class="col-md-3 name">
                  <label>Nama Pengguna</label>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fa fa-user-circle"></i>
                    </span>
                   <input type="text" class="form-control" id="inusername" name="inusername" placeholder="Nama Pengguna">
                  </div>
                </div>
              </div>
              <div class="row static-info">
                <div class="col-md-3 name">
                  <label>Kata Sandi</label>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fa fa-lock"></i>
                    </span>
                    <input type="password" class="form-control" id="inpassword" name="inpassword" placeholder="Kata Sandi">
                  </div>
                </div>
              </div>
              <br><br>
              <div class="row" >
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn yellow-casablanca" style="font-weight: bold;"><i class="icon-login"></i> Masuk</button>
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-6 col-sm-6">
            <p style="font-family: Segoe UI; font-size: 17px; color: #3E4D5C; padding-bottom: 18px" align="center"><b>PELAPORAN GRATIFIKASI SEBAGAI UPAYA<br>PENCEGAHAN KORUPSI</b></p>
            <p style="font-family: Segoe UI; font-size: 14px; color: #3E4D5C; font-weight: bold;" align="justify">
            Kementerian Sekretariat Negara terus melakukan penyempurnaan tata kelolanya sejalan dengan semangat menjadi Centre of Excellence. Perbaikan berkelanjutan difokuskan antara lain pada efisiensi proses, efektifitas pencapaian hasil, kehematan anggaran serta bebas KKN, ujungnya adalah tercapainya Good Governance dan Clean Government.<br><br>

            Korupsi seringkali berawal dari kebiasaan yang tidak disadari oleh setiap penyelenggara negara, misal : penerimaan hadiah atau suatu fasilitas tertentu yang tidak wajar,  cepat atau lambat akan memengaruhi pengambilan keputusan. Perlu difahami bahwa pemberian tersebut selalu terkait dengan jabatan yang dipangku oleh penerima serta kemungkinan adanya kepentingan-kepentingan dari pemberi, dan pada saatnya penerima akan berbuat sesuatu untuk kepentingan pemberi sebagai balas jasa. Pemberian seperti ini dapat digolongkan sebagai gratifikasi.<br><br>
            Pengertian Gratifikasi menurut penjelasan Pasal 12B UU No. 20 Tahun 2001, adalah pemberian dalam arti luas, yakni meliputi pemberian uang, barang, rabat (discount), komisi, pinjaman tanpa bunga, tiket perjalanan, fasilitas penginapan, perjalanan wisata, pengobatan cuma-cuma, dan fasilitas lainnya. Gratifikasi tersebut baik yang diterima di dalam negeri maupun di luar negeri dan yang dilakukan dengan menggunakan sarana elektronik atau tanpa sarana elektronik.<br><br>

            Setiap gratifikasi kepada pegawai negeri atau penyelenggara negara dianggap pemberian suap, apabila berhubungan dengan jabatannya dan berlawanan dengan kewajiban atau tugasnya, namun ketentuan yang sama tidak berlaku apabila penerima melaporkan gratifikasi yang diterimanya kepada Komisi Pemberantasan Tindak Pidana Korupsi (KPK) yang wajib dilakukan paling lambat 30 (tiga puluh) hari kerja terhitung sejak tanggal gratifikasi tersebut diterima.<br><br>

            Untuk memenuhi kewajiban pelaporan gratifikasi inilah, Kementerian Sekretariat Negara menggagas sebuah Sistem Informasi Pelaporan Gratifikasi yang bermanfaat bagi seluruh pegawai dan pejabatnya melaporkan setiap penolakan atau penerimaan gratifikasi sebagaimana dimaksud pasal 12b UU 20 tahun 2001.</p>
          </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>
    <br><br><br><br><br><br><br><br><br><br>
    <!-- BEGIN FOOTER -->
    <div class="footer">
      <div class="container">
        <div class="row">
          <!-- BEGIN COPYRIGHT -->
          <div class="col-md-6 col-sm-6 padding-top-10">
            2017 © Biro Informasi dan Teknologi - Kementerian Sekretariat Negara.
          </div>
          <!-- END COPYRIGHT -->
        </div>
      </div>
    </div>
    <!-- END FOOTER -->

    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="../../assets/global/plugins/respond.min.js"></script>
    <![endif]--> 
    <script src="<?= site_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/jquery-migrate.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="<?= site_url('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js'); ?>" type="text/javascript"></script><!-- pop up -->

    <script src="<?= site_url('assets/frontend/layout/scripts/layout.js'); ?>" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>