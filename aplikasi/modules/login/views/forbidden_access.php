<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <meta charset="utf-8">
  <title>SIAPGrak | 403 Akses Ditolak</title>

  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="Metronic Shop UI description" name="description">
  <meta content="Metronic Shop UI keywords" name="keywords">
  <meta content="keenthemes" name="author">

  <meta property="og:site_name" content="-CUSTOMER VALUE-">
  <meta property="og:title" content="-CUSTOMER VALUE-">
  <meta property="og:description" content="-CUSTOMER VALUE-">
  <meta property="og:type" content="website">
  <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
  <meta property="og:url" content="-CUSTOMER VALUE-">

  <link rel="shortcut icon" href="favicon.ico">

  <!-- Fonts START -->
  <link href="<?= site_url('assets/global/css/open-sans.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?= site_url('assets/global/css/sans-pro.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?= site_url('assets/global/css/lato.css'); ?>" rel="stylesheet" type="text/css">
  <!-- Fonts END -->

  <!-- Global styles START -->          
  <link href="<?= site_url('assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?= site_url('assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <!-- Global styles END --> 
   
  <!-- Page level plugin styles START -->
  <link href="<?= site_url('assets/global/plugins/fancybox/source/jquery.fancybox.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/global/plugins/uniform/css/uniform.default.css'); ?>" rel="stylesheet" type="text/css">
  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <link href="<?= site_url('assets/global/css/components.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/frontend/layout/css/style.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/frontend/layout/css/style-responsive.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/frontend/layout/css/themes/blue.css'); ?>" rel="stylesheet" id="style-color">
  <link href="<?= site_url('assets/frontend/layout/css/custom.css'); ?>" rel="stylesheet">
  <!-- Theme styles END -->
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate" oncontextmenu="return false">
    <div class="wrapper">
        <img width="100%" src="<?php echo site_url('assets/images/logo/banner-2.jpg'); ?>"/>
    </div>
    <br><br><br><br><br><br>
     <div class="main">
      <div class="container">
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12">
            <div class="content-page page-404">
               <div class="number">
                  403
               </div>
               <div class="details">
                  <h3>Akses ditolak.</h3>
                  <p>
                     Anda tidak memiliki hak untuk mengakses halaman ini.<br>
                      <a href="<?= site_url('login') ?>" class="btn btn-grey"><i class="icon-arrow-left"></i>
                        Kembali
                      </a>
                  </p>
               </div>
            </div>
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>
    <br><br><br><br><br>
    <!-- BEGIN FOOTER -->
    <div class="footer">
      <div class="container">
        <div class="row">
          <!-- BEGIN COPYRIGHT -->
          <div class="col-md-6 col-sm-6 padding-top-10">
            2017 © Biro Informasi dan Teknologi - Kementerian Sekretariat Negara.
          </div>
          <!-- END COPYRIGHT -->
        </div>
      </div>
    </div>
    <!-- END FOOTER -->

    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="../../assets/global/plugins/respond.min.js"></script>
    <![endif]--> 
    <script src="<?= site_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/jquery-migrate.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script> 
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="<?= site_url('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js'); ?>" type="text/javascript"></script><!-- pop up -->

    <script src="<?= site_url('assets/frontend/layout/scripts/layout.js'); ?>" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>