<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_login extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getloginbyuname($name) {
        $data = array();
        $sql = "SELECT * FROM conf_user a WHERE a.username = ? ";
                
        $Q = $this->db->query($sql, $name);
        if ($Q->num_rows() > 0) {
            $data = $Q->row_array();
        }

        $Q->free_result();
        return $data;
    }

    function getcountprivelegegroup($id_role) {

        $sql = "SELECT count(id_privilege) as jml FROM role_privilege WHERE id_role = ? ";
        //die($sql);
        $Q = $this->db->query($sql, array($id_role));
        $res = 0;
        if ($Q->num_rows() > 0) {
            $data = $Q->row_array();

            if ($data['jml'] > 0)
                $res = $data['jml'];
        }

        $Q->free_result();
        return $res;
    }

    function isauthprivileges($id_privilege, $id_role) {

        $sql = "SELECT count(id_privilege) as jml FROM role_privilege WHERE id_privilege = ? AND id_role = ? ";
       
        $Q = $this->db->query($sql, array($id_privilege, $id_role));
        $res = false;
        if ($Q->num_rows() > 0) {
            $data = $Q->row_array();

            if ($data['jml'] > 0)
                $res = true;
        }

        $Q->free_result();
        return $res;
    }

    public function delete($formatted_current_time) {
        $this->db->where("created_at BETWEEN '1970-00-00 00:00:00' AND '$formatted_current_time'");
        $res = $this->db->delete('login_log');

        if (!$res) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }
}
