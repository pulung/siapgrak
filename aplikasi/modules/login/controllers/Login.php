<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Login extends CI_Controller{
     public function __construct() {
        parent::__construct();
        $this->load->library(array('Lib_chipers','throttle'));
        $this->lib_chipers->inisialize($this->config->item('encryption_key'));
        $this->load->model(array('Model_login'));
        include APPPATH . 'third_party/adldap/adLDAP.php';
        @date_default_timezone_set('Asia/Jakarta');    
    }
    
    public function index() {
        $this->auth->restrict(TRUE);
        $this->load->view('view_login');
    }

    public function proseslogin() {
		$this->auth->restrict(TRUE);
		
        //LDAP
		$this->form_validation->set_rules('inusername', "Username", 'strip_tags|trim|required|max_length[71]|callback_inusername_check|xss_clean');
        $this->form_validation->set_rules('inpassword', "Password", 'strip_tags|trim|required|max_length[71]|callback_inpassword_check|xss_clean');
        
		//Non-LDAP
		//$this->form_validation->set_rules("Username", 'strip_tags|trim|required|max_length[71]|xss_clean');
		//$this->form_validation->set_rules("Password", 'strip_tags|trim|required|max_length[71]|xss_clean');
        //$this->form_validation->set_rules('inusername', "Username", 'strip_tags|trim|required|max_length[71]|callback_inusername_check|xss_clean');
        //$this->form_validation->set_rules('inpassword', "Password", 'strip_tags|trim|required|max_length[71]|callback_inpassword_check|xss_clean');
        
		$timeout=10;
        $formatted_current_time = date("Y-m-d H:i:s", strtotime('-' . (int)$timeout . ' minutes'));
        $this->Model_login->delete($formatted_current_time);
		
        if ($this->form_validation->run() == FALSE) {
            $attempts = $this->throttle->throttle(1);
            $this->index();
        } else {
            $row = $this->hasil;
			
            $this->session->set_userdata('logged_in', TRUE);
            $this->session->set_userdata('id', $row['id']);
            $this->session->set_userdata('username', $row['username']);
            $this->session->set_userdata('name', $row['name']);
            $this->session->set_userdata('jabatan', $row['jabatan']);
        $this->session->set_userdata('isldap', $row['isldap']);
            $this->session->set_userdata('role', $row['id_role']);
            redirect('beranda');
        }
    }

    function inusername_check($inusername) {
        $this->hasil = $this->Model_login->getloginbyuname(strtolower($inusername));
        if (count($this->hasil) < 1):
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-remove"></i>Gagal!</strong> Nama pengguna tidak ditemukan.</div>');
        else:
            if($this->hasil['isactive'] == '0'):
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-remove"></i>Gagal!</strong> Nama pengguna sudah tidak aktif. Hubungi Administrator.</div>');
            else:
                return TRUE;
            endif;
        endif;
    }

    function inpassword_check($pass) {
        if (count($this->hasil) >= 1):
            $isldap = $this->hasil['isldap'];
			//return TRUE;
            if ($isldap == 1):
                // autentikasi ldap
                try {
                    $adldap = new adLDAP();
                    $inusername = $this->input->post('inusername');
                    
                    if(is_numeric($inusername)) {
				$inusernamepos = strpos($inusername, '.');
			    } else {
	                    	$inusernamepos = $inusername;
			    }

                if($inusernamepos > 0)    
                    $inusername = substr($inusername, $inusernamepos + 1, strlen($inusername));
    
                                    
                    $inusername = @$adldap->user()->infoCollection($inusername, array('*'));
                    $inusername = @$inusername->distinguishedname;
                                                                    
                    if ($adldap->user()->authenticate($inusername, $pass, TRUE)){
                        return TRUE;
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-remove"></i>Gagal!</strong> Anda menggunakan SSO. Silahkan masukkan kata sandi SSO.</div>');
                        return FALSE;
                    }
                    
                } catch (adLDAPException $e) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-remove"></i>Gagal!</strong> Login SSO gagal. Silahkan coba lagi.</div>');
                    return FALSE;   
                }          
            else:
                $stored_hash = $this->hasil['password'];

                if ($this->bcrypt->check_password($pass, $stored_hash)):
                    return TRUE;
                else:
                     $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-remove"></i>Gagal!</strong> Password salah.</div>');
                    return FALSE;
                endif;
            endif;
        else:
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button><strong><i class="icon-remove"></i>Gagal!</strong> Nama pengguna tidak ditemukan.</div>');
            return FALSE;
        endif;
    }

    public function logout() {
        $this->auth->restrict();
        
        //unset userdata
        $this->session->unset_userdata('id', $row['id']);
        $this->session->unset_userdata('username', $row['username']);
        $this->session->unset_userdata('name', $row['name']);
        $this->session->unset_userdata('jabatan', $row['jabatan']);
        $this->session->unset_userdata('isldap', $row['isldap']);
        $this->session->unset_userdata('role', $row['id_role']);

        // delete cookies if they exist
        if (get_cookie("ci_session")):
            delete_cookie("ci_session");
        endif;
        if (get_cookie("csrf_cookie")):
            delete_cookie("csrf_cookie");
        endif;

        // Destroy the session
        $this->session->sess_destroy();

        //Recreate the session
        if (substr(CI_VERSION, 0, 1) == '2')
        {
            $this->session->sess_create();
        }
        else
        {
            if (version_compare(PHP_VERSION, '7.0.0') >= 0) {
                session_start();
            }
            $this->session->sess_regenerate(TRUE);
        }
        redirect('login');
    }

    function aksestolak() {
        $this->load->view('forbidden_access');
    }

    function halaman404() {
        $this->load->view('not_found404');
    }

    function halaman503() {
        $this->load->view('limit_login503');
    }
}