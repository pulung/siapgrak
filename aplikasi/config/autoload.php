<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('database','session','upload','form_validation','auth','common','bcrypt','encryption');

$autoload['drivers'] = array();

$autoload['helper'] = array('form','html','url','date','array','text','security','xss','cookie');

$autoload['config'] = array('authmenu');

$autoload['language'] = array();

$autoload['model'] = array();
