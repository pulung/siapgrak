<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['AKSES_PEGAWAI'] = "1";

$config['AKSES_INSPEKTORAT'] = "2";

$config['AKSES_INSMEN'] = "3";

$config['AKSES_MENTERI'] = "4";

$config['AKSES_SUPER_ADMIN'] = "5";

$config['AKSES_INSPEG'] = "6";

$config['AKSES_ALL'] = "7";
