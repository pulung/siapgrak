<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Throttle
{
    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->database();
        $this->CI->load->model('throttle_model');
    }

    /**
     * throttles multiple connections attempts to prevent abuse
     * @param int $type type of throttle to perform.
     *
     */
    public function throttle($type = 1, $limit = 5, $timeout = 10)
    {
        //clean up login attempts older than specified time
        // $this->throttle_cleanup($timeout);

        $data = new stdClass();
        $data->ip = $this->CI->input->ip_address();
        $data->type = $type;

        $this->CI->throttle_model->insert($data);

        $attempts = $this->CI->throttle_model->where(['ip' => $this->CI->input->ip_address(), 'type' => $type])->count_rows();

        if ($attempts > $limit) {
            redirect('login/halaman503');
        }

        return $attempts; // return current number of attempted logins
    }

    /**
     * Cleans up old throttling attempts based on throttle timeout
     *
     * @param $timeout
     * @return result of query
     */
    public function throttle_cleanup($timeout)
    {
        $formatted_current_time = date("Y-m-d H:i:s", strtotime('-' . (int)$timeout . ' minutes'));
        $modifier =  ' BETWEEN "1970-00-00 00:00:00" and ' . $formatted_current_time;

        return $this->CI->throttle_model->where(['created_at' => $modifier])->delete();
    }


}
