<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth {

    // var $CI = null;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->database();
        $this->CI->load->model('login/Model_login');
    }

    function restrict($logged_out = FALSE) {
        if ($logged_out && $this->logged_in()) {
            redirect('beranda');
        }
        
        if (!$logged_out && !$this->logged_in()) {
            redirect('login');
        }
    }

    function logged_in() {
        if ($this->CI->session->userdata('logged_in') == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function issetpriv() {
        return $this->CI->Model_login->getcountprivelegegroup($this->CI->session->userdata('role')) > 0 ? TRUE : FALSE;
		//return TRUE;
    }

    function isprivilegectrl($id_privilege) {
        $id_role = $this->CI->session->userdata('role');
       
     
        if ($this->issetpriv()) {
            if (!$this->CI->Model_login->isauthprivileges($id_privilege, $id_role))
                redirect('login/aksestolak');
        } else {
            redirect('login/aksestolak');
        }
    }

    function isprivilege($id_privilege) {
        $id_role = $this->CI->session->userdata('role');

        $res = true;
        if (!$this->issetpriv()) {
            $res = false;
        } else {
            if (!$this->CI->Model_login->isauthprivileges($id_privilege, $id_role))
                $res = false;
        }
        return $res;
    }

}

// End of library class
// Location: system/application/libraries/Auth.php
