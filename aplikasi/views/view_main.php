<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <meta charset="utf-8">
  <!--<title>Sistem Informasi Administrasi dan Pelaporan Gratifikasi Kementerian</title>-->
  <title>Sistem Informasi Lapor Gratifikasi</title>

  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="Metronic Shop UI description" name="description">
  <meta content="Metronic Shop UI keywords" name="keywords">
  <meta content="keenthemes" name="author">

  <meta property="og:site_name" content="-CUSTOMER VALUE-">
  <meta property="og:title" content="-CUSTOMER VALUE-">
  <meta property="og:description" content="-CUSTOMER VALUE-">
  <meta property="og:type" content="website">
  <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
  <meta property="og:url" content="-CUSTOMER VALUE-">

  <link rel="shortcut icon" href="<?= site_url('assets/images/logo/favicon.PNG') ?>"/>

  <!-- Fonts START -->
  <!--<link href="<?= site_url('assets/global/css/open-sans.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?= site_url('assets/global/css/sans-pro.css'); ?>" rel="stylesheet" type="text/css">-->
  <link href="<?= site_url('assets/global/css/lato.css'); ?>" rel="stylesheet" type="text/css">
  <!-- Fonts END -->

  <!-- Global styles START -->          
  <link href="<?= site_url('assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?= site_url('assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/global/plugins/bootstrap-datepicker/css/datepicker3.css'); ?>" rel="stylesheet" type="text/css"/>
  <link href="<?= site_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css'); ?>" rel="stylesheet" type="text/css"/>
  <link href="<?= site_url('assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css'); ?>"/>
  <link href="<?= site_url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css'); ?>" rel="stylesheet" type="text/css"/>
  <link href="<?= site_url('assets/global/plugins/bootstrap-select/bootstrap-select.min.css'); ?>"  rel="stylesheet" type="text/css"/>
  <link href="<?= site_url('assets/global/plugins/select2/select2.css'); ?>" rel="stylesheet" type="text/css"/>
  <link href="<?= site_url('assets/global/plugins/jquery-multi-select/css/multi-select.css'); ?>"  rel="stylesheet" type="text/css"/>
  
  
  <!-- Global styles END --> 
   
  <!-- Page level plugin styles START -->

  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <link href="<?= site_url('assets/global/css/components.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/global/css/plugins.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?= site_url('assets/frontend/layout/css/style.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/frontend/pages/css/style-shop.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?= site_url('assets/frontend/pages/css/style-layer-slider.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/frontend/layout/css/style-responsive.css'); ?>" rel="stylesheet">
  <link href="<?= site_url('assets/frontend/layout/css/themes/blue.css'); ?>" rel="stylesheet" id="style-color">
  <link href="<?= site_url('assets/frontend/layout/css/custom.css'); ?>" rel="stylesheet">
  <!-- Theme styles END -->
  
  <!-- Datepicker START -->
  <link href="<?= site_url('assets/global/jquery-ui-1.11.4/smoothness/jquery-ui.css'); ?>" rel="stylesheet">
  <script src="<?= site_url('assets/global/jquery-ui-1.11.4/external/jquery/jquery.js'); ?>"></script>
  <script src="<?= site_url('assets/global/jquery-ui-1.11.4/jquery-ui.js'); ?>"></script>
  <script src="<?= site_url('assets/global/jquery-ui-1.11.4/jquery-ui.min.js'); ?>"></script>
  <link rel="stylesheet" href="<?= site_url('assets/global/jquery-ui-1.11.4/jquery-ui.theme.css'); ?>">
  <script>
   $(document).ready(function(){
    $("#tanggal_penerimaan").datepicker({
		dateFormat: 'yyyy-mm-dd'
    })
   })
  </script>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="ecommerce" oncontextmenu="return false">
    <!-- BEGIN HEADER -->
    <?= $this->load->view('view_header'); ?>
    <!-- END HEADER -->

    <!-- BEGIN PAGE CONTAINER -->
    <?= $this->load->view($body); ?>
    <!-- END PAGE CONTAINER -->

    <!-- BEGIN FOOTER -->
    <?= $this->load->view('view_footer'); ?>
    <!-- END FOOTER -->

    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="../../assets/global/plugins/respond.min.js"></script>  
    <![endif]-->
    <script src="<?= site_url('assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/jquery-migrate.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js'); ?>"></script>
    <script src="<?= site_url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/bootstrap-select/bootstrap-select.min.js'); ?>"  type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/select2/select2.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js'); ?>"  type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/fancybox/source/jquery.fancybox.pack.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/global/plugins/jquery-validation/js/additional-methods.min.js'); ?>" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="<?= site_url('assets/global/scripts/metronic.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/frontend/layout/scripts/layout.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('assets/frontend/pages/scripts/components-pickers.js'); ?>"></script>
    <script src="<?= site_url('assets/frontend/pages/scripts/components-dropdowns.js'); ?>"></script>
    <script src="<?= site_url('assets/frontend/pages/scripts/table-managed.js'); ?>"></script>
    <script src="<?= site_url('assets/frontend/pages/validation/formpenerima.js'); ?>"></script>
    <script src="<?= site_url('assets/frontend/pages/validation/formpelapor.js'); ?>"></script>
    <script src="<?= site_url('assets/frontend/pages/validation/updatepenerima.js'); ?>"></script>
    <script src="<?= site_url('assets/frontend/pages/validation/updatepelapor.js'); ?>"></script>
    <script src="<?= site_url('assets/frontend/pages/validation/insertprofil.js'); ?>"></script>
    <script src="<?= site_url('assets/frontend/pages/validation/formsk.js'); ?>"></script>
    <script src="<?= site_url('assets/frontend/pages/validation/formtindaklanjut.js'); ?>"></script>
    <script src="<?= site_url('assets/frontend/pages/validation/formverifikasi.js'); ?>"></script>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            Metronic.init();
            Layout.init(); 
            ComponentsPickers.init();
            ComponentsDropdowns.init();
            TableManaged.init();
            FormPenerima.init();
            FormPelapor.init();
            UpdatePenerima.init();
            UpdatePelapor.init();
            InsertProfil.init();
            FormSK.init();
            FormTindakLanjut.init();
            FormVerifikasi.init();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
	
	<!-- css dan js untuk manajemen user 
	<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css" crossorigin="anonymous">
	<link rel="stylesheet" href="http://cdn.bootcss.com/font-awesome/4.5.0/css/font-awesome.min.css" type="text/css" media="screen" title="no title" charset="utf-8"/>-->
	<style>
    body { background-color:#fafafa; font-family:'Lato';}
		.treetable{

		}
		.treetable .fa{
			cursor: pointer;
			padding-right: 5px;
		}
		.treetable .rowhidden{
			display: none;
		}
		.treetable .j-addChild{
			display: none;
		}
		.treetable .selected .j-addChild{
			display: block;
		}
		.treetable .btn-outline{
			background-color: transparent;
		}
		.treetable .form-control{
			width: auto;
			display: inline-block;
		}
		.treetable .textalign-center{
			text-align: center;
		}
		.treetable .j-expend{
			cursor: pointer;
			width: 35% !important;
			text-align: left !important;
		}
		.treetable .maintitle{
			width: 35% !important;
		}
		.treetable .j-remove{
			padding: 8px;
			cursor: pointer;
			font-size: 16px;
			color:red;
		}
		.treetable .tt-header{
			margin-top:10px;
		}
		.treetable .class-level-2 .class-level-ul .j-expend{
			position: relative;
			left: 22px;
		}
		.treetable .class-level-3 .class-level-ul .j-expend{
			position: relative;
			left: 44px;
		}
		.treetable .class-level-4 .class-level-ul .j-expend{
			position: relative;
			left: 66px;
		}
		.treetable .class-level-5 .class-level-ul .j-expend{
			position: relative;
			left: 88px;
		}
		.treetable .class-level-6 .class-level-ul .j-expend{
			position: relative;
			left: 110px;
		}
		.treetable .class-level-7 .class-level-ul .j-expend{
			position: relative;
			left: 132px;
		}
		.treetable .class-level-8 .class-level-ul .j-expend{
			position: relative;
			left: 154px;
		}
		.treetable .class-level-1 {
			border-bottom: dashed 1px #eee;
		}
		.treetable .class-level-ul{
			padding: 0;
			margin-bottom: 2px;
		}
		.treetable .class-level-ul li {
			float: left;
			text-align: center;
			vertical-align: middle;
			padding: 1px 10px;
			min-width: 120px;
			list-style: none;
		}
		.treetable .class-level-ul:after {
			display: block;
			clear: both;
			height: 0;
			content: "\0020";
		}
		.treetable .tt-header div span {
			width: auto;
			line-height: 29px;
			display: inline-block;
			min-width: 120px;
			text-align: center;
		}
		.treetable .tt-body{
			border: solid 1px #DDD;
			padding-top: 1px;
			background-color:#FFF;
		}
		.treetable .tt-header div{
			border: solid 1px #DDD;
			border-bottom:none;
			background-color:#FFF;
		}
	</style>
	
	<!-- style btn-danger -->
	<style>
		.btn-danger{color:#ffffff;text-shadow:0 -1px 0 rgba(0, 0, 0, 0.25);background-color:#da4f49;background-image:-moz-linear-gradient(top, #ee5f5b, #bd362f);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#bd362f));background-image:-webkit-linear-gradient(top, #ee5f5b, #bd362f);background-image:-o-linear-gradient(top, #ee5f5b, #bd362f);background-image:linear-gradient(to bottom, #ee5f5b, #bd362f);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffbd362f', GradientType=0);border-color:#bd362f #bd362f #802420;border-color:rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);*background-color:#bd362f;filter:progid:DXImageTransform.Microsoft.gradient(enabled = false);}.btn-danger:hover,.btn-danger:focus,.btn-danger:active,.btn-danger.active,.btn-danger.disabled,.btn-danger[disabled]{color:#ffffff;background-color:#bd362f;*background-color:#a9302a;}
		.btn-danger:active,.btn-danger.active{background-color:#942a25 \9;}
		.btn-group.open .btn-danger.dropdown-toggle{background-color:#bd362f;}
		.btn-primary .caret,.btn-warning .caret,.btn-danger .caret,.btn-info .caret,.btn-success .caret,.btn-inverse .caret{border-top-color:#ffffff;border-bottom-color:#ffffff;}
	</style>

	<script type="text/javascript" src="http://cdn.bootcss.com/jquery/2.2.1/jquery.min.js"></script>
	
	<script type="text/javascript" src="<?= site_url('assets/jquery.edittreetable.js'); ?>"></script>
	<script type="text/javascript">
	var data = [{
		id:1,name:"",pid:0,innercode:"1A"
	},
	{
		id:2,name:"",pid:1,innercode:"1A2A"
	},
	{
		id:2,name:"",pid:2,innercode:"1A2A"
	},
	{
		id:3,name:"",pid:2,innercode:"1A3A"
	},
	{
		id:4,name:"",pid:3,innercode:"1A4A"
	},
	{
		id:5,name:"",pid:4,innercode:"1A5A"
	},
	{
		id:6,name:"",pid:5,innercode:"1A6A"
	},
	{
		id:7,name:"",pid:6,innercode:"1A7A"
	},
	{
		id:8,name:"",pid:7,innercode:"1A8A"
	}];
	
	$("#bs-treeetable").bstreetable({
		data:data,
		maintitle:"Hirarki User",
		nodeaddCallback:function(data,callback){
			alert(JSON.stringify(data));
			//do your things then callback 
			callback({id:18,name:data.name,innercode:"ttttt",pid:data.pid});
		},
		noderemoveCallback:function(data,callback){
			alert(JSON.stringify(data));
			//do your things then callback
			callback();
		},
		nodeupdateCallback:function(data,callback){
			alert(JSON.stringify(data));
			//do your things then callback
			callback();
		}
	}
	);

	$("#bs-ml-treetable").bstreetable({
		data:data,
		maintitle:"a",
		nodeaddCallback:function(data,callback){
			alert(JSON.stringify(data));
			//do your things then callback 
			callback({id:18,name:data.name,innercode:"ttttt",pid:data.pid});
		},
		noderemoveCallback:function(data,callback){
			alert(JSON.stringify(data));
			//do your things then callback
			callback();
		},
		nodeupdateCallback:function(data,callback){
			alert(JSON.stringify(data));
			//do your things then callback
			callback();
		},
		extfield:[
			{title:"innercode",key:"innercode",type:"input"}
		]
	})
	</script>
	
	<?php if (isset($addcsslib)) { ?>
         <?php $this->load->view($addcsslib); ?>
    <?php } ?>

	<?php if (isset($addjslib)) { ?>
         <?php $this->load->view($addjslib); ?>
    <?php } ?>

    <?php if (isset($addjslibadd)) { ?>
         <?php $this->load->view($addjslibadd); ?>
    <?php } ?>
	
	<script type="text/javascript">
	$(document).ready(function(){
    var next = 1;
    $(".add-more").click(function(e){
        e.preventDefault();
        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;
        var newIn = '<input autocomplete="off" class="input form-control" id="field' + next + '" name="field' + next + '" type="text">';
		//var newIn2 = '<select class="form-control select2" onchange="myFunctionAgenda()" id="field' + next + '" name="field' + next + '"><option value="" disabled selected hidden>Pilih</option>'+ php1 +'<option value='+ php2 +''+ php3 +' >'+ php4 +'</option>'+ php5 +'</select><br>';
        var newInput = $(newIn);
        var removeBtn = '<button id="remove' + (next - 1) + '" class="  btn-danger remove-me" >-</button></div><div id="field">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
		});      
	});
	</script>

	<script type="text/javascript">
	$(document).ready(function(){
    var next = 1;
    $(".add-more2").click(function(e){
        e.preventDefault();
        var addto = "#fieldb" + next;
        var addRemove = "#fieldb" + (next);
        next = next + 1;
        var newIn = '<input autocomplete="off" class="input form-control" id="fieldb' + next + '" name="fieldb' + next + '" type="text">';
        var newInput = $(newIn);
        var removeBtn = '<button id="remove2' + (next - 1) + '" class="  btn-danger remove-me2" >-</button></div><div id="fieldb">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#fieldb" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me2').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#fieldb" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
		});      
	});
	</script>

	<script type="text/javascript">
	$(document).ready(function(){
    var next = 1;
    $(".add-more3").click(function(e){
        e.preventDefault();
        var addto = "#fielda" + next;
        var addRemove = "#fielda" + (next);
        next = next + 1;
        var newIn = '<input autocomplete="off" class="input form-control" id="fielda' + next + '" name="fielda' + next + '" type="text">';
        var newInput = $(newIn);
        var removeBtn = '<button id="remove3' + (next - 1) + '" class="  btn-danger remove-me3" >-</button></div><div id="fielda">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#fielda" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me3').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#fielda" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
		});      
	});
	</script>

	<script type="text/javascript">
	$(document).ready(function(){
    var next = 1;
    $(".add-more4").click(function(e){
        e.preventDefault();
        var addto = "#fieldc" + next;
        var addRemove = "#fieldc" + (next);
        next = next + 1;
        var newIn = '<input autocomplete="off" class="input form-control" id="fieldc' + next + '" name="fieldc' + next + '" type="text">';
        var newInput = $(newIn);
        var removeBtn = '<button id="remove4' + (next - 1) + '" class="  btn-danger remove-me4" >-</button></div><div id="fieldc">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#fieldc" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me4').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#fieldc" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
		});      
	});
	</script>
	
	<script type="text/javascript">
	$(document).ready(function(){
    var next = 1;
    $(".add-more5").click(function(e){
        e.preventDefault();
        var addto = "#fieldd" + next;
        var addRemove = "#fieldd" + (next);
        next = next + 1;
        var newIn = '<input autocomplete="off" class="input form-control" id="fieldd' + next + '" name="fieldd' + next + '" type="text">';
        var newInput = $(newIn);
        var removeBtn = '<button id="remove5' + (next - 1) + '" class="  btn-danger remove-me5" >-</button></div><div id="fieldd">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#fieldd" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me5').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#fieldd" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
		});      
	});
	</script>
	
	<script type="text/javascript">
	$(document).ready(function(){
    var next = 1;
    $(".add-more6").click(function(e){
        e.preventDefault();
        var addto = "#fielde" + next;
        var addRemove = "#fielde" + (next);
        next = next + 1;
        var newIn = '<input autocomplete="off" class="input form-control" id="fielde' + next + '" name="fielde' + next + '" type="text">';
        var newInput = $(newIn);
        var removeBtn = '<button id="remove6' + (next - 1) + '" class="  btn-danger remove-me6" >-</button></div><div id="fielde">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#fielde" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me6').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#fielde" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
		});      
	});
	</script>
	
	<script type="text/javascript">
	$(document).ready(function(){
    var next = 1;
    $(".add-more7").click(function(e){
        e.preventDefault();
        var addto = "#fieldf" + next;
        var addRemove = "#fieldf" + (next);
        next = next + 1;
        var newIn = '<input autocomplete="off" class="input form-control" id="fieldf' + next + '" name="fieldf' + next + '" type="text">';
        var newInput = $(newIn);
        var removeBtn = '<button id="remove7' + (next - 1) + '" class="  btn-danger remove-me7" >-</button></div><div id="fieldf">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#fieldf" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me7').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#fieldf" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
		});      
	});
	</script>
	
	<script type="text/javascript">
	$(document).ready(function(){
    var next = 1;
    $(".add-more8").click(function(e){
        e.preventDefault();
        var addto = "#fieldg" + next;
        var addRemove = "#fieldg" + (next);
        next = next + 1;
        var newIn = '<input autocomplete="off" class="input form-control" id="fieldg' + next + '" name="fieldg' + next + '" type="text">';
        var newInput = $(newIn);
        var removeBtn = '<button id="remove8' + (next - 1) + '" class="  btn-danger remove-me8" >-</button></div><div id="fieldg">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#fieldg" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me8').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#fieldg" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
		});      
	});
	</script>
	<!--<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
	function myFunction() {
		var x = document.getElementById("form-upload");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
	</script>
		
	<script>
	 $(":file").filestyle({buttonName: "btn-primary"});
	</script>
	
	<script type="text/javascript">
	$(document).ready(function(){
		$('.date-picker').datepicker({
			  format: 'yyyy-mm-dd',
			  autoclose: true,
			  todayHighlight: true,
		});		
	});
	</script>



</body>
<!-- END BODY -->
</html>