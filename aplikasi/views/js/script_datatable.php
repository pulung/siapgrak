<script>
$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
    return {
        'iStart': oSettings._iDisplayStart,
        'End': oSettings.fnDisplayEnd(),
        'iLength': oSettings._iDisplayLength,
        'iTotal': oSettings.fnRecordsTotal(),
        'iFilteredTotal': oSettings.fnRecordsDisplay(),
        'iPage': Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        'iTotalPages': Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
    }
}


 $(document).ready(function() {
  $('#tabel-data').DataTable({
    'lengthMenu': [[ 5, 10, 25, 50, - 1], [ 5, 10, 25, 50, 'All']],
    'order': [[0, 'DESC']]
  });
} );
 </script>