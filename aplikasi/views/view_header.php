<div class="wrapper" style="background-color:#23e62f">
  <!--<img width="100%" src="<?php echo site_url('assets/images/logo/banner-4.jpg'); ?>"/>-->
  <img width="100%" src="<?php echo site_url('assets/images/logo/siapgrak-logo2.png'); ?>" style="
    width: 690px;
    padding-left: 90px;
"/>
</div>
<!-- BEGIN TOP BAR -->
<div class="pre-header">
  <div class="container">
      <div class="row">
          <!-- BEGIN TOP BAR LEFT PART -->
          <div class="col-md-6 col-sm-6 additional-shop-info">
          </div>
          <!-- END TOP BAR LEFT PART -->
          <!-- BEGIN TOP BAR MENU -->
          <div class="col-md-6 col-sm-6 additional-nav">
              <ul class="list-unstyled list-inline pull-right">
                  <!-- BEGIN LANGS -->
                  <li class="langs-block">
                      <a href="javascript:void(0);" class="current" style="font-weight: bold; font-size: 16px; color: #FFFFFF;"><?php cetak($this->session->userdata('name')) ?><i class="fa fa-angle-down"></i></a>
                      <div class="langs-block-others-wrapper"><div class="langs-block-others">
                        <a href="<?= site_url('profil') ?>"><i class="icon-user"></i>Profil Saya</a>
                        <a href="<?= site_url('login/logout') ?>"><i class="icon-key"></i>Log Out</a>
                      </div></div>
                  </li>
                  <!-- END LANGS -->
              </ul>
          </div>
          <!-- END TOP BAR MENU -->
      </div>
  </div>        
</div>
<!-- END TOP BAR -->

<!-- BEGIN HEADER -->
<div class="header">
<div class="container">

  <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

  <!-- BEGIN CART -->
  <!-- <div class="top-cart-block">
    <div class="top-cart-info">
      <a href="javascript:void(0);" class="top-cart-info-count">3</a>
    </div>
    <i class="fa fa-bell"></i>
                  
    <div class="top-cart-content-wrapper">
      <div class="top-cart-content">
        <ul class="scroller" style="height: 250px;">
          <li>
            <img src="<?= site_url('assets/frontend/pages/img/plus.png'); ?>" width="30" height="30">
            <span class="cart-content-count"></span>
            <strong><a href="shop-item.html">Laporan Gratifikasi Baru</a></strong>
            <em>17:08</em>
          </li>
        </ul>
        <div class="text-right">
          <a href="shop-checkout.html" class="btn btn-primary">Lihat Selengkapnya</a>
        </div>
      </div>
    </div>            
  </div> -->
  <!--END CART -->

  <!-- BEGIN NAVIGATION -->
  <div class="header-navigation">
    <ul>
      <li class="<?= $tagmenu == 'beranda' ? 'active' : ''; ?>" style="font-weight: bold; font-size: 23px;"><a href="<?= site_url('beranda') ?>">Beranda</a></li>
      <?php if($this->session->userdata('role') == '2' OR $this->session->userdata('role') == '1'): ?>
	  <li class="<?= $tagmenu == 'pelaporan' ? 'active' : ''; ?>" style="font-weight: bold; font-size: 23px;"><a href="<?= site_url('pelaporan') ?>">Prosedur Pelaporan</a></li>
      <!--<li class="<?= $tagmenu == 'faq' ? 'active' : ''; ?>" style="font-weight: bold; font-size: 23px;"><a href="<?= site_url('faq') ?>">FAQ</a></li>-->
      <li class="<?= $tagmenu == 'kontak' ? 'active' : ''; ?>" style="font-weight: bold; font-size: 23px;"><a href="<?= site_url('kontak') ?>">Kontak kami</a></li>
      <li class="<?= $tagmenu == 'bantuan' ? 'active' : ''; ?>" style="font-weight: bold; font-size: 23px;"><a href="<?= site_url('bantuan') ?>">Bantuan</a></li>
	  <?php endif; ?>
    </ul>
  </div>
  <!-- END NAVIGATION -->
</div>
</div>
<!-- Header END -->