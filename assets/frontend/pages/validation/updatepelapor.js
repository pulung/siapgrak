var UpdatePelapor = function () {
    // validation using icons
    var handleValidation = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            
            var form = $('#update_pelapor');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    name: {
                        required: true
                    },
                    tempatlahir: {
                        required: true
                    },
                    tgllahir: {
                        required: true
                    },
                    no_ktp: {
                        required: true
                    },
                    jabatan: {
                        required: true
                    },
                    golongan: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    nohp: {
                        required: true
                    },
                    inskerja: {
                        required: true
                    },
                    biro: {
                        required: true
                    },
                    alamat_kantor: {
                        required: true
                    },
                    kelurahan_kantor: {
                        required: true
                    },
                    kecamatan_kantor: {
                        required: true
                    },
                    kota_kantor: {
                        required: true
                    },
                    provinsi_kantor: {
                        required: true
                    },
                    kode_pos_kantor: {
                        required: true
                    },
                    no_kantor: {
                        required: true
                    },
                    alamat_rumah: {
                        required: true
                    },
                    kelurahan_rumah: {
                        required: true
                    },
                    kecamatan_rumah: {
                        required: true
                    },
                    kota_rumah: {
                        required: true
                    },
                    provinsi_rumah: {
                        required: true
                    },
                    kode_pos_rumah: {
                        required: true
                    },
                    alamat_pengiriman: {
                        required: true
                    },
                    jenis_penerimaan: {
                        required: true
                    },
                    uraian: {
                        required: true,
                        maxlength: 250 
                    },
                    nilai_nominal: {
                        required: true,
                    },
                    kode_peristiwa: {
                        required: true
                    },
                    tempat_penerimaan: {
                        required: true
                    },
                    tanggal_penerimaan: {
                        required: true
                    },
                    nama_pemberi: {
                        required: true
                    },
                    pekerjaan_pemberi: {
                        required: true,
                    },
                    alamat_pemberi: {
                        required: true,
                    },
                    hubungan_pemberi: {
                        required: true
                    },
                    alasan_pemberian: {
                        required: true
                    },
                    kronologi_penerimaan: {
                        required: true
                    },
                    status_dokumen_dilampirkan: {
                        required: true
                    },
                    persyaratan: {
                        required: true
                    },
                    tempat_laporan: {
                        required: true
                    },
                },
                messages: {
                    name: {
                        required: "Nama Penerima harus diisi."
                    },
                    tempatlahir: {
                        required: "Tempat Lahir Penerima harus diisi."
                    },
                    tgllahir: {
                        required: "Tanggal Lahir Penerima harus diisi."
                    },
                    no_ktp: {
                        required: "No KTP Penerima harus diisi."
                    },
                    jabatan: {
                        required: "Jabatan Penerima harus diisi."
                    },
                    golongan: {
                        required: "Golongan Penerima harus diisi."
                    },
                    email: {
                        required: "Email Penerima harus diisi."
                    },
                    nohp: {
                        required: "Nomor Seluler Penerima harus diisi."
                    },
                    inskerja: {
                        required: "Nama Instansi Penerima harus diisi."
                    },
                    biro: {
                        required: "Unit Penerima harus diisi."
                    },
                    alamat_kantor: {
                        required: "Alamat Kantor harus diisi."
                    },
                    kelurahan_kantor: {
                        required: "kelurahan kantor harus diisi."
                    },
                    kecamatan_kantor: {
                        required: "kecamatan kantor harus diisi."
                    },
                    kota_kantor: {
                        required: "kota kantor harus diisi."
                    },
                    provinsi_kantor: {
                        required: "provinsi kantor harus diisi."
                    },
                    kode_pos_kantor: {
                        required: "kode pos kantor harus diisi."
                    },
                    no_kantor: {
                        required: "Nomor telepon kantor harus diisi."
                    },
                    alamat_rumah: {
                        required: "alamat rumah harus diisi."
                    },
                    kelurahan_rumah: {
                        required: "kelurahan rumah harus diisi."
                    },
                    kecamatan_rumah: {
                        required: "kecamatan rumah harus diisi."
                    },
                    kota_rumah: {
                        required: "kota rumah harus diisi."
                    },
                    provinsi_rumah: {
                        required: "provinsi rumah harus diisi."
                    },
                    kode_pos_rumah: {
                        required: "kode pos rumah harus diisi."
                    },
                    alamat_pengiriman: {
                        required: "alamat pengiriman harus diisi."
                    },
                    jenis_penerimaan: {
                        required: "Jenis Penerimaan harus diisi."
                    },
                    uraian: {
                        required: "Uraian harus diisi.",
                        maxlength: "Uraian maksimal 250 karakter"
                    },
                    nilai_nominal: {
                        required: "Nilai Nominal harus diisi."
                    },
                    kode_peristiwa: {
                        required: "Kode Peristiwa harus diisi."
                    },
                    tempat_penerimaan: {
                        required: "Tempat Penerimaan harus diisi."
                    },
                    tanggal_penerimaan: {
                        required: "Tanggal Penerimaan harus diisi."
                    },      
                    nama_pemberi: {
                        required: "Nama Pemberi harus diisi."
                    }, 
                    pekerjaan_pemberi: {
                        required: "Pekerjaan Pemberi harus diisi."
                    },       
                    alamat_pemberi: {
                        required: "Alamat Pemberi harus diisi."
                    }, 
                    telepon_pemberi: {
                        required: "Telepon Pemberi harus diisi."
                    }, 
                    email_pemberi: {
                        required: "Email Pemberi harus diisi."
                    },
                    hubungan_pemberi: {
                        required: "Hubungan dengan Pemberi harus diisi."
                    },
                    alasan_pemberian: {
                        required: "Alasan Pemberian harus diisi."
                    },
                    kronologi_penerimaan: {
                        required: "Kronologi Penerimaan harus diisi."
                    },
                    status_dokumen_dilampirkan: {
                        required: "Dokumen yang dilampirkan harus diisi."
                    }, 
                    persyaratan: {
                        required: "Persyaratan harus diisi."
                    }, 
                    tempat_laporan: {
                        required: "Tempat Pelaporan harus diisi."
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");  
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    
                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    document.bgColor="lightgreen";
                    icon.removeClass("fa-warning").addClass("fa-check");
                },
                
                submitHandler: function (form) {
                    info.show();
                    success.hide();
                    error.hide();
                    
                    $('#submit').addClass("disabled");
                    $.ajax({
                        type: "post",
                        dataType: "json",
                        url: "update",
                        data: new FormData(this),
                        success: function(data) {
                            $('.alert-' + data['status']).html('<button class="close" data-close="alert"></button>' + data['message']);
                            $('.alert-' + data['status']).show();
                            info.hide();
                            $('#submit').removeClass("disabled");
        //                        window.location.replace('newroom');
                        },
                        error: function() {
                            $('.alert-danger').html('<button class="close" data-close="alert"></button><i class="fa fa-times"></i> Permintaan gagal diproses. Sistem sedang mengalami gangguan. Silahkan ulangi beberapa saat lagi.');
                            $('.alert-danger').show();
                            $('.alert-info').hide();
                            $('#submit').removeClass("disabled");
                        }
                    });
                }

            });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleValidation();
        }

    };
    
}();