var FormPenerima = function () {
    // validation using icons
    var handleValidation = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            
            var form = $('#form_penerima');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    jenis_penerimaan: {
                        required: true
                    },
                    uraian: {
                        required: true,
                        maxlength: 250 
                    },
                    nilai_nominal: {
                        required: true,
                    },
                    kode_peristiwa: {
                        required: true
                    },
                    tempat_penerimaan: {
                        required: true
                    },
                    tanggal_penerimaan: {
                        required: true
                    },
                    nama_pemberi: {
                        required: true
                    },
                    pekerjaan_pemberi: {
                        required: true,
                    },
                    alamat_pemberi: {
                        required: true,
                    },
                    hubungan_pemberi: {
                        required: true
                    },
                    alasan_pemberian: {
                        required: true
                    },
                    kronologi_penerimaan: {
                        required: true
                    },
                    status_dokumen_dilampirkan: {
                        required: true
                    },
                    persyaratan: {
                        required: true
                    },
                    tempat_laporan: {
                        required: true
                    },
                },
                messages: {
                    jenis_penerimaan: {
                        required: "Jenis Penerimaan harus diisi."
                    },
                    uraian: {
                        required: "Uraian harus diisi.",
                        maxlength: "Uraian maksimal 250 karakter"
                    },
                    nilai_nominal: {
                        required: "Nilai Nominal harus diisi."
                    },
                    kode_peristiwa: {
                        required: "Kode Peristiwa harus diisi."
                    },
                    tempat_penerimaan: {
                        required: "Tempat Penerimaan harus diisi."
                    },
                    tanggal_penerimaan: {
                        required: "Tanggal Penerimaan harus diisi."
                    },      
                    nama_pemberi: {
                        required: "Nama Pemberi harus diisi."
                    }, 
                    pekerjaan_pemberi: {
                        required: "Pekerjaan Pemberi harus diisi."
                    },       
                    alamat_pemberi: {
                        required: "Alamat Pemberi harus diisi."
                    }, 
                    telepon_pemberi: {
                        required: "Telepon Pemberi harus diisi."
                    }, 
                    email_pemberi: {
                        required: "Email Pemberi harus diisi."
                    },
                    hubungan_pemberi: {
                        required: "Hubungan dengan Pemberi harus diisi."
                    },
                    alasan_pemberian: {
                        required: "Alasan Pemberian harus diisi."
                    },
                    kronologi_penerimaan: {
                        required: "Kronologi Penerimaan harus diisi."
                    },
                    status_dokumen_dilampirkan: {
                        required: "Dokumen yang dilampirkan harus diisi."
                    },
                    persyaratan: {
                        required: "Persyaratan harus diisi."
                    }, 
                    tempat_laporan: {
                        required: "Tempat Pelaporan harus diisi."
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");  
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    
                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    document.bgColor="lightgreen";
                    icon.removeClass("fa-warning").addClass("fa-check");
                },
                
                submitHandler: function (form) {
                    info.show();
                    success.hide();
                    error.hide();
                    
                    $('#submit').addClass("disabled");
                    $.ajax({
                        type: "post",
                        dataType: "json",
                        url: "insert",
                        data: new FormData(this),
                        success: function(data) {
                            $('.alert-' + data['status']).html('<button class="close" data-close="alert"></button>' + data['message']);
                            $('.alert-' + data['status']).show();
                            info.hide();
                            $('#submit').removeClass("disabled");
        //                        window.location.replace('newroom');
                        },
                        error: function() {
                            $('.alert-danger').html('<button class="close" data-close="alert"></button><i class="fa fa-times"></i> Permintaan gagal diproses. Sistem sedang mengalami gangguan. Silahkan ulangi beberapa saat lagi.');
                            $('.alert-danger').show();
                            $('.alert-info').hide();
                            $('#submit').removeClass("disabled");
                        }
                    });
                }

            });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleValidation();
        }

    };
    
}();