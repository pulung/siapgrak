var InsertProfil = function () {
    // validation using icons
    var handleValidation = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            
            var form = $('#insert_profil');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    name: {
                        required: true,
                    },
                    tempatlahir: {
                        required: true
                    },
                    tgllahir: {
                        required: true
                    },
                    no_ktp: {
                        required: true
                    },
                    jabatan: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    nohp: {
                        required: true
                    },
                    inskerja: {
                        required: true
                    },
                    biro: {
                        required: true
                    },
                    // bagian: {
                    //     required: true
                    // },
                    jabatan: {
                        required: true
                    },
                    golongan: {
                        required: true
                    },
                    pangkat: {
                        required: true
                    },
                    alamat_kantor: {
                        required: true
                    },
                    kelurahan_kantor: {
                        required: true
                    },
                    kecamatan_kantor: {
                        required: true
                    },
                    kota_kantor: {
                        required: true
                    },
                    provinsi_kantor: {
                        required: true
                    },
                    kode_pos_kantor: {
                        required: true
                    },
                    no_kantor: {
                        required: true
                    },
                    alamat_rumah: {
                        required: true
                    },
                    kelurahan_rumah: {
                        required: true
                    },
                    kecamatan_rumah: {
                        required: true
                    },
                    kota_rumah: {
                        required: true
                    },
                    provinsi_rumah: {
                        required: true
                    },
                    kode_pos_rumah: {
                        required: true
                    },
                },
                messages: {
                    name: {
                        required: "Nama Penerima harus diisi."
                    },
                    tempatlahir: {
                        required: "Tempat Lahir Penerima harus diisi."
                    },
                    tgllahir: {
                        required: "Tanggal Lahir Penerima harus diisi."
                    },
                    no_ktp: {
                        required: "No KTP Penerima harus diisi."
                    },
                    jabatan: {
                        required: "Jabatan Penerima harus diisi.",
                        pattern: "Hanya boleh character"
                    },
                    email: {
                        required: "Email Penerima harus diisi."
                    },
                    nohp: {
                        required: "Nomor Seluler Penerima harus diisi."
                    },
                    inskerja: {
                        required: "Nama Instansi Penerima harus diisi."
                    },
                    biro: {
                        required: "Unit Penerima harus diisi."
                    },
                    // bagian: {
                    //     required: "Bagian Penerima harus diisi."
                    // },
                    jabatan: {
                        required: "Jabatan Penerima harus diisi."
                    },
                    golongan: {
                        required: "Golongan Penerima harus diisi."
                    },
                    pangkat: {
                        required: "Pangkat Penerima harus diisi."
                    },
                    alamat_kantor: {
                        required: "Alamat Kantor harus diisi."
                    },
                    kelurahan_kantor: {
                        required: "kelurahan kantor harus diisi."
                    },
                    kecamatan_kantor: {
                        required: "kecamatan kantor harus diisi."
                    },
                    kota_kantor: {
                        required: "kota kantor harus diisi."
                    },
                    provinsi_kantor: {
                        required: "provinsi kantor harus diisi."
                    },
                    kode_pos_kantor: {
                        required: "kode pos kantor harus diisi."
                    },
                    no_kantor: {
                        required: "Nomor telepon kantor harus diisi."
                    },
                    alamat_rumah: {
                        required: "alamat rumah harus diisi."
                    },
                    kelurahan_rumah: {
                        required: "kelurahan rumah harus diisi."
                    },
                    kecamatan_rumah: {
                        required: "kecamatan rumah harus diisi."
                    },
                    kota_rumah: {
                        required: "kota rumah harus diisi."
                    },
                    provinsi_rumah: {
                        required: "provinsi rumah harus diisi."
                    },
                    kode_pos_rumah: {
                        required: "kode pos rumah harus diisi."
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");  
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    
                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    document.bgColor="lightgreen";
                    icon.removeClass("fa-warning").addClass("fa-check");
                },
                
                submitHandler: function (form) {
                    info.show();
                    success.hide();
                    error.hide();
                    
                    $('#submit').addClass("disabled");
                    $.ajax({
                        type: "post",
                        dataType: "json",
                        url: "insert",
                        data: new FormData(this),
                        success: function(data) {
                            $('.alert-' + data['status']).html('<button class="close" data-close="alert"></button>' + data['message']);
                            $('.alert-' + data['status']).show();
                            info.hide();
                            $('#submit').removeClass("disabled");
        //                        window.location.replace('newroom');
                        },
                        error: function() {
                            $('.alert-danger').html('<button class="close" data-close="alert"></button><i class="fa fa-times"></i> Permintaan gagal diproses. Sistem sedang mengalami gangguan. Silahkan ulangi beberapa saat lagi.');
                            $('.alert-danger').show();
                            $('.alert-info').hide();
                            $('#submit').removeClass("disabled");
                        }
                    });
                }

            });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleValidation();
        }

    };
    
}();